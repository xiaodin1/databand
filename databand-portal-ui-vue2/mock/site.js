const Mock = require('mockjs')

const List = []
const count = 2

for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
    'value|1': ['1', '2'],
    // 'type|1': ['ad', 'se'],
    comment: '@title(5, 10)',
    'title|1': ['广告分析站点', '产品销售分析站点']
  }))
}

module.exports = [
  {
    url: '/sites/list',
    type: 'get',
    response: config => {
      return {
        code: 20000,
        data: {
          total: count,
          // items: List
          items: [
            { value: '1', title: '广告分析站点' },
            { value: '2', title: '产品销售分析站点' }
          ]
        }
      }
    }
  },
  {
    url: '/sites/menu',
    type: 'get',
    response: config => {
      return {
        msg: '操作成功',
        code: 200,
        data: [
          {
            name: 'Tool',
            path: '/tool',
            hidden: false,
            redirect: 'noRedirect',
            component: 'Layout',
            alwaysShow: true,
            meta: {
              title: '系统工具',
              icon: 'tool',
              noCache: false
            },
            children: [
              {
                name: 'Build',
                path: 'build',
                hidden: false,
                component: 'tool/build/index',
                meta: {
                  title: '表单构建',
                  icon: 'build',
                  noCache: false
                }
              },
              {
                name: 'Gen',
                path: 'gen',
                hidden: false,
                component: 'tool/gen/index',
                meta: {
                  title: '代码生成',
                  icon: 'code',
                  noCache: false
                }
              },
              {
                name: 'Swagger',
                path: 'swagger',
                hidden: false,
                component: 'tool/swagger/index',
                meta: {
                  title: '系统接口',
                  icon: 'swagger',
                  noCache: false
                }
              }
            ]
          }
        ]
      }
    }
  },
  {
    url: '/sites/tree',
    type: 'get',
    response: config => {
      return {
        msg: '操作成功',
        code: 20000,
        data: [
          {
            // 一级
            entity: {
              id: 0,
              name: 'aa',
              icon: 'el-icon-message',
              alias: '一级菜单'
            }
          },
          {
            // 一级
            entity: {
              id: 1,
              name: 'systemManage',
              icon: 'el-icon-message',
              alias: '两级菜单'
            },
            // 二级
            childs: [
              {
                entity: {
                  id: 3,
                  name: 'authManage',
                  icon: 'el-icon-loading',
                  alias: '权限管理',
                  value: { path: '/example/menuexample' }
                }
              },
              {
                entity: {
                  id: 4,
                  name: 'roleManage',
                  icon: 'el-icon-bell',
                  alias: '角色管理',
                  value: '/system/role'
                }
              },
              {
                entity: {
                  id: 2,
                  name: 'menuManage',
                  icon: 'el-icon-edit',
                  alias: '菜单管理',
                  value: '/system/menu'
                }
              },
              {
                entity: {
                  id: 5,
                  name: 'groupManage',
                  icon: 'el-icon-mobile-phone\r\n',
                  alias: '分组管理',
                  value: '/system/group'
                }
              }
            ]
          },
          {
            // 一级
            entity: {
              id: 6,
              name: 'userManage',
              icon: 'el-icon-news',
              alias: '三级菜单'
            },
            // 二级
            childs: [
              {
                entity: {
                  id: 7,
                  name: 'accountManage',
                  icon: 'el-icon-phone-outline\r\n',
                  alias: '帐号管理',
                  value: ''
                },
                // 三级
                childs: [
                  {
                    entity: {
                      id: 14,
                      name: 'emailManage',
                      icon: 'el-icon-sold-out\r\n',
                      alias: '邮箱管理',
                      value: '/content/email'
                    }
                  },
                  {
                    entity: {
                      id: 13,
                      name: 'passManage',
                      icon: 'el-icon-service\r\n',
                      alias: '密码管理',
                      value: '/content/pass'
                    }
                  }
                ]
              },
              {
                entity: {
                  id: 8,
                  name: 'integralManage',
                  icon: 'el-icon-picture',
                  alias: '积分管理',
                  value: '/user/integral'
                }
              }
            ]
          },
          {
            // 一级
            entity: {
              id: 40,
              name: 'contentManage',
              icon: 'el-icon-rank',
              alias: '四级菜单'
            },
            // er级
            childs: [
              {
                entity: {
                  id: 41,
                  name: 'classifyManage2',
                  icon: 'el-icon-printer',
                  alias: '分类管理'
                },
                // 三级
                childs: [
                  {
                    entity: {
                      id: 42,
                      name: 'classifyManage3',
                      icon: 'el-icon-printer',
                      alias: '分类管理'
                    },
                    // 四级
                    childs: [
                      {
                        entity: {
                          id: 43,
                          name: 'classifyManage4',
                          icon: 'el-icon-printer',
                          alias: '分类管理',
                          value: '/content/classify'
                        }
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    }
  }
]

