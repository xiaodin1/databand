package org.databandtech.hadoop;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.BlockLocation;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.io.IOUtils;
import org.junit.Test;

/**
 * A simple example for hdfsclient.
 */
public class HDFSClient {

    private FileSystem fs;

    @Test
    public void before() throws IOException, InterruptedException {

        //获取一个HDFS的抽象封装对象
        Configuration conf = new Configuration();
        fs = FileSystem.get(URI.create("hdfs://hadoop100:9000"), conf, "atguigu");
        System.out.println("Before!!!!!");
    }

    @Test
    public void put() throws IOException, InterruptedException {
        //用这个对象操作文件系统 //上传文件
        fs.copyFromLocalFile(new Path("C:\\Users\\DELL\\Desktop\\hadoop\\1.txt"), new Path("/"));

    }

    @Test
    public void get() throws IOException, InterruptedException {
        //用这个对象操作文件系统
        //下载文件
        fs.copyToLocalFile(new Path("/test"), new Path("C:\\Users\\DELL\\Desktop\\hadoop"));

    }

    @Test
    public void rename() throws IOException, InterruptedException {
        //重命名
        fs.rename(new Path("/test"), new Path("/test2"));

    }

    @Test
    public void delete() throws IOException {
        //删除
        boolean delete = fs.delete(new Path("/1.txt"), true);
        if (delete) {
            System.out.println("删除成功");
        } else {
            System.out.println("删除失败");
        }
    }

    @Test
    public void du() throws IOException {        //追加文件信息
        final Integer bufSize = 1430;
        FSDataOutputStream append = fs.append(new Path("/1.txt"), bufSize);
        FileInputStream open = new FileInputStream("C:\\Users\\DELL\\Desktop\\hadoop\\1.txt");
        IOUtils.copyBytes(open, append, bufSize, true);
    }

    @Test
    public void ls() throws IOException {
        //查看文件信息
        FileStatus[] filestatuses = fs.listStatus(new Path("/"));

        for (FileStatus filestatus:filestatuses) {
            if (filestatus.isFile()) {
                System.out.println("以下信息是一个文件的信息");
                System.out.println(filestatus.getPath());
                System.out.println(filestatus.getLen());
            } else {
                System.out.println("这是一个文件夹");
                System.out.println(filestatus.getPath());
            }
        }
    }

    @Test
    public void listFiles() throws IOException {
        //查看文件名称、权限、长度、块信息
        RemoteIterator<LocatedFileStatus> files = fs.listFiles(new Path("/"), true);
        while (files.hasNext()) {
            LocatedFileStatus file = files.next();

            System.out.println("===============================================");
            System.out.println(file.getPath());

            System.out.println("块信息");
            BlockLocation[] blockLocations = file.getBlockLocations();
            for (BlockLocation blockLocation : blockLocations) {
                String[] hosts = blockLocation.getHosts();
                System.out.println("块在");
                for (String host : hosts) {
                    System.out.println(host + " ");
                }
            }
        }
    }

    @Test
    public void after() throws IOException {
        System.out.println("After!!!!!");
        fs.close();
    }
}