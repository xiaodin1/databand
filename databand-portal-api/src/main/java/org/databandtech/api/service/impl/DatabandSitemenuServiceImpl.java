package org.databandtech.api.service.impl;

import java.util.List;

import org.databandtech.api.entity.DatabandSitemenu;
import org.databandtech.api.mapper.DatabandSitemenuMapper;
import org.databandtech.api.service.IDatabandSitemenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 站点菜单Service业务层处理
 * 
 * @author databand
 * @date 2020-12-31
 */
@Service
public class DatabandSitemenuServiceImpl implements IDatabandSitemenuService 
{
    @Autowired
    private DatabandSitemenuMapper databandSitemenuMapper;

    @Override
    public DatabandSitemenu selectDatabandSitemenuById(Long id)
    {
        return databandSitemenuMapper.selectDatabandSitemenuById(id);
    }

    @Override
    public List<DatabandSitemenu> selectDatabandSitemenuList(DatabandSitemenu databandSitemenu)
    {
        return databandSitemenuMapper.selectDatabandSitemenuList(databandSitemenu);
    }

    @Override
    public int insertDatabandSitemenu(DatabandSitemenu databandSitemenu)
    {
        return databandSitemenuMapper.insertDatabandSitemenu(databandSitemenu);
    }


    @Override
    public int updateDatabandSitemenu(DatabandSitemenu databandSitemenu)
    {
        return databandSitemenuMapper.updateDatabandSitemenu(databandSitemenu);
    }

	
	@Override
	public List<DatabandSitemenu> selectDatabandSitemenuListBySiteid(Long siteid) {
		// TODO Auto-generated method stub
		return databandSitemenuMapper.selectDatabandSitemenuListBySiteid(siteid);
	}


}
