/*
Navicat MySQL Data Transfer

Source Server         : local57
Source Server Version : 50722
Source Host           : localhost:3307
Source Database       : databand

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2021-01-07 10:53:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `databand_city`
-- ----------------------------
DROP TABLE IF EXISTS `databand_city`;
CREATE TABLE `databand_city` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` char(35) NOT NULL DEFAULT '',
  `CountryCode` char(3) NOT NULL DEFAULT '',
  `District` char(20) NOT NULL DEFAULT '',
  `Info` json DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4080 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of databand_city
-- ----------------------------
INSERT INTO `databand_city` VALUES ('1', 'Kabul', 'AFG', 'Kabol');
INSERT INTO `databand_city` VALUES ('2', 'Qandahar', 'AFG', 'Qandahar');
INSERT INTO `databand_city` VALUES ('3', 'Herat', 'AFG', 'Herat');
INSERT INTO `databand_city` VALUES ('4', 'Mazar-e-Sharif', 'AFG', 'Balkh');
INSERT INTO `databand_city` VALUES ('5', 'Amsterdam', 'NLD', 'Noord-Holland');
INSERT INTO `databand_city` VALUES ('6', 'Rotterdam', 'NLD', 'Zuid-Holland');
INSERT INTO `databand_city` VALUES ('7', 'Haag', 'NLD', 'Zuid-Holland');
INSERT INTO `databand_city` VALUES ('8', 'Utrecht', 'NLD', 'Utrecht');
INSERT INTO `databand_city` VALUES ('9', 'Eindhoven', 'NLD', 'Noord-Brabant');
INSERT INTO `databand_city` VALUES ('10', 'Tilburg', 'NLD', 'Noord-Brabant');
INSERT INTO `databand_city` VALUES ('11', 'Groningen', 'NLD', 'Groningen');
INSERT INTO `databand_city` VALUES ('12', 'Breda', 'NLD', 'Noord-Brabant');
INSERT INTO `databand_city` VALUES ('13', 'Apeldoorn', 'NLD', 'Gelderland');
INSERT INTO `databand_city` VALUES ('14', 'Nijmegen', 'NLD', 'Gelderland');
INSERT INTO `databand_city` VALUES ('15', 'Enschede', 'NLD', 'Overijssel');
INSERT INTO `databand_city` VALUES ('16', 'Haarlem', 'NLD', 'Noord-Holland');
INSERT INTO `databand_city` VALUES ('17', 'Almere', 'NLD', 'Flevoland');
INSERT INTO `databand_city` VALUES ('18', 'Arnhem', 'NLD', 'Gelderland');
INSERT INTO `databand_city` VALUES ('19', 'Zaanstad', 'NLD', 'Noord-Holland');
INSERT INTO `databand_city` VALUES ('20', '´s-Hertogenbosch', 'NLD', 'Noord-Brabant');
INSERT INTO `databand_city` VALUES ('21', 'Amersfoort', 'NLD', 'Utrecht');
INSERT INTO `databand_city` VALUES ('22', 'Maastricht', 'NLD', 'Limburg');
INSERT INTO `databand_city` VALUES ('23', 'Dordrecht', 'NLD', 'Zuid-Holland');
INSERT INTO `databand_city` VALUES ('24', 'Leiden', 'NLD', 'Zuid-Holland');
INSERT INTO `databand_city` VALUES ('25', 'Haarlemmermeer', 'NLD', 'Noord-Holland');
INSERT INTO `databand_city` VALUES ('26', 'Zoetermeer', 'NLD', 'Zuid-Holland');
INSERT INTO `databand_city` VALUES ('27', 'Emmen', 'NLD', 'Drenthe');
INSERT INTO `databand_city` VALUES ('28', 'Zwolle', 'NLD', 'Overijssel');
INSERT INTO `databand_city` VALUES ('29', 'Ede', 'NLD', 'Gelderland');
INSERT INTO `databand_city` VALUES ('30', 'Delft', 'NLD', 'Zuid-Holland');
INSERT INTO `databand_city` VALUES ('31', 'Heerlen', 'NLD', 'Limburg');
INSERT INTO `databand_city` VALUES ('32', 'Alkmaar', 'NLD', 'Noord-Holland');
INSERT INTO `databand_city` VALUES ('33', 'Willemstad', 'ANT', 'Curaçao');
INSERT INTO `databand_city` VALUES ('34', 'Tirana', 'ALB', 'Tirana');
INSERT INTO `databand_city` VALUES ('35', 'Alger', 'DZA', 'Alger');
INSERT INTO `databand_city` VALUES ('36', 'Oran', 'DZA', 'Oran');
INSERT INTO `databand_city` VALUES ('37', 'Constantine', 'DZA', 'Constantine');
INSERT INTO `databand_city` VALUES ('38', 'Annaba', 'DZA', 'Annaba');
INSERT INTO `databand_city` VALUES ('39', 'Batna', 'DZA', 'Batna');
INSERT INTO `databand_city` VALUES ('40', 'Sétif', 'DZA', 'Sétif');
INSERT INTO `databand_city` VALUES ('41', 'Sidi Bel Abbès', 'DZA', 'Sidi Bel Abbès');
INSERT INTO `databand_city` VALUES ('42', 'Skikda', 'DZA', 'Skikda');
INSERT INTO `databand_city` VALUES ('43', 'Biskra', 'DZA', 'Biskra');
INSERT INTO `databand_city` VALUES ('44', 'Blida (el-Boulaida)', 'DZA', 'Blida');
INSERT INTO `databand_city` VALUES ('45', 'Béjaïa', 'DZA', 'Béjaïa');
INSERT INTO `databand_city` VALUES ('46', 'Mostaganem', 'DZA', 'Mostaganem');
INSERT INTO `databand_city` VALUES ('47', 'Tébessa', 'DZA', 'Tébessa');
INSERT INTO `databand_city` VALUES ('48', 'Tlemcen (Tilimsen)', 'DZA', 'Tlemcen');
INSERT INTO `databand_city` VALUES ('49', 'Béchar', 'DZA', 'Béchar');
INSERT INTO `databand_city` VALUES ('50', 'Tiaret', 'DZA', 'Tiaret');
INSERT INTO `databand_city` VALUES ('51', 'Ech-Chleff (el-Asnam)', 'DZA', 'Chlef');
INSERT INTO `databand_city` VALUES ('52', 'Ghardaïa', 'DZA', 'Ghardaïa');
INSERT INTO `databand_city` VALUES ('53', 'Tafuna', 'ASM', 'Tutuila');
INSERT INTO `databand_city` VALUES ('54', 'Fagatogo', 'ASM', 'Tutuila');
INSERT INTO `databand_city` VALUES ('55', 'Andorra la Vella', 'AND', 'Andorra la Vella');
INSERT INTO `databand_city` VALUES ('56', 'Luanda', 'AGO', 'Luanda');
INSERT INTO `databand_city` VALUES ('57', 'Huambo', 'AGO', 'Huambo');
INSERT INTO `databand_city` VALUES ('58', 'Lobito', 'AGO', 'Benguela');
INSERT INTO `databand_city` VALUES ('59', 'Benguela', 'AGO', 'Benguela');
INSERT INTO `databand_city` VALUES ('60', 'Namibe', 'AGO', 'Namibe');
INSERT INTO `databand_city` VALUES ('61', 'South Hill', 'AIA', '–');
INSERT INTO `databand_city` VALUES ('62', 'The Valley', 'AIA', '–');
INSERT INTO `databand_city` VALUES ('63', 'Saint John´s', 'ATG', 'St John');
INSERT INTO `databand_city` VALUES ('64', 'Dubai', 'ARE', 'Dubai');
INSERT INTO `databand_city` VALUES ('65', 'Abu Dhabi', 'ARE', 'Abu Dhabi');
INSERT INTO `databand_city` VALUES ('66', 'Sharja', 'ARE', 'Sharja');
INSERT INTO `databand_city` VALUES ('67', 'al-Ayn', 'ARE', 'Abu Dhabi');
INSERT INTO `databand_city` VALUES ('68', 'Ajman', 'ARE', 'Ajman');
INSERT INTO `databand_city` VALUES ('69', 'Buenos Aires', 'ARG', 'Distrito Federal');
INSERT INTO `databand_city` VALUES ('70', 'La Matanza', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('71', 'Córdoba', 'ARG', 'Córdoba');
INSERT INTO `databand_city` VALUES ('72', 'Rosario', 'ARG', 'Santa Fé');
INSERT INTO `databand_city` VALUES ('73', 'Lomas de Zamora', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('74', 'Quilmes', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('75', 'Almirante Brown', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('76', 'La Plata', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('77', 'Mar del Plata', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('78', 'San Miguel de Tucumán', 'ARG', 'Tucumán');
INSERT INTO `databand_city` VALUES ('79', 'Lanús', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('80', 'Merlo', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('81', 'General San Martín', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('82', 'Salta', 'ARG', 'Salta');
INSERT INTO `databand_city` VALUES ('83', 'Moreno', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('84', 'Santa Fé', 'ARG', 'Santa Fé');
INSERT INTO `databand_city` VALUES ('85', 'Avellaneda', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('86', 'Tres de Febrero', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('87', 'Morón', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('88', 'Florencio Varela', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('89', 'San Isidro', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('90', 'Tigre', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('91', 'Malvinas Argentinas', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('92', 'Vicente López', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('93', 'Berazategui', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('94', 'Corrientes', 'ARG', 'Corrientes');
INSERT INTO `databand_city` VALUES ('95', 'San Miguel', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('96', 'Bahía Blanca', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('97', 'Esteban Echeverría', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('98', 'Resistencia', 'ARG', 'Chaco');
INSERT INTO `databand_city` VALUES ('99', 'José C. Paz', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('100', 'Paraná', 'ARG', 'Entre Rios');
INSERT INTO `databand_city` VALUES ('101', 'Godoy Cruz', 'ARG', 'Mendoza');
INSERT INTO `databand_city` VALUES ('102', 'Posadas', 'ARG', 'Misiones');
INSERT INTO `databand_city` VALUES ('103', 'Guaymallén', 'ARG', 'Mendoza');
INSERT INTO `databand_city` VALUES ('104', 'Santiago del Estero', 'ARG', 'Santiago del Estero');
INSERT INTO `databand_city` VALUES ('105', 'San Salvador de Jujuy', 'ARG', 'Jujuy');
INSERT INTO `databand_city` VALUES ('106', 'Hurlingham', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('107', 'Neuquén', 'ARG', 'Neuquén');
INSERT INTO `databand_city` VALUES ('108', 'Ituzaingó', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('109', 'San Fernando', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('110', 'Formosa', 'ARG', 'Formosa');
INSERT INTO `databand_city` VALUES ('111', 'Las Heras', 'ARG', 'Mendoza');
INSERT INTO `databand_city` VALUES ('112', 'La Rioja', 'ARG', 'La Rioja');
INSERT INTO `databand_city` VALUES ('113', 'San Fernando del Valle de Cata', 'ARG', 'Catamarca');
INSERT INTO `databand_city` VALUES ('114', 'Río Cuarto', 'ARG', 'Córdoba');
INSERT INTO `databand_city` VALUES ('115', 'Comodoro Rivadavia', 'ARG', 'Chubut');
INSERT INTO `databand_city` VALUES ('116', 'Mendoza', 'ARG', 'Mendoza');
INSERT INTO `databand_city` VALUES ('117', 'San Nicolás de los Arroyos', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('118', 'San Juan', 'ARG', 'San Juan');
INSERT INTO `databand_city` VALUES ('119', 'Escobar', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('120', 'Concordia', 'ARG', 'Entre Rios');
INSERT INTO `databand_city` VALUES ('121', 'Pilar', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('122', 'San Luis', 'ARG', 'San Luis');
INSERT INTO `databand_city` VALUES ('123', 'Ezeiza', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('124', 'San Rafael', 'ARG', 'Mendoza');
INSERT INTO `databand_city` VALUES ('125', 'Tandil', 'ARG', 'Buenos Aires');
INSERT INTO `databand_city` VALUES ('126', 'Yerevan', 'ARM', 'Yerevan');
INSERT INTO `databand_city` VALUES ('127', 'Gjumri', 'ARM', 'Širak');
INSERT INTO `databand_city` VALUES ('128', 'Vanadzor', 'ARM', 'Lori');
INSERT INTO `databand_city` VALUES ('129', 'Oranjestad', 'ABW', '–');
INSERT INTO `databand_city` VALUES ('130', 'Sydney', 'AUS', 'New South Wales');
INSERT INTO `databand_city` VALUES ('131', 'Melbourne', 'AUS', 'Victoria');
INSERT INTO `databand_city` VALUES ('132', 'Brisbane', 'AUS', 'Queensland');
INSERT INTO `databand_city` VALUES ('133', 'Perth', 'AUS', 'West Australia');
INSERT INTO `databand_city` VALUES ('134', 'Adelaide', 'AUS', 'South Australia');
INSERT INTO `databand_city` VALUES ('135', 'Canberra', 'AUS', 'Capital Region');
INSERT INTO `databand_city` VALUES ('136', 'Gold Coast', 'AUS', 'Queensland');
INSERT INTO `databand_city` VALUES ('137', 'Newcastle', 'AUS', 'New South Wales');
INSERT INTO `databand_city` VALUES ('138', 'Central Coast', 'AUS', 'New South Wales');
INSERT INTO `databand_city` VALUES ('139', 'Wollongong', 'AUS', 'New South Wales');
INSERT INTO `databand_city` VALUES ('140', 'Hobart', 'AUS', 'Tasmania');
INSERT INTO `databand_city` VALUES ('141', 'Geelong', 'AUS', 'Victoria');
INSERT INTO `databand_city` VALUES ('142', 'Townsville', 'AUS', 'Queensland');
INSERT INTO `databand_city` VALUES ('143', 'Cairns', 'AUS', 'Queensland');
INSERT INTO `databand_city` VALUES ('144', 'Baku', 'AZE', 'Baki');
INSERT INTO `databand_city` VALUES ('145', 'Gäncä', 'AZE', 'Gäncä');
INSERT INTO `databand_city` VALUES ('146', 'Sumqayit', 'AZE', 'Sumqayit');
INSERT INTO `databand_city` VALUES ('147', 'Mingäçevir', 'AZE', 'Mingäçevir');
INSERT INTO `databand_city` VALUES ('148', 'Nassau', 'BHS', 'New Providence');
INSERT INTO `databand_city` VALUES ('149', 'al-Manama', 'BHR', 'al-Manama');
INSERT INTO `databand_city` VALUES ('150', 'Dhaka', 'BGD', 'Dhaka');
INSERT INTO `databand_city` VALUES ('151', 'Chittagong', 'BGD', 'Chittagong');
INSERT INTO `databand_city` VALUES ('152', 'Khulna', 'BGD', 'Khulna');
INSERT INTO `databand_city` VALUES ('153', 'Rajshahi', 'BGD', 'Rajshahi');
INSERT INTO `databand_city` VALUES ('154', 'Narayanganj', 'BGD', 'Dhaka');
INSERT INTO `databand_city` VALUES ('155', 'Rangpur', 'BGD', 'Rajshahi');
INSERT INTO `databand_city` VALUES ('156', 'Mymensingh', 'BGD', 'Dhaka');
INSERT INTO `databand_city` VALUES ('157', 'Barisal', 'BGD', 'Barisal');
INSERT INTO `databand_city` VALUES ('158', 'Tungi', 'BGD', 'Dhaka');
INSERT INTO `databand_city` VALUES ('159', 'Jessore', 'BGD', 'Khulna');
INSERT INTO `databand_city` VALUES ('160', 'Comilla', 'BGD', 'Chittagong');
INSERT INTO `databand_city` VALUES ('161', 'Nawabganj', 'BGD', 'Rajshahi');
INSERT INTO `databand_city` VALUES ('162', 'Dinajpur', 'BGD', 'Rajshahi');
INSERT INTO `databand_city` VALUES ('163', 'Bogra', 'BGD', 'Rajshahi');
INSERT INTO `databand_city` VALUES ('164', 'Sylhet', 'BGD', 'Sylhet');
INSERT INTO `databand_city` VALUES ('165', 'Brahmanbaria', 'BGD', 'Chittagong');
INSERT INTO `databand_city` VALUES ('166', 'Tangail', 'BGD', 'Dhaka');
INSERT INTO `databand_city` VALUES ('167', 'Jamalpur', 'BGD', 'Dhaka');
INSERT INTO `databand_city` VALUES ('168', 'Pabna', 'BGD', 'Rajshahi');
INSERT INTO `databand_city` VALUES ('169', 'Naogaon', 'BGD', 'Rajshahi');
INSERT INTO `databand_city` VALUES ('170', 'Sirajganj', 'BGD', 'Rajshahi');
INSERT INTO `databand_city` VALUES ('171', 'Narsinghdi', 'BGD', 'Dhaka');
INSERT INTO `databand_city` VALUES ('172', 'Saidpur', 'BGD', 'Rajshahi');
INSERT INTO `databand_city` VALUES ('173', 'Gazipur', 'BGD', 'Dhaka');
INSERT INTO `databand_city` VALUES ('174', 'Bridgetown', 'BRB', 'St Michael');
INSERT INTO `databand_city` VALUES ('175', 'Antwerpen', 'BEL', 'Antwerpen');
INSERT INTO `databand_city` VALUES ('176', 'Gent', 'BEL', 'East Flanderi');
INSERT INTO `databand_city` VALUES ('177', 'Charleroi', 'BEL', 'Hainaut');
INSERT INTO `databand_city` VALUES ('178', 'Liège', 'BEL', 'Liège');
INSERT INTO `databand_city` VALUES ('179', 'Bruxelles [Brussel]', 'BEL', 'Bryssel');
INSERT INTO `databand_city` VALUES ('180', 'Brugge', 'BEL', 'West Flanderi');
INSERT INTO `databand_city` VALUES ('181', 'Schaerbeek', 'BEL', 'Bryssel');
INSERT INTO `databand_city` VALUES ('182', 'Namur', 'BEL', 'Namur');
INSERT INTO `databand_city` VALUES ('183', 'Mons', 'BEL', 'Hainaut');
INSERT INTO `databand_city` VALUES ('184', 'Belize City', 'BLZ', 'Belize City');
INSERT INTO `databand_city` VALUES ('185', 'Belmopan', 'BLZ', 'Cayo');
INSERT INTO `databand_city` VALUES ('186', 'Cotonou', 'BEN', 'Atlantique');
INSERT INTO `databand_city` VALUES ('187', 'Porto-Novo', 'BEN', 'Ouémé');
INSERT INTO `databand_city` VALUES ('188', 'Djougou', 'BEN', 'Atacora');
INSERT INTO `databand_city` VALUES ('189', 'Parakou', 'BEN', 'Borgou');
INSERT INTO `databand_city` VALUES ('190', 'Saint George', 'BMU', 'Saint George´s');
INSERT INTO `databand_city` VALUES ('191', 'Hamilton', 'BMU', 'Hamilton');
INSERT INTO `databand_city` VALUES ('192', 'Thimphu', 'BTN', 'Thimphu');
INSERT INTO `databand_city` VALUES ('193', 'Santa Cruz de la Sierra', 'BOL', 'Santa Cruz');
INSERT INTO `databand_city` VALUES ('194', 'La Paz', 'BOL', 'La Paz');
INSERT INTO `databand_city` VALUES ('195', 'El Alto', 'BOL', 'La Paz');
INSERT INTO `databand_city` VALUES ('196', 'Cochabamba', 'BOL', 'Cochabamba');
INSERT INTO `databand_city` VALUES ('197', 'Oruro', 'BOL', 'Oruro');
INSERT INTO `databand_city` VALUES ('198', 'Sucre', 'BOL', 'Chuquisaca');
INSERT INTO `databand_city` VALUES ('199', 'Potosí', 'BOL', 'Potosí');
INSERT INTO `databand_city` VALUES ('200', 'Tarija', 'BOL', 'Tarija');
INSERT INTO `databand_city` VALUES ('201', 'Sarajevo', 'BIH', 'Federaatio');
INSERT INTO `databand_city` VALUES ('202', 'Banja Luka', 'BIH', 'Republika Srpska');
INSERT INTO `databand_city` VALUES ('203', 'Zenica', 'BIH', 'Federaatio');
INSERT INTO `databand_city` VALUES ('204', 'Gaborone', 'BWA', 'Gaborone');
INSERT INTO `databand_city` VALUES ('205', 'Francistown', 'BWA', 'Francistown');
INSERT INTO `databand_city` VALUES ('206', 'São Paulo', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('207', 'Rio de Janeiro', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('208', 'Salvador', 'BRA', 'Bahia');
INSERT INTO `databand_city` VALUES ('209', 'Belo Horizonte', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('210', 'Fortaleza', 'BRA', 'Ceará');
INSERT INTO `databand_city` VALUES ('211', 'Brasília', 'BRA', 'Distrito Federal');
INSERT INTO `databand_city` VALUES ('212', 'Curitiba', 'BRA', 'Paraná');
INSERT INTO `databand_city` VALUES ('213', 'Recife', 'BRA', 'Pernambuco');
INSERT INTO `databand_city` VALUES ('214', 'Porto Alegre', 'BRA', 'Rio Grande do Sul');
INSERT INTO `databand_city` VALUES ('215', 'Manaus', 'BRA', 'Amazonas');
INSERT INTO `databand_city` VALUES ('216', 'Belém', 'BRA', 'Pará');
INSERT INTO `databand_city` VALUES ('217', 'Guarulhos', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('218', 'Goiânia', 'BRA', 'Goiás');
INSERT INTO `databand_city` VALUES ('219', 'Campinas', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('220', 'São Gonçalo', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('221', 'Nova Iguaçu', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('222', 'São Luís', 'BRA', 'Maranhão');
INSERT INTO `databand_city` VALUES ('223', 'Maceió', 'BRA', 'Alagoas');
INSERT INTO `databand_city` VALUES ('224', 'Duque de Caxias', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('225', 'São Bernardo do Campo', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('226', 'Teresina', 'BRA', 'Piauí');
INSERT INTO `databand_city` VALUES ('227', 'Natal', 'BRA', 'Rio Grande do Norte');
INSERT INTO `databand_city` VALUES ('228', 'Osasco', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('229', 'Campo Grande', 'BRA', 'Mato Grosso do Sul');
INSERT INTO `databand_city` VALUES ('230', 'Santo André', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('231', 'João Pessoa', 'BRA', 'Paraíba');
INSERT INTO `databand_city` VALUES ('232', 'Jaboatão dos Guararapes', 'BRA', 'Pernambuco');
INSERT INTO `databand_city` VALUES ('233', 'Contagem', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('234', 'São José dos Campos', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('235', 'Uberlândia', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('236', 'Feira de Santana', 'BRA', 'Bahia');
INSERT INTO `databand_city` VALUES ('237', 'Ribeirão Preto', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('238', 'Sorocaba', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('239', 'Niterói', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('240', 'Cuiabá', 'BRA', 'Mato Grosso');
INSERT INTO `databand_city` VALUES ('241', 'Juiz de Fora', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('242', 'Aracaju', 'BRA', 'Sergipe');
INSERT INTO `databand_city` VALUES ('243', 'São João de Meriti', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('244', 'Londrina', 'BRA', 'Paraná');
INSERT INTO `databand_city` VALUES ('245', 'Joinville', 'BRA', 'Santa Catarina');
INSERT INTO `databand_city` VALUES ('246', 'Belford Roxo', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('247', 'Santos', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('248', 'Ananindeua', 'BRA', 'Pará');
INSERT INTO `databand_city` VALUES ('249', 'Campos dos Goytacazes', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('250', 'Mauá', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('251', 'Carapicuíba', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('252', 'Olinda', 'BRA', 'Pernambuco');
INSERT INTO `databand_city` VALUES ('253', 'Campina Grande', 'BRA', 'Paraíba');
INSERT INTO `databand_city` VALUES ('254', 'São José do Rio Preto', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('255', 'Caxias do Sul', 'BRA', 'Rio Grande do Sul');
INSERT INTO `databand_city` VALUES ('256', 'Moji das Cruzes', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('257', 'Diadema', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('258', 'Aparecida de Goiânia', 'BRA', 'Goiás');
INSERT INTO `databand_city` VALUES ('259', 'Piracicaba', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('260', 'Cariacica', 'BRA', 'Espírito Santo');
INSERT INTO `databand_city` VALUES ('261', 'Vila Velha', 'BRA', 'Espírito Santo');
INSERT INTO `databand_city` VALUES ('262', 'Pelotas', 'BRA', 'Rio Grande do Sul');
INSERT INTO `databand_city` VALUES ('263', 'Bauru', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('264', 'Porto Velho', 'BRA', 'Rondônia');
INSERT INTO `databand_city` VALUES ('265', 'Serra', 'BRA', 'Espírito Santo');
INSERT INTO `databand_city` VALUES ('266', 'Betim', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('267', 'Jundíaí', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('268', 'Canoas', 'BRA', 'Rio Grande do Sul');
INSERT INTO `databand_city` VALUES ('269', 'Franca', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('270', 'São Vicente', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('271', 'Maringá', 'BRA', 'Paraná');
INSERT INTO `databand_city` VALUES ('272', 'Montes Claros', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('273', 'Anápolis', 'BRA', 'Goiás');
INSERT INTO `databand_city` VALUES ('274', 'Florianópolis', 'BRA', 'Santa Catarina');
INSERT INTO `databand_city` VALUES ('275', 'Petrópolis', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('276', 'Itaquaquecetuba', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('277', 'Vitória', 'BRA', 'Espírito Santo');
INSERT INTO `databand_city` VALUES ('278', 'Ponta Grossa', 'BRA', 'Paraná');
INSERT INTO `databand_city` VALUES ('279', 'Rio Branco', 'BRA', 'Acre');
INSERT INTO `databand_city` VALUES ('280', 'Foz do Iguaçu', 'BRA', 'Paraná');
INSERT INTO `databand_city` VALUES ('281', 'Macapá', 'BRA', 'Amapá');
INSERT INTO `databand_city` VALUES ('282', 'Ilhéus', 'BRA', 'Bahia');
INSERT INTO `databand_city` VALUES ('283', 'Vitória da Conquista', 'BRA', 'Bahia');
INSERT INTO `databand_city` VALUES ('284', 'Uberaba', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('285', 'Paulista', 'BRA', 'Pernambuco');
INSERT INTO `databand_city` VALUES ('286', 'Limeira', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('287', 'Blumenau', 'BRA', 'Santa Catarina');
INSERT INTO `databand_city` VALUES ('288', 'Caruaru', 'BRA', 'Pernambuco');
INSERT INTO `databand_city` VALUES ('289', 'Santarém', 'BRA', 'Pará');
INSERT INTO `databand_city` VALUES ('290', 'Volta Redonda', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('291', 'Novo Hamburgo', 'BRA', 'Rio Grande do Sul');
INSERT INTO `databand_city` VALUES ('292', 'Caucaia', 'BRA', 'Ceará');
INSERT INTO `databand_city` VALUES ('293', 'Santa Maria', 'BRA', 'Rio Grande do Sul');
INSERT INTO `databand_city` VALUES ('294', 'Cascavel', 'BRA', 'Paraná');
INSERT INTO `databand_city` VALUES ('295', 'Guarujá', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('296', 'Ribeirão das Neves', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('297', 'Governador Valadares', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('298', 'Taubaté', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('299', 'Imperatriz', 'BRA', 'Maranhão');
INSERT INTO `databand_city` VALUES ('300', 'Gravataí', 'BRA', 'Rio Grande do Sul');
INSERT INTO `databand_city` VALUES ('301', 'Embu', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('302', 'Mossoró', 'BRA', 'Rio Grande do Norte');
INSERT INTO `databand_city` VALUES ('303', 'Várzea Grande', 'BRA', 'Mato Grosso');
INSERT INTO `databand_city` VALUES ('304', 'Petrolina', 'BRA', 'Pernambuco');
INSERT INTO `databand_city` VALUES ('305', 'Barueri', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('306', 'Viamão', 'BRA', 'Rio Grande do Sul');
INSERT INTO `databand_city` VALUES ('307', 'Ipatinga', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('308', 'Juazeiro', 'BRA', 'Bahia');
INSERT INTO `databand_city` VALUES ('309', 'Juazeiro do Norte', 'BRA', 'Ceará');
INSERT INTO `databand_city` VALUES ('310', 'Taboão da Serra', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('311', 'São José dos Pinhais', 'BRA', 'Paraná');
INSERT INTO `databand_city` VALUES ('312', 'Magé', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('313', 'Suzano', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('314', 'São Leopoldo', 'BRA', 'Rio Grande do Sul');
INSERT INTO `databand_city` VALUES ('315', 'Marília', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('316', 'São Carlos', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('317', 'Sumaré', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('318', 'Presidente Prudente', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('319', 'Divinópolis', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('320', 'Sete Lagoas', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('321', 'Rio Grande', 'BRA', 'Rio Grande do Sul');
INSERT INTO `databand_city` VALUES ('322', 'Itabuna', 'BRA', 'Bahia');
INSERT INTO `databand_city` VALUES ('323', 'Jequié', 'BRA', 'Bahia');
INSERT INTO `databand_city` VALUES ('324', 'Arapiraca', 'BRA', 'Alagoas');
INSERT INTO `databand_city` VALUES ('325', 'Colombo', 'BRA', 'Paraná');
INSERT INTO `databand_city` VALUES ('326', 'Americana', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('327', 'Alvorada', 'BRA', 'Rio Grande do Sul');
INSERT INTO `databand_city` VALUES ('328', 'Araraquara', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('329', 'Itaboraí', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('330', 'Santa Bárbara d´Oeste', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('331', 'Nova Friburgo', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('332', 'Jacareí', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('333', 'Araçatuba', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('334', 'Barra Mansa', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('335', 'Praia Grande', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('336', 'Marabá', 'BRA', 'Pará');
INSERT INTO `databand_city` VALUES ('337', 'Criciúma', 'BRA', 'Santa Catarina');
INSERT INTO `databand_city` VALUES ('338', 'Boa Vista', 'BRA', 'Roraima');
INSERT INTO `databand_city` VALUES ('339', 'Passo Fundo', 'BRA', 'Rio Grande do Sul');
INSERT INTO `databand_city` VALUES ('340', 'Dourados', 'BRA', 'Mato Grosso do Sul');
INSERT INTO `databand_city` VALUES ('341', 'Santa Luzia', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('342', 'Rio Claro', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('343', 'Maracanaú', 'BRA', 'Ceará');
INSERT INTO `databand_city` VALUES ('344', 'Guarapuava', 'BRA', 'Paraná');
INSERT INTO `databand_city` VALUES ('345', 'Rondonópolis', 'BRA', 'Mato Grosso');
INSERT INTO `databand_city` VALUES ('346', 'São José', 'BRA', 'Santa Catarina');
INSERT INTO `databand_city` VALUES ('347', 'Cachoeiro de Itapemirim', 'BRA', 'Espírito Santo');
INSERT INTO `databand_city` VALUES ('348', 'Nilópolis', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('349', 'Itapevi', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('350', 'Cabo de Santo Agostinho', 'BRA', 'Pernambuco');
INSERT INTO `databand_city` VALUES ('351', 'Camaçari', 'BRA', 'Bahia');
INSERT INTO `databand_city` VALUES ('352', 'Sobral', 'BRA', 'Ceará');
INSERT INTO `databand_city` VALUES ('353', 'Itajaí', 'BRA', 'Santa Catarina');
INSERT INTO `databand_city` VALUES ('354', 'Chapecó', 'BRA', 'Santa Catarina');
INSERT INTO `databand_city` VALUES ('355', 'Cotia', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('356', 'Lages', 'BRA', 'Santa Catarina');
INSERT INTO `databand_city` VALUES ('357', 'Ferraz de Vasconcelos', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('358', 'Indaiatuba', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('359', 'Hortolândia', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('360', 'Caxias', 'BRA', 'Maranhão');
INSERT INTO `databand_city` VALUES ('361', 'São Caetano do Sul', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('362', 'Itu', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('363', 'Nossa Senhora do Socorro', 'BRA', 'Sergipe');
INSERT INTO `databand_city` VALUES ('364', 'Parnaíba', 'BRA', 'Piauí');
INSERT INTO `databand_city` VALUES ('365', 'Poços de Caldas', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('366', 'Teresópolis', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('367', 'Barreiras', 'BRA', 'Bahia');
INSERT INTO `databand_city` VALUES ('368', 'Castanhal', 'BRA', 'Pará');
INSERT INTO `databand_city` VALUES ('369', 'Alagoinhas', 'BRA', 'Bahia');
INSERT INTO `databand_city` VALUES ('370', 'Itapecerica da Serra', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('371', 'Uruguaiana', 'BRA', 'Rio Grande do Sul');
INSERT INTO `databand_city` VALUES ('372', 'Paranaguá', 'BRA', 'Paraná');
INSERT INTO `databand_city` VALUES ('373', 'Ibirité', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('374', 'Timon', 'BRA', 'Maranhão');
INSERT INTO `databand_city` VALUES ('375', 'Luziânia', 'BRA', 'Goiás');
INSERT INTO `databand_city` VALUES ('376', 'Macaé', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('377', 'Teófilo Otoni', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('378', 'Moji-Guaçu', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('379', 'Palmas', 'BRA', 'Tocantins');
INSERT INTO `databand_city` VALUES ('380', 'Pindamonhangaba', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('381', 'Francisco Morato', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('382', 'Bagé', 'BRA', 'Rio Grande do Sul');
INSERT INTO `databand_city` VALUES ('383', 'Sapucaia do Sul', 'BRA', 'Rio Grande do Sul');
INSERT INTO `databand_city` VALUES ('384', 'Cabo Frio', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('385', 'Itapetininga', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('386', 'Patos de Minas', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('387', 'Camaragibe', 'BRA', 'Pernambuco');
INSERT INTO `databand_city` VALUES ('388', 'Bragança Paulista', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('389', 'Queimados', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('390', 'Araguaína', 'BRA', 'Tocantins');
INSERT INTO `databand_city` VALUES ('391', 'Garanhuns', 'BRA', 'Pernambuco');
INSERT INTO `databand_city` VALUES ('392', 'Vitória de Santo Antão', 'BRA', 'Pernambuco');
INSERT INTO `databand_city` VALUES ('393', 'Santa Rita', 'BRA', 'Paraíba');
INSERT INTO `databand_city` VALUES ('394', 'Barbacena', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('395', 'Abaetetuba', 'BRA', 'Pará');
INSERT INTO `databand_city` VALUES ('396', 'Jaú', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('397', 'Lauro de Freitas', 'BRA', 'Bahia');
INSERT INTO `databand_city` VALUES ('398', 'Franco da Rocha', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('399', 'Teixeira de Freitas', 'BRA', 'Bahia');
INSERT INTO `databand_city` VALUES ('400', 'Varginha', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('401', 'Ribeirão Pires', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('402', 'Sabará', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('403', 'Catanduva', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('404', 'Rio Verde', 'BRA', 'Goiás');
INSERT INTO `databand_city` VALUES ('405', 'Botucatu', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('406', 'Colatina', 'BRA', 'Espírito Santo');
INSERT INTO `databand_city` VALUES ('407', 'Santa Cruz do Sul', 'BRA', 'Rio Grande do Sul');
INSERT INTO `databand_city` VALUES ('408', 'Linhares', 'BRA', 'Espírito Santo');
INSERT INTO `databand_city` VALUES ('409', 'Apucarana', 'BRA', 'Paraná');
INSERT INTO `databand_city` VALUES ('410', 'Barretos', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('411', 'Guaratinguetá', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('412', 'Cachoeirinha', 'BRA', 'Rio Grande do Sul');
INSERT INTO `databand_city` VALUES ('413', 'Codó', 'BRA', 'Maranhão');
INSERT INTO `databand_city` VALUES ('414', 'Jaraguá do Sul', 'BRA', 'Santa Catarina');
INSERT INTO `databand_city` VALUES ('415', 'Cubatão', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('416', 'Itabira', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('417', 'Itaituba', 'BRA', 'Pará');
INSERT INTO `databand_city` VALUES ('418', 'Araras', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('419', 'Resende', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('420', 'Atibaia', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('421', 'Pouso Alegre', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('422', 'Toledo', 'BRA', 'Paraná');
INSERT INTO `databand_city` VALUES ('423', 'Crato', 'BRA', 'Ceará');
INSERT INTO `databand_city` VALUES ('424', 'Passos', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('425', 'Araguari', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('426', 'São José de Ribamar', 'BRA', 'Maranhão');
INSERT INTO `databand_city` VALUES ('427', 'Pinhais', 'BRA', 'Paraná');
INSERT INTO `databand_city` VALUES ('428', 'Sertãozinho', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('429', 'Conselheiro Lafaiete', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('430', 'Paulo Afonso', 'BRA', 'Bahia');
INSERT INTO `databand_city` VALUES ('431', 'Angra dos Reis', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('432', 'Eunápolis', 'BRA', 'Bahia');
INSERT INTO `databand_city` VALUES ('433', 'Salto', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('434', 'Ourinhos', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('435', 'Parnamirim', 'BRA', 'Rio Grande do Norte');
INSERT INTO `databand_city` VALUES ('436', 'Jacobina', 'BRA', 'Bahia');
INSERT INTO `databand_city` VALUES ('437', 'Coronel Fabriciano', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('438', 'Birigui', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('439', 'Tatuí', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('440', 'Ji-Paraná', 'BRA', 'Rondônia');
INSERT INTO `databand_city` VALUES ('441', 'Bacabal', 'BRA', 'Maranhão');
INSERT INTO `databand_city` VALUES ('442', 'Cametá', 'BRA', 'Pará');
INSERT INTO `databand_city` VALUES ('443', 'Guaíba', 'BRA', 'Rio Grande do Sul');
INSERT INTO `databand_city` VALUES ('444', 'São Lourenço da Mata', 'BRA', 'Pernambuco');
INSERT INTO `databand_city` VALUES ('445', 'Santana do Livramento', 'BRA', 'Rio Grande do Sul');
INSERT INTO `databand_city` VALUES ('446', 'Votorantim', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('447', 'Campo Largo', 'BRA', 'Paraná');
INSERT INTO `databand_city` VALUES ('448', 'Patos', 'BRA', 'Paraíba');
INSERT INTO `databand_city` VALUES ('449', 'Ituiutaba', 'BRA', 'Minas Gerais');
INSERT INTO `databand_city` VALUES ('450', 'Corumbá', 'BRA', 'Mato Grosso do Sul');
INSERT INTO `databand_city` VALUES ('451', 'Palhoça', 'BRA', 'Santa Catarina');
INSERT INTO `databand_city` VALUES ('452', 'Barra do Piraí', 'BRA', 'Rio de Janeiro');
INSERT INTO `databand_city` VALUES ('453', 'Bento Gonçalves', 'BRA', 'Rio Grande do Sul');
INSERT INTO `databand_city` VALUES ('454', 'Poá', 'BRA', 'São Paulo');
INSERT INTO `databand_city` VALUES ('455', 'Águas Lindas de Goiás', 'BRA', 'Goiás');
INSERT INTO `databand_city` VALUES ('456', 'London', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('457', 'Birmingham', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('458', 'Glasgow', 'GBR', 'Scotland');
INSERT INTO `databand_city` VALUES ('459', 'Liverpool', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('460', 'Edinburgh', 'GBR', 'Scotland');
INSERT INTO `databand_city` VALUES ('461', 'Sheffield', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('462', 'Manchester', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('463', 'Leeds', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('464', 'Bristol', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('465', 'Cardiff', 'GBR', 'Wales');
INSERT INTO `databand_city` VALUES ('466', 'Coventry', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('467', 'Leicester', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('468', 'Bradford', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('469', 'Belfast', 'GBR', 'North Ireland');
INSERT INTO `databand_city` VALUES ('470', 'Nottingham', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('471', 'Kingston upon Hull', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('472', 'Plymouth', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('473', 'Stoke-on-Trent', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('474', 'Wolverhampton', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('475', 'Derby', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('476', 'Swansea', 'GBR', 'Wales');
INSERT INTO `databand_city` VALUES ('477', 'Southampton', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('478', 'Aberdeen', 'GBR', 'Scotland');
INSERT INTO `databand_city` VALUES ('479', 'Northampton', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('480', 'Dudley', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('481', 'Portsmouth', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('482', 'Newcastle upon Tyne', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('483', 'Sunderland', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('484', 'Luton', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('485', 'Swindon', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('486', 'Southend-on-Sea', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('487', 'Walsall', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('488', 'Bournemouth', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('489', 'Peterborough', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('490', 'Brighton', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('491', 'Blackpool', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('492', 'Dundee', 'GBR', 'Scotland');
INSERT INTO `databand_city` VALUES ('493', 'West Bromwich', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('494', 'Reading', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('495', 'Oldbury/Smethwick (Warley)', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('496', 'Middlesbrough', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('497', 'Huddersfield', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('498', 'Oxford', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('499', 'Poole', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('500', 'Bolton', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('501', 'Blackburn', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('502', 'Newport', 'GBR', 'Wales');
INSERT INTO `databand_city` VALUES ('503', 'Preston', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('504', 'Stockport', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('505', 'Norwich', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('506', 'Rotherham', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('507', 'Cambridge', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('508', 'Watford', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('509', 'Ipswich', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('510', 'Slough', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('511', 'Exeter', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('512', 'Cheltenham', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('513', 'Gloucester', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('514', 'Saint Helens', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('515', 'Sutton Coldfield', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('516', 'York', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('517', 'Oldham', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('518', 'Basildon', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('519', 'Worthing', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('520', 'Chelmsford', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('521', 'Colchester', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('522', 'Crawley', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('523', 'Gillingham', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('524', 'Solihull', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('525', 'Rochdale', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('526', 'Birkenhead', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('527', 'Worcester', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('528', 'Hartlepool', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('529', 'Halifax', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('530', 'Woking/Byfleet', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('531', 'Southport', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('532', 'Maidstone', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('533', 'Eastbourne', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('534', 'Grimsby', 'GBR', 'England');
INSERT INTO `databand_city` VALUES ('535', 'Saint Helier', 'GBR', 'Jersey');
INSERT INTO `databand_city` VALUES ('536', 'Douglas', 'GBR', '–');
INSERT INTO `databand_city` VALUES ('537', 'Road Town', 'VGB', 'Tortola');
INSERT INTO `databand_city` VALUES ('538', 'Bandar Seri Begawan', 'BRN', 'Brunei and Muara');
INSERT INTO `databand_city` VALUES ('539', 'Sofija', 'BGR', 'Grad Sofija');
INSERT INTO `databand_city` VALUES ('540', 'Plovdiv', 'BGR', 'Plovdiv');
INSERT INTO `databand_city` VALUES ('541', 'Varna', 'BGR', 'Varna');
INSERT INTO `databand_city` VALUES ('542', 'Burgas', 'BGR', 'Burgas');
INSERT INTO `databand_city` VALUES ('543', 'Ruse', 'BGR', 'Ruse');
INSERT INTO `databand_city` VALUES ('544', 'Stara Zagora', 'BGR', 'Haskovo');
INSERT INTO `databand_city` VALUES ('545', 'Pleven', 'BGR', 'Lovec');
INSERT INTO `databand_city` VALUES ('546', 'Sliven', 'BGR', 'Burgas');
INSERT INTO `databand_city` VALUES ('547', 'Dobric', 'BGR', 'Varna');
INSERT INTO `databand_city` VALUES ('548', 'Šumen', 'BGR', 'Varna');
INSERT INTO `databand_city` VALUES ('549', 'Ouagadougou', 'BFA', 'Kadiogo');
INSERT INTO `databand_city` VALUES ('550', 'Bobo-Dioulasso', 'BFA', 'Houet');
INSERT INTO `databand_city` VALUES ('551', 'Koudougou', 'BFA', 'Boulkiemdé');
INSERT INTO `databand_city` VALUES ('552', 'Bujumbura', 'BDI', 'Bujumbura');
INSERT INTO `databand_city` VALUES ('553', 'George Town', 'CYM', 'Grand Cayman');
INSERT INTO `databand_city` VALUES ('554', 'Santiago de Chile', 'CHL', 'Santiago');
INSERT INTO `databand_city` VALUES ('555', 'Puente Alto', 'CHL', 'Santiago');
INSERT INTO `databand_city` VALUES ('556', 'Viña del Mar', 'CHL', 'Valparaíso');
INSERT INTO `databand_city` VALUES ('557', 'Valparaíso', 'CHL', 'Valparaíso');
INSERT INTO `databand_city` VALUES ('558', 'Talcahuano', 'CHL', 'Bíobío');
INSERT INTO `databand_city` VALUES ('559', 'Antofagasta', 'CHL', 'Antofagasta');
INSERT INTO `databand_city` VALUES ('560', 'San Bernardo', 'CHL', 'Santiago');
INSERT INTO `databand_city` VALUES ('561', 'Temuco', 'CHL', 'La Araucanía');
INSERT INTO `databand_city` VALUES ('562', 'Concepción', 'CHL', 'Bíobío');
INSERT INTO `databand_city` VALUES ('563', 'Rancagua', 'CHL', 'O´Higgins');
INSERT INTO `databand_city` VALUES ('564', 'Arica', 'CHL', 'Tarapacá');
INSERT INTO `databand_city` VALUES ('565', 'Talca', 'CHL', 'Maule');
INSERT INTO `databand_city` VALUES ('566', 'Chillán', 'CHL', 'Bíobío');
INSERT INTO `databand_city` VALUES ('567', 'Iquique', 'CHL', 'Tarapacá');
INSERT INTO `databand_city` VALUES ('568', 'Los Angeles', 'CHL', 'Bíobío');
INSERT INTO `databand_city` VALUES ('569', 'Puerto Montt', 'CHL', 'Los Lagos');
INSERT INTO `databand_city` VALUES ('570', 'Coquimbo', 'CHL', 'Coquimbo');
INSERT INTO `databand_city` VALUES ('571', 'Osorno', 'CHL', 'Los Lagos');
INSERT INTO `databand_city` VALUES ('572', 'La Serena', 'CHL', 'Coquimbo');
INSERT INTO `databand_city` VALUES ('573', 'Calama', 'CHL', 'Antofagasta');
INSERT INTO `databand_city` VALUES ('574', 'Valdivia', 'CHL', 'Los Lagos');
INSERT INTO `databand_city` VALUES ('575', 'Punta Arenas', 'CHL', 'Magallanes');
INSERT INTO `databand_city` VALUES ('576', 'Copiapó', 'CHL', 'Atacama');
INSERT INTO `databand_city` VALUES ('577', 'Quilpué', 'CHL', 'Valparaíso');
INSERT INTO `databand_city` VALUES ('578', 'Curicó', 'CHL', 'Maule');
INSERT INTO `databand_city` VALUES ('579', 'Ovalle', 'CHL', 'Coquimbo');
INSERT INTO `databand_city` VALUES ('580', 'Coronel', 'CHL', 'Bíobío');
INSERT INTO `databand_city` VALUES ('581', 'San Pedro de la Paz', 'CHL', 'Bíobío');
INSERT INTO `databand_city` VALUES ('582', 'Melipilla', 'CHL', 'Santiago');
INSERT INTO `databand_city` VALUES ('583', 'Avarua', 'COK', 'Rarotonga');
INSERT INTO `databand_city` VALUES ('584', 'San José', 'CRI', 'San José');
INSERT INTO `databand_city` VALUES ('585', 'Djibouti', 'DJI', 'Djibouti');
INSERT INTO `databand_city` VALUES ('586', 'Roseau', 'DMA', 'St George');
INSERT INTO `databand_city` VALUES ('587', 'Santo Domingo de Guzmán', 'DOM', 'Distrito Nacional');
INSERT INTO `databand_city` VALUES ('588', 'Santiago de los Caballeros', 'DOM', 'Santiago');
INSERT INTO `databand_city` VALUES ('589', 'La Romana', 'DOM', 'La Romana');
INSERT INTO `databand_city` VALUES ('590', 'San Pedro de Macorís', 'DOM', 'San Pedro de Macorís');
INSERT INTO `databand_city` VALUES ('591', 'San Francisco de Macorís', 'DOM', 'Duarte');
INSERT INTO `databand_city` VALUES ('592', 'San Felipe de Puerto Plata', 'DOM', 'Puerto Plata');
INSERT INTO `databand_city` VALUES ('593', 'Guayaquil', 'ECU', 'Guayas');
INSERT INTO `databand_city` VALUES ('594', 'Quito', 'ECU', 'Pichincha');
INSERT INTO `databand_city` VALUES ('595', 'Cuenca', 'ECU', 'Azuay');
INSERT INTO `databand_city` VALUES ('596', 'Machala', 'ECU', 'El Oro');
INSERT INTO `databand_city` VALUES ('597', 'Santo Domingo de los Colorados', 'ECU', 'Pichincha');
INSERT INTO `databand_city` VALUES ('598', 'Portoviejo', 'ECU', 'Manabí');
INSERT INTO `databand_city` VALUES ('599', 'Ambato', 'ECU', 'Tungurahua');
INSERT INTO `databand_city` VALUES ('600', 'Manta', 'ECU', 'Manabí');
INSERT INTO `databand_city` VALUES ('601', 'Duran [Eloy Alfaro]', 'ECU', 'Guayas');
INSERT INTO `databand_city` VALUES ('602', 'Ibarra', 'ECU', 'Imbabura');
INSERT INTO `databand_city` VALUES ('603', 'Quevedo', 'ECU', 'Los Ríos');
INSERT INTO `databand_city` VALUES ('604', 'Milagro', 'ECU', 'Guayas');
INSERT INTO `databand_city` VALUES ('605', 'Loja', 'ECU', 'Loja');
INSERT INTO `databand_city` VALUES ('606', 'Ríobamba', 'ECU', 'Chimborazo');
INSERT INTO `databand_city` VALUES ('607', 'Esmeraldas', 'ECU', 'Esmeraldas');
INSERT INTO `databand_city` VALUES ('608', 'Cairo', 'EGY', 'Kairo');
INSERT INTO `databand_city` VALUES ('609', 'Alexandria', 'EGY', 'Aleksandria');
INSERT INTO `databand_city` VALUES ('610', 'Giza', 'EGY', 'Giza');
INSERT INTO `databand_city` VALUES ('611', 'Shubra al-Khayma', 'EGY', 'al-Qalyubiya');
INSERT INTO `databand_city` VALUES ('612', 'Port Said', 'EGY', 'Port Said');
INSERT INTO `databand_city` VALUES ('613', 'Suez', 'EGY', 'Suez');
INSERT INTO `databand_city` VALUES ('614', 'al-Mahallat al-Kubra', 'EGY', 'al-Gharbiya');
INSERT INTO `databand_city` VALUES ('615', 'Tanta', 'EGY', 'al-Gharbiya');
INSERT INTO `databand_city` VALUES ('616', 'al-Mansura', 'EGY', 'al-Daqahliya');
INSERT INTO `databand_city` VALUES ('617', 'Luxor', 'EGY', 'Luxor');
INSERT INTO `databand_city` VALUES ('618', 'Asyut', 'EGY', 'Asyut');
INSERT INTO `databand_city` VALUES ('619', 'Bahtim', 'EGY', 'al-Qalyubiya');
INSERT INTO `databand_city` VALUES ('620', 'Zagazig', 'EGY', 'al-Sharqiya');
INSERT INTO `databand_city` VALUES ('621', 'al-Faiyum', 'EGY', 'al-Faiyum');
INSERT INTO `databand_city` VALUES ('622', 'Ismailia', 'EGY', 'Ismailia');
INSERT INTO `databand_city` VALUES ('623', 'Kafr al-Dawwar', 'EGY', 'al-Buhayra');
INSERT INTO `databand_city` VALUES ('624', 'Assuan', 'EGY', 'Assuan');
INSERT INTO `databand_city` VALUES ('625', 'Damanhur', 'EGY', 'al-Buhayra');
INSERT INTO `databand_city` VALUES ('626', 'al-Minya', 'EGY', 'al-Minya');
INSERT INTO `databand_city` VALUES ('627', 'Bani Suwayf', 'EGY', 'Bani Suwayf');
INSERT INTO `databand_city` VALUES ('628', 'Qina', 'EGY', 'Qina');
INSERT INTO `databand_city` VALUES ('629', 'Sawhaj', 'EGY', 'Sawhaj');
INSERT INTO `databand_city` VALUES ('630', 'Shibin al-Kawm', 'EGY', 'al-Minufiya');
INSERT INTO `databand_city` VALUES ('631', 'Bulaq al-Dakrur', 'EGY', 'Giza');
INSERT INTO `databand_city` VALUES ('632', 'Banha', 'EGY', 'al-Qalyubiya');
INSERT INTO `databand_city` VALUES ('633', 'Warraq al-Arab', 'EGY', 'Giza');
INSERT INTO `databand_city` VALUES ('634', 'Kafr al-Shaykh', 'EGY', 'Kafr al-Shaykh');
INSERT INTO `databand_city` VALUES ('635', 'Mallawi', 'EGY', 'al-Minya');
INSERT INTO `databand_city` VALUES ('636', 'Bilbays', 'EGY', 'al-Sharqiya');
INSERT INTO `databand_city` VALUES ('637', 'Mit Ghamr', 'EGY', 'al-Daqahliya');
INSERT INTO `databand_city` VALUES ('638', 'al-Arish', 'EGY', 'Shamal Sina');
INSERT INTO `databand_city` VALUES ('639', 'Talkha', 'EGY', 'al-Daqahliya');
INSERT INTO `databand_city` VALUES ('640', 'Qalyub', 'EGY', 'al-Qalyubiya');
INSERT INTO `databand_city` VALUES ('641', 'Jirja', 'EGY', 'Sawhaj');
INSERT INTO `databand_city` VALUES ('642', 'Idfu', 'EGY', 'Qina');
INSERT INTO `databand_city` VALUES ('643', 'al-Hawamidiya', 'EGY', 'Giza');
INSERT INTO `databand_city` VALUES ('644', 'Disuq', 'EGY', 'Kafr al-Shaykh');
INSERT INTO `databand_city` VALUES ('645', 'San Salvador', 'SLV', 'San Salvador');
INSERT INTO `databand_city` VALUES ('646', 'Santa Ana', 'SLV', 'Santa Ana');
INSERT INTO `databand_city` VALUES ('647', 'Mejicanos', 'SLV', 'San Salvador');
INSERT INTO `databand_city` VALUES ('648', 'Soyapango', 'SLV', 'San Salvador');
INSERT INTO `databand_city` VALUES ('649', 'San Miguel', 'SLV', 'San Miguel');
INSERT INTO `databand_city` VALUES ('650', 'Nueva San Salvador', 'SLV', 'La Libertad');
INSERT INTO `databand_city` VALUES ('651', 'Apopa', 'SLV', 'San Salvador');
INSERT INTO `databand_city` VALUES ('652', 'Asmara', 'ERI', 'Maekel');
INSERT INTO `databand_city` VALUES ('653', 'Madrid', 'ESP', 'Madrid');
INSERT INTO `databand_city` VALUES ('654', 'Barcelona', 'ESP', 'Katalonia');
INSERT INTO `databand_city` VALUES ('655', 'Valencia', 'ESP', 'Valencia');
INSERT INTO `databand_city` VALUES ('656', 'Sevilla', 'ESP', 'Andalusia');
INSERT INTO `databand_city` VALUES ('657', 'Zaragoza', 'ESP', 'Aragonia');
INSERT INTO `databand_city` VALUES ('658', 'Málaga', 'ESP', 'Andalusia');
INSERT INTO `databand_city` VALUES ('659', 'Bilbao', 'ESP', 'Baskimaa');
INSERT INTO `databand_city` VALUES ('660', 'Las Palmas de Gran Canaria', 'ESP', 'Canary Islands');
INSERT INTO `databand_city` VALUES ('661', 'Murcia', 'ESP', 'Murcia');
INSERT INTO `databand_city` VALUES ('662', 'Palma de Mallorca', 'ESP', 'Balears');
INSERT INTO `databand_city` VALUES ('663', 'Valladolid', 'ESP', 'Castilla and León');
INSERT INTO `databand_city` VALUES ('664', 'Córdoba', 'ESP', 'Andalusia');
INSERT INTO `databand_city` VALUES ('665', 'Vigo', 'ESP', 'Galicia');
INSERT INTO `databand_city` VALUES ('666', 'Alicante [Alacant]', 'ESP', 'Valencia');
INSERT INTO `databand_city` VALUES ('667', 'Gijón', 'ESP', 'Asturia');
INSERT INTO `databand_city` VALUES ('668', 'L´Hospitalet de Llobregat', 'ESP', 'Katalonia');
INSERT INTO `databand_city` VALUES ('669', 'Granada', 'ESP', 'Andalusia');
INSERT INTO `databand_city` VALUES ('670', 'A Coruña (La Coruña)', 'ESP', 'Galicia');
INSERT INTO `databand_city` VALUES ('671', 'Vitoria-Gasteiz', 'ESP', 'Baskimaa');
INSERT INTO `databand_city` VALUES ('672', 'Santa Cruz de Tenerife', 'ESP', 'Canary Islands');
INSERT INTO `databand_city` VALUES ('673', 'Badalona', 'ESP', 'Katalonia');
INSERT INTO `databand_city` VALUES ('674', 'Oviedo', 'ESP', 'Asturia');
INSERT INTO `databand_city` VALUES ('675', 'Móstoles', 'ESP', 'Madrid');
INSERT INTO `databand_city` VALUES ('676', 'Elche [Elx]', 'ESP', 'Valencia');
INSERT INTO `databand_city` VALUES ('677', 'Sabadell', 'ESP', 'Katalonia');
INSERT INTO `databand_city` VALUES ('678', 'Santander', 'ESP', 'Cantabria');
INSERT INTO `databand_city` VALUES ('679', 'Jerez de la Frontera', 'ESP', 'Andalusia');
INSERT INTO `databand_city` VALUES ('680', 'Pamplona [Iruña]', 'ESP', 'Navarra');
INSERT INTO `databand_city` VALUES ('681', 'Donostia-San Sebastián', 'ESP', 'Baskimaa');
INSERT INTO `databand_city` VALUES ('682', 'Cartagena', 'ESP', 'Murcia');
INSERT INTO `databand_city` VALUES ('683', 'Leganés', 'ESP', 'Madrid');
INSERT INTO `databand_city` VALUES ('684', 'Fuenlabrada', 'ESP', 'Madrid');
INSERT INTO `databand_city` VALUES ('685', 'Almería', 'ESP', 'Andalusia');
INSERT INTO `databand_city` VALUES ('686', 'Terrassa', 'ESP', 'Katalonia');
INSERT INTO `databand_city` VALUES ('687', 'Alcalá de Henares', 'ESP', 'Madrid');
INSERT INTO `databand_city` VALUES ('688', 'Burgos', 'ESP', 'Castilla and León');
INSERT INTO `databand_city` VALUES ('689', 'Salamanca', 'ESP', 'Castilla and León');
INSERT INTO `databand_city` VALUES ('690', 'Albacete', 'ESP', 'Kastilia-La Mancha');
INSERT INTO `databand_city` VALUES ('691', 'Getafe', 'ESP', 'Madrid');
INSERT INTO `databand_city` VALUES ('692', 'Cádiz', 'ESP', 'Andalusia');
INSERT INTO `databand_city` VALUES ('693', 'Alcorcón', 'ESP', 'Madrid');
INSERT INTO `databand_city` VALUES ('694', 'Huelva', 'ESP', 'Andalusia');
INSERT INTO `databand_city` VALUES ('695', 'León', 'ESP', 'Castilla and León');
INSERT INTO `databand_city` VALUES ('696', 'Castellón de la Plana [Castell', 'ESP', 'Valencia');
INSERT INTO `databand_city` VALUES ('697', 'Badajoz', 'ESP', 'Extremadura');
INSERT INTO `databand_city` VALUES ('698', '[San Cristóbal de] la Laguna', 'ESP', 'Canary Islands');
INSERT INTO `databand_city` VALUES ('699', 'Logroño', 'ESP', 'La Rioja');
INSERT INTO `databand_city` VALUES ('700', 'Santa Coloma de Gramenet', 'ESP', 'Katalonia');
INSERT INTO `databand_city` VALUES ('701', 'Tarragona', 'ESP', 'Katalonia');
INSERT INTO `databand_city` VALUES ('702', 'Lleida (Lérida)', 'ESP', 'Katalonia');
INSERT INTO `databand_city` VALUES ('703', 'Jaén', 'ESP', 'Andalusia');
INSERT INTO `databand_city` VALUES ('704', 'Ourense (Orense)', 'ESP', 'Galicia');
INSERT INTO `databand_city` VALUES ('705', 'Mataró', 'ESP', 'Katalonia');
INSERT INTO `databand_city` VALUES ('706', 'Algeciras', 'ESP', 'Andalusia');
INSERT INTO `databand_city` VALUES ('707', 'Marbella', 'ESP', 'Andalusia');
INSERT INTO `databand_city` VALUES ('708', 'Barakaldo', 'ESP', 'Baskimaa');
INSERT INTO `databand_city` VALUES ('709', 'Dos Hermanas', 'ESP', 'Andalusia');
INSERT INTO `databand_city` VALUES ('710', 'Santiago de Compostela', 'ESP', 'Galicia');
INSERT INTO `databand_city` VALUES ('711', 'Torrejón de Ardoz', 'ESP', 'Madrid');
INSERT INTO `databand_city` VALUES ('712', 'Cape Town', 'ZAF', 'Western Cape');
INSERT INTO `databand_city` VALUES ('713', 'Soweto', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('714', 'Johannesburg', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('715', 'Port Elizabeth', 'ZAF', 'Eastern Cape');
INSERT INTO `databand_city` VALUES ('716', 'Pretoria', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('717', 'Inanda', 'ZAF', 'KwaZulu-Natal');
INSERT INTO `databand_city` VALUES ('718', 'Durban', 'ZAF', 'KwaZulu-Natal');
INSERT INTO `databand_city` VALUES ('719', 'Vanderbijlpark', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('720', 'Kempton Park', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('721', 'Alberton', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('722', 'Pinetown', 'ZAF', 'KwaZulu-Natal');
INSERT INTO `databand_city` VALUES ('723', 'Pietermaritzburg', 'ZAF', 'KwaZulu-Natal');
INSERT INTO `databand_city` VALUES ('724', 'Benoni', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('725', 'Randburg', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('726', 'Umlazi', 'ZAF', 'KwaZulu-Natal');
INSERT INTO `databand_city` VALUES ('727', 'Bloemfontein', 'ZAF', 'Free State');
INSERT INTO `databand_city` VALUES ('728', 'Vereeniging', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('729', 'Wonderboom', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('730', 'Roodepoort', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('731', 'Boksburg', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('732', 'Klerksdorp', 'ZAF', 'North West');
INSERT INTO `databand_city` VALUES ('733', 'Soshanguve', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('734', 'Newcastle', 'ZAF', 'KwaZulu-Natal');
INSERT INTO `databand_city` VALUES ('735', 'East London', 'ZAF', 'Eastern Cape');
INSERT INTO `databand_city` VALUES ('736', 'Welkom', 'ZAF', 'Free State');
INSERT INTO `databand_city` VALUES ('737', 'Kimberley', 'ZAF', 'Northern Cape');
INSERT INTO `databand_city` VALUES ('738', 'Uitenhage', 'ZAF', 'Eastern Cape');
INSERT INTO `databand_city` VALUES ('739', 'Chatsworth', 'ZAF', 'KwaZulu-Natal');
INSERT INTO `databand_city` VALUES ('740', 'Mdantsane', 'ZAF', 'Eastern Cape');
INSERT INTO `databand_city` VALUES ('741', 'Krugersdorp', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('742', 'Botshabelo', 'ZAF', 'Free State');
INSERT INTO `databand_city` VALUES ('743', 'Brakpan', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('744', 'Witbank', 'ZAF', 'Mpumalanga');
INSERT INTO `databand_city` VALUES ('745', 'Oberholzer', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('746', 'Germiston', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('747', 'Springs', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('748', 'Westonaria', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('749', 'Randfontein', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('750', 'Paarl', 'ZAF', 'Western Cape');
INSERT INTO `databand_city` VALUES ('751', 'Potchefstroom', 'ZAF', 'North West');
INSERT INTO `databand_city` VALUES ('752', 'Rustenburg', 'ZAF', 'North West');
INSERT INTO `databand_city` VALUES ('753', 'Nigel', 'ZAF', 'Gauteng');
INSERT INTO `databand_city` VALUES ('754', 'George', 'ZAF', 'Western Cape');
INSERT INTO `databand_city` VALUES ('755', 'Ladysmith', 'ZAF', 'KwaZulu-Natal');
INSERT INTO `databand_city` VALUES ('756', 'Addis Abeba', 'ETH', 'Addis Abeba');
INSERT INTO `databand_city` VALUES ('757', 'Dire Dawa', 'ETH', 'Dire Dawa');
INSERT INTO `databand_city` VALUES ('758', 'Nazret', 'ETH', 'Oromia');
INSERT INTO `databand_city` VALUES ('759', 'Gonder', 'ETH', 'Amhara');
INSERT INTO `databand_city` VALUES ('760', 'Dese', 'ETH', 'Amhara');
INSERT INTO `databand_city` VALUES ('761', 'Mekele', 'ETH', 'Tigray');
INSERT INTO `databand_city` VALUES ('762', 'Bahir Dar', 'ETH', 'Amhara');
INSERT INTO `databand_city` VALUES ('763', 'Stanley', 'FLK', 'East Falkland');
INSERT INTO `databand_city` VALUES ('764', 'Suva', 'FJI', 'Central');
INSERT INTO `databand_city` VALUES ('765', 'Quezon', 'PHL', 'National Capital Reg');
INSERT INTO `databand_city` VALUES ('766', 'Manila', 'PHL', 'National Capital Reg');
INSERT INTO `databand_city` VALUES ('767', 'Kalookan', 'PHL', 'National Capital Reg');
INSERT INTO `databand_city` VALUES ('768', 'Davao', 'PHL', 'Southern Mindanao');
INSERT INTO `databand_city` VALUES ('769', 'Cebu', 'PHL', 'Central Visayas');
INSERT INTO `databand_city` VALUES ('770', 'Zamboanga', 'PHL', 'Western Mindanao');
INSERT INTO `databand_city` VALUES ('771', 'Pasig', 'PHL', 'National Capital Reg');
INSERT INTO `databand_city` VALUES ('772', 'Valenzuela', 'PHL', 'National Capital Reg');
INSERT INTO `databand_city` VALUES ('773', 'Las Piñas', 'PHL', 'National Capital Reg');
INSERT INTO `databand_city` VALUES ('774', 'Antipolo', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('775', 'Taguig', 'PHL', 'National Capital Reg');
INSERT INTO `databand_city` VALUES ('776', 'Cagayan de Oro', 'PHL', 'Northern Mindanao');
INSERT INTO `databand_city` VALUES ('777', 'Parañaque', 'PHL', 'National Capital Reg');
INSERT INTO `databand_city` VALUES ('778', 'Makati', 'PHL', 'National Capital Reg');
INSERT INTO `databand_city` VALUES ('779', 'Bacolod', 'PHL', 'Western Visayas');
INSERT INTO `databand_city` VALUES ('780', 'General Santos', 'PHL', 'Southern Mindanao');
INSERT INTO `databand_city` VALUES ('781', 'Marikina', 'PHL', 'National Capital Reg');
INSERT INTO `databand_city` VALUES ('782', 'Dasmariñas', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('783', 'Muntinlupa', 'PHL', 'National Capital Reg');
INSERT INTO `databand_city` VALUES ('784', 'Iloilo', 'PHL', 'Western Visayas');
INSERT INTO `databand_city` VALUES ('785', 'Pasay', 'PHL', 'National Capital Reg');
INSERT INTO `databand_city` VALUES ('786', 'Malabon', 'PHL', 'National Capital Reg');
INSERT INTO `databand_city` VALUES ('787', 'San José del Monte', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('788', 'Bacoor', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('789', 'Iligan', 'PHL', 'Central Mindanao');
INSERT INTO `databand_city` VALUES ('790', 'Calamba', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('791', 'Mandaluyong', 'PHL', 'National Capital Reg');
INSERT INTO `databand_city` VALUES ('792', 'Butuan', 'PHL', 'Caraga');
INSERT INTO `databand_city` VALUES ('793', 'Angeles', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('794', 'Tarlac', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('795', 'Mandaue', 'PHL', 'Central Visayas');
INSERT INTO `databand_city` VALUES ('796', 'Baguio', 'PHL', 'CAR');
INSERT INTO `databand_city` VALUES ('797', 'Batangas', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('798', 'Cainta', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('799', 'San Pedro', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('800', 'Navotas', 'PHL', 'National Capital Reg');
INSERT INTO `databand_city` VALUES ('801', 'Cabanatuan', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('802', 'San Fernando', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('803', 'Lipa', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('804', 'Lapu-Lapu', 'PHL', 'Central Visayas');
INSERT INTO `databand_city` VALUES ('805', 'San Pablo', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('806', 'Biñan', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('807', 'Taytay', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('808', 'Lucena', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('809', 'Imus', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('810', 'Olongapo', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('811', 'Binangonan', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('812', 'Santa Rosa', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('813', 'Tagum', 'PHL', 'Southern Mindanao');
INSERT INTO `databand_city` VALUES ('814', 'Tacloban', 'PHL', 'Eastern Visayas');
INSERT INTO `databand_city` VALUES ('815', 'Malolos', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('816', 'Mabalacat', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('817', 'Cotabato', 'PHL', 'Central Mindanao');
INSERT INTO `databand_city` VALUES ('818', 'Meycauayan', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('819', 'Puerto Princesa', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('820', 'Legazpi', 'PHL', 'Bicol');
INSERT INTO `databand_city` VALUES ('821', 'Silang', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('822', 'Ormoc', 'PHL', 'Eastern Visayas');
INSERT INTO `databand_city` VALUES ('823', 'San Carlos', 'PHL', 'Ilocos');
INSERT INTO `databand_city` VALUES ('824', 'Kabankalan', 'PHL', 'Western Visayas');
INSERT INTO `databand_city` VALUES ('825', 'Talisay', 'PHL', 'Central Visayas');
INSERT INTO `databand_city` VALUES ('826', 'Valencia', 'PHL', 'Northern Mindanao');
INSERT INTO `databand_city` VALUES ('827', 'Calbayog', 'PHL', 'Eastern Visayas');
INSERT INTO `databand_city` VALUES ('828', 'Santa Maria', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('829', 'Pagadian', 'PHL', 'Western Mindanao');
INSERT INTO `databand_city` VALUES ('830', 'Cadiz', 'PHL', 'Western Visayas');
INSERT INTO `databand_city` VALUES ('831', 'Bago', 'PHL', 'Western Visayas');
INSERT INTO `databand_city` VALUES ('832', 'Toledo', 'PHL', 'Central Visayas');
INSERT INTO `databand_city` VALUES ('833', 'Naga', 'PHL', 'Bicol');
INSERT INTO `databand_city` VALUES ('834', 'San Mateo', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('835', 'Panabo', 'PHL', 'Southern Mindanao');
INSERT INTO `databand_city` VALUES ('836', 'Koronadal', 'PHL', 'Southern Mindanao');
INSERT INTO `databand_city` VALUES ('837', 'Marawi', 'PHL', 'Central Mindanao');
INSERT INTO `databand_city` VALUES ('838', 'Dagupan', 'PHL', 'Ilocos');
INSERT INTO `databand_city` VALUES ('839', 'Sagay', 'PHL', 'Western Visayas');
INSERT INTO `databand_city` VALUES ('840', 'Roxas', 'PHL', 'Western Visayas');
INSERT INTO `databand_city` VALUES ('841', 'Lubao', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('842', 'Digos', 'PHL', 'Southern Mindanao');
INSERT INTO `databand_city` VALUES ('843', 'San Miguel', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('844', 'Malaybalay', 'PHL', 'Northern Mindanao');
INSERT INTO `databand_city` VALUES ('845', 'Tuguegarao', 'PHL', 'Cagayan Valley');
INSERT INTO `databand_city` VALUES ('846', 'Ilagan', 'PHL', 'Cagayan Valley');
INSERT INTO `databand_city` VALUES ('847', 'Baliuag', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('848', 'Surigao', 'PHL', 'Caraga');
INSERT INTO `databand_city` VALUES ('849', 'San Carlos', 'PHL', 'Western Visayas');
INSERT INTO `databand_city` VALUES ('850', 'San Juan del Monte', 'PHL', 'National Capital Reg');
INSERT INTO `databand_city` VALUES ('851', 'Tanauan', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('852', 'Concepcion', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('853', 'Rodriguez (Montalban)', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('854', 'Sariaya', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('855', 'Malasiqui', 'PHL', 'Ilocos');
INSERT INTO `databand_city` VALUES ('856', 'General Mariano Alvarez', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('857', 'Urdaneta', 'PHL', 'Ilocos');
INSERT INTO `databand_city` VALUES ('858', 'Hagonoy', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('859', 'San Jose', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('860', 'Polomolok', 'PHL', 'Southern Mindanao');
INSERT INTO `databand_city` VALUES ('861', 'Santiago', 'PHL', 'Cagayan Valley');
INSERT INTO `databand_city` VALUES ('862', 'Tanza', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('863', 'Ozamis', 'PHL', 'Northern Mindanao');
INSERT INTO `databand_city` VALUES ('864', 'Mexico', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('865', 'San Jose', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('866', 'Silay', 'PHL', 'Western Visayas');
INSERT INTO `databand_city` VALUES ('867', 'General Trias', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('868', 'Tabaco', 'PHL', 'Bicol');
INSERT INTO `databand_city` VALUES ('869', 'Cabuyao', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('870', 'Calapan', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('871', 'Mati', 'PHL', 'Southern Mindanao');
INSERT INTO `databand_city` VALUES ('872', 'Midsayap', 'PHL', 'Central Mindanao');
INSERT INTO `databand_city` VALUES ('873', 'Cauayan', 'PHL', 'Cagayan Valley');
INSERT INTO `databand_city` VALUES ('874', 'Gingoog', 'PHL', 'Northern Mindanao');
INSERT INTO `databand_city` VALUES ('875', 'Dumaguete', 'PHL', 'Central Visayas');
INSERT INTO `databand_city` VALUES ('876', 'San Fernando', 'PHL', 'Ilocos');
INSERT INTO `databand_city` VALUES ('877', 'Arayat', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('878', 'Bayawan (Tulong)', 'PHL', 'Central Visayas');
INSERT INTO `databand_city` VALUES ('879', 'Kidapawan', 'PHL', 'Central Mindanao');
INSERT INTO `databand_city` VALUES ('880', 'Daraga (Locsin)', 'PHL', 'Bicol');
INSERT INTO `databand_city` VALUES ('881', 'Marilao', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('882', 'Malita', 'PHL', 'Southern Mindanao');
INSERT INTO `databand_city` VALUES ('883', 'Dipolog', 'PHL', 'Western Mindanao');
INSERT INTO `databand_city` VALUES ('884', 'Cavite', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('885', 'Danao', 'PHL', 'Central Visayas');
INSERT INTO `databand_city` VALUES ('886', 'Bislig', 'PHL', 'Caraga');
INSERT INTO `databand_city` VALUES ('887', 'Talavera', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('888', 'Guagua', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('889', 'Bayambang', 'PHL', 'Ilocos');
INSERT INTO `databand_city` VALUES ('890', 'Nasugbu', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('891', 'Baybay', 'PHL', 'Eastern Visayas');
INSERT INTO `databand_city` VALUES ('892', 'Capas', 'PHL', 'Central Luzon');
INSERT INTO `databand_city` VALUES ('893', 'Sultan Kudarat', 'PHL', 'ARMM');
INSERT INTO `databand_city` VALUES ('894', 'Laoag', 'PHL', 'Ilocos');
INSERT INTO `databand_city` VALUES ('895', 'Bayugan', 'PHL', 'Caraga');
INSERT INTO `databand_city` VALUES ('896', 'Malungon', 'PHL', 'Southern Mindanao');
INSERT INTO `databand_city` VALUES ('897', 'Santa Cruz', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('898', 'Sorsogon', 'PHL', 'Bicol');
INSERT INTO `databand_city` VALUES ('899', 'Candelaria', 'PHL', 'Southern Tagalog');
INSERT INTO `databand_city` VALUES ('900', 'Ligao', 'PHL', 'Bicol');
INSERT INTO `databand_city` VALUES ('901', 'Tórshavn', 'FRO', 'Streymoyar');
INSERT INTO `databand_city` VALUES ('902', 'Libreville', 'GAB', 'Estuaire');
INSERT INTO `databand_city` VALUES ('903', 'Serekunda', 'GMB', 'Kombo St Mary');
INSERT INTO `databand_city` VALUES ('904', 'Banjul', 'GMB', 'Banjul');
INSERT INTO `databand_city` VALUES ('905', 'Tbilisi', 'GEO', 'Tbilisi');
INSERT INTO `databand_city` VALUES ('906', 'Kutaisi', 'GEO', 'Imereti');
INSERT INTO `databand_city` VALUES ('907', 'Rustavi', 'GEO', 'Kvemo Kartli');
INSERT INTO `databand_city` VALUES ('908', 'Batumi', 'GEO', 'Adzaria [Atšara]');
INSERT INTO `databand_city` VALUES ('909', 'Sohumi', 'GEO', 'Abhasia [Aphazeti]');
INSERT INTO `databand_city` VALUES ('910', 'Accra', 'GHA', 'Greater Accra');
INSERT INTO `databand_city` VALUES ('911', 'Kumasi', 'GHA', 'Ashanti');
INSERT INTO `databand_city` VALUES ('912', 'Tamale', 'GHA', 'Northern');
INSERT INTO `databand_city` VALUES ('913', 'Tema', 'GHA', 'Greater Accra');
INSERT INTO `databand_city` VALUES ('914', 'Sekondi-Takoradi', 'GHA', 'Western');
INSERT INTO `databand_city` VALUES ('915', 'Gibraltar', 'GIB', '–');
INSERT INTO `databand_city` VALUES ('916', 'Saint George´s', 'GRD', 'St George');
INSERT INTO `databand_city` VALUES ('917', 'Nuuk', 'GRL', 'Kitaa');
INSERT INTO `databand_city` VALUES ('918', 'Les Abymes', 'GLP', 'Grande-Terre');
INSERT INTO `databand_city` VALUES ('919', 'Basse-Terre', 'GLP', 'Basse-Terre');
INSERT INTO `databand_city` VALUES ('920', 'Tamuning', 'GUM', '–');
INSERT INTO `databand_city` VALUES ('921', 'Agaña', 'GUM', '–');
INSERT INTO `databand_city` VALUES ('922', 'Ciudad de Guatemala', 'GTM', 'Guatemala');
INSERT INTO `databand_city` VALUES ('923', 'Mixco', 'GTM', 'Guatemala');
INSERT INTO `databand_city` VALUES ('924', 'Villa Nueva', 'GTM', 'Guatemala');
INSERT INTO `databand_city` VALUES ('925', 'Quetzaltenango', 'GTM', 'Quetzaltenango');
INSERT INTO `databand_city` VALUES ('926', 'Conakry', 'GIN', 'Conakry');
INSERT INTO `databand_city` VALUES ('927', 'Bissau', 'GNB', 'Bissau');
INSERT INTO `databand_city` VALUES ('928', 'Georgetown', 'GUY', 'Georgetown');
INSERT INTO `databand_city` VALUES ('929', 'Port-au-Prince', 'HTI', 'Ouest');
INSERT INTO `databand_city` VALUES ('930', 'Carrefour', 'HTI', 'Ouest');
INSERT INTO `databand_city` VALUES ('931', 'Delmas', 'HTI', 'Ouest');
INSERT INTO `databand_city` VALUES ('932', 'Le-Cap-Haïtien', 'HTI', 'Nord');
INSERT INTO `databand_city` VALUES ('933', 'Tegucigalpa', 'HND', 'Distrito Central');
INSERT INTO `databand_city` VALUES ('934', 'San Pedro Sula', 'HND', 'Cortés');
INSERT INTO `databand_city` VALUES ('935', 'La Ceiba', 'HND', 'Atlántida');
INSERT INTO `databand_city` VALUES ('936', 'Kowloon and New Kowloon', 'HKG', 'Kowloon and New Kowl');
INSERT INTO `databand_city` VALUES ('937', 'Victoria', 'HKG', 'Hongkong');
INSERT INTO `databand_city` VALUES ('938', 'Longyearbyen', 'SJM', 'Länsimaa');
INSERT INTO `databand_city` VALUES ('939', 'Jakarta', 'IDN', 'Jakarta Raya');
INSERT INTO `databand_city` VALUES ('940', 'Surabaya', 'IDN', 'East Java');
INSERT INTO `databand_city` VALUES ('941', 'Bandung', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('942', 'Medan', 'IDN', 'Sumatera Utara');
INSERT INTO `databand_city` VALUES ('943', 'Palembang', 'IDN', 'Sumatera Selatan');
INSERT INTO `databand_city` VALUES ('944', 'Tangerang', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('945', 'Semarang', 'IDN', 'Central Java');
INSERT INTO `databand_city` VALUES ('946', 'Ujung Pandang', 'IDN', 'Sulawesi Selatan');
INSERT INTO `databand_city` VALUES ('947', 'Malang', 'IDN', 'East Java');
INSERT INTO `databand_city` VALUES ('948', 'Bandar Lampung', 'IDN', 'Lampung');
INSERT INTO `databand_city` VALUES ('949', 'Bekasi', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('950', 'Padang', 'IDN', 'Sumatera Barat');
INSERT INTO `databand_city` VALUES ('951', 'Surakarta', 'IDN', 'Central Java');
INSERT INTO `databand_city` VALUES ('952', 'Banjarmasin', 'IDN', 'Kalimantan Selatan');
INSERT INTO `databand_city` VALUES ('953', 'Pekan Baru', 'IDN', 'Riau');
INSERT INTO `databand_city` VALUES ('954', 'Denpasar', 'IDN', 'Bali');
INSERT INTO `databand_city` VALUES ('955', 'Yogyakarta', 'IDN', 'Yogyakarta');
INSERT INTO `databand_city` VALUES ('956', 'Pontianak', 'IDN', 'Kalimantan Barat');
INSERT INTO `databand_city` VALUES ('957', 'Samarinda', 'IDN', 'Kalimantan Timur');
INSERT INTO `databand_city` VALUES ('958', 'Jambi', 'IDN', 'Jambi');
INSERT INTO `databand_city` VALUES ('959', 'Depok', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('960', 'Cimahi', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('961', 'Balikpapan', 'IDN', 'Kalimantan Timur');
INSERT INTO `databand_city` VALUES ('962', 'Manado', 'IDN', 'Sulawesi Utara');
INSERT INTO `databand_city` VALUES ('963', 'Mataram', 'IDN', 'Nusa Tenggara Barat');
INSERT INTO `databand_city` VALUES ('964', 'Pekalongan', 'IDN', 'Central Java');
INSERT INTO `databand_city` VALUES ('965', 'Tegal', 'IDN', 'Central Java');
INSERT INTO `databand_city` VALUES ('966', 'Bogor', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('967', 'Ciputat', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('968', 'Pondokgede', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('969', 'Cirebon', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('970', 'Kediri', 'IDN', 'East Java');
INSERT INTO `databand_city` VALUES ('971', 'Ambon', 'IDN', 'Molukit');
INSERT INTO `databand_city` VALUES ('972', 'Jember', 'IDN', 'East Java');
INSERT INTO `databand_city` VALUES ('973', 'Cilacap', 'IDN', 'Central Java');
INSERT INTO `databand_city` VALUES ('974', 'Cimanggis', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('975', 'Pematang Siantar', 'IDN', 'Sumatera Utara');
INSERT INTO `databand_city` VALUES ('976', 'Purwokerto', 'IDN', 'Central Java');
INSERT INTO `databand_city` VALUES ('977', 'Ciomas', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('978', 'Tasikmalaya', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('979', 'Madiun', 'IDN', 'East Java');
INSERT INTO `databand_city` VALUES ('980', 'Bengkulu', 'IDN', 'Bengkulu');
INSERT INTO `databand_city` VALUES ('981', 'Karawang', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('982', 'Banda Aceh', 'IDN', 'Aceh');
INSERT INTO `databand_city` VALUES ('983', 'Palu', 'IDN', 'Sulawesi Tengah');
INSERT INTO `databand_city` VALUES ('984', 'Pasuruan', 'IDN', 'East Java');
INSERT INTO `databand_city` VALUES ('985', 'Kupang', 'IDN', 'Nusa Tenggara Timur');
INSERT INTO `databand_city` VALUES ('986', 'Tebing Tinggi', 'IDN', 'Sumatera Utara');
INSERT INTO `databand_city` VALUES ('987', 'Percut Sei Tuan', 'IDN', 'Sumatera Utara');
INSERT INTO `databand_city` VALUES ('988', 'Binjai', 'IDN', 'Sumatera Utara');
INSERT INTO `databand_city` VALUES ('989', 'Sukabumi', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('990', 'Waru', 'IDN', 'East Java');
INSERT INTO `databand_city` VALUES ('991', 'Pangkal Pinang', 'IDN', 'Sumatera Selatan');
INSERT INTO `databand_city` VALUES ('992', 'Magelang', 'IDN', 'Central Java');
INSERT INTO `databand_city` VALUES ('993', 'Blitar', 'IDN', 'East Java');
INSERT INTO `databand_city` VALUES ('994', 'Serang', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('995', 'Probolinggo', 'IDN', 'East Java');
INSERT INTO `databand_city` VALUES ('996', 'Cilegon', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('997', 'Cianjur', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('998', 'Ciparay', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('999', 'Lhokseumawe', 'IDN', 'Aceh');
INSERT INTO `databand_city` VALUES ('1000', 'Taman', 'IDN', 'East Java');
INSERT INTO `databand_city` VALUES ('1001', 'Depok', 'IDN', 'Yogyakarta');
INSERT INTO `databand_city` VALUES ('1002', 'Citeureup', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('1003', 'Pemalang', 'IDN', 'Central Java');
INSERT INTO `databand_city` VALUES ('1004', 'Klaten', 'IDN', 'Central Java');
INSERT INTO `databand_city` VALUES ('1005', 'Salatiga', 'IDN', 'Central Java');
INSERT INTO `databand_city` VALUES ('1006', 'Cibinong', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('1007', 'Palangka Raya', 'IDN', 'Kalimantan Tengah');
INSERT INTO `databand_city` VALUES ('1008', 'Mojokerto', 'IDN', 'East Java');
INSERT INTO `databand_city` VALUES ('1009', 'Purwakarta', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('1010', 'Garut', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('1011', 'Kudus', 'IDN', 'Central Java');
INSERT INTO `databand_city` VALUES ('1012', 'Kendari', 'IDN', 'Sulawesi Tenggara');
INSERT INTO `databand_city` VALUES ('1013', 'Jaya Pura', 'IDN', 'West Irian');
INSERT INTO `databand_city` VALUES ('1014', 'Gorontalo', 'IDN', 'Sulawesi Utara');
INSERT INTO `databand_city` VALUES ('1015', 'Majalaya', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('1016', 'Pondok Aren', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('1017', 'Jombang', 'IDN', 'East Java');
INSERT INTO `databand_city` VALUES ('1018', 'Sunggal', 'IDN', 'Sumatera Utara');
INSERT INTO `databand_city` VALUES ('1019', 'Batam', 'IDN', 'Riau');
INSERT INTO `databand_city` VALUES ('1020', 'Padang Sidempuan', 'IDN', 'Sumatera Utara');
INSERT INTO `databand_city` VALUES ('1021', 'Sawangan', 'IDN', 'West Java');
INSERT INTO `databand_city` VALUES ('1022', 'Banyuwangi', 'IDN', 'East Java');
INSERT INTO `databand_city` VALUES ('1023', 'Tanjung Pinang', 'IDN', 'Riau');
INSERT INTO `databand_city` VALUES ('1024', 'Mumbai (Bombay)', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1025', 'Delhi', 'IND', 'Delhi');
INSERT INTO `databand_city` VALUES ('1026', 'Calcutta [Kolkata]', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1027', 'Chennai (Madras)', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1028', 'Hyderabad', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1029', 'Ahmedabad', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1030', 'Bangalore', 'IND', 'Karnataka');
INSERT INTO `databand_city` VALUES ('1031', 'Kanpur', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1032', 'Nagpur', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1033', 'Lucknow', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1034', 'Pune', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1035', 'Surat', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1036', 'Jaipur', 'IND', 'Rajasthan');
INSERT INTO `databand_city` VALUES ('1037', 'Indore', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1038', 'Bhopal', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1039', 'Ludhiana', 'IND', 'Punjab');
INSERT INTO `databand_city` VALUES ('1040', 'Vadodara (Baroda)', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1041', 'Kalyan', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1042', 'Madurai', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1043', 'Haora (Howrah)', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1044', 'Varanasi (Benares)', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1045', 'Patna', 'IND', 'Bihar');
INSERT INTO `databand_city` VALUES ('1046', 'Srinagar', 'IND', 'Jammu and Kashmir');
INSERT INTO `databand_city` VALUES ('1047', 'Agra', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1048', 'Coimbatore', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1049', 'Thane (Thana)', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1050', 'Allahabad', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1051', 'Meerut', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1052', 'Vishakhapatnam', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1053', 'Jabalpur', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1054', 'Amritsar', 'IND', 'Punjab');
INSERT INTO `databand_city` VALUES ('1055', 'Faridabad', 'IND', 'Haryana');
INSERT INTO `databand_city` VALUES ('1056', 'Vijayawada', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1057', 'Gwalior', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1058', 'Jodhpur', 'IND', 'Rajasthan');
INSERT INTO `databand_city` VALUES ('1059', 'Nashik (Nasik)', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1060', 'Hubli-Dharwad', 'IND', 'Karnataka');
INSERT INTO `databand_city` VALUES ('1061', 'Solapur (Sholapur)', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1062', 'Ranchi', 'IND', 'Jharkhand');
INSERT INTO `databand_city` VALUES ('1063', 'Bareilly', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1064', 'Guwahati (Gauhati)', 'IND', 'Assam');
INSERT INTO `databand_city` VALUES ('1065', 'Shambajinagar (Aurangabad)', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1066', 'Cochin (Kochi)', 'IND', 'Kerala');
INSERT INTO `databand_city` VALUES ('1067', 'Rajkot', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1068', 'Kota', 'IND', 'Rajasthan');
INSERT INTO `databand_city` VALUES ('1069', 'Thiruvananthapuram (Trivandrum', 'IND', 'Kerala');
INSERT INTO `databand_city` VALUES ('1070', 'Pimpri-Chinchwad', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1071', 'Jalandhar (Jullundur)', 'IND', 'Punjab');
INSERT INTO `databand_city` VALUES ('1072', 'Gorakhpur', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1073', 'Chandigarh', 'IND', 'Chandigarh');
INSERT INTO `databand_city` VALUES ('1074', 'Mysore', 'IND', 'Karnataka');
INSERT INTO `databand_city` VALUES ('1075', 'Aligarh', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1076', 'Guntur', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1077', 'Jamshedpur', 'IND', 'Jharkhand');
INSERT INTO `databand_city` VALUES ('1078', 'Ghaziabad', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1079', 'Warangal', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1080', 'Raipur', 'IND', 'Chhatisgarh');
INSERT INTO `databand_city` VALUES ('1081', 'Moradabad', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1082', 'Durgapur', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1083', 'Amravati', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1084', 'Calicut (Kozhikode)', 'IND', 'Kerala');
INSERT INTO `databand_city` VALUES ('1085', 'Bikaner', 'IND', 'Rajasthan');
INSERT INTO `databand_city` VALUES ('1086', 'Bhubaneswar', 'IND', 'Orissa');
INSERT INTO `databand_city` VALUES ('1087', 'Kolhapur', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1088', 'Kataka (Cuttack)', 'IND', 'Orissa');
INSERT INTO `databand_city` VALUES ('1089', 'Ajmer', 'IND', 'Rajasthan');
INSERT INTO `databand_city` VALUES ('1090', 'Bhavnagar', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1091', 'Tiruchirapalli', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1092', 'Bhilai', 'IND', 'Chhatisgarh');
INSERT INTO `databand_city` VALUES ('1093', 'Bhiwandi', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1094', 'Saharanpur', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1095', 'Ulhasnagar', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1096', 'Salem', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1097', 'Ujjain', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1098', 'Malegaon', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1099', 'Jamnagar', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1100', 'Bokaro Steel City', 'IND', 'Jharkhand');
INSERT INTO `databand_city` VALUES ('1101', 'Akola', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1102', 'Belgaum', 'IND', 'Karnataka');
INSERT INTO `databand_city` VALUES ('1103', 'Rajahmundry', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1104', 'Nellore', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1105', 'Udaipur', 'IND', 'Rajasthan');
INSERT INTO `databand_city` VALUES ('1106', 'New Bombay', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1107', 'Bhatpara', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1108', 'Gulbarga', 'IND', 'Karnataka');
INSERT INTO `databand_city` VALUES ('1109', 'New Delhi', 'IND', 'Delhi');
INSERT INTO `databand_city` VALUES ('1110', 'Jhansi', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1111', 'Gaya', 'IND', 'Bihar');
INSERT INTO `databand_city` VALUES ('1112', 'Kakinada', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1113', 'Dhule (Dhulia)', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1114', 'Panihati', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1115', 'Nanded (Nander)', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1116', 'Mangalore', 'IND', 'Karnataka');
INSERT INTO `databand_city` VALUES ('1117', 'Dehra Dun', 'IND', 'Uttaranchal');
INSERT INTO `databand_city` VALUES ('1118', 'Kamarhati', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1119', 'Davangere', 'IND', 'Karnataka');
INSERT INTO `databand_city` VALUES ('1120', 'Asansol', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1121', 'Bhagalpur', 'IND', 'Bihar');
INSERT INTO `databand_city` VALUES ('1122', 'Bellary', 'IND', 'Karnataka');
INSERT INTO `databand_city` VALUES ('1123', 'Barddhaman (Burdwan)', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1124', 'Rampur', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1125', 'Jalgaon', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1126', 'Muzaffarpur', 'IND', 'Bihar');
INSERT INTO `databand_city` VALUES ('1127', 'Nizamabad', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1128', 'Muzaffarnagar', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1129', 'Patiala', 'IND', 'Punjab');
INSERT INTO `databand_city` VALUES ('1130', 'Shahjahanpur', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1131', 'Kurnool', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1132', 'Tiruppur (Tirupper)', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1133', 'Rohtak', 'IND', 'Haryana');
INSERT INTO `databand_city` VALUES ('1134', 'South Dum Dum', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1135', 'Mathura', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1136', 'Chandrapur', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1137', 'Barahanagar (Baranagar)', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1138', 'Darbhanga', 'IND', 'Bihar');
INSERT INTO `databand_city` VALUES ('1139', 'Siliguri (Shiliguri)', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1140', 'Raurkela', 'IND', 'Orissa');
INSERT INTO `databand_city` VALUES ('1141', 'Ambattur', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1142', 'Panipat', 'IND', 'Haryana');
INSERT INTO `databand_city` VALUES ('1143', 'Firozabad', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1144', 'Ichalkaranji', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1145', 'Jammu', 'IND', 'Jammu and Kashmir');
INSERT INTO `databand_city` VALUES ('1146', 'Ramagundam', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1147', 'Eluru', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1148', 'Brahmapur', 'IND', 'Orissa');
INSERT INTO `databand_city` VALUES ('1149', 'Alwar', 'IND', 'Rajasthan');
INSERT INTO `databand_city` VALUES ('1150', 'Pondicherry', 'IND', 'Pondicherry');
INSERT INTO `databand_city` VALUES ('1151', 'Thanjavur', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1152', 'Bihar Sharif', 'IND', 'Bihar');
INSERT INTO `databand_city` VALUES ('1153', 'Tuticorin', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1154', 'Imphal', 'IND', 'Manipur');
INSERT INTO `databand_city` VALUES ('1155', 'Latur', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1156', 'Sagar', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1157', 'Farrukhabad-cum-Fatehgarh', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1158', 'Sangli', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1159', 'Parbhani', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1160', 'Nagar Coil', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1161', 'Bijapur', 'IND', 'Karnataka');
INSERT INTO `databand_city` VALUES ('1162', 'Kukatpalle', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1163', 'Bally', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1164', 'Bhilwara', 'IND', 'Rajasthan');
INSERT INTO `databand_city` VALUES ('1165', 'Ratlam', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1166', 'Avadi', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1167', 'Dindigul', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1168', 'Ahmadnagar', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1169', 'Bilaspur', 'IND', 'Chhatisgarh');
INSERT INTO `databand_city` VALUES ('1170', 'Shimoga', 'IND', 'Karnataka');
INSERT INTO `databand_city` VALUES ('1171', 'Kharagpur', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1172', 'Mira Bhayandar', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1173', 'Vellore', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1174', 'Jalna', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1175', 'Burnpur', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1176', 'Anantapur', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1177', 'Allappuzha (Alleppey)', 'IND', 'Kerala');
INSERT INTO `databand_city` VALUES ('1178', 'Tirupati', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1179', 'Karnal', 'IND', 'Haryana');
INSERT INTO `databand_city` VALUES ('1180', 'Burhanpur', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1181', 'Hisar (Hissar)', 'IND', 'Haryana');
INSERT INTO `databand_city` VALUES ('1182', 'Tiruvottiyur', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1183', 'Mirzapur-cum-Vindhyachal', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1184', 'Secunderabad', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1185', 'Nadiad', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1186', 'Dewas', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1187', 'Murwara (Katni)', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1188', 'Ganganagar', 'IND', 'Rajasthan');
INSERT INTO `databand_city` VALUES ('1189', 'Vizianagaram', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1190', 'Erode', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1191', 'Machilipatnam (Masulipatam)', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1192', 'Bhatinda (Bathinda)', 'IND', 'Punjab');
INSERT INTO `databand_city` VALUES ('1193', 'Raichur', 'IND', 'Karnataka');
INSERT INTO `databand_city` VALUES ('1194', 'Agartala', 'IND', 'Tripura');
INSERT INTO `databand_city` VALUES ('1195', 'Arrah (Ara)', 'IND', 'Bihar');
INSERT INTO `databand_city` VALUES ('1196', 'Satna', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1197', 'Lalbahadur Nagar', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1198', 'Aizawl', 'IND', 'Mizoram');
INSERT INTO `databand_city` VALUES ('1199', 'Uluberia', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1200', 'Katihar', 'IND', 'Bihar');
INSERT INTO `databand_city` VALUES ('1201', 'Cuddalore', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1202', 'Hugli-Chinsurah', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1203', 'Dhanbad', 'IND', 'Jharkhand');
INSERT INTO `databand_city` VALUES ('1204', 'Raiganj', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1205', 'Sambhal', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1206', 'Durg', 'IND', 'Chhatisgarh');
INSERT INTO `databand_city` VALUES ('1207', 'Munger (Monghyr)', 'IND', 'Bihar');
INSERT INTO `databand_city` VALUES ('1208', 'Kanchipuram', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1209', 'North Dum Dum', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1210', 'Karimnagar', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1211', 'Bharatpur', 'IND', 'Rajasthan');
INSERT INTO `databand_city` VALUES ('1212', 'Sikar', 'IND', 'Rajasthan');
INSERT INTO `databand_city` VALUES ('1213', 'Hardwar (Haridwar)', 'IND', 'Uttaranchal');
INSERT INTO `databand_city` VALUES ('1214', 'Dabgram', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1215', 'Morena', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1216', 'Noida', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1217', 'Hapur', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1218', 'Bhusawal', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1219', 'Khandwa', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1220', 'Yamuna Nagar', 'IND', 'Haryana');
INSERT INTO `databand_city` VALUES ('1221', 'Sonipat (Sonepat)', 'IND', 'Haryana');
INSERT INTO `databand_city` VALUES ('1222', 'Tenali', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1223', 'Raurkela Civil Township', 'IND', 'Orissa');
INSERT INTO `databand_city` VALUES ('1224', 'Kollam (Quilon)', 'IND', 'Kerala');
INSERT INTO `databand_city` VALUES ('1225', 'Kumbakonam', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1226', 'Ingraj Bazar (English Bazar)', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1227', 'Timkur', 'IND', 'Karnataka');
INSERT INTO `databand_city` VALUES ('1228', 'Amroha', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1229', 'Serampore', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1230', 'Chapra', 'IND', 'Bihar');
INSERT INTO `databand_city` VALUES ('1231', 'Pali', 'IND', 'Rajasthan');
INSERT INTO `databand_city` VALUES ('1232', 'Maunath Bhanjan', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1233', 'Adoni', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1234', 'Jaunpur', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1235', 'Tirunelveli', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1236', 'Bahraich', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1237', 'Gadag Betigeri', 'IND', 'Karnataka');
INSERT INTO `databand_city` VALUES ('1238', 'Proddatur', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1239', 'Chittoor', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1240', 'Barrackpur', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1241', 'Bharuch (Broach)', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1242', 'Naihati', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1243', 'Shillong', 'IND', 'Meghalaya');
INSERT INTO `databand_city` VALUES ('1244', 'Sambalpur', 'IND', 'Orissa');
INSERT INTO `databand_city` VALUES ('1245', 'Junagadh', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1246', 'Rae Bareli', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1247', 'Rewa', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1248', 'Gurgaon', 'IND', 'Haryana');
INSERT INTO `databand_city` VALUES ('1249', 'Khammam', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1250', 'Bulandshahr', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1251', 'Navsari', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1252', 'Malkajgiri', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1253', 'Midnapore (Medinipur)', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1254', 'Miraj', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1255', 'Raj Nandgaon', 'IND', 'Chhatisgarh');
INSERT INTO `databand_city` VALUES ('1256', 'Alandur', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1257', 'Puri', 'IND', 'Orissa');
INSERT INTO `databand_city` VALUES ('1258', 'Navadwip', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1259', 'Sirsa', 'IND', 'Haryana');
INSERT INTO `databand_city` VALUES ('1260', 'Korba', 'IND', 'Chhatisgarh');
INSERT INTO `databand_city` VALUES ('1261', 'Faizabad', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1262', 'Etawah', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1263', 'Pathankot', 'IND', 'Punjab');
INSERT INTO `databand_city` VALUES ('1264', 'Gandhinagar', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1265', 'Palghat (Palakkad)', 'IND', 'Kerala');
INSERT INTO `databand_city` VALUES ('1266', 'Veraval', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1267', 'Hoshiarpur', 'IND', 'Punjab');
INSERT INTO `databand_city` VALUES ('1268', 'Ambala', 'IND', 'Haryana');
INSERT INTO `databand_city` VALUES ('1269', 'Sitapur', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1270', 'Bhiwani', 'IND', 'Haryana');
INSERT INTO `databand_city` VALUES ('1271', 'Cuddapah', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1272', 'Bhimavaram', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1273', 'Krishnanagar', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1274', 'Chandannagar', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1275', 'Mandya', 'IND', 'Karnataka');
INSERT INTO `databand_city` VALUES ('1276', 'Dibrugarh', 'IND', 'Assam');
INSERT INTO `databand_city` VALUES ('1277', 'Nandyal', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1278', 'Balurghat', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1279', 'Neyveli', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1280', 'Fatehpur', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1281', 'Mahbubnagar', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1282', 'Budaun', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1283', 'Porbandar', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1284', 'Silchar', 'IND', 'Assam');
INSERT INTO `databand_city` VALUES ('1285', 'Berhampore (Baharampur)', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1286', 'Purnea (Purnia)', 'IND', 'Jharkhand');
INSERT INTO `databand_city` VALUES ('1287', 'Bankura', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1288', 'Rajapalaiyam', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1289', 'Titagarh', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1290', 'Halisahar', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1291', 'Hathras', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1292', 'Bhir (Bid)', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1293', 'Pallavaram', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1294', 'Anand', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1295', 'Mango', 'IND', 'Jharkhand');
INSERT INTO `databand_city` VALUES ('1296', 'Santipur', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1297', 'Bhind', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1298', 'Gondiya', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1299', 'Tiruvannamalai', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1300', 'Yeotmal (Yavatmal)', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1301', 'Kulti-Barakar', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1302', 'Moga', 'IND', 'Punjab');
INSERT INTO `databand_city` VALUES ('1303', 'Shivapuri', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1304', 'Bidar', 'IND', 'Karnataka');
INSERT INTO `databand_city` VALUES ('1305', 'Guntakal', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1306', 'Unnao', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1307', 'Barasat', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1308', 'Tambaram', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1309', 'Abohar', 'IND', 'Punjab');
INSERT INTO `databand_city` VALUES ('1310', 'Pilibhit', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1311', 'Valparai', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1312', 'Gonda', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1313', 'Surendranagar', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1314', 'Qutubullapur', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1315', 'Beawar', 'IND', 'Rajasthan');
INSERT INTO `databand_city` VALUES ('1316', 'Hindupur', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1317', 'Gandhidham', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1318', 'Haldwani-cum-Kathgodam', 'IND', 'Uttaranchal');
INSERT INTO `databand_city` VALUES ('1319', 'Tellicherry (Thalassery)', 'IND', 'Kerala');
INSERT INTO `databand_city` VALUES ('1320', 'Wardha', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1321', 'Rishra', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1322', 'Bhuj', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1323', 'Modinagar', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1324', 'Gudivada', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1325', 'Basirhat', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1326', 'Uttarpara-Kotrung', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1327', 'Ongole', 'IND', 'Andhra Pradesh');
INSERT INTO `databand_city` VALUES ('1328', 'North Barrackpur', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1329', 'Guna', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1330', 'Haldia', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1331', 'Habra', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1332', 'Kanchrapara', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1333', 'Tonk', 'IND', 'Rajasthan');
INSERT INTO `databand_city` VALUES ('1334', 'Champdani', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1335', 'Orai', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1336', 'Pudukkottai', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1337', 'Sasaram', 'IND', 'Bihar');
INSERT INTO `databand_city` VALUES ('1338', 'Hazaribag', 'IND', 'Jharkhand');
INSERT INTO `databand_city` VALUES ('1339', 'Palayankottai', 'IND', 'Tamil Nadu');
INSERT INTO `databand_city` VALUES ('1340', 'Banda', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1341', 'Godhra', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1342', 'Hospet', 'IND', 'Karnataka');
INSERT INTO `databand_city` VALUES ('1343', 'Ashoknagar-Kalyangarh', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1344', 'Achalpur', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1345', 'Patan', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1346', 'Mandasor', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1347', 'Damoh', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1348', 'Satara', 'IND', 'Maharashtra');
INSERT INTO `databand_city` VALUES ('1349', 'Meerut Cantonment', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1350', 'Dehri', 'IND', 'Bihar');
INSERT INTO `databand_city` VALUES ('1351', 'Delhi Cantonment', 'IND', 'Delhi');
INSERT INTO `databand_city` VALUES ('1352', 'Chhindwara', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1353', 'Bansberia', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1354', 'Nagaon', 'IND', 'Assam');
INSERT INTO `databand_city` VALUES ('1355', 'Kanpur Cantonment', 'IND', 'Uttar Pradesh');
INSERT INTO `databand_city` VALUES ('1356', 'Vidisha', 'IND', 'Madhya Pradesh');
INSERT INTO `databand_city` VALUES ('1357', 'Bettiah', 'IND', 'Bihar');
INSERT INTO `databand_city` VALUES ('1358', 'Purulia', 'IND', 'Jharkhand');
INSERT INTO `databand_city` VALUES ('1359', 'Hassan', 'IND', 'Karnataka');
INSERT INTO `databand_city` VALUES ('1360', 'Ambala Sadar', 'IND', 'Haryana');
INSERT INTO `databand_city` VALUES ('1361', 'Baidyabati', 'IND', 'West Bengali');
INSERT INTO `databand_city` VALUES ('1362', 'Morvi', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1363', 'Raigarh', 'IND', 'Chhatisgarh');
INSERT INTO `databand_city` VALUES ('1364', 'Vejalpur', 'IND', 'Gujarat');
INSERT INTO `databand_city` VALUES ('1365', 'Baghdad', 'IRQ', 'Baghdad');
INSERT INTO `databand_city` VALUES ('1366', 'Mosul', 'IRQ', 'Ninawa');
INSERT INTO `databand_city` VALUES ('1367', 'Irbil', 'IRQ', 'Irbil');
INSERT INTO `databand_city` VALUES ('1368', 'Kirkuk', 'IRQ', 'al-Tamim');
INSERT INTO `databand_city` VALUES ('1369', 'Basra', 'IRQ', 'Basra');
INSERT INTO `databand_city` VALUES ('1370', 'al-Sulaymaniya', 'IRQ', 'al-Sulaymaniya');
INSERT INTO `databand_city` VALUES ('1371', 'al-Najaf', 'IRQ', 'al-Najaf');
INSERT INTO `databand_city` VALUES ('1372', 'Karbala', 'IRQ', 'Karbala');
INSERT INTO `databand_city` VALUES ('1373', 'al-Hilla', 'IRQ', 'Babil');
INSERT INTO `databand_city` VALUES ('1374', 'al-Nasiriya', 'IRQ', 'DhiQar');
INSERT INTO `databand_city` VALUES ('1375', 'al-Amara', 'IRQ', 'Maysan');
INSERT INTO `databand_city` VALUES ('1376', 'al-Diwaniya', 'IRQ', 'al-Qadisiya');
INSERT INTO `databand_city` VALUES ('1377', 'al-Ramadi', 'IRQ', 'al-Anbar');
INSERT INTO `databand_city` VALUES ('1378', 'al-Kut', 'IRQ', 'Wasit');
INSERT INTO `databand_city` VALUES ('1379', 'Baquba', 'IRQ', 'Diyala');
INSERT INTO `databand_city` VALUES ('1380', 'Teheran', 'IRN', 'Teheran');
INSERT INTO `databand_city` VALUES ('1381', 'Mashhad', 'IRN', 'Khorasan');
INSERT INTO `databand_city` VALUES ('1382', 'Esfahan', 'IRN', 'Esfahan');
INSERT INTO `databand_city` VALUES ('1383', 'Tabriz', 'IRN', 'East Azerbaidzan');
INSERT INTO `databand_city` VALUES ('1384', 'Shiraz', 'IRN', 'Fars');
INSERT INTO `databand_city` VALUES ('1385', 'Karaj', 'IRN', 'Teheran');
INSERT INTO `databand_city` VALUES ('1386', 'Ahvaz', 'IRN', 'Khuzestan');
INSERT INTO `databand_city` VALUES ('1387', 'Qom', 'IRN', 'Qom');
INSERT INTO `databand_city` VALUES ('1388', 'Kermanshah', 'IRN', 'Kermanshah');
INSERT INTO `databand_city` VALUES ('1389', 'Urmia', 'IRN', 'West Azerbaidzan');
INSERT INTO `databand_city` VALUES ('1390', 'Zahedan', 'IRN', 'Sistan va Baluchesta');
INSERT INTO `databand_city` VALUES ('1391', 'Rasht', 'IRN', 'Gilan');
INSERT INTO `databand_city` VALUES ('1392', 'Hamadan', 'IRN', 'Hamadan');
INSERT INTO `databand_city` VALUES ('1393', 'Kerman', 'IRN', 'Kerman');
INSERT INTO `databand_city` VALUES ('1394', 'Arak', 'IRN', 'Markazi');
INSERT INTO `databand_city` VALUES ('1395', 'Ardebil', 'IRN', 'Ardebil');
INSERT INTO `databand_city` VALUES ('1396', 'Yazd', 'IRN', 'Yazd');
INSERT INTO `databand_city` VALUES ('1397', 'Qazvin', 'IRN', 'Qazvin');
INSERT INTO `databand_city` VALUES ('1398', 'Zanjan', 'IRN', 'Zanjan');
INSERT INTO `databand_city` VALUES ('1399', 'Sanandaj', 'IRN', 'Kordestan');
INSERT INTO `databand_city` VALUES ('1400', 'Bandar-e-Abbas', 'IRN', 'Hormozgan');
INSERT INTO `databand_city` VALUES ('1401', 'Khorramabad', 'IRN', 'Lorestan');
INSERT INTO `databand_city` VALUES ('1402', 'Eslamshahr', 'IRN', 'Teheran');
INSERT INTO `databand_city` VALUES ('1403', 'Borujerd', 'IRN', 'Lorestan');
INSERT INTO `databand_city` VALUES ('1404', 'Abadan', 'IRN', 'Khuzestan');
INSERT INTO `databand_city` VALUES ('1405', 'Dezful', 'IRN', 'Khuzestan');
INSERT INTO `databand_city` VALUES ('1406', 'Kashan', 'IRN', 'Esfahan');
INSERT INTO `databand_city` VALUES ('1407', 'Sari', 'IRN', 'Mazandaran');
INSERT INTO `databand_city` VALUES ('1408', 'Gorgan', 'IRN', 'Golestan');
INSERT INTO `databand_city` VALUES ('1409', 'Najafabad', 'IRN', 'Esfahan');
INSERT INTO `databand_city` VALUES ('1410', 'Sabzevar', 'IRN', 'Khorasan');
INSERT INTO `databand_city` VALUES ('1411', 'Khomeynishahr', 'IRN', 'Esfahan');
INSERT INTO `databand_city` VALUES ('1412', 'Amol', 'IRN', 'Mazandaran');
INSERT INTO `databand_city` VALUES ('1413', 'Neyshabur', 'IRN', 'Khorasan');
INSERT INTO `databand_city` VALUES ('1414', 'Babol', 'IRN', 'Mazandaran');
INSERT INTO `databand_city` VALUES ('1415', 'Khoy', 'IRN', 'West Azerbaidzan');
INSERT INTO `databand_city` VALUES ('1416', 'Malayer', 'IRN', 'Hamadan');
INSERT INTO `databand_city` VALUES ('1417', 'Bushehr', 'IRN', 'Bushehr');
INSERT INTO `databand_city` VALUES ('1418', 'Qaemshahr', 'IRN', 'Mazandaran');
INSERT INTO `databand_city` VALUES ('1419', 'Qarchak', 'IRN', 'Teheran');
INSERT INTO `databand_city` VALUES ('1420', 'Qods', 'IRN', 'Teheran');
INSERT INTO `databand_city` VALUES ('1421', 'Sirjan', 'IRN', 'Kerman');
INSERT INTO `databand_city` VALUES ('1422', 'Bojnurd', 'IRN', 'Khorasan');
INSERT INTO `databand_city` VALUES ('1423', 'Maragheh', 'IRN', 'East Azerbaidzan');
INSERT INTO `databand_city` VALUES ('1424', 'Birjand', 'IRN', 'Khorasan');
INSERT INTO `databand_city` VALUES ('1425', 'Ilam', 'IRN', 'Ilam');
INSERT INTO `databand_city` VALUES ('1426', 'Bukan', 'IRN', 'West Azerbaidzan');
INSERT INTO `databand_city` VALUES ('1427', 'Masjed-e-Soleyman', 'IRN', 'Khuzestan');
INSERT INTO `databand_city` VALUES ('1428', 'Saqqez', 'IRN', 'Kordestan');
INSERT INTO `databand_city` VALUES ('1429', 'Gonbad-e Qabus', 'IRN', 'Mazandaran');
INSERT INTO `databand_city` VALUES ('1430', 'Saveh', 'IRN', 'Qom');
INSERT INTO `databand_city` VALUES ('1431', 'Mahabad', 'IRN', 'West Azerbaidzan');
INSERT INTO `databand_city` VALUES ('1432', 'Varamin', 'IRN', 'Teheran');
INSERT INTO `databand_city` VALUES ('1433', 'Andimeshk', 'IRN', 'Khuzestan');
INSERT INTO `databand_city` VALUES ('1434', 'Khorramshahr', 'IRN', 'Khuzestan');
INSERT INTO `databand_city` VALUES ('1435', 'Shahrud', 'IRN', 'Semnan');
INSERT INTO `databand_city` VALUES ('1436', 'Marv Dasht', 'IRN', 'Fars');
INSERT INTO `databand_city` VALUES ('1437', 'Zabol', 'IRN', 'Sistan va Baluchesta');
INSERT INTO `databand_city` VALUES ('1438', 'Shahr-e Kord', 'IRN', 'Chaharmahal va Bakht');
INSERT INTO `databand_city` VALUES ('1439', 'Bandar-e Anzali', 'IRN', 'Gilan');
INSERT INTO `databand_city` VALUES ('1440', 'Rafsanjan', 'IRN', 'Kerman');
INSERT INTO `databand_city` VALUES ('1441', 'Marand', 'IRN', 'East Azerbaidzan');
INSERT INTO `databand_city` VALUES ('1442', 'Torbat-e Heydariyeh', 'IRN', 'Khorasan');
INSERT INTO `databand_city` VALUES ('1443', 'Jahrom', 'IRN', 'Fars');
INSERT INTO `databand_city` VALUES ('1444', 'Semnan', 'IRN', 'Semnan');
INSERT INTO `databand_city` VALUES ('1445', 'Miandoab', 'IRN', 'West Azerbaidzan');
INSERT INTO `databand_city` VALUES ('1446', 'Qomsheh', 'IRN', 'Esfahan');
INSERT INTO `databand_city` VALUES ('1447', 'Dublin', 'IRL', 'Leinster');
INSERT INTO `databand_city` VALUES ('1448', 'Cork', 'IRL', 'Munster');
INSERT INTO `databand_city` VALUES ('1449', 'Reykjavík', 'ISL', 'Höfuðborgarsvæði');
INSERT INTO `databand_city` VALUES ('1450', 'Jerusalem', 'ISR', 'Jerusalem');
INSERT INTO `databand_city` VALUES ('1451', 'Tel Aviv-Jaffa', 'ISR', 'Tel Aviv');
INSERT INTO `databand_city` VALUES ('1452', 'Haifa', 'ISR', 'Haifa');
INSERT INTO `databand_city` VALUES ('1453', 'Rishon Le Ziyyon', 'ISR', 'Ha Merkaz');
INSERT INTO `databand_city` VALUES ('1454', 'Beerseba', 'ISR', 'Ha Darom');
INSERT INTO `databand_city` VALUES ('1455', 'Holon', 'ISR', 'Tel Aviv');
INSERT INTO `databand_city` VALUES ('1456', 'Petah Tiqwa', 'ISR', 'Ha Merkaz');
INSERT INTO `databand_city` VALUES ('1457', 'Ashdod', 'ISR', 'Ha Darom');
INSERT INTO `databand_city` VALUES ('1458', 'Netanya', 'ISR', 'Ha Merkaz');
INSERT INTO `databand_city` VALUES ('1459', 'Bat Yam', 'ISR', 'Tel Aviv');
INSERT INTO `databand_city` VALUES ('1460', 'Bene Beraq', 'ISR', 'Tel Aviv');
INSERT INTO `databand_city` VALUES ('1461', 'Ramat Gan', 'ISR', 'Tel Aviv');
INSERT INTO `databand_city` VALUES ('1462', 'Ashqelon', 'ISR', 'Ha Darom');
INSERT INTO `databand_city` VALUES ('1463', 'Rehovot', 'ISR', 'Ha Merkaz');
INSERT INTO `databand_city` VALUES ('1464', 'Roma', 'ITA', 'Latium');
INSERT INTO `databand_city` VALUES ('1465', 'Milano', 'ITA', 'Lombardia');
INSERT INTO `databand_city` VALUES ('1466', 'Napoli', 'ITA', 'Campania');
INSERT INTO `databand_city` VALUES ('1467', 'Torino', 'ITA', 'Piemonte');
INSERT INTO `databand_city` VALUES ('1468', 'Palermo', 'ITA', 'Sisilia');
INSERT INTO `databand_city` VALUES ('1469', 'Genova', 'ITA', 'Liguria');
INSERT INTO `databand_city` VALUES ('1470', 'Bologna', 'ITA', 'Emilia-Romagna');
INSERT INTO `databand_city` VALUES ('1471', 'Firenze', 'ITA', 'Toscana');
INSERT INTO `databand_city` VALUES ('1472', 'Catania', 'ITA', 'Sisilia');
INSERT INTO `databand_city` VALUES ('1473', 'Bari', 'ITA', 'Apulia');
INSERT INTO `databand_city` VALUES ('1474', 'Venezia', 'ITA', 'Veneto');
INSERT INTO `databand_city` VALUES ('1475', 'Messina', 'ITA', 'Sisilia');
INSERT INTO `databand_city` VALUES ('1476', 'Verona', 'ITA', 'Veneto');
INSERT INTO `databand_city` VALUES ('1477', 'Trieste', 'ITA', 'Friuli-Venezia Giuli');
INSERT INTO `databand_city` VALUES ('1478', 'Padova', 'ITA', 'Veneto');
INSERT INTO `databand_city` VALUES ('1479', 'Taranto', 'ITA', 'Apulia');
INSERT INTO `databand_city` VALUES ('1480', 'Brescia', 'ITA', 'Lombardia');
INSERT INTO `databand_city` VALUES ('1481', 'Reggio di Calabria', 'ITA', 'Calabria');
INSERT INTO `databand_city` VALUES ('1482', 'Modena', 'ITA', 'Emilia-Romagna');
INSERT INTO `databand_city` VALUES ('1483', 'Prato', 'ITA', 'Toscana');
INSERT INTO `databand_city` VALUES ('1484', 'Parma', 'ITA', 'Emilia-Romagna');
INSERT INTO `databand_city` VALUES ('1485', 'Cagliari', 'ITA', 'Sardinia');
INSERT INTO `databand_city` VALUES ('1486', 'Livorno', 'ITA', 'Toscana');
INSERT INTO `databand_city` VALUES ('1487', 'Perugia', 'ITA', 'Umbria');
INSERT INTO `databand_city` VALUES ('1488', 'Foggia', 'ITA', 'Apulia');
INSERT INTO `databand_city` VALUES ('1489', 'Reggio nell´ Emilia', 'ITA', 'Emilia-Romagna');
INSERT INTO `databand_city` VALUES ('1490', 'Salerno', 'ITA', 'Campania');
INSERT INTO `databand_city` VALUES ('1491', 'Ravenna', 'ITA', 'Emilia-Romagna');
INSERT INTO `databand_city` VALUES ('1492', 'Ferrara', 'ITA', 'Emilia-Romagna');
INSERT INTO `databand_city` VALUES ('1493', 'Rimini', 'ITA', 'Emilia-Romagna');
INSERT INTO `databand_city` VALUES ('1494', 'Syrakusa', 'ITA', 'Sisilia');
INSERT INTO `databand_city` VALUES ('1495', 'Sassari', 'ITA', 'Sardinia');
INSERT INTO `databand_city` VALUES ('1496', 'Monza', 'ITA', 'Lombardia');
INSERT INTO `databand_city` VALUES ('1497', 'Bergamo', 'ITA', 'Lombardia');
INSERT INTO `databand_city` VALUES ('1498', 'Pescara', 'ITA', 'Abruzzit');
INSERT INTO `databand_city` VALUES ('1499', 'Latina', 'ITA', 'Latium');
INSERT INTO `databand_city` VALUES ('1500', 'Vicenza', 'ITA', 'Veneto');
INSERT INTO `databand_city` VALUES ('1501', 'Terni', 'ITA', 'Umbria');
INSERT INTO `databand_city` VALUES ('1502', 'Forlì', 'ITA', 'Emilia-Romagna');
INSERT INTO `databand_city` VALUES ('1503', 'Trento', 'ITA', 'Trentino-Alto Adige');
INSERT INTO `databand_city` VALUES ('1504', 'Novara', 'ITA', 'Piemonte');
INSERT INTO `databand_city` VALUES ('1505', 'Piacenza', 'ITA', 'Emilia-Romagna');
INSERT INTO `databand_city` VALUES ('1506', 'Ancona', 'ITA', 'Marche');
INSERT INTO `databand_city` VALUES ('1507', 'Lecce', 'ITA', 'Apulia');
INSERT INTO `databand_city` VALUES ('1508', 'Bolzano', 'ITA', 'Trentino-Alto Adige');
INSERT INTO `databand_city` VALUES ('1509', 'Catanzaro', 'ITA', 'Calabria');
INSERT INTO `databand_city` VALUES ('1510', 'La Spezia', 'ITA', 'Liguria');
INSERT INTO `databand_city` VALUES ('1511', 'Udine', 'ITA', 'Friuli-Venezia Giuli');
INSERT INTO `databand_city` VALUES ('1512', 'Torre del Greco', 'ITA', 'Campania');
INSERT INTO `databand_city` VALUES ('1513', 'Andria', 'ITA', 'Apulia');
INSERT INTO `databand_city` VALUES ('1514', 'Brindisi', 'ITA', 'Apulia');
INSERT INTO `databand_city` VALUES ('1515', 'Giugliano in Campania', 'ITA', 'Campania');
INSERT INTO `databand_city` VALUES ('1516', 'Pisa', 'ITA', 'Toscana');
INSERT INTO `databand_city` VALUES ('1517', 'Barletta', 'ITA', 'Apulia');
INSERT INTO `databand_city` VALUES ('1518', 'Arezzo', 'ITA', 'Toscana');
INSERT INTO `databand_city` VALUES ('1519', 'Alessandria', 'ITA', 'Piemonte');
INSERT INTO `databand_city` VALUES ('1520', 'Cesena', 'ITA', 'Emilia-Romagna');
INSERT INTO `databand_city` VALUES ('1521', 'Pesaro', 'ITA', 'Marche');
INSERT INTO `databand_city` VALUES ('1522', 'Dili', 'TMP', 'Dili');
INSERT INTO `databand_city` VALUES ('1523', 'Wien', 'AUT', 'Wien');
INSERT INTO `databand_city` VALUES ('1524', 'Graz', 'AUT', 'Steiermark');
INSERT INTO `databand_city` VALUES ('1525', 'Linz', 'AUT', 'North Austria');
INSERT INTO `databand_city` VALUES ('1526', 'Salzburg', 'AUT', 'Salzburg');
INSERT INTO `databand_city` VALUES ('1527', 'Innsbruck', 'AUT', 'Tiroli');
INSERT INTO `databand_city` VALUES ('1528', 'Klagenfurt', 'AUT', 'Kärnten');
INSERT INTO `databand_city` VALUES ('1529', 'Spanish Town', 'JAM', 'St. Catherine');
INSERT INTO `databand_city` VALUES ('1530', 'Kingston', 'JAM', 'St. Andrew');
INSERT INTO `databand_city` VALUES ('1531', 'Portmore', 'JAM', 'St. Andrew');
INSERT INTO `databand_city` VALUES ('1532', 'Tokyo', 'JPN', 'Tokyo-to');
INSERT INTO `databand_city` VALUES ('1533', 'Jokohama [Yokohama]', 'JPN', 'Kanagawa');
INSERT INTO `databand_city` VALUES ('1534', 'Osaka', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1535', 'Nagoya', 'JPN', 'Aichi');
INSERT INTO `databand_city` VALUES ('1536', 'Sapporo', 'JPN', 'Hokkaido');
INSERT INTO `databand_city` VALUES ('1537', 'Kioto', 'JPN', 'Kyoto');
INSERT INTO `databand_city` VALUES ('1538', 'Kobe', 'JPN', 'Hyogo');
INSERT INTO `databand_city` VALUES ('1539', 'Fukuoka', 'JPN', 'Fukuoka');
INSERT INTO `databand_city` VALUES ('1540', 'Kawasaki', 'JPN', 'Kanagawa');
INSERT INTO `databand_city` VALUES ('1541', 'Hiroshima', 'JPN', 'Hiroshima');
INSERT INTO `databand_city` VALUES ('1542', 'Kitakyushu', 'JPN', 'Fukuoka');
INSERT INTO `databand_city` VALUES ('1543', 'Sendai', 'JPN', 'Miyagi');
INSERT INTO `databand_city` VALUES ('1544', 'Chiba', 'JPN', 'Chiba');
INSERT INTO `databand_city` VALUES ('1545', 'Sakai', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1546', 'Kumamoto', 'JPN', 'Kumamoto');
INSERT INTO `databand_city` VALUES ('1547', 'Okayama', 'JPN', 'Okayama');
INSERT INTO `databand_city` VALUES ('1548', 'Sagamihara', 'JPN', 'Kanagawa');
INSERT INTO `databand_city` VALUES ('1549', 'Hamamatsu', 'JPN', 'Shizuoka');
INSERT INTO `databand_city` VALUES ('1550', 'Kagoshima', 'JPN', 'Kagoshima');
INSERT INTO `databand_city` VALUES ('1551', 'Funabashi', 'JPN', 'Chiba');
INSERT INTO `databand_city` VALUES ('1552', 'Higashiosaka', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1553', 'Hachioji', 'JPN', 'Tokyo-to');
INSERT INTO `databand_city` VALUES ('1554', 'Niigata', 'JPN', 'Niigata');
INSERT INTO `databand_city` VALUES ('1555', 'Amagasaki', 'JPN', 'Hyogo');
INSERT INTO `databand_city` VALUES ('1556', 'Himeji', 'JPN', 'Hyogo');
INSERT INTO `databand_city` VALUES ('1557', 'Shizuoka', 'JPN', 'Shizuoka');
INSERT INTO `databand_city` VALUES ('1558', 'Urawa', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1559', 'Matsuyama', 'JPN', 'Ehime');
INSERT INTO `databand_city` VALUES ('1560', 'Matsudo', 'JPN', 'Chiba');
INSERT INTO `databand_city` VALUES ('1561', 'Kanazawa', 'JPN', 'Ishikawa');
INSERT INTO `databand_city` VALUES ('1562', 'Kawaguchi', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1563', 'Ichikawa', 'JPN', 'Chiba');
INSERT INTO `databand_city` VALUES ('1564', 'Omiya', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1565', 'Utsunomiya', 'JPN', 'Tochigi');
INSERT INTO `databand_city` VALUES ('1566', 'Oita', 'JPN', 'Oita');
INSERT INTO `databand_city` VALUES ('1567', 'Nagasaki', 'JPN', 'Nagasaki');
INSERT INTO `databand_city` VALUES ('1568', 'Yokosuka', 'JPN', 'Kanagawa');
INSERT INTO `databand_city` VALUES ('1569', 'Kurashiki', 'JPN', 'Okayama');
INSERT INTO `databand_city` VALUES ('1570', 'Gifu', 'JPN', 'Gifu');
INSERT INTO `databand_city` VALUES ('1571', 'Hirakata', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1572', 'Nishinomiya', 'JPN', 'Hyogo');
INSERT INTO `databand_city` VALUES ('1573', 'Toyonaka', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1574', 'Wakayama', 'JPN', 'Wakayama');
INSERT INTO `databand_city` VALUES ('1575', 'Fukuyama', 'JPN', 'Hiroshima');
INSERT INTO `databand_city` VALUES ('1576', 'Fujisawa', 'JPN', 'Kanagawa');
INSERT INTO `databand_city` VALUES ('1577', 'Asahikawa', 'JPN', 'Hokkaido');
INSERT INTO `databand_city` VALUES ('1578', 'Machida', 'JPN', 'Tokyo-to');
INSERT INTO `databand_city` VALUES ('1579', 'Nara', 'JPN', 'Nara');
INSERT INTO `databand_city` VALUES ('1580', 'Takatsuki', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1581', 'Iwaki', 'JPN', 'Fukushima');
INSERT INTO `databand_city` VALUES ('1582', 'Nagano', 'JPN', 'Nagano');
INSERT INTO `databand_city` VALUES ('1583', 'Toyohashi', 'JPN', 'Aichi');
INSERT INTO `databand_city` VALUES ('1584', 'Toyota', 'JPN', 'Aichi');
INSERT INTO `databand_city` VALUES ('1585', 'Suita', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1586', 'Takamatsu', 'JPN', 'Kagawa');
INSERT INTO `databand_city` VALUES ('1587', 'Koriyama', 'JPN', 'Fukushima');
INSERT INTO `databand_city` VALUES ('1588', 'Okazaki', 'JPN', 'Aichi');
INSERT INTO `databand_city` VALUES ('1589', 'Kawagoe', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1590', 'Tokorozawa', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1591', 'Toyama', 'JPN', 'Toyama');
INSERT INTO `databand_city` VALUES ('1592', 'Kochi', 'JPN', 'Kochi');
INSERT INTO `databand_city` VALUES ('1593', 'Kashiwa', 'JPN', 'Chiba');
INSERT INTO `databand_city` VALUES ('1594', 'Akita', 'JPN', 'Akita');
INSERT INTO `databand_city` VALUES ('1595', 'Miyazaki', 'JPN', 'Miyazaki');
INSERT INTO `databand_city` VALUES ('1596', 'Koshigaya', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1597', 'Naha', 'JPN', 'Okinawa');
INSERT INTO `databand_city` VALUES ('1598', 'Aomori', 'JPN', 'Aomori');
INSERT INTO `databand_city` VALUES ('1599', 'Hakodate', 'JPN', 'Hokkaido');
INSERT INTO `databand_city` VALUES ('1600', 'Akashi', 'JPN', 'Hyogo');
INSERT INTO `databand_city` VALUES ('1601', 'Yokkaichi', 'JPN', 'Mie');
INSERT INTO `databand_city` VALUES ('1602', 'Fukushima', 'JPN', 'Fukushima');
INSERT INTO `databand_city` VALUES ('1603', 'Morioka', 'JPN', 'Iwate');
INSERT INTO `databand_city` VALUES ('1604', 'Maebashi', 'JPN', 'Gumma');
INSERT INTO `databand_city` VALUES ('1605', 'Kasugai', 'JPN', 'Aichi');
INSERT INTO `databand_city` VALUES ('1606', 'Otsu', 'JPN', 'Shiga');
INSERT INTO `databand_city` VALUES ('1607', 'Ichihara', 'JPN', 'Chiba');
INSERT INTO `databand_city` VALUES ('1608', 'Yao', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1609', 'Ichinomiya', 'JPN', 'Aichi');
INSERT INTO `databand_city` VALUES ('1610', 'Tokushima', 'JPN', 'Tokushima');
INSERT INTO `databand_city` VALUES ('1611', 'Kakogawa', 'JPN', 'Hyogo');
INSERT INTO `databand_city` VALUES ('1612', 'Ibaraki', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1613', 'Neyagawa', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1614', 'Shimonoseki', 'JPN', 'Yamaguchi');
INSERT INTO `databand_city` VALUES ('1615', 'Yamagata', 'JPN', 'Yamagata');
INSERT INTO `databand_city` VALUES ('1616', 'Fukui', 'JPN', 'Fukui');
INSERT INTO `databand_city` VALUES ('1617', 'Hiratsuka', 'JPN', 'Kanagawa');
INSERT INTO `databand_city` VALUES ('1618', 'Mito', 'JPN', 'Ibaragi');
INSERT INTO `databand_city` VALUES ('1619', 'Sasebo', 'JPN', 'Nagasaki');
INSERT INTO `databand_city` VALUES ('1620', 'Hachinohe', 'JPN', 'Aomori');
INSERT INTO `databand_city` VALUES ('1621', 'Takasaki', 'JPN', 'Gumma');
INSERT INTO `databand_city` VALUES ('1622', 'Shimizu', 'JPN', 'Shizuoka');
INSERT INTO `databand_city` VALUES ('1623', 'Kurume', 'JPN', 'Fukuoka');
INSERT INTO `databand_city` VALUES ('1624', 'Fuji', 'JPN', 'Shizuoka');
INSERT INTO `databand_city` VALUES ('1625', 'Soka', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1626', 'Fuchu', 'JPN', 'Tokyo-to');
INSERT INTO `databand_city` VALUES ('1627', 'Chigasaki', 'JPN', 'Kanagawa');
INSERT INTO `databand_city` VALUES ('1628', 'Atsugi', 'JPN', 'Kanagawa');
INSERT INTO `databand_city` VALUES ('1629', 'Numazu', 'JPN', 'Shizuoka');
INSERT INTO `databand_city` VALUES ('1630', 'Ageo', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1631', 'Yamato', 'JPN', 'Kanagawa');
INSERT INTO `databand_city` VALUES ('1632', 'Matsumoto', 'JPN', 'Nagano');
INSERT INTO `databand_city` VALUES ('1633', 'Kure', 'JPN', 'Hiroshima');
INSERT INTO `databand_city` VALUES ('1634', 'Takarazuka', 'JPN', 'Hyogo');
INSERT INTO `databand_city` VALUES ('1635', 'Kasukabe', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1636', 'Chofu', 'JPN', 'Tokyo-to');
INSERT INTO `databand_city` VALUES ('1637', 'Odawara', 'JPN', 'Kanagawa');
INSERT INTO `databand_city` VALUES ('1638', 'Kofu', 'JPN', 'Yamanashi');
INSERT INTO `databand_city` VALUES ('1639', 'Kushiro', 'JPN', 'Hokkaido');
INSERT INTO `databand_city` VALUES ('1640', 'Kishiwada', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1641', 'Hitachi', 'JPN', 'Ibaragi');
INSERT INTO `databand_city` VALUES ('1642', 'Nagaoka', 'JPN', 'Niigata');
INSERT INTO `databand_city` VALUES ('1643', 'Itami', 'JPN', 'Hyogo');
INSERT INTO `databand_city` VALUES ('1644', 'Uji', 'JPN', 'Kyoto');
INSERT INTO `databand_city` VALUES ('1645', 'Suzuka', 'JPN', 'Mie');
INSERT INTO `databand_city` VALUES ('1646', 'Hirosaki', 'JPN', 'Aomori');
INSERT INTO `databand_city` VALUES ('1647', 'Ube', 'JPN', 'Yamaguchi');
INSERT INTO `databand_city` VALUES ('1648', 'Kodaira', 'JPN', 'Tokyo-to');
INSERT INTO `databand_city` VALUES ('1649', 'Takaoka', 'JPN', 'Toyama');
INSERT INTO `databand_city` VALUES ('1650', 'Obihiro', 'JPN', 'Hokkaido');
INSERT INTO `databand_city` VALUES ('1651', 'Tomakomai', 'JPN', 'Hokkaido');
INSERT INTO `databand_city` VALUES ('1652', 'Saga', 'JPN', 'Saga');
INSERT INTO `databand_city` VALUES ('1653', 'Sakura', 'JPN', 'Chiba');
INSERT INTO `databand_city` VALUES ('1654', 'Kamakura', 'JPN', 'Kanagawa');
INSERT INTO `databand_city` VALUES ('1655', 'Mitaka', 'JPN', 'Tokyo-to');
INSERT INTO `databand_city` VALUES ('1656', 'Izumi', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1657', 'Hino', 'JPN', 'Tokyo-to');
INSERT INTO `databand_city` VALUES ('1658', 'Hadano', 'JPN', 'Kanagawa');
INSERT INTO `databand_city` VALUES ('1659', 'Ashikaga', 'JPN', 'Tochigi');
INSERT INTO `databand_city` VALUES ('1660', 'Tsu', 'JPN', 'Mie');
INSERT INTO `databand_city` VALUES ('1661', 'Sayama', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1662', 'Yachiyo', 'JPN', 'Chiba');
INSERT INTO `databand_city` VALUES ('1663', 'Tsukuba', 'JPN', 'Ibaragi');
INSERT INTO `databand_city` VALUES ('1664', 'Tachikawa', 'JPN', 'Tokyo-to');
INSERT INTO `databand_city` VALUES ('1665', 'Kumagaya', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1666', 'Moriguchi', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1667', 'Otaru', 'JPN', 'Hokkaido');
INSERT INTO `databand_city` VALUES ('1668', 'Anjo', 'JPN', 'Aichi');
INSERT INTO `databand_city` VALUES ('1669', 'Narashino', 'JPN', 'Chiba');
INSERT INTO `databand_city` VALUES ('1670', 'Oyama', 'JPN', 'Tochigi');
INSERT INTO `databand_city` VALUES ('1671', 'Ogaki', 'JPN', 'Gifu');
INSERT INTO `databand_city` VALUES ('1672', 'Matsue', 'JPN', 'Shimane');
INSERT INTO `databand_city` VALUES ('1673', 'Kawanishi', 'JPN', 'Hyogo');
INSERT INTO `databand_city` VALUES ('1674', 'Hitachinaka', 'JPN', 'Tokyo-to');
INSERT INTO `databand_city` VALUES ('1675', 'Niiza', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1676', 'Nagareyama', 'JPN', 'Chiba');
INSERT INTO `databand_city` VALUES ('1677', 'Tottori', 'JPN', 'Tottori');
INSERT INTO `databand_city` VALUES ('1678', 'Tama', 'JPN', 'Ibaragi');
INSERT INTO `databand_city` VALUES ('1679', 'Iruma', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1680', 'Ota', 'JPN', 'Gumma');
INSERT INTO `databand_city` VALUES ('1681', 'Omuta', 'JPN', 'Fukuoka');
INSERT INTO `databand_city` VALUES ('1682', 'Komaki', 'JPN', 'Aichi');
INSERT INTO `databand_city` VALUES ('1683', 'Ome', 'JPN', 'Tokyo-to');
INSERT INTO `databand_city` VALUES ('1684', 'Kadoma', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1685', 'Yamaguchi', 'JPN', 'Yamaguchi');
INSERT INTO `databand_city` VALUES ('1686', 'Higashimurayama', 'JPN', 'Tokyo-to');
INSERT INTO `databand_city` VALUES ('1687', 'Yonago', 'JPN', 'Tottori');
INSERT INTO `databand_city` VALUES ('1688', 'Matsubara', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1689', 'Musashino', 'JPN', 'Tokyo-to');
INSERT INTO `databand_city` VALUES ('1690', 'Tsuchiura', 'JPN', 'Ibaragi');
INSERT INTO `databand_city` VALUES ('1691', 'Joetsu', 'JPN', 'Niigata');
INSERT INTO `databand_city` VALUES ('1692', 'Miyakonojo', 'JPN', 'Miyazaki');
INSERT INTO `databand_city` VALUES ('1693', 'Misato', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1694', 'Kakamigahara', 'JPN', 'Gifu');
INSERT INTO `databand_city` VALUES ('1695', 'Daito', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1696', 'Seto', 'JPN', 'Aichi');
INSERT INTO `databand_city` VALUES ('1697', 'Kariya', 'JPN', 'Aichi');
INSERT INTO `databand_city` VALUES ('1698', 'Urayasu', 'JPN', 'Chiba');
INSERT INTO `databand_city` VALUES ('1699', 'Beppu', 'JPN', 'Oita');
INSERT INTO `databand_city` VALUES ('1700', 'Niihama', 'JPN', 'Ehime');
INSERT INTO `databand_city` VALUES ('1701', 'Minoo', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1702', 'Fujieda', 'JPN', 'Shizuoka');
INSERT INTO `databand_city` VALUES ('1703', 'Abiko', 'JPN', 'Chiba');
INSERT INTO `databand_city` VALUES ('1704', 'Nobeoka', 'JPN', 'Miyazaki');
INSERT INTO `databand_city` VALUES ('1705', 'Tondabayashi', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1706', 'Ueda', 'JPN', 'Nagano');
INSERT INTO `databand_city` VALUES ('1707', 'Kashihara', 'JPN', 'Nara');
INSERT INTO `databand_city` VALUES ('1708', 'Matsusaka', 'JPN', 'Mie');
INSERT INTO `databand_city` VALUES ('1709', 'Isesaki', 'JPN', 'Gumma');
INSERT INTO `databand_city` VALUES ('1710', 'Zama', 'JPN', 'Kanagawa');
INSERT INTO `databand_city` VALUES ('1711', 'Kisarazu', 'JPN', 'Chiba');
INSERT INTO `databand_city` VALUES ('1712', 'Noda', 'JPN', 'Chiba');
INSERT INTO `databand_city` VALUES ('1713', 'Ishinomaki', 'JPN', 'Miyagi');
INSERT INTO `databand_city` VALUES ('1714', 'Fujinomiya', 'JPN', 'Shizuoka');
INSERT INTO `databand_city` VALUES ('1715', 'Kawachinagano', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1716', 'Imabari', 'JPN', 'Ehime');
INSERT INTO `databand_city` VALUES ('1717', 'Aizuwakamatsu', 'JPN', 'Fukushima');
INSERT INTO `databand_city` VALUES ('1718', 'Higashihiroshima', 'JPN', 'Hiroshima');
INSERT INTO `databand_city` VALUES ('1719', 'Habikino', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1720', 'Ebetsu', 'JPN', 'Hokkaido');
INSERT INTO `databand_city` VALUES ('1721', 'Hofu', 'JPN', 'Yamaguchi');
INSERT INTO `databand_city` VALUES ('1722', 'Kiryu', 'JPN', 'Gumma');
INSERT INTO `databand_city` VALUES ('1723', 'Okinawa', 'JPN', 'Okinawa');
INSERT INTO `databand_city` VALUES ('1724', 'Yaizu', 'JPN', 'Shizuoka');
INSERT INTO `databand_city` VALUES ('1725', 'Toyokawa', 'JPN', 'Aichi');
INSERT INTO `databand_city` VALUES ('1726', 'Ebina', 'JPN', 'Kanagawa');
INSERT INTO `databand_city` VALUES ('1727', 'Asaka', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1728', 'Higashikurume', 'JPN', 'Tokyo-to');
INSERT INTO `databand_city` VALUES ('1729', 'Ikoma', 'JPN', 'Nara');
INSERT INTO `databand_city` VALUES ('1730', 'Kitami', 'JPN', 'Hokkaido');
INSERT INTO `databand_city` VALUES ('1731', 'Koganei', 'JPN', 'Tokyo-to');
INSERT INTO `databand_city` VALUES ('1732', 'Iwatsuki', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1733', 'Mishima', 'JPN', 'Shizuoka');
INSERT INTO `databand_city` VALUES ('1734', 'Handa', 'JPN', 'Aichi');
INSERT INTO `databand_city` VALUES ('1735', 'Muroran', 'JPN', 'Hokkaido');
INSERT INTO `databand_city` VALUES ('1736', 'Komatsu', 'JPN', 'Ishikawa');
INSERT INTO `databand_city` VALUES ('1737', 'Yatsushiro', 'JPN', 'Kumamoto');
INSERT INTO `databand_city` VALUES ('1738', 'Iida', 'JPN', 'Nagano');
INSERT INTO `databand_city` VALUES ('1739', 'Tokuyama', 'JPN', 'Yamaguchi');
INSERT INTO `databand_city` VALUES ('1740', 'Kokubunji', 'JPN', 'Tokyo-to');
INSERT INTO `databand_city` VALUES ('1741', 'Akishima', 'JPN', 'Tokyo-to');
INSERT INTO `databand_city` VALUES ('1742', 'Iwakuni', 'JPN', 'Yamaguchi');
INSERT INTO `databand_city` VALUES ('1743', 'Kusatsu', 'JPN', 'Shiga');
INSERT INTO `databand_city` VALUES ('1744', 'Kuwana', 'JPN', 'Mie');
INSERT INTO `databand_city` VALUES ('1745', 'Sanda', 'JPN', 'Hyogo');
INSERT INTO `databand_city` VALUES ('1746', 'Hikone', 'JPN', 'Shiga');
INSERT INTO `databand_city` VALUES ('1747', 'Toda', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1748', 'Tajimi', 'JPN', 'Gifu');
INSERT INTO `databand_city` VALUES ('1749', 'Ikeda', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1750', 'Fukaya', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1751', 'Ise', 'JPN', 'Mie');
INSERT INTO `databand_city` VALUES ('1752', 'Sakata', 'JPN', 'Yamagata');
INSERT INTO `databand_city` VALUES ('1753', 'Kasuga', 'JPN', 'Fukuoka');
INSERT INTO `databand_city` VALUES ('1754', 'Kamagaya', 'JPN', 'Chiba');
INSERT INTO `databand_city` VALUES ('1755', 'Tsuruoka', 'JPN', 'Yamagata');
INSERT INTO `databand_city` VALUES ('1756', 'Hoya', 'JPN', 'Tokyo-to');
INSERT INTO `databand_city` VALUES ('1757', 'Nishio', 'JPN', 'Chiba');
INSERT INTO `databand_city` VALUES ('1758', 'Tokai', 'JPN', 'Aichi');
INSERT INTO `databand_city` VALUES ('1759', 'Inazawa', 'JPN', 'Aichi');
INSERT INTO `databand_city` VALUES ('1760', 'Sakado', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1761', 'Isehara', 'JPN', 'Kanagawa');
INSERT INTO `databand_city` VALUES ('1762', 'Takasago', 'JPN', 'Hyogo');
INSERT INTO `databand_city` VALUES ('1763', 'Fujimi', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1764', 'Urasoe', 'JPN', 'Okinawa');
INSERT INTO `databand_city` VALUES ('1765', 'Yonezawa', 'JPN', 'Yamagata');
INSERT INTO `databand_city` VALUES ('1766', 'Konan', 'JPN', 'Aichi');
INSERT INTO `databand_city` VALUES ('1767', 'Yamatokoriyama', 'JPN', 'Nara');
INSERT INTO `databand_city` VALUES ('1768', 'Maizuru', 'JPN', 'Kyoto');
INSERT INTO `databand_city` VALUES ('1769', 'Onomichi', 'JPN', 'Hiroshima');
INSERT INTO `databand_city` VALUES ('1770', 'Higashimatsuyama', 'JPN', 'Saitama');
INSERT INTO `databand_city` VALUES ('1771', 'Kimitsu', 'JPN', 'Chiba');
INSERT INTO `databand_city` VALUES ('1772', 'Isahaya', 'JPN', 'Nagasaki');
INSERT INTO `databand_city` VALUES ('1773', 'Kanuma', 'JPN', 'Tochigi');
INSERT INTO `databand_city` VALUES ('1774', 'Izumisano', 'JPN', 'Osaka');
INSERT INTO `databand_city` VALUES ('1775', 'Kameoka', 'JPN', 'Kyoto');
INSERT INTO `databand_city` VALUES ('1776', 'Mobara', 'JPN', 'Chiba');
INSERT INTO `databand_city` VALUES ('1777', 'Narita', 'JPN', 'Chiba');
INSERT INTO `databand_city` VALUES ('1778', 'Kashiwazaki', 'JPN', 'Niigata');
INSERT INTO `databand_city` VALUES ('1779', 'Tsuyama', 'JPN', 'Okayama');
INSERT INTO `databand_city` VALUES ('1780', 'Sanaa', 'YEM', 'Sanaa');
INSERT INTO `databand_city` VALUES ('1781', 'Aden', 'YEM', 'Aden');
INSERT INTO `databand_city` VALUES ('1782', 'Taizz', 'YEM', 'Taizz');
INSERT INTO `databand_city` VALUES ('1783', 'Hodeida', 'YEM', 'Hodeida');
INSERT INTO `databand_city` VALUES ('1784', 'al-Mukalla', 'YEM', 'Hadramawt');
INSERT INTO `databand_city` VALUES ('1785', 'Ibb', 'YEM', 'Ibb');
INSERT INTO `databand_city` VALUES ('1786', 'Amman', 'JOR', 'Amman');
INSERT INTO `databand_city` VALUES ('1787', 'al-Zarqa', 'JOR', 'al-Zarqa');
INSERT INTO `databand_city` VALUES ('1788', 'Irbid', 'JOR', 'Irbid');
INSERT INTO `databand_city` VALUES ('1789', 'al-Rusayfa', 'JOR', 'al-Zarqa');
INSERT INTO `databand_city` VALUES ('1790', 'Wadi al-Sir', 'JOR', 'Amman');
INSERT INTO `databand_city` VALUES ('1791', 'Flying Fish Cove', 'CXR', '–');
INSERT INTO `databand_city` VALUES ('1792', 'Beograd', 'YUG', 'Central Serbia');
INSERT INTO `databand_city` VALUES ('1793', 'Novi Sad', 'YUG', 'Vojvodina');
INSERT INTO `databand_city` VALUES ('1794', 'Niš', 'YUG', 'Central Serbia');
INSERT INTO `databand_city` VALUES ('1795', 'Priština', 'YUG', 'Kosovo and Metohija');
INSERT INTO `databand_city` VALUES ('1796', 'Kragujevac', 'YUG', 'Central Serbia');
INSERT INTO `databand_city` VALUES ('1797', 'Podgorica', 'YUG', 'Montenegro');
INSERT INTO `databand_city` VALUES ('1798', 'Subotica', 'YUG', 'Vojvodina');
INSERT INTO `databand_city` VALUES ('1799', 'Prizren', 'YUG', 'Kosovo and Metohija');
INSERT INTO `databand_city` VALUES ('1800', 'Phnom Penh', 'KHM', 'Phnom Penh');
INSERT INTO `databand_city` VALUES ('1801', 'Battambang', 'KHM', 'Battambang');
INSERT INTO `databand_city` VALUES ('1802', 'Siem Reap', 'KHM', 'Siem Reap');
INSERT INTO `databand_city` VALUES ('1803', 'Douala', 'CMR', 'Littoral');
INSERT INTO `databand_city` VALUES ('1804', 'Yaoundé', 'CMR', 'Centre');
INSERT INTO `databand_city` VALUES ('1805', 'Garoua', 'CMR', 'Nord');
INSERT INTO `databand_city` VALUES ('1806', 'Maroua', 'CMR', 'Extrême-Nord');
INSERT INTO `databand_city` VALUES ('1807', 'Bamenda', 'CMR', 'Nord-Ouest');
INSERT INTO `databand_city` VALUES ('1808', 'Bafoussam', 'CMR', 'Ouest');
INSERT INTO `databand_city` VALUES ('1809', 'Nkongsamba', 'CMR', 'Littoral');
INSERT INTO `databand_city` VALUES ('1810', 'Montréal', 'CAN', 'Québec');
INSERT INTO `databand_city` VALUES ('1811', 'Calgary', 'CAN', 'Alberta');
INSERT INTO `databand_city` VALUES ('1812', 'Toronto', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1813', 'North York', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1814', 'Winnipeg', 'CAN', 'Manitoba');
INSERT INTO `databand_city` VALUES ('1815', 'Edmonton', 'CAN', 'Alberta');
INSERT INTO `databand_city` VALUES ('1816', 'Mississauga', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1817', 'Scarborough', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1818', 'Vancouver', 'CAN', 'British Colombia');
INSERT INTO `databand_city` VALUES ('1819', 'Etobicoke', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1820', 'London', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1821', 'Hamilton', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1822', 'Ottawa', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1823', 'Laval', 'CAN', 'Québec');
INSERT INTO `databand_city` VALUES ('1824', 'Surrey', 'CAN', 'British Colombia');
INSERT INTO `databand_city` VALUES ('1825', 'Brampton', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1826', 'Windsor', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1827', 'Saskatoon', 'CAN', 'Saskatchewan');
INSERT INTO `databand_city` VALUES ('1828', 'Kitchener', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1829', 'Markham', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1830', 'Regina', 'CAN', 'Saskatchewan');
INSERT INTO `databand_city` VALUES ('1831', 'Burnaby', 'CAN', 'British Colombia');
INSERT INTO `databand_city` VALUES ('1832', 'Québec', 'CAN', 'Québec');
INSERT INTO `databand_city` VALUES ('1833', 'York', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1834', 'Richmond', 'CAN', 'British Colombia');
INSERT INTO `databand_city` VALUES ('1835', 'Vaughan', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1836', 'Burlington', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1837', 'Oshawa', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1838', 'Oakville', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1839', 'Saint Catharines', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1840', 'Longueuil', 'CAN', 'Québec');
INSERT INTO `databand_city` VALUES ('1841', 'Richmond Hill', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1842', 'Thunder Bay', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1843', 'Nepean', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1844', 'Cape Breton', 'CAN', 'Nova Scotia');
INSERT INTO `databand_city` VALUES ('1845', 'East York', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1846', 'Halifax', 'CAN', 'Nova Scotia');
INSERT INTO `databand_city` VALUES ('1847', 'Cambridge', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1848', 'Gloucester', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1849', 'Abbotsford', 'CAN', 'British Colombia');
INSERT INTO `databand_city` VALUES ('1850', 'Guelph', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1851', 'Saint John´s', 'CAN', 'Newfoundland');
INSERT INTO `databand_city` VALUES ('1852', 'Coquitlam', 'CAN', 'British Colombia');
INSERT INTO `databand_city` VALUES ('1853', 'Saanich', 'CAN', 'British Colombia');
INSERT INTO `databand_city` VALUES ('1854', 'Gatineau', 'CAN', 'Québec');
INSERT INTO `databand_city` VALUES ('1855', 'Delta', 'CAN', 'British Colombia');
INSERT INTO `databand_city` VALUES ('1856', 'Sudbury', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1857', 'Kelowna', 'CAN', 'British Colombia');
INSERT INTO `databand_city` VALUES ('1858', 'Barrie', 'CAN', 'Ontario');
INSERT INTO `databand_city` VALUES ('1859', 'Praia', 'CPV', 'São Tiago');
INSERT INTO `databand_city` VALUES ('1860', 'Almaty', 'KAZ', 'Almaty Qalasy');
INSERT INTO `databand_city` VALUES ('1861', 'Qaraghandy', 'KAZ', 'Qaraghandy');
INSERT INTO `databand_city` VALUES ('1862', 'Shymkent', 'KAZ', 'South Kazakstan');
INSERT INTO `databand_city` VALUES ('1863', 'Taraz', 'KAZ', 'Taraz');
INSERT INTO `databand_city` VALUES ('1864', 'Astana', 'KAZ', 'Astana');
INSERT INTO `databand_city` VALUES ('1865', 'Öskemen', 'KAZ', 'East Kazakstan');
INSERT INTO `databand_city` VALUES ('1866', 'Pavlodar', 'KAZ', 'Pavlodar');
INSERT INTO `databand_city` VALUES ('1867', 'Semey', 'KAZ', 'East Kazakstan');
INSERT INTO `databand_city` VALUES ('1868', 'Aqtöbe', 'KAZ', 'Aqtöbe');
INSERT INTO `databand_city` VALUES ('1869', 'Qostanay', 'KAZ', 'Qostanay');
INSERT INTO `databand_city` VALUES ('1870', 'Petropavl', 'KAZ', 'North Kazakstan');
INSERT INTO `databand_city` VALUES ('1871', 'Oral', 'KAZ', 'West Kazakstan');
INSERT INTO `databand_city` VALUES ('1872', 'Temirtau', 'KAZ', 'Qaraghandy');
INSERT INTO `databand_city` VALUES ('1873', 'Qyzylorda', 'KAZ', 'Qyzylorda');
INSERT INTO `databand_city` VALUES ('1874', 'Aqtau', 'KAZ', 'Mangghystau');
INSERT INTO `databand_city` VALUES ('1875', 'Atyrau', 'KAZ', 'Atyrau');
INSERT INTO `databand_city` VALUES ('1876', 'Ekibastuz', 'KAZ', 'Pavlodar');
INSERT INTO `databand_city` VALUES ('1877', 'Kökshetau', 'KAZ', 'North Kazakstan');
INSERT INTO `databand_city` VALUES ('1878', 'Rudnyy', 'KAZ', 'Qostanay');
INSERT INTO `databand_city` VALUES ('1879', 'Taldyqorghan', 'KAZ', 'Almaty');
INSERT INTO `databand_city` VALUES ('1880', 'Zhezqazghan', 'KAZ', 'Qaraghandy');
INSERT INTO `databand_city` VALUES ('1881', 'Nairobi', 'KEN', 'Nairobi');
INSERT INTO `databand_city` VALUES ('1882', 'Mombasa', 'KEN', 'Coast');
INSERT INTO `databand_city` VALUES ('1883', 'Kisumu', 'KEN', 'Nyanza');
INSERT INTO `databand_city` VALUES ('1884', 'Nakuru', 'KEN', 'Rift Valley');
INSERT INTO `databand_city` VALUES ('1885', 'Machakos', 'KEN', 'Eastern');
INSERT INTO `databand_city` VALUES ('1886', 'Eldoret', 'KEN', 'Rift Valley');
INSERT INTO `databand_city` VALUES ('1887', 'Meru', 'KEN', 'Eastern');
INSERT INTO `databand_city` VALUES ('1888', 'Nyeri', 'KEN', 'Central');
INSERT INTO `databand_city` VALUES ('1889', 'Bangui', 'CAF', 'Bangui');
INSERT INTO `databand_city` VALUES ('1890', 'Shanghai', 'CHN', 'Shanghai');
INSERT INTO `databand_city` VALUES ('1891', 'Peking', 'CHN', 'Peking');
INSERT INTO `databand_city` VALUES ('1892', 'Chongqing', 'CHN', 'Chongqing');
INSERT INTO `databand_city` VALUES ('1893', 'Tianjin', 'CHN', 'Tianjin');
INSERT INTO `databand_city` VALUES ('1894', 'Wuhan', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('1895', 'Harbin', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('1896', 'Shenyang', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('1897', 'Kanton [Guangzhou]', 'CHN', 'Guangdong');
INSERT INTO `databand_city` VALUES ('1898', 'Chengdu', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('1899', 'Nanking [Nanjing]', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('1900', 'Changchun', 'CHN', 'Jilin');
INSERT INTO `databand_city` VALUES ('1901', 'Xi´an', 'CHN', 'Shaanxi');
INSERT INTO `databand_city` VALUES ('1902', 'Dalian', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('1903', 'Qingdao', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('1904', 'Jinan', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('1905', 'Hangzhou', 'CHN', 'Zhejiang');
INSERT INTO `databand_city` VALUES ('1906', 'Zhengzhou', 'CHN', 'Henan');
INSERT INTO `databand_city` VALUES ('1907', 'Shijiazhuang', 'CHN', 'Hebei');
INSERT INTO `databand_city` VALUES ('1908', 'Taiyuan', 'CHN', 'Shanxi');
INSERT INTO `databand_city` VALUES ('1909', 'Kunming', 'CHN', 'Yunnan');
INSERT INTO `databand_city` VALUES ('1910', 'Changsha', 'CHN', 'Hunan');
INSERT INTO `databand_city` VALUES ('1911', 'Nanchang', 'CHN', 'Jiangxi');
INSERT INTO `databand_city` VALUES ('1912', 'Fuzhou', 'CHN', 'Fujian');
INSERT INTO `databand_city` VALUES ('1913', 'Lanzhou', 'CHN', 'Gansu');
INSERT INTO `databand_city` VALUES ('1914', 'Guiyang', 'CHN', 'Guizhou');
INSERT INTO `databand_city` VALUES ('1915', 'Ningbo', 'CHN', 'Zhejiang');
INSERT INTO `databand_city` VALUES ('1916', 'Hefei', 'CHN', 'Anhui');
INSERT INTO `databand_city` VALUES ('1917', 'Urumtši [Ürümqi]', 'CHN', 'Xinxiang');
INSERT INTO `databand_city` VALUES ('1918', 'Anshan', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('1919', 'Fushun', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('1920', 'Nanning', 'CHN', 'Guangxi');
INSERT INTO `databand_city` VALUES ('1921', 'Zibo', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('1922', 'Qiqihar', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('1923', 'Jilin', 'CHN', 'Jilin');
INSERT INTO `databand_city` VALUES ('1924', 'Tangshan', 'CHN', 'Hebei');
INSERT INTO `databand_city` VALUES ('1925', 'Baotou', 'CHN', 'Inner Mongolia');
INSERT INTO `databand_city` VALUES ('1926', 'Shenzhen', 'CHN', 'Guangdong');
INSERT INTO `databand_city` VALUES ('1927', 'Hohhot', 'CHN', 'Inner Mongolia');
INSERT INTO `databand_city` VALUES ('1928', 'Handan', 'CHN', 'Hebei');
INSERT INTO `databand_city` VALUES ('1929', 'Wuxi', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('1930', 'Xuzhou', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('1931', 'Datong', 'CHN', 'Shanxi');
INSERT INTO `databand_city` VALUES ('1932', 'Yichun', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('1933', 'Benxi', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('1934', 'Luoyang', 'CHN', 'Henan');
INSERT INTO `databand_city` VALUES ('1935', 'Suzhou', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('1936', 'Xining', 'CHN', 'Qinghai');
INSERT INTO `databand_city` VALUES ('1937', 'Huainan', 'CHN', 'Anhui');
INSERT INTO `databand_city` VALUES ('1938', 'Jixi', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('1939', 'Daqing', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('1940', 'Fuxin', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('1941', 'Amoy [Xiamen]', 'CHN', 'Fujian');
INSERT INTO `databand_city` VALUES ('1942', 'Liuzhou', 'CHN', 'Guangxi');
INSERT INTO `databand_city` VALUES ('1943', 'Shantou', 'CHN', 'Guangdong');
INSERT INTO `databand_city` VALUES ('1944', 'Jinzhou', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('1945', 'Mudanjiang', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('1946', 'Yinchuan', 'CHN', 'Ningxia');
INSERT INTO `databand_city` VALUES ('1947', 'Changzhou', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('1948', 'Zhangjiakou', 'CHN', 'Hebei');
INSERT INTO `databand_city` VALUES ('1949', 'Dandong', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('1950', 'Hegang', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('1951', 'Kaifeng', 'CHN', 'Henan');
INSERT INTO `databand_city` VALUES ('1952', 'Jiamusi', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('1953', 'Liaoyang', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('1954', 'Hengyang', 'CHN', 'Hunan');
INSERT INTO `databand_city` VALUES ('1955', 'Baoding', 'CHN', 'Hebei');
INSERT INTO `databand_city` VALUES ('1956', 'Hunjiang', 'CHN', 'Jilin');
INSERT INTO `databand_city` VALUES ('1957', 'Xinxiang', 'CHN', 'Henan');
INSERT INTO `databand_city` VALUES ('1958', 'Huangshi', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('1959', 'Haikou', 'CHN', 'Hainan');
INSERT INTO `databand_city` VALUES ('1960', 'Yantai', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('1961', 'Bengbu', 'CHN', 'Anhui');
INSERT INTO `databand_city` VALUES ('1962', 'Xiangtan', 'CHN', 'Hunan');
INSERT INTO `databand_city` VALUES ('1963', 'Weifang', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('1964', 'Wuhu', 'CHN', 'Anhui');
INSERT INTO `databand_city` VALUES ('1965', 'Pingxiang', 'CHN', 'Jiangxi');
INSERT INTO `databand_city` VALUES ('1966', 'Yingkou', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('1967', 'Anyang', 'CHN', 'Henan');
INSERT INTO `databand_city` VALUES ('1968', 'Panzhihua', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('1969', 'Pingdingshan', 'CHN', 'Henan');
INSERT INTO `databand_city` VALUES ('1970', 'Xiangfan', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('1971', 'Zhuzhou', 'CHN', 'Hunan');
INSERT INTO `databand_city` VALUES ('1972', 'Jiaozuo', 'CHN', 'Henan');
INSERT INTO `databand_city` VALUES ('1973', 'Wenzhou', 'CHN', 'Zhejiang');
INSERT INTO `databand_city` VALUES ('1974', 'Zhangjiang', 'CHN', 'Guangdong');
INSERT INTO `databand_city` VALUES ('1975', 'Zigong', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('1976', 'Shuangyashan', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('1977', 'Zaozhuang', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('1978', 'Yakeshi', 'CHN', 'Inner Mongolia');
INSERT INTO `databand_city` VALUES ('1979', 'Yichang', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('1980', 'Zhenjiang', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('1981', 'Huaibei', 'CHN', 'Anhui');
INSERT INTO `databand_city` VALUES ('1982', 'Qinhuangdao', 'CHN', 'Hebei');
INSERT INTO `databand_city` VALUES ('1983', 'Guilin', 'CHN', 'Guangxi');
INSERT INTO `databand_city` VALUES ('1984', 'Liupanshui', 'CHN', 'Guizhou');
INSERT INTO `databand_city` VALUES ('1985', 'Panjin', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('1986', 'Yangquan', 'CHN', 'Shanxi');
INSERT INTO `databand_city` VALUES ('1987', 'Jinxi', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('1988', 'Liaoyuan', 'CHN', 'Jilin');
INSERT INTO `databand_city` VALUES ('1989', 'Lianyungang', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('1990', 'Xianyang', 'CHN', 'Shaanxi');
INSERT INTO `databand_city` VALUES ('1991', 'Tai´an', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('1992', 'Chifeng', 'CHN', 'Inner Mongolia');
INSERT INTO `databand_city` VALUES ('1993', 'Shaoguan', 'CHN', 'Guangdong');
INSERT INTO `databand_city` VALUES ('1994', 'Nantong', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('1995', 'Leshan', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('1996', 'Baoji', 'CHN', 'Shaanxi');
INSERT INTO `databand_city` VALUES ('1997', 'Linyi', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('1998', 'Tonghua', 'CHN', 'Jilin');
INSERT INTO `databand_city` VALUES ('1999', 'Siping', 'CHN', 'Jilin');
INSERT INTO `databand_city` VALUES ('2000', 'Changzhi', 'CHN', 'Shanxi');
INSERT INTO `databand_city` VALUES ('2001', 'Tengzhou', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2002', 'Chaozhou', 'CHN', 'Guangdong');
INSERT INTO `databand_city` VALUES ('2003', 'Yangzhou', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('2004', 'Dongwan', 'CHN', 'Guangdong');
INSERT INTO `databand_city` VALUES ('2005', 'Ma´anshan', 'CHN', 'Anhui');
INSERT INTO `databand_city` VALUES ('2006', 'Foshan', 'CHN', 'Guangdong');
INSERT INTO `databand_city` VALUES ('2007', 'Yueyang', 'CHN', 'Hunan');
INSERT INTO `databand_city` VALUES ('2008', 'Xingtai', 'CHN', 'Hebei');
INSERT INTO `databand_city` VALUES ('2009', 'Changde', 'CHN', 'Hunan');
INSERT INTO `databand_city` VALUES ('2010', 'Shihezi', 'CHN', 'Xinxiang');
INSERT INTO `databand_city` VALUES ('2011', 'Yancheng', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('2012', 'Jiujiang', 'CHN', 'Jiangxi');
INSERT INTO `databand_city` VALUES ('2013', 'Dongying', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2014', 'Shashi', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('2015', 'Xintai', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2016', 'Jingdezhen', 'CHN', 'Jiangxi');
INSERT INTO `databand_city` VALUES ('2017', 'Tongchuan', 'CHN', 'Shaanxi');
INSERT INTO `databand_city` VALUES ('2018', 'Zhongshan', 'CHN', 'Guangdong');
INSERT INTO `databand_city` VALUES ('2019', 'Shiyan', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('2020', 'Tieli', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('2021', 'Jining', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2022', 'Wuhai', 'CHN', 'Inner Mongolia');
INSERT INTO `databand_city` VALUES ('2023', 'Mianyang', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('2024', 'Luzhou', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('2025', 'Zunyi', 'CHN', 'Guizhou');
INSERT INTO `databand_city` VALUES ('2026', 'Shizuishan', 'CHN', 'Ningxia');
INSERT INTO `databand_city` VALUES ('2027', 'Neijiang', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('2028', 'Tongliao', 'CHN', 'Inner Mongolia');
INSERT INTO `databand_city` VALUES ('2029', 'Tieling', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('2030', 'Wafangdian', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('2031', 'Anqing', 'CHN', 'Anhui');
INSERT INTO `databand_city` VALUES ('2032', 'Shaoyang', 'CHN', 'Hunan');
INSERT INTO `databand_city` VALUES ('2033', 'Laiwu', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2034', 'Chengde', 'CHN', 'Hebei');
INSERT INTO `databand_city` VALUES ('2035', 'Tianshui', 'CHN', 'Gansu');
INSERT INTO `databand_city` VALUES ('2036', 'Nanyang', 'CHN', 'Henan');
INSERT INTO `databand_city` VALUES ('2037', 'Cangzhou', 'CHN', 'Hebei');
INSERT INTO `databand_city` VALUES ('2038', 'Yibin', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('2039', 'Huaiyin', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('2040', 'Dunhua', 'CHN', 'Jilin');
INSERT INTO `databand_city` VALUES ('2041', 'Yanji', 'CHN', 'Jilin');
INSERT INTO `databand_city` VALUES ('2042', 'Jiangmen', 'CHN', 'Guangdong');
INSERT INTO `databand_city` VALUES ('2043', 'Tongling', 'CHN', 'Anhui');
INSERT INTO `databand_city` VALUES ('2044', 'Suihua', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('2045', 'Gongziling', 'CHN', 'Jilin');
INSERT INTO `databand_city` VALUES ('2046', 'Xiantao', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('2047', 'Chaoyang', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('2048', 'Ganzhou', 'CHN', 'Jiangxi');
INSERT INTO `databand_city` VALUES ('2049', 'Huzhou', 'CHN', 'Zhejiang');
INSERT INTO `databand_city` VALUES ('2050', 'Baicheng', 'CHN', 'Jilin');
INSERT INTO `databand_city` VALUES ('2051', 'Shangzi', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('2052', 'Yangjiang', 'CHN', 'Guangdong');
INSERT INTO `databand_city` VALUES ('2053', 'Qitaihe', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('2054', 'Gejiu', 'CHN', 'Yunnan');
INSERT INTO `databand_city` VALUES ('2055', 'Jiangyin', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('2056', 'Hebi', 'CHN', 'Henan');
INSERT INTO `databand_city` VALUES ('2057', 'Jiaxing', 'CHN', 'Zhejiang');
INSERT INTO `databand_city` VALUES ('2058', 'Wuzhou', 'CHN', 'Guangxi');
INSERT INTO `databand_city` VALUES ('2059', 'Meihekou', 'CHN', 'Jilin');
INSERT INTO `databand_city` VALUES ('2060', 'Xuchang', 'CHN', 'Henan');
INSERT INTO `databand_city` VALUES ('2061', 'Liaocheng', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2062', 'Haicheng', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('2063', 'Qianjiang', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('2064', 'Baiyin', 'CHN', 'Gansu');
INSERT INTO `databand_city` VALUES ('2065', 'Bei´an', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('2066', 'Yixing', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('2067', 'Laizhou', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2068', 'Qaramay', 'CHN', 'Xinxiang');
INSERT INTO `databand_city` VALUES ('2069', 'Acheng', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('2070', 'Dezhou', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2071', 'Nanping', 'CHN', 'Fujian');
INSERT INTO `databand_city` VALUES ('2072', 'Zhaoqing', 'CHN', 'Guangdong');
INSERT INTO `databand_city` VALUES ('2073', 'Beipiao', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('2074', 'Fengcheng', 'CHN', 'Jiangxi');
INSERT INTO `databand_city` VALUES ('2075', 'Fuyu', 'CHN', 'Jilin');
INSERT INTO `databand_city` VALUES ('2076', 'Xinyang', 'CHN', 'Henan');
INSERT INTO `databand_city` VALUES ('2077', 'Dongtai', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('2078', 'Yuci', 'CHN', 'Shanxi');
INSERT INTO `databand_city` VALUES ('2079', 'Honghu', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('2080', 'Ezhou', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('2081', 'Heze', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2082', 'Daxian', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('2083', 'Linfen', 'CHN', 'Shanxi');
INSERT INTO `databand_city` VALUES ('2084', 'Tianmen', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('2085', 'Yiyang', 'CHN', 'Hunan');
INSERT INTO `databand_city` VALUES ('2086', 'Quanzhou', 'CHN', 'Fujian');
INSERT INTO `databand_city` VALUES ('2087', 'Rizhao', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2088', 'Deyang', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('2089', 'Guangyuan', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('2090', 'Changshu', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('2091', 'Zhangzhou', 'CHN', 'Fujian');
INSERT INTO `databand_city` VALUES ('2092', 'Hailar', 'CHN', 'Inner Mongolia');
INSERT INTO `databand_city` VALUES ('2093', 'Nanchong', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('2094', 'Jiutai', 'CHN', 'Jilin');
INSERT INTO `databand_city` VALUES ('2095', 'Zhaodong', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('2096', 'Shaoxing', 'CHN', 'Zhejiang');
INSERT INTO `databand_city` VALUES ('2097', 'Fuyang', 'CHN', 'Anhui');
INSERT INTO `databand_city` VALUES ('2098', 'Maoming', 'CHN', 'Guangdong');
INSERT INTO `databand_city` VALUES ('2099', 'Qujing', 'CHN', 'Yunnan');
INSERT INTO `databand_city` VALUES ('2100', 'Ghulja', 'CHN', 'Xinxiang');
INSERT INTO `databand_city` VALUES ('2101', 'Jiaohe', 'CHN', 'Jilin');
INSERT INTO `databand_city` VALUES ('2102', 'Puyang', 'CHN', 'Henan');
INSERT INTO `databand_city` VALUES ('2103', 'Huadian', 'CHN', 'Jilin');
INSERT INTO `databand_city` VALUES ('2104', 'Jiangyou', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('2105', 'Qashqar', 'CHN', 'Xinxiang');
INSERT INTO `databand_city` VALUES ('2106', 'Anshun', 'CHN', 'Guizhou');
INSERT INTO `databand_city` VALUES ('2107', 'Fuling', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('2108', 'Xinyu', 'CHN', 'Jiangxi');
INSERT INTO `databand_city` VALUES ('2109', 'Hanzhong', 'CHN', 'Shaanxi');
INSERT INTO `databand_city` VALUES ('2110', 'Danyang', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('2111', 'Chenzhou', 'CHN', 'Hunan');
INSERT INTO `databand_city` VALUES ('2112', 'Xiaogan', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('2113', 'Shangqiu', 'CHN', 'Henan');
INSERT INTO `databand_city` VALUES ('2114', 'Zhuhai', 'CHN', 'Guangdong');
INSERT INTO `databand_city` VALUES ('2115', 'Qingyuan', 'CHN', 'Guangdong');
INSERT INTO `databand_city` VALUES ('2116', 'Aqsu', 'CHN', 'Xinxiang');
INSERT INTO `databand_city` VALUES ('2117', 'Jining', 'CHN', 'Inner Mongolia');
INSERT INTO `databand_city` VALUES ('2118', 'Xiaoshan', 'CHN', 'Zhejiang');
INSERT INTO `databand_city` VALUES ('2119', 'Zaoyang', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('2120', 'Xinghua', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('2121', 'Hami', 'CHN', 'Xinxiang');
INSERT INTO `databand_city` VALUES ('2122', 'Huizhou', 'CHN', 'Guangdong');
INSERT INTO `databand_city` VALUES ('2123', 'Jinmen', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('2124', 'Sanming', 'CHN', 'Fujian');
INSERT INTO `databand_city` VALUES ('2125', 'Ulanhot', 'CHN', 'Inner Mongolia');
INSERT INTO `databand_city` VALUES ('2126', 'Korla', 'CHN', 'Xinxiang');
INSERT INTO `databand_city` VALUES ('2127', 'Wanxian', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('2128', 'Rui´an', 'CHN', 'Zhejiang');
INSERT INTO `databand_city` VALUES ('2129', 'Zhoushan', 'CHN', 'Zhejiang');
INSERT INTO `databand_city` VALUES ('2130', 'Liangcheng', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2131', 'Jiaozhou', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2132', 'Taizhou', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('2133', 'Suzhou', 'CHN', 'Anhui');
INSERT INTO `databand_city` VALUES ('2134', 'Yichun', 'CHN', 'Jiangxi');
INSERT INTO `databand_city` VALUES ('2135', 'Taonan', 'CHN', 'Jilin');
INSERT INTO `databand_city` VALUES ('2136', 'Pingdu', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2137', 'Ji´an', 'CHN', 'Jiangxi');
INSERT INTO `databand_city` VALUES ('2138', 'Longkou', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2139', 'Langfang', 'CHN', 'Hebei');
INSERT INTO `databand_city` VALUES ('2140', 'Zhoukou', 'CHN', 'Henan');
INSERT INTO `databand_city` VALUES ('2141', 'Suining', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('2142', 'Yulin', 'CHN', 'Guangxi');
INSERT INTO `databand_city` VALUES ('2143', 'Jinhua', 'CHN', 'Zhejiang');
INSERT INTO `databand_city` VALUES ('2144', 'Liu´an', 'CHN', 'Anhui');
INSERT INTO `databand_city` VALUES ('2145', 'Shuangcheng', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('2146', 'Suizhou', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('2147', 'Ankang', 'CHN', 'Shaanxi');
INSERT INTO `databand_city` VALUES ('2148', 'Weinan', 'CHN', 'Shaanxi');
INSERT INTO `databand_city` VALUES ('2149', 'Longjing', 'CHN', 'Jilin');
INSERT INTO `databand_city` VALUES ('2150', 'Da´an', 'CHN', 'Jilin');
INSERT INTO `databand_city` VALUES ('2151', 'Lengshuijiang', 'CHN', 'Hunan');
INSERT INTO `databand_city` VALUES ('2152', 'Laiyang', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2153', 'Xianning', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('2154', 'Dali', 'CHN', 'Yunnan');
INSERT INTO `databand_city` VALUES ('2155', 'Anda', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('2156', 'Jincheng', 'CHN', 'Shanxi');
INSERT INTO `databand_city` VALUES ('2157', 'Longyan', 'CHN', 'Fujian');
INSERT INTO `databand_city` VALUES ('2158', 'Xichang', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('2159', 'Wendeng', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2160', 'Hailun', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('2161', 'Binzhou', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2162', 'Linhe', 'CHN', 'Inner Mongolia');
INSERT INTO `databand_city` VALUES ('2163', 'Wuwei', 'CHN', 'Gansu');
INSERT INTO `databand_city` VALUES ('2164', 'Duyun', 'CHN', 'Guizhou');
INSERT INTO `databand_city` VALUES ('2165', 'Mishan', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('2166', 'Shangrao', 'CHN', 'Jiangxi');
INSERT INTO `databand_city` VALUES ('2167', 'Changji', 'CHN', 'Xinxiang');
INSERT INTO `databand_city` VALUES ('2168', 'Meixian', 'CHN', 'Guangdong');
INSERT INTO `databand_city` VALUES ('2169', 'Yushu', 'CHN', 'Jilin');
INSERT INTO `databand_city` VALUES ('2170', 'Tiefa', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('2171', 'Huai´an', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('2172', 'Leiyang', 'CHN', 'Hunan');
INSERT INTO `databand_city` VALUES ('2173', 'Zalantun', 'CHN', 'Inner Mongolia');
INSERT INTO `databand_city` VALUES ('2174', 'Weihai', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2175', 'Loudi', 'CHN', 'Hunan');
INSERT INTO `databand_city` VALUES ('2176', 'Qingzhou', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2177', 'Qidong', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('2178', 'Huaihua', 'CHN', 'Hunan');
INSERT INTO `databand_city` VALUES ('2179', 'Luohe', 'CHN', 'Henan');
INSERT INTO `databand_city` VALUES ('2180', 'Chuzhou', 'CHN', 'Anhui');
INSERT INTO `databand_city` VALUES ('2181', 'Kaiyuan', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('2182', 'Linqing', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2183', 'Chaohu', 'CHN', 'Anhui');
INSERT INTO `databand_city` VALUES ('2184', 'Laohekou', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('2185', 'Dujiangyan', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('2186', 'Zhumadian', 'CHN', 'Henan');
INSERT INTO `databand_city` VALUES ('2187', 'Linchuan', 'CHN', 'Jiangxi');
INSERT INTO `databand_city` VALUES ('2188', 'Jiaonan', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2189', 'Sanmenxia', 'CHN', 'Henan');
INSERT INTO `databand_city` VALUES ('2190', 'Heyuan', 'CHN', 'Guangdong');
INSERT INTO `databand_city` VALUES ('2191', 'Manzhouli', 'CHN', 'Inner Mongolia');
INSERT INTO `databand_city` VALUES ('2192', 'Lhasa', 'CHN', 'Tibet');
INSERT INTO `databand_city` VALUES ('2193', 'Lianyuan', 'CHN', 'Hunan');
INSERT INTO `databand_city` VALUES ('2194', 'Kuytun', 'CHN', 'Xinxiang');
INSERT INTO `databand_city` VALUES ('2195', 'Puqi', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('2196', 'Hongjiang', 'CHN', 'Hunan');
INSERT INTO `databand_city` VALUES ('2197', 'Qinzhou', 'CHN', 'Guangxi');
INSERT INTO `databand_city` VALUES ('2198', 'Renqiu', 'CHN', 'Hebei');
INSERT INTO `databand_city` VALUES ('2199', 'Yuyao', 'CHN', 'Zhejiang');
INSERT INTO `databand_city` VALUES ('2200', 'Guigang', 'CHN', 'Guangxi');
INSERT INTO `databand_city` VALUES ('2201', 'Kaili', 'CHN', 'Guizhou');
INSERT INTO `databand_city` VALUES ('2202', 'Yan´an', 'CHN', 'Shaanxi');
INSERT INTO `databand_city` VALUES ('2203', 'Beihai', 'CHN', 'Guangxi');
INSERT INTO `databand_city` VALUES ('2204', 'Xuangzhou', 'CHN', 'Anhui');
INSERT INTO `databand_city` VALUES ('2205', 'Quzhou', 'CHN', 'Zhejiang');
INSERT INTO `databand_city` VALUES ('2206', 'Yong´an', 'CHN', 'Fujian');
INSERT INTO `databand_city` VALUES ('2207', 'Zixing', 'CHN', 'Hunan');
INSERT INTO `databand_city` VALUES ('2208', 'Liyang', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('2209', 'Yizheng', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('2210', 'Yumen', 'CHN', 'Gansu');
INSERT INTO `databand_city` VALUES ('2211', 'Liling', 'CHN', 'Hunan');
INSERT INTO `databand_city` VALUES ('2212', 'Yuncheng', 'CHN', 'Shanxi');
INSERT INTO `databand_city` VALUES ('2213', 'Shanwei', 'CHN', 'Guangdong');
INSERT INTO `databand_city` VALUES ('2214', 'Cixi', 'CHN', 'Zhejiang');
INSERT INTO `databand_city` VALUES ('2215', 'Yuanjiang', 'CHN', 'Hunan');
INSERT INTO `databand_city` VALUES ('2216', 'Bozhou', 'CHN', 'Anhui');
INSERT INTO `databand_city` VALUES ('2217', 'Jinchang', 'CHN', 'Gansu');
INSERT INTO `databand_city` VALUES ('2218', 'Fu´an', 'CHN', 'Fujian');
INSERT INTO `databand_city` VALUES ('2219', 'Suqian', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('2220', 'Shishou', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('2221', 'Hengshui', 'CHN', 'Hebei');
INSERT INTO `databand_city` VALUES ('2222', 'Danjiangkou', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('2223', 'Fujin', 'CHN', 'Heilongjiang');
INSERT INTO `databand_city` VALUES ('2224', 'Sanya', 'CHN', 'Hainan');
INSERT INTO `databand_city` VALUES ('2225', 'Guangshui', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('2226', 'Huangshan', 'CHN', 'Anhui');
INSERT INTO `databand_city` VALUES ('2227', 'Xingcheng', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('2228', 'Zhucheng', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2229', 'Kunshan', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('2230', 'Haining', 'CHN', 'Zhejiang');
INSERT INTO `databand_city` VALUES ('2231', 'Pingliang', 'CHN', 'Gansu');
INSERT INTO `databand_city` VALUES ('2232', 'Fuqing', 'CHN', 'Fujian');
INSERT INTO `databand_city` VALUES ('2233', 'Xinzhou', 'CHN', 'Shanxi');
INSERT INTO `databand_city` VALUES ('2234', 'Jieyang', 'CHN', 'Guangdong');
INSERT INTO `databand_city` VALUES ('2235', 'Zhangjiagang', 'CHN', 'Jiangsu');
INSERT INTO `databand_city` VALUES ('2236', 'Tong Xian', 'CHN', 'Peking');
INSERT INTO `databand_city` VALUES ('2237', 'Ya´an', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('2238', 'Jinzhou', 'CHN', 'Liaoning');
INSERT INTO `databand_city` VALUES ('2239', 'Emeishan', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('2240', 'Enshi', 'CHN', 'Hubei');
INSERT INTO `databand_city` VALUES ('2241', 'Bose', 'CHN', 'Guangxi');
INSERT INTO `databand_city` VALUES ('2242', 'Yuzhou', 'CHN', 'Henan');
INSERT INTO `databand_city` VALUES ('2243', 'Kaiyuan', 'CHN', 'Yunnan');
INSERT INTO `databand_city` VALUES ('2244', 'Tumen', 'CHN', 'Jilin');
INSERT INTO `databand_city` VALUES ('2245', 'Putian', 'CHN', 'Fujian');
INSERT INTO `databand_city` VALUES ('2246', 'Linhai', 'CHN', 'Zhejiang');
INSERT INTO `databand_city` VALUES ('2247', 'Xilin Hot', 'CHN', 'Inner Mongolia');
INSERT INTO `databand_city` VALUES ('2248', 'Shaowu', 'CHN', 'Fujian');
INSERT INTO `databand_city` VALUES ('2249', 'Junan', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2250', 'Huaying', 'CHN', 'Sichuan');
INSERT INTO `databand_city` VALUES ('2251', 'Pingyi', 'CHN', 'Shandong');
INSERT INTO `databand_city` VALUES ('2252', 'Huangyan', 'CHN', 'Zhejiang');
INSERT INTO `databand_city` VALUES ('2253', 'Bishkek', 'KGZ', 'Bishkek shaary');
INSERT INTO `databand_city` VALUES ('2254', 'Osh', 'KGZ', 'Osh');
INSERT INTO `databand_city` VALUES ('2255', 'Bikenibeu', 'KIR', 'South Tarawa');
INSERT INTO `databand_city` VALUES ('2256', 'Bairiki', 'KIR', 'South Tarawa');
INSERT INTO `databand_city` VALUES ('2257', 'Santafé de Bogotá', 'COL', 'Santafé de Bogotá');
INSERT INTO `databand_city` VALUES ('2258', 'Cali', 'COL', 'Valle');
INSERT INTO `databand_city` VALUES ('2259', 'Medellín', 'COL', 'Antioquia');
INSERT INTO `databand_city` VALUES ('2260', 'Barranquilla', 'COL', 'Atlántico');
INSERT INTO `databand_city` VALUES ('2261', 'Cartagena', 'COL', 'Bolívar');
INSERT INTO `databand_city` VALUES ('2262', 'Cúcuta', 'COL', 'Norte de Santander');
INSERT INTO `databand_city` VALUES ('2263', 'Bucaramanga', 'COL', 'Santander');
INSERT INTO `databand_city` VALUES ('2264', 'Ibagué', 'COL', 'Tolima');
INSERT INTO `databand_city` VALUES ('2265', 'Pereira', 'COL', 'Risaralda');
INSERT INTO `databand_city` VALUES ('2266', 'Santa Marta', 'COL', 'Magdalena');
INSERT INTO `databand_city` VALUES ('2267', 'Manizales', 'COL', 'Caldas');
INSERT INTO `databand_city` VALUES ('2268', 'Bello', 'COL', 'Antioquia');
INSERT INTO `databand_city` VALUES ('2269', 'Pasto', 'COL', 'Nariño');
INSERT INTO `databand_city` VALUES ('2270', 'Neiva', 'COL', 'Huila');
INSERT INTO `databand_city` VALUES ('2271', 'Soledad', 'COL', 'Atlántico');
INSERT INTO `databand_city` VALUES ('2272', 'Armenia', 'COL', 'Quindío');
INSERT INTO `databand_city` VALUES ('2273', 'Villavicencio', 'COL', 'Meta');
INSERT INTO `databand_city` VALUES ('2274', 'Soacha', 'COL', 'Cundinamarca');
INSERT INTO `databand_city` VALUES ('2275', 'Valledupar', 'COL', 'Cesar');
INSERT INTO `databand_city` VALUES ('2276', 'Montería', 'COL', 'Córdoba');
INSERT INTO `databand_city` VALUES ('2277', 'Itagüí', 'COL', 'Antioquia');
INSERT INTO `databand_city` VALUES ('2278', 'Palmira', 'COL', 'Valle');
INSERT INTO `databand_city` VALUES ('2279', 'Buenaventura', 'COL', 'Valle');
INSERT INTO `databand_city` VALUES ('2280', 'Floridablanca', 'COL', 'Santander');
INSERT INTO `databand_city` VALUES ('2281', 'Sincelejo', 'COL', 'Sucre');
INSERT INTO `databand_city` VALUES ('2282', 'Popayán', 'COL', 'Cauca');
INSERT INTO `databand_city` VALUES ('2283', 'Barrancabermeja', 'COL', 'Santander');
INSERT INTO `databand_city` VALUES ('2284', 'Dos Quebradas', 'COL', 'Risaralda');
INSERT INTO `databand_city` VALUES ('2285', 'Tuluá', 'COL', 'Valle');
INSERT INTO `databand_city` VALUES ('2286', 'Envigado', 'COL', 'Antioquia');
INSERT INTO `databand_city` VALUES ('2287', 'Cartago', 'COL', 'Valle');
INSERT INTO `databand_city` VALUES ('2288', 'Girardot', 'COL', 'Cundinamarca');
INSERT INTO `databand_city` VALUES ('2289', 'Buga', 'COL', 'Valle');
INSERT INTO `databand_city` VALUES ('2290', 'Tunja', 'COL', 'Boyacá');
INSERT INTO `databand_city` VALUES ('2291', 'Florencia', 'COL', 'Caquetá');
INSERT INTO `databand_city` VALUES ('2292', 'Maicao', 'COL', 'La Guajira');
INSERT INTO `databand_city` VALUES ('2293', 'Sogamoso', 'COL', 'Boyacá');
INSERT INTO `databand_city` VALUES ('2294', 'Giron', 'COL', 'Santander');
INSERT INTO `databand_city` VALUES ('2295', 'Moroni', 'COM', 'Njazidja');
INSERT INTO `databand_city` VALUES ('2296', 'Brazzaville', 'COG', 'Brazzaville');
INSERT INTO `databand_city` VALUES ('2297', 'Pointe-Noire', 'COG', 'Kouilou');
INSERT INTO `databand_city` VALUES ('2298', 'Kinshasa', 'COD', 'Kinshasa');
INSERT INTO `databand_city` VALUES ('2299', 'Lubumbashi', 'COD', 'Shaba');
INSERT INTO `databand_city` VALUES ('2300', 'Mbuji-Mayi', 'COD', 'East Kasai');
INSERT INTO `databand_city` VALUES ('2301', 'Kolwezi', 'COD', 'Shaba');
INSERT INTO `databand_city` VALUES ('2302', 'Kisangani', 'COD', 'Haute-Zaïre');
INSERT INTO `databand_city` VALUES ('2303', 'Kananga', 'COD', 'West Kasai');
INSERT INTO `databand_city` VALUES ('2304', 'Likasi', 'COD', 'Shaba');
INSERT INTO `databand_city` VALUES ('2305', 'Bukavu', 'COD', 'South Kivu');
INSERT INTO `databand_city` VALUES ('2306', 'Kikwit', 'COD', 'Bandundu');
INSERT INTO `databand_city` VALUES ('2307', 'Tshikapa', 'COD', 'West Kasai');
INSERT INTO `databand_city` VALUES ('2308', 'Matadi', 'COD', 'Bas-Zaïre');
INSERT INTO `databand_city` VALUES ('2309', 'Mbandaka', 'COD', 'Equateur');
INSERT INTO `databand_city` VALUES ('2310', 'Mwene-Ditu', 'COD', 'East Kasai');
INSERT INTO `databand_city` VALUES ('2311', 'Boma', 'COD', 'Bas-Zaïre');
INSERT INTO `databand_city` VALUES ('2312', 'Uvira', 'COD', 'South Kivu');
INSERT INTO `databand_city` VALUES ('2313', 'Butembo', 'COD', 'North Kivu');
INSERT INTO `databand_city` VALUES ('2314', 'Goma', 'COD', 'North Kivu');
INSERT INTO `databand_city` VALUES ('2315', 'Kalemie', 'COD', 'Shaba');
INSERT INTO `databand_city` VALUES ('2316', 'Bantam', 'CCK', 'Home Island');
INSERT INTO `databand_city` VALUES ('2317', 'West Island', 'CCK', 'West Island');
INSERT INTO `databand_city` VALUES ('2318', 'Pyongyang', 'PRK', 'Pyongyang-si');
INSERT INTO `databand_city` VALUES ('2319', 'Hamhung', 'PRK', 'Hamgyong N');
INSERT INTO `databand_city` VALUES ('2320', 'Chongjin', 'PRK', 'Hamgyong P');
INSERT INTO `databand_city` VALUES ('2321', 'Nampo', 'PRK', 'Nampo-si');
INSERT INTO `databand_city` VALUES ('2322', 'Sinuiju', 'PRK', 'Pyongan P');
INSERT INTO `databand_city` VALUES ('2323', 'Wonsan', 'PRK', 'Kangwon');
INSERT INTO `databand_city` VALUES ('2324', 'Phyongsong', 'PRK', 'Pyongan N');
INSERT INTO `databand_city` VALUES ('2325', 'Sariwon', 'PRK', 'Hwanghae P');
INSERT INTO `databand_city` VALUES ('2326', 'Haeju', 'PRK', 'Hwanghae N');
INSERT INTO `databand_city` VALUES ('2327', 'Kanggye', 'PRK', 'Chagang');
INSERT INTO `databand_city` VALUES ('2328', 'Kimchaek', 'PRK', 'Hamgyong P');
INSERT INTO `databand_city` VALUES ('2329', 'Hyesan', 'PRK', 'Yanggang');
INSERT INTO `databand_city` VALUES ('2330', 'Kaesong', 'PRK', 'Kaesong-si');
INSERT INTO `databand_city` VALUES ('2331', 'Seoul', 'KOR', 'Seoul');
INSERT INTO `databand_city` VALUES ('2332', 'Pusan', 'KOR', 'Pusan');
INSERT INTO `databand_city` VALUES ('2333', 'Inchon', 'KOR', 'Inchon');
INSERT INTO `databand_city` VALUES ('2334', 'Taegu', 'KOR', 'Taegu');
INSERT INTO `databand_city` VALUES ('2335', 'Taejon', 'KOR', 'Taejon');
INSERT INTO `databand_city` VALUES ('2336', 'Kwangju', 'KOR', 'Kwangju');
INSERT INTO `databand_city` VALUES ('2337', 'Ulsan', 'KOR', 'Kyongsangnam');
INSERT INTO `databand_city` VALUES ('2338', 'Songnam', 'KOR', 'Kyonggi');
INSERT INTO `databand_city` VALUES ('2339', 'Puchon', 'KOR', 'Kyonggi');
INSERT INTO `databand_city` VALUES ('2340', 'Suwon', 'KOR', 'Kyonggi');
INSERT INTO `databand_city` VALUES ('2341', 'Anyang', 'KOR', 'Kyonggi');
INSERT INTO `databand_city` VALUES ('2342', 'Chonju', 'KOR', 'Chollabuk');
INSERT INTO `databand_city` VALUES ('2343', 'Chongju', 'KOR', 'Chungchongbuk');
INSERT INTO `databand_city` VALUES ('2344', 'Koyang', 'KOR', 'Kyonggi');
INSERT INTO `databand_city` VALUES ('2345', 'Ansan', 'KOR', 'Kyonggi');
INSERT INTO `databand_city` VALUES ('2346', 'Pohang', 'KOR', 'Kyongsangbuk');
INSERT INTO `databand_city` VALUES ('2347', 'Chang-won', 'KOR', 'Kyongsangnam');
INSERT INTO `databand_city` VALUES ('2348', 'Masan', 'KOR', 'Kyongsangnam');
INSERT INTO `databand_city` VALUES ('2349', 'Kwangmyong', 'KOR', 'Kyonggi');
INSERT INTO `databand_city` VALUES ('2350', 'Chonan', 'KOR', 'Chungchongnam');
INSERT INTO `databand_city` VALUES ('2351', 'Chinju', 'KOR', 'Kyongsangnam');
INSERT INTO `databand_city` VALUES ('2352', 'Iksan', 'KOR', 'Chollabuk');
INSERT INTO `databand_city` VALUES ('2353', 'Pyongtaek', 'KOR', 'Kyonggi');
INSERT INTO `databand_city` VALUES ('2354', 'Kumi', 'KOR', 'Kyongsangbuk');
INSERT INTO `databand_city` VALUES ('2355', 'Uijongbu', 'KOR', 'Kyonggi');
INSERT INTO `databand_city` VALUES ('2356', 'Kyongju', 'KOR', 'Kyongsangbuk');
INSERT INTO `databand_city` VALUES ('2357', 'Kunsan', 'KOR', 'Chollabuk');
INSERT INTO `databand_city` VALUES ('2358', 'Cheju', 'KOR', 'Cheju');
INSERT INTO `databand_city` VALUES ('2359', 'Kimhae', 'KOR', 'Kyongsangnam');
INSERT INTO `databand_city` VALUES ('2360', 'Sunchon', 'KOR', 'Chollanam');
INSERT INTO `databand_city` VALUES ('2361', 'Mokpo', 'KOR', 'Chollanam');
INSERT INTO `databand_city` VALUES ('2362', 'Yong-in', 'KOR', 'Kyonggi');
INSERT INTO `databand_city` VALUES ('2363', 'Wonju', 'KOR', 'Kang-won');
INSERT INTO `databand_city` VALUES ('2364', 'Kunpo', 'KOR', 'Kyonggi');
INSERT INTO `databand_city` VALUES ('2365', 'Chunchon', 'KOR', 'Kang-won');
INSERT INTO `databand_city` VALUES ('2366', 'Namyangju', 'KOR', 'Kyonggi');
INSERT INTO `databand_city` VALUES ('2367', 'Kangnung', 'KOR', 'Kang-won');
INSERT INTO `databand_city` VALUES ('2368', 'Chungju', 'KOR', 'Chungchongbuk');
INSERT INTO `databand_city` VALUES ('2369', 'Andong', 'KOR', 'Kyongsangbuk');
INSERT INTO `databand_city` VALUES ('2370', 'Yosu', 'KOR', 'Chollanam');
INSERT INTO `databand_city` VALUES ('2371', 'Kyongsan', 'KOR', 'Kyongsangbuk');
INSERT INTO `databand_city` VALUES ('2372', 'Paju', 'KOR', 'Kyonggi');
INSERT INTO `databand_city` VALUES ('2373', 'Yangsan', 'KOR', 'Kyongsangnam');
INSERT INTO `databand_city` VALUES ('2374', 'Ichon', 'KOR', 'Kyonggi');
INSERT INTO `databand_city` VALUES ('2375', 'Asan', 'KOR', 'Chungchongnam');
INSERT INTO `databand_city` VALUES ('2376', 'Koje', 'KOR', 'Kyongsangnam');
INSERT INTO `databand_city` VALUES ('2377', 'Kimchon', 'KOR', 'Kyongsangbuk');
INSERT INTO `databand_city` VALUES ('2378', 'Nonsan', 'KOR', 'Chungchongnam');
INSERT INTO `databand_city` VALUES ('2379', 'Kuri', 'KOR', 'Kyonggi');
INSERT INTO `databand_city` VALUES ('2380', 'Chong-up', 'KOR', 'Chollabuk');
INSERT INTO `databand_city` VALUES ('2381', 'Chechon', 'KOR', 'Chungchongbuk');
INSERT INTO `databand_city` VALUES ('2382', 'Sosan', 'KOR', 'Chungchongnam');
INSERT INTO `databand_city` VALUES ('2383', 'Shihung', 'KOR', 'Kyonggi');
INSERT INTO `databand_city` VALUES ('2384', 'Tong-yong', 'KOR', 'Kyongsangnam');
INSERT INTO `databand_city` VALUES ('2385', 'Kongju', 'KOR', 'Chungchongnam');
INSERT INTO `databand_city` VALUES ('2386', 'Yongju', 'KOR', 'Kyongsangbuk');
INSERT INTO `databand_city` VALUES ('2387', 'Chinhae', 'KOR', 'Kyongsangnam');
INSERT INTO `databand_city` VALUES ('2388', 'Sangju', 'KOR', 'Kyongsangbuk');
INSERT INTO `databand_city` VALUES ('2389', 'Poryong', 'KOR', 'Chungchongnam');
INSERT INTO `databand_city` VALUES ('2390', 'Kwang-yang', 'KOR', 'Chollanam');
INSERT INTO `databand_city` VALUES ('2391', 'Miryang', 'KOR', 'Kyongsangnam');
INSERT INTO `databand_city` VALUES ('2392', 'Hanam', 'KOR', 'Kyonggi');
INSERT INTO `databand_city` VALUES ('2393', 'Kimje', 'KOR', 'Chollabuk');
INSERT INTO `databand_city` VALUES ('2394', 'Yongchon', 'KOR', 'Kyongsangbuk');
INSERT INTO `databand_city` VALUES ('2395', 'Sachon', 'KOR', 'Kyongsangnam');
INSERT INTO `databand_city` VALUES ('2396', 'Uiwang', 'KOR', 'Kyonggi');
INSERT INTO `databand_city` VALUES ('2397', 'Naju', 'KOR', 'Chollanam');
INSERT INTO `databand_city` VALUES ('2398', 'Namwon', 'KOR', 'Chollabuk');
INSERT INTO `databand_city` VALUES ('2399', 'Tonghae', 'KOR', 'Kang-won');
INSERT INTO `databand_city` VALUES ('2400', 'Mun-gyong', 'KOR', 'Kyongsangbuk');
INSERT INTO `databand_city` VALUES ('2401', 'Athenai', 'GRC', 'Attika');
INSERT INTO `databand_city` VALUES ('2402', 'Thessaloniki', 'GRC', 'Central Macedonia');
INSERT INTO `databand_city` VALUES ('2403', 'Pireus', 'GRC', 'Attika');
INSERT INTO `databand_city` VALUES ('2404', 'Patras', 'GRC', 'West Greece');
INSERT INTO `databand_city` VALUES ('2405', 'Peristerion', 'GRC', 'Attika');
INSERT INTO `databand_city` VALUES ('2406', 'Herakleion', 'GRC', 'Crete');
INSERT INTO `databand_city` VALUES ('2407', 'Kallithea', 'GRC', 'Attika');
INSERT INTO `databand_city` VALUES ('2408', 'Larisa', 'GRC', 'Thessalia');
INSERT INTO `databand_city` VALUES ('2409', 'Zagreb', 'HRV', 'Grad Zagreb');
INSERT INTO `databand_city` VALUES ('2410', 'Split', 'HRV', 'Split-Dalmatia');
INSERT INTO `databand_city` VALUES ('2411', 'Rijeka', 'HRV', 'Primorje-Gorski Kota');
INSERT INTO `databand_city` VALUES ('2412', 'Osijek', 'HRV', 'Osijek-Baranja');
INSERT INTO `databand_city` VALUES ('2413', 'La Habana', 'CUB', 'La Habana');
INSERT INTO `databand_city` VALUES ('2414', 'Santiago de Cuba', 'CUB', 'Santiago de Cuba');
INSERT INTO `databand_city` VALUES ('2415', 'Camagüey', 'CUB', 'Camagüey');
INSERT INTO `databand_city` VALUES ('2416', 'Holguín', 'CUB', 'Holguín');
INSERT INTO `databand_city` VALUES ('2417', 'Santa Clara', 'CUB', 'Villa Clara');
INSERT INTO `databand_city` VALUES ('2418', 'Guantánamo', 'CUB', 'Guantánamo');
INSERT INTO `databand_city` VALUES ('2419', 'Pinar del Río', 'CUB', 'Pinar del Río');
INSERT INTO `databand_city` VALUES ('2420', 'Bayamo', 'CUB', 'Granma');
INSERT INTO `databand_city` VALUES ('2421', 'Cienfuegos', 'CUB', 'Cienfuegos');
INSERT INTO `databand_city` VALUES ('2422', 'Victoria de las Tunas', 'CUB', 'Las Tunas');
INSERT INTO `databand_city` VALUES ('2423', 'Matanzas', 'CUB', 'Matanzas');
INSERT INTO `databand_city` VALUES ('2424', 'Manzanillo', 'CUB', 'Granma');
INSERT INTO `databand_city` VALUES ('2425', 'Sancti-Spíritus', 'CUB', 'Sancti-Spíritus');
INSERT INTO `databand_city` VALUES ('2426', 'Ciego de Ávila', 'CUB', 'Ciego de Ávila');
INSERT INTO `databand_city` VALUES ('2427', 'al-Salimiya', 'KWT', 'Hawalli');
INSERT INTO `databand_city` VALUES ('2428', 'Jalib al-Shuyukh', 'KWT', 'Hawalli');
INSERT INTO `databand_city` VALUES ('2429', 'Kuwait', 'KWT', 'al-Asima');
INSERT INTO `databand_city` VALUES ('2430', 'Nicosia', 'CYP', 'Nicosia');
INSERT INTO `databand_city` VALUES ('2431', 'Limassol', 'CYP', 'Limassol');
INSERT INTO `databand_city` VALUES ('2432', 'Vientiane', 'LAO', 'Viangchan');
INSERT INTO `databand_city` VALUES ('2433', 'Savannakhet', 'LAO', 'Savannakhet');
INSERT INTO `databand_city` VALUES ('2434', 'Riga', 'LVA', 'Riika');
INSERT INTO `databand_city` VALUES ('2435', 'Daugavpils', 'LVA', 'Daugavpils');
INSERT INTO `databand_city` VALUES ('2436', 'Liepaja', 'LVA', 'Liepaja');
INSERT INTO `databand_city` VALUES ('2437', 'Maseru', 'LSO', 'Maseru');
INSERT INTO `databand_city` VALUES ('2438', 'Beirut', 'LBN', 'Beirut');
INSERT INTO `databand_city` VALUES ('2439', 'Tripoli', 'LBN', 'al-Shamal');
INSERT INTO `databand_city` VALUES ('2440', 'Monrovia', 'LBR', 'Montserrado');
INSERT INTO `databand_city` VALUES ('2441', 'Tripoli', 'LBY', 'Tripoli');
INSERT INTO `databand_city` VALUES ('2442', 'Bengasi', 'LBY', 'Bengasi');
INSERT INTO `databand_city` VALUES ('2443', 'Misrata', 'LBY', 'Misrata');
INSERT INTO `databand_city` VALUES ('2444', 'al-Zawiya', 'LBY', 'al-Zawiya');
INSERT INTO `databand_city` VALUES ('2445', 'Schaan', 'LIE', 'Schaan');
INSERT INTO `databand_city` VALUES ('2446', 'Vaduz', 'LIE', 'Vaduz');
INSERT INTO `databand_city` VALUES ('2447', 'Vilnius', 'LTU', 'Vilna');
INSERT INTO `databand_city` VALUES ('2448', 'Kaunas', 'LTU', 'Kaunas');
INSERT INTO `databand_city` VALUES ('2449', 'Klaipeda', 'LTU', 'Klaipeda');
INSERT INTO `databand_city` VALUES ('2450', 'Šiauliai', 'LTU', 'Šiauliai');
INSERT INTO `databand_city` VALUES ('2451', 'Panevezys', 'LTU', 'Panevezys');
INSERT INTO `databand_city` VALUES ('2452', 'Luxembourg [Luxemburg/Lëtzebuerg]', 'LUX', 'Luxembourg');
INSERT INTO `databand_city` VALUES ('2453', 'El-Aaiún', 'ESH', 'El-Aaiún');
INSERT INTO `databand_city` VALUES ('2454', 'Macao', 'MAC', 'Macau');
INSERT INTO `databand_city` VALUES ('2455', 'Antananarivo', 'MDG', 'Antananarivo');
INSERT INTO `databand_city` VALUES ('2456', 'Toamasina', 'MDG', 'Toamasina');
INSERT INTO `databand_city` VALUES ('2457', 'Antsirabé', 'MDG', 'Antananarivo');
INSERT INTO `databand_city` VALUES ('2458', 'Mahajanga', 'MDG', 'Mahajanga');
INSERT INTO `databand_city` VALUES ('2459', 'Fianarantsoa', 'MDG', 'Fianarantsoa');
INSERT INTO `databand_city` VALUES ('2460', 'Skopje', 'MKD', 'Skopje');
INSERT INTO `databand_city` VALUES ('2461', 'Blantyre', 'MWI', 'Blantyre');
INSERT INTO `databand_city` VALUES ('2462', 'Lilongwe', 'MWI', 'Lilongwe');
INSERT INTO `databand_city` VALUES ('2463', 'Male', 'MDV', 'Maale');
INSERT INTO `databand_city` VALUES ('2464', 'Kuala Lumpur', 'MYS', 'Wilayah Persekutuan');
INSERT INTO `databand_city` VALUES ('2465', 'Ipoh', 'MYS', 'Perak');
INSERT INTO `databand_city` VALUES ('2466', 'Johor Baharu', 'MYS', 'Johor');
INSERT INTO `databand_city` VALUES ('2467', 'Petaling Jaya', 'MYS', 'Selangor');
INSERT INTO `databand_city` VALUES ('2468', 'Kelang', 'MYS', 'Selangor');
INSERT INTO `databand_city` VALUES ('2469', 'Kuala Terengganu', 'MYS', 'Terengganu');
INSERT INTO `databand_city` VALUES ('2470', 'Pinang', 'MYS', 'Pulau Pinang');
INSERT INTO `databand_city` VALUES ('2471', 'Kota Bharu', 'MYS', 'Kelantan');
INSERT INTO `databand_city` VALUES ('2472', 'Kuantan', 'MYS', 'Pahang');
INSERT INTO `databand_city` VALUES ('2473', 'Taiping', 'MYS', 'Perak');
INSERT INTO `databand_city` VALUES ('2474', 'Seremban', 'MYS', 'Negeri Sembilan');
INSERT INTO `databand_city` VALUES ('2475', 'Kuching', 'MYS', 'Sarawak');
INSERT INTO `databand_city` VALUES ('2476', 'Sibu', 'MYS', 'Sarawak');
INSERT INTO `databand_city` VALUES ('2477', 'Sandakan', 'MYS', 'Sabah');
INSERT INTO `databand_city` VALUES ('2478', 'Alor Setar', 'MYS', 'Kedah');
INSERT INTO `databand_city` VALUES ('2479', 'Selayang Baru', 'MYS', 'Selangor');
INSERT INTO `databand_city` VALUES ('2480', 'Sungai Petani', 'MYS', 'Kedah');
INSERT INTO `databand_city` VALUES ('2481', 'Shah Alam', 'MYS', 'Selangor');
INSERT INTO `databand_city` VALUES ('2482', 'Bamako', 'MLI', 'Bamako');
INSERT INTO `databand_city` VALUES ('2483', 'Birkirkara', 'MLT', 'Outer Harbour');
INSERT INTO `databand_city` VALUES ('2484', 'Valletta', 'MLT', 'Inner Harbour');
INSERT INTO `databand_city` VALUES ('2485', 'Casablanca', 'MAR', 'Casablanca');
INSERT INTO `databand_city` VALUES ('2486', 'Rabat', 'MAR', 'Rabat-Salé-Zammour-Z');
INSERT INTO `databand_city` VALUES ('2487', 'Marrakech', 'MAR', 'Marrakech-Tensift-Al');
INSERT INTO `databand_city` VALUES ('2488', 'Fès', 'MAR', 'Fès-Boulemane');
INSERT INTO `databand_city` VALUES ('2489', 'Tanger', 'MAR', 'Tanger-Tétouan');
INSERT INTO `databand_city` VALUES ('2490', 'Salé', 'MAR', 'Rabat-Salé-Zammour-Z');
INSERT INTO `databand_city` VALUES ('2491', 'Meknès', 'MAR', 'Meknès-Tafilalet');
INSERT INTO `databand_city` VALUES ('2492', 'Oujda', 'MAR', 'Oriental');
INSERT INTO `databand_city` VALUES ('2493', 'Kénitra', 'MAR', 'Gharb-Chrarda-Béni H');
INSERT INTO `databand_city` VALUES ('2494', 'Tétouan', 'MAR', 'Tanger-Tétouan');
INSERT INTO `databand_city` VALUES ('2495', 'Safi', 'MAR', 'Doukkala-Abda');
INSERT INTO `databand_city` VALUES ('2496', 'Agadir', 'MAR', 'Souss Massa-Draâ');
INSERT INTO `databand_city` VALUES ('2497', 'Mohammedia', 'MAR', 'Casablanca');
INSERT INTO `databand_city` VALUES ('2498', 'Khouribga', 'MAR', 'Chaouia-Ouardigha');
INSERT INTO `databand_city` VALUES ('2499', 'Beni-Mellal', 'MAR', 'Tadla-Azilal');
INSERT INTO `databand_city` VALUES ('2500', 'Témara', 'MAR', 'Rabat-Salé-Zammour-Z');
INSERT INTO `databand_city` VALUES ('2501', 'El Jadida', 'MAR', 'Doukkala-Abda');
INSERT INTO `databand_city` VALUES ('2502', 'Nador', 'MAR', 'Oriental');
INSERT INTO `databand_city` VALUES ('2503', 'Ksar el Kebir', 'MAR', 'Tanger-Tétouan');
INSERT INTO `databand_city` VALUES ('2504', 'Settat', 'MAR', 'Chaouia-Ouardigha');
INSERT INTO `databand_city` VALUES ('2505', 'Taza', 'MAR', 'Taza-Al Hoceima-Taou');
INSERT INTO `databand_city` VALUES ('2506', 'El Araich', 'MAR', 'Tanger-Tétouan');
INSERT INTO `databand_city` VALUES ('2507', 'Dalap-Uliga-Darrit', 'MHL', 'Majuro');
INSERT INTO `databand_city` VALUES ('2508', 'Fort-de-France', 'MTQ', 'Fort-de-France');
INSERT INTO `databand_city` VALUES ('2509', 'Nouakchott', 'MRT', 'Nouakchott');
INSERT INTO `databand_city` VALUES ('2510', 'Nouâdhibou', 'MRT', 'Dakhlet Nouâdhibou');
INSERT INTO `databand_city` VALUES ('2511', 'Port-Louis', 'MUS', 'Port-Louis');
INSERT INTO `databand_city` VALUES ('2512', 'Beau Bassin-Rose Hill', 'MUS', 'Plaines Wilhelms');
INSERT INTO `databand_city` VALUES ('2513', 'Vacoas-Phoenix', 'MUS', 'Plaines Wilhelms');
INSERT INTO `databand_city` VALUES ('2514', 'Mamoutzou', 'MYT', 'Mamoutzou');
INSERT INTO `databand_city` VALUES ('2515', 'Ciudad de México', 'MEX', 'Distrito Federal');
INSERT INTO `databand_city` VALUES ('2516', 'Guadalajara', 'MEX', 'Jalisco');
INSERT INTO `databand_city` VALUES ('2517', 'Ecatepec de Morelos', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2518', 'Puebla', 'MEX', 'Puebla');
INSERT INTO `databand_city` VALUES ('2519', 'Nezahualcóyotl', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2520', 'Juárez', 'MEX', 'Chihuahua');
INSERT INTO `databand_city` VALUES ('2521', 'Tijuana', 'MEX', 'Baja California');
INSERT INTO `databand_city` VALUES ('2522', 'León', 'MEX', 'Guanajuato');
INSERT INTO `databand_city` VALUES ('2523', 'Monterrey', 'MEX', 'Nuevo León');
INSERT INTO `databand_city` VALUES ('2524', 'Zapopan', 'MEX', 'Jalisco');
INSERT INTO `databand_city` VALUES ('2525', 'Naucalpan de Juárez', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2526', 'Mexicali', 'MEX', 'Baja California');
INSERT INTO `databand_city` VALUES ('2527', 'Culiacán', 'MEX', 'Sinaloa');
INSERT INTO `databand_city` VALUES ('2528', 'Acapulco de Juárez', 'MEX', 'Guerrero');
INSERT INTO `databand_city` VALUES ('2529', 'Tlalnepantla de Baz', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2530', 'Mérida', 'MEX', 'Yucatán');
INSERT INTO `databand_city` VALUES ('2531', 'Chihuahua', 'MEX', 'Chihuahua');
INSERT INTO `databand_city` VALUES ('2532', 'San Luis Potosí', 'MEX', 'San Luis Potosí');
INSERT INTO `databand_city` VALUES ('2533', 'Guadalupe', 'MEX', 'Nuevo León');
INSERT INTO `databand_city` VALUES ('2534', 'Toluca', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2535', 'Aguascalientes', 'MEX', 'Aguascalientes');
INSERT INTO `databand_city` VALUES ('2536', 'Querétaro', 'MEX', 'Querétaro de Arteaga');
INSERT INTO `databand_city` VALUES ('2537', 'Morelia', 'MEX', 'Michoacán de Ocampo');
INSERT INTO `databand_city` VALUES ('2538', 'Hermosillo', 'MEX', 'Sonora');
INSERT INTO `databand_city` VALUES ('2539', 'Saltillo', 'MEX', 'Coahuila de Zaragoza');
INSERT INTO `databand_city` VALUES ('2540', 'Torreón', 'MEX', 'Coahuila de Zaragoza');
INSERT INTO `databand_city` VALUES ('2541', 'Centro (Villahermosa)', 'MEX', 'Tabasco');
INSERT INTO `databand_city` VALUES ('2542', 'San Nicolás de los Garza', 'MEX', 'Nuevo León');
INSERT INTO `databand_city` VALUES ('2543', 'Durango', 'MEX', 'Durango');
INSERT INTO `databand_city` VALUES ('2544', 'Chimalhuacán', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2545', 'Tlaquepaque', 'MEX', 'Jalisco');
INSERT INTO `databand_city` VALUES ('2546', 'Atizapán de Zaragoza', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2547', 'Veracruz', 'MEX', 'Veracruz');
INSERT INTO `databand_city` VALUES ('2548', 'Cuautitlán Izcalli', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2549', 'Irapuato', 'MEX', 'Guanajuato');
INSERT INTO `databand_city` VALUES ('2550', 'Tuxtla Gutiérrez', 'MEX', 'Chiapas');
INSERT INTO `databand_city` VALUES ('2551', 'Tultitlán', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2552', 'Reynosa', 'MEX', 'Tamaulipas');
INSERT INTO `databand_city` VALUES ('2553', 'Benito Juárez', 'MEX', 'Quintana Roo');
INSERT INTO `databand_city` VALUES ('2554', 'Matamoros', 'MEX', 'Tamaulipas');
INSERT INTO `databand_city` VALUES ('2555', 'Xalapa', 'MEX', 'Veracruz');
INSERT INTO `databand_city` VALUES ('2556', 'Celaya', 'MEX', 'Guanajuato');
INSERT INTO `databand_city` VALUES ('2557', 'Mazatlán', 'MEX', 'Sinaloa');
INSERT INTO `databand_city` VALUES ('2558', 'Ensenada', 'MEX', 'Baja California');
INSERT INTO `databand_city` VALUES ('2559', 'Ahome', 'MEX', 'Sinaloa');
INSERT INTO `databand_city` VALUES ('2560', 'Cajeme', 'MEX', 'Sonora');
INSERT INTO `databand_city` VALUES ('2561', 'Cuernavaca', 'MEX', 'Morelos');
INSERT INTO `databand_city` VALUES ('2562', 'Tonalá', 'MEX', 'Jalisco');
INSERT INTO `databand_city` VALUES ('2563', 'Valle de Chalco Solidaridad', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2564', 'Nuevo Laredo', 'MEX', 'Tamaulipas');
INSERT INTO `databand_city` VALUES ('2565', 'Tepic', 'MEX', 'Nayarit');
INSERT INTO `databand_city` VALUES ('2566', 'Tampico', 'MEX', 'Tamaulipas');
INSERT INTO `databand_city` VALUES ('2567', 'Ixtapaluca', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2568', 'Apodaca', 'MEX', 'Nuevo León');
INSERT INTO `databand_city` VALUES ('2569', 'Guasave', 'MEX', 'Sinaloa');
INSERT INTO `databand_city` VALUES ('2570', 'Gómez Palacio', 'MEX', 'Durango');
INSERT INTO `databand_city` VALUES ('2571', 'Tapachula', 'MEX', 'Chiapas');
INSERT INTO `databand_city` VALUES ('2572', 'Nicolás Romero', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2573', 'Coatzacoalcos', 'MEX', 'Veracruz');
INSERT INTO `databand_city` VALUES ('2574', 'Uruapan', 'MEX', 'Michoacán de Ocampo');
INSERT INTO `databand_city` VALUES ('2575', 'Victoria', 'MEX', 'Tamaulipas');
INSERT INTO `databand_city` VALUES ('2576', 'Oaxaca de Juárez', 'MEX', 'Oaxaca');
INSERT INTO `databand_city` VALUES ('2577', 'Coacalco de Berriozábal', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2578', 'Pachuca de Soto', 'MEX', 'Hidalgo');
INSERT INTO `databand_city` VALUES ('2579', 'General Escobedo', 'MEX', 'Nuevo León');
INSERT INTO `databand_city` VALUES ('2580', 'Salamanca', 'MEX', 'Guanajuato');
INSERT INTO `databand_city` VALUES ('2581', 'Santa Catarina', 'MEX', 'Nuevo León');
INSERT INTO `databand_city` VALUES ('2582', 'Tehuacán', 'MEX', 'Puebla');
INSERT INTO `databand_city` VALUES ('2583', 'Chalco', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2584', 'Cárdenas', 'MEX', 'Tabasco');
INSERT INTO `databand_city` VALUES ('2585', 'Campeche', 'MEX', 'Campeche');
INSERT INTO `databand_city` VALUES ('2586', 'La Paz', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2587', 'Othón P. Blanco (Chetumal)', 'MEX', 'Quintana Roo');
INSERT INTO `databand_city` VALUES ('2588', 'Texcoco', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2589', 'La Paz', 'MEX', 'Baja California Sur');
INSERT INTO `databand_city` VALUES ('2590', 'Metepec', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2591', 'Monclova', 'MEX', 'Coahuila de Zaragoza');
INSERT INTO `databand_city` VALUES ('2592', 'Huixquilucan', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2593', 'Chilpancingo de los Bravo', 'MEX', 'Guerrero');
INSERT INTO `databand_city` VALUES ('2594', 'Puerto Vallarta', 'MEX', 'Jalisco');
INSERT INTO `databand_city` VALUES ('2595', 'Fresnillo', 'MEX', 'Zacatecas');
INSERT INTO `databand_city` VALUES ('2596', 'Ciudad Madero', 'MEX', 'Tamaulipas');
INSERT INTO `databand_city` VALUES ('2597', 'Soledad de Graciano Sánchez', 'MEX', 'San Luis Potosí');
INSERT INTO `databand_city` VALUES ('2598', 'San Juan del Río', 'MEX', 'Querétaro');
INSERT INTO `databand_city` VALUES ('2599', 'San Felipe del Progreso', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2600', 'Córdoba', 'MEX', 'Veracruz');
INSERT INTO `databand_city` VALUES ('2601', 'Tecámac', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2602', 'Ocosingo', 'MEX', 'Chiapas');
INSERT INTO `databand_city` VALUES ('2603', 'Carmen', 'MEX', 'Campeche');
INSERT INTO `databand_city` VALUES ('2604', 'Lázaro Cárdenas', 'MEX', 'Michoacán de Ocampo');
INSERT INTO `databand_city` VALUES ('2605', 'Jiutepec', 'MEX', 'Morelos');
INSERT INTO `databand_city` VALUES ('2606', 'Papantla', 'MEX', 'Veracruz');
INSERT INTO `databand_city` VALUES ('2607', 'Comalcalco', 'MEX', 'Tabasco');
INSERT INTO `databand_city` VALUES ('2608', 'Zamora', 'MEX', 'Michoacán de Ocampo');
INSERT INTO `databand_city` VALUES ('2609', 'Nogales', 'MEX', 'Sonora');
INSERT INTO `databand_city` VALUES ('2610', 'Huimanguillo', 'MEX', 'Tabasco');
INSERT INTO `databand_city` VALUES ('2611', 'Cuautla', 'MEX', 'Morelos');
INSERT INTO `databand_city` VALUES ('2612', 'Minatitlán', 'MEX', 'Veracruz');
INSERT INTO `databand_city` VALUES ('2613', 'Poza Rica de Hidalgo', 'MEX', 'Veracruz');
INSERT INTO `databand_city` VALUES ('2614', 'Ciudad Valles', 'MEX', 'San Luis Potosí');
INSERT INTO `databand_city` VALUES ('2615', 'Navolato', 'MEX', 'Sinaloa');
INSERT INTO `databand_city` VALUES ('2616', 'San Luis Río Colorado', 'MEX', 'Sonora');
INSERT INTO `databand_city` VALUES ('2617', 'Pénjamo', 'MEX', 'Guanajuato');
INSERT INTO `databand_city` VALUES ('2618', 'San Andrés Tuxtla', 'MEX', 'Veracruz');
INSERT INTO `databand_city` VALUES ('2619', 'Guanajuato', 'MEX', 'Guanajuato');
INSERT INTO `databand_city` VALUES ('2620', 'Navojoa', 'MEX', 'Sonora');
INSERT INTO `databand_city` VALUES ('2621', 'Zitácuaro', 'MEX', 'Michoacán de Ocampo');
INSERT INTO `databand_city` VALUES ('2622', 'Boca del Río', 'MEX', 'Veracruz-Llave');
INSERT INTO `databand_city` VALUES ('2623', 'Allende', 'MEX', 'Guanajuato');
INSERT INTO `databand_city` VALUES ('2624', 'Silao', 'MEX', 'Guanajuato');
INSERT INTO `databand_city` VALUES ('2625', 'Macuspana', 'MEX', 'Tabasco');
INSERT INTO `databand_city` VALUES ('2626', 'San Juan Bautista Tuxtepec', 'MEX', 'Oaxaca');
INSERT INTO `databand_city` VALUES ('2627', 'San Cristóbal de las Casas', 'MEX', 'Chiapas');
INSERT INTO `databand_city` VALUES ('2628', 'Valle de Santiago', 'MEX', 'Guanajuato');
INSERT INTO `databand_city` VALUES ('2629', 'Guaymas', 'MEX', 'Sonora');
INSERT INTO `databand_city` VALUES ('2630', 'Colima', 'MEX', 'Colima');
INSERT INTO `databand_city` VALUES ('2631', 'Dolores Hidalgo', 'MEX', 'Guanajuato');
INSERT INTO `databand_city` VALUES ('2632', 'Lagos de Moreno', 'MEX', 'Jalisco');
INSERT INTO `databand_city` VALUES ('2633', 'Piedras Negras', 'MEX', 'Coahuila de Zaragoza');
INSERT INTO `databand_city` VALUES ('2634', 'Altamira', 'MEX', 'Tamaulipas');
INSERT INTO `databand_city` VALUES ('2635', 'Túxpam', 'MEX', 'Veracruz');
INSERT INTO `databand_city` VALUES ('2636', 'San Pedro Garza García', 'MEX', 'Nuevo León');
INSERT INTO `databand_city` VALUES ('2637', 'Cuauhtémoc', 'MEX', 'Chihuahua');
INSERT INTO `databand_city` VALUES ('2638', 'Manzanillo', 'MEX', 'Colima');
INSERT INTO `databand_city` VALUES ('2639', 'Iguala de la Independencia', 'MEX', 'Guerrero');
INSERT INTO `databand_city` VALUES ('2640', 'Zacatecas', 'MEX', 'Zacatecas');
INSERT INTO `databand_city` VALUES ('2641', 'Tlajomulco de Zúñiga', 'MEX', 'Jalisco');
INSERT INTO `databand_city` VALUES ('2642', 'Tulancingo de Bravo', 'MEX', 'Hidalgo');
INSERT INTO `databand_city` VALUES ('2643', 'Zinacantepec', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2644', 'San Martín Texmelucan', 'MEX', 'Puebla');
INSERT INTO `databand_city` VALUES ('2645', 'Tepatitlán de Morelos', 'MEX', 'Jalisco');
INSERT INTO `databand_city` VALUES ('2646', 'Martínez de la Torre', 'MEX', 'Veracruz');
INSERT INTO `databand_city` VALUES ('2647', 'Orizaba', 'MEX', 'Veracruz');
INSERT INTO `databand_city` VALUES ('2648', 'Apatzingán', 'MEX', 'Michoacán de Ocampo');
INSERT INTO `databand_city` VALUES ('2649', 'Atlixco', 'MEX', 'Puebla');
INSERT INTO `databand_city` VALUES ('2650', 'Delicias', 'MEX', 'Chihuahua');
INSERT INTO `databand_city` VALUES ('2651', 'Ixtlahuaca', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2652', 'El Mante', 'MEX', 'Tamaulipas');
INSERT INTO `databand_city` VALUES ('2653', 'Lerdo', 'MEX', 'Durango');
INSERT INTO `databand_city` VALUES ('2654', 'Almoloya de Juárez', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2655', 'Acámbaro', 'MEX', 'Guanajuato');
INSERT INTO `databand_city` VALUES ('2656', 'Acuña', 'MEX', 'Coahuila de Zaragoza');
INSERT INTO `databand_city` VALUES ('2657', 'Guadalupe', 'MEX', 'Zacatecas');
INSERT INTO `databand_city` VALUES ('2658', 'Huejutla de Reyes', 'MEX', 'Hidalgo');
INSERT INTO `databand_city` VALUES ('2659', 'Hidalgo', 'MEX', 'Michoacán de Ocampo');
INSERT INTO `databand_city` VALUES ('2660', 'Los Cabos', 'MEX', 'Baja California Sur');
INSERT INTO `databand_city` VALUES ('2661', 'Comitán de Domínguez', 'MEX', 'Chiapas');
INSERT INTO `databand_city` VALUES ('2662', 'Cunduacán', 'MEX', 'Tabasco');
INSERT INTO `databand_city` VALUES ('2663', 'Río Bravo', 'MEX', 'Tamaulipas');
INSERT INTO `databand_city` VALUES ('2664', 'Temapache', 'MEX', 'Veracruz');
INSERT INTO `databand_city` VALUES ('2665', 'Chilapa de Alvarez', 'MEX', 'Guerrero');
INSERT INTO `databand_city` VALUES ('2666', 'Hidalgo del Parral', 'MEX', 'Chihuahua');
INSERT INTO `databand_city` VALUES ('2667', 'San Francisco del Rincón', 'MEX', 'Guanajuato');
INSERT INTO `databand_city` VALUES ('2668', 'Taxco de Alarcón', 'MEX', 'Guerrero');
INSERT INTO `databand_city` VALUES ('2669', 'Zumpango', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2670', 'San Pedro Cholula', 'MEX', 'Puebla');
INSERT INTO `databand_city` VALUES ('2671', 'Lerma', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2672', 'Tecomán', 'MEX', 'Colima');
INSERT INTO `databand_city` VALUES ('2673', 'Las Margaritas', 'MEX', 'Chiapas');
INSERT INTO `databand_city` VALUES ('2674', 'Cosoleacaque', 'MEX', 'Veracruz');
INSERT INTO `databand_city` VALUES ('2675', 'San Luis de la Paz', 'MEX', 'Guanajuato');
INSERT INTO `databand_city` VALUES ('2676', 'José Azueta', 'MEX', 'Guerrero');
INSERT INTO `databand_city` VALUES ('2677', 'Santiago Ixcuintla', 'MEX', 'Nayarit');
INSERT INTO `databand_city` VALUES ('2678', 'San Felipe', 'MEX', 'Guanajuato');
INSERT INTO `databand_city` VALUES ('2679', 'Tejupilco', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2680', 'Tantoyuca', 'MEX', 'Veracruz');
INSERT INTO `databand_city` VALUES ('2681', 'Salvatierra', 'MEX', 'Guanajuato');
INSERT INTO `databand_city` VALUES ('2682', 'Tultepec', 'MEX', 'México');
INSERT INTO `databand_city` VALUES ('2683', 'Temixco', 'MEX', 'Morelos');
INSERT INTO `databand_city` VALUES ('2684', 'Matamoros', 'MEX', 'Coahuila de Zaragoza');
INSERT INTO `databand_city` VALUES ('2685', 'Pánuco', 'MEX', 'Veracruz');
INSERT INTO `databand_city` VALUES ('2686', 'El Fuerte', 'MEX', 'Sinaloa');
INSERT INTO `databand_city` VALUES ('2687', 'Tierra Blanca', 'MEX', 'Veracruz');
INSERT INTO `databand_city` VALUES ('2688', 'Weno', 'FSM', 'Chuuk');
INSERT INTO `databand_city` VALUES ('2689', 'Palikir', 'FSM', 'Pohnpei');
INSERT INTO `databand_city` VALUES ('2690', 'Chisinau', 'MDA', 'Chisinau');
INSERT INTO `databand_city` VALUES ('2691', 'Tiraspol', 'MDA', 'Dnjestria');
INSERT INTO `databand_city` VALUES ('2692', 'Balti', 'MDA', 'Balti');
INSERT INTO `databand_city` VALUES ('2693', 'Bender (Tîghina)', 'MDA', 'Bender (Tîghina)');
INSERT INTO `databand_city` VALUES ('2694', 'Monte-Carlo', 'MCO', '–');
INSERT INTO `databand_city` VALUES ('2695', 'Monaco-Ville', 'MCO', '–');
INSERT INTO `databand_city` VALUES ('2696', 'Ulan Bator', 'MNG', 'Ulaanbaatar');
INSERT INTO `databand_city` VALUES ('2697', 'Plymouth', 'MSR', 'Plymouth');
INSERT INTO `databand_city` VALUES ('2698', 'Maputo', 'MOZ', 'Maputo');
INSERT INTO `databand_city` VALUES ('2699', 'Matola', 'MOZ', 'Maputo');
INSERT INTO `databand_city` VALUES ('2700', 'Beira', 'MOZ', 'Sofala');
INSERT INTO `databand_city` VALUES ('2701', 'Nampula', 'MOZ', 'Nampula');
INSERT INTO `databand_city` VALUES ('2702', 'Chimoio', 'MOZ', 'Manica');
INSERT INTO `databand_city` VALUES ('2703', 'Naçala-Porto', 'MOZ', 'Nampula');
INSERT INTO `databand_city` VALUES ('2704', 'Quelimane', 'MOZ', 'Zambézia');
INSERT INTO `databand_city` VALUES ('2705', 'Mocuba', 'MOZ', 'Zambézia');
INSERT INTO `databand_city` VALUES ('2706', 'Tete', 'MOZ', 'Tete');
INSERT INTO `databand_city` VALUES ('2707', 'Xai-Xai', 'MOZ', 'Gaza');
INSERT INTO `databand_city` VALUES ('2708', 'Gurue', 'MOZ', 'Zambézia');
INSERT INTO `databand_city` VALUES ('2709', 'Maxixe', 'MOZ', 'Inhambane');
INSERT INTO `databand_city` VALUES ('2710', 'Rangoon (Yangon)', 'MMR', 'Rangoon [Yangon]');
INSERT INTO `databand_city` VALUES ('2711', 'Mandalay', 'MMR', 'Mandalay');
INSERT INTO `databand_city` VALUES ('2712', 'Moulmein (Mawlamyine)', 'MMR', 'Mon');
INSERT INTO `databand_city` VALUES ('2713', 'Pegu (Bago)', 'MMR', 'Pegu [Bago]');
INSERT INTO `databand_city` VALUES ('2714', 'Bassein (Pathein)', 'MMR', 'Irrawaddy [Ayeyarwad');
INSERT INTO `databand_city` VALUES ('2715', 'Monywa', 'MMR', 'Sagaing');
INSERT INTO `databand_city` VALUES ('2716', 'Sittwe (Akyab)', 'MMR', 'Rakhine');
INSERT INTO `databand_city` VALUES ('2717', 'Taunggyi (Taunggye)', 'MMR', 'Shan');
INSERT INTO `databand_city` VALUES ('2718', 'Meikhtila', 'MMR', 'Mandalay');
INSERT INTO `databand_city` VALUES ('2719', 'Mergui (Myeik)', 'MMR', 'Tenasserim [Tanintha');
INSERT INTO `databand_city` VALUES ('2720', 'Lashio (Lasho)', 'MMR', 'Shan');
INSERT INTO `databand_city` VALUES ('2721', 'Prome (Pyay)', 'MMR', 'Pegu [Bago]');
INSERT INTO `databand_city` VALUES ('2722', 'Henzada (Hinthada)', 'MMR', 'Irrawaddy [Ayeyarwad');
INSERT INTO `databand_city` VALUES ('2723', 'Myingyan', 'MMR', 'Mandalay');
INSERT INTO `databand_city` VALUES ('2724', 'Tavoy (Dawei)', 'MMR', 'Tenasserim [Tanintha');
INSERT INTO `databand_city` VALUES ('2725', 'Pagakku (Pakokku)', 'MMR', 'Magwe [Magway]');
INSERT INTO `databand_city` VALUES ('2726', 'Windhoek', 'NAM', 'Khomas');
INSERT INTO `databand_city` VALUES ('2727', 'Yangor', 'NRU', '–');
INSERT INTO `databand_city` VALUES ('2728', 'Yaren', 'NRU', '–');
INSERT INTO `databand_city` VALUES ('2729', 'Kathmandu', 'NPL', 'Central');
INSERT INTO `databand_city` VALUES ('2730', 'Biratnagar', 'NPL', 'Eastern');
INSERT INTO `databand_city` VALUES ('2731', 'Pokhara', 'NPL', 'Western');
INSERT INTO `databand_city` VALUES ('2732', 'Lalitapur', 'NPL', 'Central');
INSERT INTO `databand_city` VALUES ('2733', 'Birgunj', 'NPL', 'Central');
INSERT INTO `databand_city` VALUES ('2734', 'Managua', 'NIC', 'Managua');
INSERT INTO `databand_city` VALUES ('2735', 'León', 'NIC', 'León');
INSERT INTO `databand_city` VALUES ('2736', 'Chinandega', 'NIC', 'Chinandega');
INSERT INTO `databand_city` VALUES ('2737', 'Masaya', 'NIC', 'Masaya');
INSERT INTO `databand_city` VALUES ('2738', 'Niamey', 'NER', 'Niamey');
INSERT INTO `databand_city` VALUES ('2739', 'Zinder', 'NER', 'Zinder');
INSERT INTO `databand_city` VALUES ('2740', 'Maradi', 'NER', 'Maradi');
INSERT INTO `databand_city` VALUES ('2741', 'Lagos', 'NGA', 'Lagos');
INSERT INTO `databand_city` VALUES ('2742', 'Ibadan', 'NGA', 'Oyo & Osun');
INSERT INTO `databand_city` VALUES ('2743', 'Ogbomosho', 'NGA', 'Oyo & Osun');
INSERT INTO `databand_city` VALUES ('2744', 'Kano', 'NGA', 'Kano & Jigawa');
INSERT INTO `databand_city` VALUES ('2745', 'Oshogbo', 'NGA', 'Oyo & Osun');
INSERT INTO `databand_city` VALUES ('2746', 'Ilorin', 'NGA', 'Kwara & Kogi');
INSERT INTO `databand_city` VALUES ('2747', 'Abeokuta', 'NGA', 'Ogun');
INSERT INTO `databand_city` VALUES ('2748', 'Port Harcourt', 'NGA', 'Rivers & Bayelsa');
INSERT INTO `databand_city` VALUES ('2749', 'Zaria', 'NGA', 'Kaduna');
INSERT INTO `databand_city` VALUES ('2750', 'Ilesha', 'NGA', 'Oyo & Osun');
INSERT INTO `databand_city` VALUES ('2751', 'Onitsha', 'NGA', 'Anambra & Enugu & Eb');
INSERT INTO `databand_city` VALUES ('2752', 'Iwo', 'NGA', 'Oyo & Osun');
INSERT INTO `databand_city` VALUES ('2753', 'Ado-Ekiti', 'NGA', 'Ondo & Ekiti');
INSERT INTO `databand_city` VALUES ('2754', 'Abuja', 'NGA', 'Federal Capital Dist');
INSERT INTO `databand_city` VALUES ('2755', 'Kaduna', 'NGA', 'Kaduna');
INSERT INTO `databand_city` VALUES ('2756', 'Mushin', 'NGA', 'Lagos');
INSERT INTO `databand_city` VALUES ('2757', 'Maiduguri', 'NGA', 'Borno & Yobe');
INSERT INTO `databand_city` VALUES ('2758', 'Enugu', 'NGA', 'Anambra & Enugu & Eb');
INSERT INTO `databand_city` VALUES ('2759', 'Ede', 'NGA', 'Oyo & Osun');
INSERT INTO `databand_city` VALUES ('2760', 'Aba', 'NGA', 'Imo & Abia');
INSERT INTO `databand_city` VALUES ('2761', 'Ife', 'NGA', 'Oyo & Osun');
INSERT INTO `databand_city` VALUES ('2762', 'Ila', 'NGA', 'Oyo & Osun');
INSERT INTO `databand_city` VALUES ('2763', 'Oyo', 'NGA', 'Oyo & Osun');
INSERT INTO `databand_city` VALUES ('2764', 'Ikerre', 'NGA', 'Ondo & Ekiti');
INSERT INTO `databand_city` VALUES ('2765', 'Benin City', 'NGA', 'Edo & Delta');
INSERT INTO `databand_city` VALUES ('2766', 'Iseyin', 'NGA', 'Oyo & Osun');
INSERT INTO `databand_city` VALUES ('2767', 'Katsina', 'NGA', 'Katsina');
INSERT INTO `databand_city` VALUES ('2768', 'Jos', 'NGA', 'Plateau & Nassarawa');
INSERT INTO `databand_city` VALUES ('2769', 'Sokoto', 'NGA', 'Sokoto & Kebbi & Zam');
INSERT INTO `databand_city` VALUES ('2770', 'Ilobu', 'NGA', 'Oyo & Osun');
INSERT INTO `databand_city` VALUES ('2771', 'Offa', 'NGA', 'Kwara & Kogi');
INSERT INTO `databand_city` VALUES ('2772', 'Ikorodu', 'NGA', 'Lagos');
INSERT INTO `databand_city` VALUES ('2773', 'Ilawe-Ekiti', 'NGA', 'Ondo & Ekiti');
INSERT INTO `databand_city` VALUES ('2774', 'Owo', 'NGA', 'Ondo & Ekiti');
INSERT INTO `databand_city` VALUES ('2775', 'Ikirun', 'NGA', 'Oyo & Osun');
INSERT INTO `databand_city` VALUES ('2776', 'Shaki', 'NGA', 'Oyo & Osun');
INSERT INTO `databand_city` VALUES ('2777', 'Calabar', 'NGA', 'Cross River');
INSERT INTO `databand_city` VALUES ('2778', 'Ondo', 'NGA', 'Ondo & Ekiti');
INSERT INTO `databand_city` VALUES ('2779', 'Akure', 'NGA', 'Ondo & Ekiti');
INSERT INTO `databand_city` VALUES ('2780', 'Gusau', 'NGA', 'Sokoto & Kebbi & Zam');
INSERT INTO `databand_city` VALUES ('2781', 'Ijebu-Ode', 'NGA', 'Ogun');
INSERT INTO `databand_city` VALUES ('2782', 'Effon-Alaiye', 'NGA', 'Oyo & Osun');
INSERT INTO `databand_city` VALUES ('2783', 'Kumo', 'NGA', 'Bauchi & Gombe');
INSERT INTO `databand_city` VALUES ('2784', 'Shomolu', 'NGA', 'Lagos');
INSERT INTO `databand_city` VALUES ('2785', 'Oka-Akoko', 'NGA', 'Ondo & Ekiti');
INSERT INTO `databand_city` VALUES ('2786', 'Ikare', 'NGA', 'Ondo & Ekiti');
INSERT INTO `databand_city` VALUES ('2787', 'Sapele', 'NGA', 'Edo & Delta');
INSERT INTO `databand_city` VALUES ('2788', 'Deba Habe', 'NGA', 'Bauchi & Gombe');
INSERT INTO `databand_city` VALUES ('2789', 'Minna', 'NGA', 'Niger');
INSERT INTO `databand_city` VALUES ('2790', 'Warri', 'NGA', 'Edo & Delta');
INSERT INTO `databand_city` VALUES ('2791', 'Bida', 'NGA', 'Niger');
INSERT INTO `databand_city` VALUES ('2792', 'Ikire', 'NGA', 'Oyo & Osun');
INSERT INTO `databand_city` VALUES ('2793', 'Makurdi', 'NGA', 'Benue');
INSERT INTO `databand_city` VALUES ('2794', 'Lafia', 'NGA', 'Plateau & Nassarawa');
INSERT INTO `databand_city` VALUES ('2795', 'Inisa', 'NGA', 'Oyo & Osun');
INSERT INTO `databand_city` VALUES ('2796', 'Shagamu', 'NGA', 'Ogun');
INSERT INTO `databand_city` VALUES ('2797', 'Awka', 'NGA', 'Anambra & Enugu & Eb');
INSERT INTO `databand_city` VALUES ('2798', 'Gombe', 'NGA', 'Bauchi & Gombe');
INSERT INTO `databand_city` VALUES ('2799', 'Igboho', 'NGA', 'Oyo & Osun');
INSERT INTO `databand_city` VALUES ('2800', 'Ejigbo', 'NGA', 'Oyo & Osun');
INSERT INTO `databand_city` VALUES ('2801', 'Agege', 'NGA', 'Lagos');
INSERT INTO `databand_city` VALUES ('2802', 'Ise-Ekiti', 'NGA', 'Ondo & Ekiti');
INSERT INTO `databand_city` VALUES ('2803', 'Ugep', 'NGA', 'Cross River');
INSERT INTO `databand_city` VALUES ('2804', 'Epe', 'NGA', 'Lagos');
INSERT INTO `databand_city` VALUES ('2805', 'Alofi', 'NIU', '–');
INSERT INTO `databand_city` VALUES ('2806', 'Kingston', 'NFK', '–');
INSERT INTO `databand_city` VALUES ('2807', 'Oslo', 'NOR', 'Oslo');
INSERT INTO `databand_city` VALUES ('2808', 'Bergen', 'NOR', 'Hordaland');
INSERT INTO `databand_city` VALUES ('2809', 'Trondheim', 'NOR', 'Sør-Trøndelag');
INSERT INTO `databand_city` VALUES ('2810', 'Stavanger', 'NOR', 'Rogaland');
INSERT INTO `databand_city` VALUES ('2811', 'Bærum', 'NOR', 'Akershus');
INSERT INTO `databand_city` VALUES ('2812', 'Abidjan', 'CIV', 'Abidjan');
INSERT INTO `databand_city` VALUES ('2813', 'Bouaké', 'CIV', 'Bouaké');
INSERT INTO `databand_city` VALUES ('2814', 'Yamoussoukro', 'CIV', 'Yamoussoukro');
INSERT INTO `databand_city` VALUES ('2815', 'Daloa', 'CIV', 'Daloa');
INSERT INTO `databand_city` VALUES ('2816', 'Korhogo', 'CIV', 'Korhogo');
INSERT INTO `databand_city` VALUES ('2817', 'al-Sib', 'OMN', 'Masqat');
INSERT INTO `databand_city` VALUES ('2818', 'Salala', 'OMN', 'Zufar');
INSERT INTO `databand_city` VALUES ('2819', 'Bawshar', 'OMN', 'Masqat');
INSERT INTO `databand_city` VALUES ('2820', 'Suhar', 'OMN', 'al-Batina');
INSERT INTO `databand_city` VALUES ('2821', 'Masqat', 'OMN', 'Masqat');
INSERT INTO `databand_city` VALUES ('2822', 'Karachi', 'PAK', 'Sindh');
INSERT INTO `databand_city` VALUES ('2823', 'Lahore', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2824', 'Faisalabad', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2825', 'Rawalpindi', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2826', 'Multan', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2827', 'Hyderabad', 'PAK', 'Sindh');
INSERT INTO `databand_city` VALUES ('2828', 'Gujranwala', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2829', 'Peshawar', 'PAK', 'Nothwest Border Prov');
INSERT INTO `databand_city` VALUES ('2830', 'Quetta', 'PAK', 'Baluchistan');
INSERT INTO `databand_city` VALUES ('2831', 'Islamabad', 'PAK', 'Islamabad');
INSERT INTO `databand_city` VALUES ('2832', 'Sargodha', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2833', 'Sialkot', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2834', 'Bahawalpur', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2835', 'Sukkur', 'PAK', 'Sindh');
INSERT INTO `databand_city` VALUES ('2836', 'Jhang', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2837', 'Sheikhupura', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2838', 'Larkana', 'PAK', 'Sindh');
INSERT INTO `databand_city` VALUES ('2839', 'Gujrat', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2840', 'Mardan', 'PAK', 'Nothwest Border Prov');
INSERT INTO `databand_city` VALUES ('2841', 'Kasur', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2842', 'Rahim Yar Khan', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2843', 'Sahiwal', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2844', 'Okara', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2845', 'Wah', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2846', 'Dera Ghazi Khan', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2847', 'Mirpur Khas', 'PAK', 'Sind');
INSERT INTO `databand_city` VALUES ('2848', 'Nawabshah', 'PAK', 'Sind');
INSERT INTO `databand_city` VALUES ('2849', 'Mingora', 'PAK', 'Nothwest Border Prov');
INSERT INTO `databand_city` VALUES ('2850', 'Chiniot', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2851', 'Kamoke', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2852', 'Mandi Burewala', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2853', 'Jhelum', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2854', 'Sadiqabad', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2855', 'Jacobabad', 'PAK', 'Sind');
INSERT INTO `databand_city` VALUES ('2856', 'Shikarpur', 'PAK', 'Sind');
INSERT INTO `databand_city` VALUES ('2857', 'Khanewal', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2858', 'Hafizabad', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2859', 'Kohat', 'PAK', 'Nothwest Border Prov');
INSERT INTO `databand_city` VALUES ('2860', 'Muzaffargarh', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2861', 'Khanpur', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2862', 'Gojra', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2863', 'Bahawalnagar', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2864', 'Muridke', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2865', 'Pak Pattan', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2866', 'Abottabad', 'PAK', 'Nothwest Border Prov');
INSERT INTO `databand_city` VALUES ('2867', 'Tando Adam', 'PAK', 'Sind');
INSERT INTO `databand_city` VALUES ('2868', 'Jaranwala', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2869', 'Khairpur', 'PAK', 'Sind');
INSERT INTO `databand_city` VALUES ('2870', 'Chishtian Mandi', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2871', 'Daska', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2872', 'Dadu', 'PAK', 'Sind');
INSERT INTO `databand_city` VALUES ('2873', 'Mandi Bahauddin', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2874', 'Ahmadpur East', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2875', 'Kamalia', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2876', 'Khuzdar', 'PAK', 'Baluchistan');
INSERT INTO `databand_city` VALUES ('2877', 'Vihari', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2878', 'Dera Ismail Khan', 'PAK', 'Nothwest Border Prov');
INSERT INTO `databand_city` VALUES ('2879', 'Wazirabad', 'PAK', 'Punjab');
INSERT INTO `databand_city` VALUES ('2880', 'Nowshera', 'PAK', 'Nothwest Border Prov');
INSERT INTO `databand_city` VALUES ('2881', 'Koror', 'PLW', 'Koror');
INSERT INTO `databand_city` VALUES ('2882', 'Ciudad de Panamá', 'PAN', 'Panamá');
INSERT INTO `databand_city` VALUES ('2883', 'San Miguelito', 'PAN', 'San Miguelito');
INSERT INTO `databand_city` VALUES ('2884', 'Port Moresby', 'PNG', 'National Capital Dis');
INSERT INTO `databand_city` VALUES ('2885', 'Asunción', 'PRY', 'Asunción');
INSERT INTO `databand_city` VALUES ('2886', 'Ciudad del Este', 'PRY', 'Alto Paraná');
INSERT INTO `databand_city` VALUES ('2887', 'San Lorenzo', 'PRY', 'Central');
INSERT INTO `databand_city` VALUES ('2888', 'Lambaré', 'PRY', 'Central');
INSERT INTO `databand_city` VALUES ('2889', 'Fernando de la Mora', 'PRY', 'Central');
INSERT INTO `databand_city` VALUES ('2890', 'Lima', 'PER', 'Lima');
INSERT INTO `databand_city` VALUES ('2891', 'Arequipa', 'PER', 'Arequipa');
INSERT INTO `databand_city` VALUES ('2892', 'Trujillo', 'PER', 'La Libertad');
INSERT INTO `databand_city` VALUES ('2893', 'Chiclayo', 'PER', 'Lambayeque');
INSERT INTO `databand_city` VALUES ('2894', 'Callao', 'PER', 'Callao');
INSERT INTO `databand_city` VALUES ('2895', 'Iquitos', 'PER', 'Loreto');
INSERT INTO `databand_city` VALUES ('2896', 'Chimbote', 'PER', 'Ancash');
INSERT INTO `databand_city` VALUES ('2897', 'Huancayo', 'PER', 'Junín');
INSERT INTO `databand_city` VALUES ('2898', 'Piura', 'PER', 'Piura');
INSERT INTO `databand_city` VALUES ('2899', 'Cusco', 'PER', 'Cusco');
INSERT INTO `databand_city` VALUES ('2900', 'Pucallpa', 'PER', 'Ucayali');
INSERT INTO `databand_city` VALUES ('2901', 'Tacna', 'PER', 'Tacna');
INSERT INTO `databand_city` VALUES ('2902', 'Ica', 'PER', 'Ica');
INSERT INTO `databand_city` VALUES ('2903', 'Sullana', 'PER', 'Piura');
INSERT INTO `databand_city` VALUES ('2904', 'Juliaca', 'PER', 'Puno');
INSERT INTO `databand_city` VALUES ('2905', 'Huánuco', 'PER', 'Huanuco');
INSERT INTO `databand_city` VALUES ('2906', 'Ayacucho', 'PER', 'Ayacucho');
INSERT INTO `databand_city` VALUES ('2907', 'Chincha Alta', 'PER', 'Ica');
INSERT INTO `databand_city` VALUES ('2908', 'Cajamarca', 'PER', 'Cajamarca');
INSERT INTO `databand_city` VALUES ('2909', 'Puno', 'PER', 'Puno');
INSERT INTO `databand_city` VALUES ('2910', 'Ventanilla', 'PER', 'Callao');
INSERT INTO `databand_city` VALUES ('2911', 'Castilla', 'PER', 'Piura');
INSERT INTO `databand_city` VALUES ('2912', 'Adamstown', 'PCN', '–');
INSERT INTO `databand_city` VALUES ('2913', 'Garapan', 'MNP', 'Saipan');
INSERT INTO `databand_city` VALUES ('2914', 'Lisboa', 'PRT', 'Lisboa');
INSERT INTO `databand_city` VALUES ('2915', 'Porto', 'PRT', 'Porto');
INSERT INTO `databand_city` VALUES ('2916', 'Amadora', 'PRT', 'Lisboa');
INSERT INTO `databand_city` VALUES ('2917', 'Coímbra', 'PRT', 'Coímbra');
INSERT INTO `databand_city` VALUES ('2918', 'Braga', 'PRT', 'Braga');
INSERT INTO `databand_city` VALUES ('2919', 'San Juan', 'PRI', 'San Juan');
INSERT INTO `databand_city` VALUES ('2920', 'Bayamón', 'PRI', 'Bayamón');
INSERT INTO `databand_city` VALUES ('2921', 'Ponce', 'PRI', 'Ponce');
INSERT INTO `databand_city` VALUES ('2922', 'Carolina', 'PRI', 'Carolina');
INSERT INTO `databand_city` VALUES ('2923', 'Caguas', 'PRI', 'Caguas');
INSERT INTO `databand_city` VALUES ('2924', 'Arecibo', 'PRI', 'Arecibo');
INSERT INTO `databand_city` VALUES ('2925', 'Guaynabo', 'PRI', 'Guaynabo');
INSERT INTO `databand_city` VALUES ('2926', 'Mayagüez', 'PRI', 'Mayagüez');
INSERT INTO `databand_city` VALUES ('2927', 'Toa Baja', 'PRI', 'Toa Baja');
INSERT INTO `databand_city` VALUES ('2928', 'Warszawa', 'POL', 'Mazowieckie');
INSERT INTO `databand_city` VALUES ('2929', 'Lódz', 'POL', 'Lodzkie');
INSERT INTO `databand_city` VALUES ('2930', 'Kraków', 'POL', 'Malopolskie');
INSERT INTO `databand_city` VALUES ('2931', 'Wroclaw', 'POL', 'Dolnoslaskie');
INSERT INTO `databand_city` VALUES ('2932', 'Poznan', 'POL', 'Wielkopolskie');
INSERT INTO `databand_city` VALUES ('2933', 'Gdansk', 'POL', 'Pomorskie');
INSERT INTO `databand_city` VALUES ('2934', 'Szczecin', 'POL', 'Zachodnio-Pomorskie');
INSERT INTO `databand_city` VALUES ('2935', 'Bydgoszcz', 'POL', 'Kujawsko-Pomorskie');
INSERT INTO `databand_city` VALUES ('2936', 'Lublin', 'POL', 'Lubelskie');
INSERT INTO `databand_city` VALUES ('2937', 'Katowice', 'POL', 'Slaskie');
INSERT INTO `databand_city` VALUES ('2938', 'Bialystok', 'POL', 'Podlaskie');
INSERT INTO `databand_city` VALUES ('2939', 'Czestochowa', 'POL', 'Slaskie');
INSERT INTO `databand_city` VALUES ('2940', 'Gdynia', 'POL', 'Pomorskie');
INSERT INTO `databand_city` VALUES ('2941', 'Sosnowiec', 'POL', 'Slaskie');
INSERT INTO `databand_city` VALUES ('2942', 'Radom', 'POL', 'Mazowieckie');
INSERT INTO `databand_city` VALUES ('2943', 'Kielce', 'POL', 'Swietokrzyskie');
INSERT INTO `databand_city` VALUES ('2944', 'Gliwice', 'POL', 'Slaskie');
INSERT INTO `databand_city` VALUES ('2945', 'Torun', 'POL', 'Kujawsko-Pomorskie');
INSERT INTO `databand_city` VALUES ('2946', 'Bytom', 'POL', 'Slaskie');
INSERT INTO `databand_city` VALUES ('2947', 'Zabrze', 'POL', 'Slaskie');
INSERT INTO `databand_city` VALUES ('2948', 'Bielsko-Biala', 'POL', 'Slaskie');
INSERT INTO `databand_city` VALUES ('2949', 'Olsztyn', 'POL', 'Warminsko-Mazurskie');
INSERT INTO `databand_city` VALUES ('2950', 'Rzeszów', 'POL', 'Podkarpackie');
INSERT INTO `databand_city` VALUES ('2951', 'Ruda Slaska', 'POL', 'Slaskie');
INSERT INTO `databand_city` VALUES ('2952', 'Rybnik', 'POL', 'Slaskie');
INSERT INTO `databand_city` VALUES ('2953', 'Walbrzych', 'POL', 'Dolnoslaskie');
INSERT INTO `databand_city` VALUES ('2954', 'Tychy', 'POL', 'Slaskie');
INSERT INTO `databand_city` VALUES ('2955', 'Dabrowa Górnicza', 'POL', 'Slaskie');
INSERT INTO `databand_city` VALUES ('2956', 'Plock', 'POL', 'Mazowieckie');
INSERT INTO `databand_city` VALUES ('2957', 'Elblag', 'POL', 'Warminsko-Mazurskie');
INSERT INTO `databand_city` VALUES ('2958', 'Opole', 'POL', 'Opolskie');
INSERT INTO `databand_city` VALUES ('2959', 'Gorzów Wielkopolski', 'POL', 'Lubuskie');
INSERT INTO `databand_city` VALUES ('2960', 'Wloclawek', 'POL', 'Kujawsko-Pomorskie');
INSERT INTO `databand_city` VALUES ('2961', 'Chorzów', 'POL', 'Slaskie');
INSERT INTO `databand_city` VALUES ('2962', 'Tarnów', 'POL', 'Malopolskie');
INSERT INTO `databand_city` VALUES ('2963', 'Zielona Góra', 'POL', 'Lubuskie');
INSERT INTO `databand_city` VALUES ('2964', 'Koszalin', 'POL', 'Zachodnio-Pomorskie');
INSERT INTO `databand_city` VALUES ('2965', 'Legnica', 'POL', 'Dolnoslaskie');
INSERT INTO `databand_city` VALUES ('2966', 'Kalisz', 'POL', 'Wielkopolskie');
INSERT INTO `databand_city` VALUES ('2967', 'Grudziadz', 'POL', 'Kujawsko-Pomorskie');
INSERT INTO `databand_city` VALUES ('2968', 'Slupsk', 'POL', 'Pomorskie');
INSERT INTO `databand_city` VALUES ('2969', 'Jastrzebie-Zdrój', 'POL', 'Slaskie');
INSERT INTO `databand_city` VALUES ('2970', 'Jaworzno', 'POL', 'Slaskie');
INSERT INTO `databand_city` VALUES ('2971', 'Jelenia Góra', 'POL', 'Dolnoslaskie');
INSERT INTO `databand_city` VALUES ('2972', 'Malabo', 'GNQ', 'Bioko');
INSERT INTO `databand_city` VALUES ('2973', 'Doha', 'QAT', 'Doha');
INSERT INTO `databand_city` VALUES ('2974', 'Paris', 'FRA', 'Île-de-France');
INSERT INTO `databand_city` VALUES ('2975', 'Marseille', 'FRA', 'Provence-Alpes-Côte');
INSERT INTO `databand_city` VALUES ('2976', 'Lyon', 'FRA', 'Rhône-Alpes');
INSERT INTO `databand_city` VALUES ('2977', 'Toulouse', 'FRA', 'Midi-Pyrénées');
INSERT INTO `databand_city` VALUES ('2978', 'Nice', 'FRA', 'Provence-Alpes-Côte');
INSERT INTO `databand_city` VALUES ('2979', 'Nantes', 'FRA', 'Pays de la Loire');
INSERT INTO `databand_city` VALUES ('2980', 'Strasbourg', 'FRA', 'Alsace');
INSERT INTO `databand_city` VALUES ('2981', 'Montpellier', 'FRA', 'Languedoc-Roussillon');
INSERT INTO `databand_city` VALUES ('2982', 'Bordeaux', 'FRA', 'Aquitaine');
INSERT INTO `databand_city` VALUES ('2983', 'Rennes', 'FRA', 'Haute-Normandie');
INSERT INTO `databand_city` VALUES ('2984', 'Le Havre', 'FRA', 'Champagne-Ardenne');
INSERT INTO `databand_city` VALUES ('2985', 'Reims', 'FRA', 'Nord-Pas-de-Calais');
INSERT INTO `databand_city` VALUES ('2986', 'Lille', 'FRA', 'Rhône-Alpes');
INSERT INTO `databand_city` VALUES ('2987', 'St-Étienne', 'FRA', 'Bretagne');
INSERT INTO `databand_city` VALUES ('2988', 'Toulon', 'FRA', 'Provence-Alpes-Côte');
INSERT INTO `databand_city` VALUES ('2989', 'Grenoble', 'FRA', 'Rhône-Alpes');
INSERT INTO `databand_city` VALUES ('2990', 'Angers', 'FRA', 'Pays de la Loire');
INSERT INTO `databand_city` VALUES ('2991', 'Dijon', 'FRA', 'Bourgogne');
INSERT INTO `databand_city` VALUES ('2992', 'Brest', 'FRA', 'Bretagne');
INSERT INTO `databand_city` VALUES ('2993', 'Le Mans', 'FRA', 'Pays de la Loire');
INSERT INTO `databand_city` VALUES ('2994', 'Clermont-Ferrand', 'FRA', 'Auvergne');
INSERT INTO `databand_city` VALUES ('2995', 'Amiens', 'FRA', 'Picardie');
INSERT INTO `databand_city` VALUES ('2996', 'Aix-en-Provence', 'FRA', 'Provence-Alpes-Côte');
INSERT INTO `databand_city` VALUES ('2997', 'Limoges', 'FRA', 'Limousin');
INSERT INTO `databand_city` VALUES ('2998', 'Nîmes', 'FRA', 'Languedoc-Roussillon');
INSERT INTO `databand_city` VALUES ('2999', 'Tours', 'FRA', 'Centre');
INSERT INTO `databand_city` VALUES ('3000', 'Villeurbanne', 'FRA', 'Rhône-Alpes');
INSERT INTO `databand_city` VALUES ('3001', 'Metz', 'FRA', 'Lorraine');
INSERT INTO `databand_city` VALUES ('3002', 'Besançon', 'FRA', 'Franche-Comté');
INSERT INTO `databand_city` VALUES ('3003', 'Caen', 'FRA', 'Basse-Normandie');
INSERT INTO `databand_city` VALUES ('3004', 'Orléans', 'FRA', 'Centre');
INSERT INTO `databand_city` VALUES ('3005', 'Mulhouse', 'FRA', 'Alsace');
INSERT INTO `databand_city` VALUES ('3006', 'Rouen', 'FRA', 'Haute-Normandie');
INSERT INTO `databand_city` VALUES ('3007', 'Boulogne-Billancourt', 'FRA', 'Île-de-France');
INSERT INTO `databand_city` VALUES ('3008', 'Perpignan', 'FRA', 'Languedoc-Roussillon');
INSERT INTO `databand_city` VALUES ('3009', 'Nancy', 'FRA', 'Lorraine');
INSERT INTO `databand_city` VALUES ('3010', 'Roubaix', 'FRA', 'Nord-Pas-de-Calais');
INSERT INTO `databand_city` VALUES ('3011', 'Argenteuil', 'FRA', 'Île-de-France');
INSERT INTO `databand_city` VALUES ('3012', 'Tourcoing', 'FRA', 'Nord-Pas-de-Calais');
INSERT INTO `databand_city` VALUES ('3013', 'Montreuil', 'FRA', 'Île-de-France');
INSERT INTO `databand_city` VALUES ('3014', 'Cayenne', 'GUF', 'Cayenne');
INSERT INTO `databand_city` VALUES ('3015', 'Faaa', 'PYF', 'Tahiti');
INSERT INTO `databand_city` VALUES ('3016', 'Papeete', 'PYF', 'Tahiti');
INSERT INTO `databand_city` VALUES ('3017', 'Saint-Denis', 'REU', 'Saint-Denis');
INSERT INTO `databand_city` VALUES ('3018', 'Bucuresti', 'ROM', 'Bukarest');
INSERT INTO `databand_city` VALUES ('3019', 'Iasi', 'ROM', 'Iasi');
INSERT INTO `databand_city` VALUES ('3020', 'Constanta', 'ROM', 'Constanta');
INSERT INTO `databand_city` VALUES ('3021', 'Cluj-Napoca', 'ROM', 'Cluj');
INSERT INTO `databand_city` VALUES ('3022', 'Galati', 'ROM', 'Galati');
INSERT INTO `databand_city` VALUES ('3023', 'Timisoara', 'ROM', 'Timis');
INSERT INTO `databand_city` VALUES ('3024', 'Brasov', 'ROM', 'Brasov');
INSERT INTO `databand_city` VALUES ('3025', 'Craiova', 'ROM', 'Dolj');
INSERT INTO `databand_city` VALUES ('3026', 'Ploiesti', 'ROM', 'Prahova');
INSERT INTO `databand_city` VALUES ('3027', 'Braila', 'ROM', 'Braila');
INSERT INTO `databand_city` VALUES ('3028', 'Oradea', 'ROM', 'Bihor');
INSERT INTO `databand_city` VALUES ('3029', 'Bacau', 'ROM', 'Bacau');
INSERT INTO `databand_city` VALUES ('3030', 'Pitesti', 'ROM', 'Arges');
INSERT INTO `databand_city` VALUES ('3031', 'Arad', 'ROM', 'Arad');
INSERT INTO `databand_city` VALUES ('3032', 'Sibiu', 'ROM', 'Sibiu');
INSERT INTO `databand_city` VALUES ('3033', 'Târgu Mures', 'ROM', 'Mures');
INSERT INTO `databand_city` VALUES ('3034', 'Baia Mare', 'ROM', 'Maramures');
INSERT INTO `databand_city` VALUES ('3035', 'Buzau', 'ROM', 'Buzau');
INSERT INTO `databand_city` VALUES ('3036', 'Satu Mare', 'ROM', 'Satu Mare');
INSERT INTO `databand_city` VALUES ('3037', 'Botosani', 'ROM', 'Botosani');
INSERT INTO `databand_city` VALUES ('3038', 'Piatra Neamt', 'ROM', 'Neamt');
INSERT INTO `databand_city` VALUES ('3039', 'Râmnicu Vâlcea', 'ROM', 'Vâlcea');
INSERT INTO `databand_city` VALUES ('3040', 'Suceava', 'ROM', 'Suceava');
INSERT INTO `databand_city` VALUES ('3041', 'Drobeta-Turnu Severin', 'ROM', 'Mehedinti');
INSERT INTO `databand_city` VALUES ('3042', 'Târgoviste', 'ROM', 'Dâmbovita');
INSERT INTO `databand_city` VALUES ('3043', 'Focsani', 'ROM', 'Vrancea');
INSERT INTO `databand_city` VALUES ('3044', 'Târgu Jiu', 'ROM', 'Gorj');
INSERT INTO `databand_city` VALUES ('3045', 'Tulcea', 'ROM', 'Tulcea');
INSERT INTO `databand_city` VALUES ('3046', 'Resita', 'ROM', 'Caras-Severin');
INSERT INTO `databand_city` VALUES ('3047', 'Kigali', 'RWA', 'Kigali');
INSERT INTO `databand_city` VALUES ('3048', 'Stockholm', 'SWE', 'Lisboa');
INSERT INTO `databand_city` VALUES ('3049', 'Gothenburg [Göteborg]', 'SWE', 'West Götanmaan län');
INSERT INTO `databand_city` VALUES ('3050', 'Malmö', 'SWE', 'Skåne län');
INSERT INTO `databand_city` VALUES ('3051', 'Uppsala', 'SWE', 'Uppsala län');
INSERT INTO `databand_city` VALUES ('3052', 'Linköping', 'SWE', 'East Götanmaan län');
INSERT INTO `databand_city` VALUES ('3053', 'Västerås', 'SWE', 'Västmanlands län');
INSERT INTO `databand_city` VALUES ('3054', 'Örebro', 'SWE', 'Örebros län');
INSERT INTO `databand_city` VALUES ('3055', 'Norrköping', 'SWE', 'East Götanmaan län');
INSERT INTO `databand_city` VALUES ('3056', 'Helsingborg', 'SWE', 'Skåne län');
INSERT INTO `databand_city` VALUES ('3057', 'Jönköping', 'SWE', 'Jönköpings län');
INSERT INTO `databand_city` VALUES ('3058', 'Umeå', 'SWE', 'Västerbottens län');
INSERT INTO `databand_city` VALUES ('3059', 'Lund', 'SWE', 'Skåne län');
INSERT INTO `databand_city` VALUES ('3060', 'Borås', 'SWE', 'West Götanmaan län');
INSERT INTO `databand_city` VALUES ('3061', 'Sundsvall', 'SWE', 'Västernorrlands län');
INSERT INTO `databand_city` VALUES ('3062', 'Gävle', 'SWE', 'Gävleborgs län');
INSERT INTO `databand_city` VALUES ('3063', 'Jamestown', 'SHN', 'Saint Helena');
INSERT INTO `databand_city` VALUES ('3064', 'Basseterre', 'KNA', 'St George Basseterre');
INSERT INTO `databand_city` VALUES ('3065', 'Castries', 'LCA', 'Castries');
INSERT INTO `databand_city` VALUES ('3066', 'Kingstown', 'VCT', 'St George');
INSERT INTO `databand_city` VALUES ('3067', 'Saint-Pierre', 'SPM', 'Saint-Pierre');
INSERT INTO `databand_city` VALUES ('3068', 'Berlin', 'DEU', 'Berliini');
INSERT INTO `databand_city` VALUES ('3069', 'Hamburg', 'DEU', 'Hamburg');
INSERT INTO `databand_city` VALUES ('3070', 'Munich [München]', 'DEU', 'Baijeri');
INSERT INTO `databand_city` VALUES ('3071', 'Köln', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3072', 'Frankfurt am Main', 'DEU', 'Hessen');
INSERT INTO `databand_city` VALUES ('3073', 'Essen', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3074', 'Dortmund', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3075', 'Stuttgart', 'DEU', 'Baden-Württemberg');
INSERT INTO `databand_city` VALUES ('3076', 'Düsseldorf', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3077', 'Bremen', 'DEU', 'Bremen');
INSERT INTO `databand_city` VALUES ('3078', 'Duisburg', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3079', 'Hannover', 'DEU', 'Niedersachsen');
INSERT INTO `databand_city` VALUES ('3080', 'Leipzig', 'DEU', 'Saksi');
INSERT INTO `databand_city` VALUES ('3081', 'Nürnberg', 'DEU', 'Baijeri');
INSERT INTO `databand_city` VALUES ('3082', 'Dresden', 'DEU', 'Saksi');
INSERT INTO `databand_city` VALUES ('3083', 'Bochum', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3084', 'Wuppertal', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3085', 'Bielefeld', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3086', 'Mannheim', 'DEU', 'Baden-Württemberg');
INSERT INTO `databand_city` VALUES ('3087', 'Bonn', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3088', 'Gelsenkirchen', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3089', 'Karlsruhe', 'DEU', 'Baden-Württemberg');
INSERT INTO `databand_city` VALUES ('3090', 'Wiesbaden', 'DEU', 'Hessen');
INSERT INTO `databand_city` VALUES ('3091', 'Münster', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3092', 'Mönchengladbach', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3093', 'Chemnitz', 'DEU', 'Saksi');
INSERT INTO `databand_city` VALUES ('3094', 'Augsburg', 'DEU', 'Baijeri');
INSERT INTO `databand_city` VALUES ('3095', 'Halle/Saale', 'DEU', 'Anhalt Sachsen');
INSERT INTO `databand_city` VALUES ('3096', 'Braunschweig', 'DEU', 'Niedersachsen');
INSERT INTO `databand_city` VALUES ('3097', 'Aachen', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3098', 'Krefeld', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3099', 'Magdeburg', 'DEU', 'Anhalt Sachsen');
INSERT INTO `databand_city` VALUES ('3100', 'Kiel', 'DEU', 'Schleswig-Holstein');
INSERT INTO `databand_city` VALUES ('3101', 'Oberhausen', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3102', 'Lübeck', 'DEU', 'Schleswig-Holstein');
INSERT INTO `databand_city` VALUES ('3103', 'Hagen', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3104', 'Rostock', 'DEU', 'Mecklenburg-Vorpomme');
INSERT INTO `databand_city` VALUES ('3105', 'Freiburg im Breisgau', 'DEU', 'Baden-Württemberg');
INSERT INTO `databand_city` VALUES ('3106', 'Erfurt', 'DEU', 'Thüringen');
INSERT INTO `databand_city` VALUES ('3107', 'Kassel', 'DEU', 'Hessen');
INSERT INTO `databand_city` VALUES ('3108', 'Saarbrücken', 'DEU', 'Saarland');
INSERT INTO `databand_city` VALUES ('3109', 'Mainz', 'DEU', 'Rheinland-Pfalz');
INSERT INTO `databand_city` VALUES ('3110', 'Hamm', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3111', 'Herne', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3112', 'Mülheim an der Ruhr', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3113', 'Solingen', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3114', 'Osnabrück', 'DEU', 'Niedersachsen');
INSERT INTO `databand_city` VALUES ('3115', 'Ludwigshafen am Rhein', 'DEU', 'Rheinland-Pfalz');
INSERT INTO `databand_city` VALUES ('3116', 'Leverkusen', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3117', 'Oldenburg', 'DEU', 'Niedersachsen');
INSERT INTO `databand_city` VALUES ('3118', 'Neuss', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3119', 'Heidelberg', 'DEU', 'Baden-Württemberg');
INSERT INTO `databand_city` VALUES ('3120', 'Darmstadt', 'DEU', 'Hessen');
INSERT INTO `databand_city` VALUES ('3121', 'Paderborn', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3122', 'Potsdam', 'DEU', 'Brandenburg');
INSERT INTO `databand_city` VALUES ('3123', 'Würzburg', 'DEU', 'Baijeri');
INSERT INTO `databand_city` VALUES ('3124', 'Regensburg', 'DEU', 'Baijeri');
INSERT INTO `databand_city` VALUES ('3125', 'Recklinghausen', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3126', 'Göttingen', 'DEU', 'Niedersachsen');
INSERT INTO `databand_city` VALUES ('3127', 'Bremerhaven', 'DEU', 'Bremen');
INSERT INTO `databand_city` VALUES ('3128', 'Wolfsburg', 'DEU', 'Niedersachsen');
INSERT INTO `databand_city` VALUES ('3129', 'Bottrop', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3130', 'Remscheid', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3131', 'Heilbronn', 'DEU', 'Baden-Württemberg');
INSERT INTO `databand_city` VALUES ('3132', 'Pforzheim', 'DEU', 'Baden-Württemberg');
INSERT INTO `databand_city` VALUES ('3133', 'Offenbach am Main', 'DEU', 'Hessen');
INSERT INTO `databand_city` VALUES ('3134', 'Ulm', 'DEU', 'Baden-Württemberg');
INSERT INTO `databand_city` VALUES ('3135', 'Ingolstadt', 'DEU', 'Baijeri');
INSERT INTO `databand_city` VALUES ('3136', 'Gera', 'DEU', 'Thüringen');
INSERT INTO `databand_city` VALUES ('3137', 'Salzgitter', 'DEU', 'Niedersachsen');
INSERT INTO `databand_city` VALUES ('3138', 'Cottbus', 'DEU', 'Brandenburg');
INSERT INTO `databand_city` VALUES ('3139', 'Reutlingen', 'DEU', 'Baden-Württemberg');
INSERT INTO `databand_city` VALUES ('3140', 'Fürth', 'DEU', 'Baijeri');
INSERT INTO `databand_city` VALUES ('3141', 'Siegen', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3142', 'Koblenz', 'DEU', 'Rheinland-Pfalz');
INSERT INTO `databand_city` VALUES ('3143', 'Moers', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3144', 'Bergisch Gladbach', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3145', 'Zwickau', 'DEU', 'Saksi');
INSERT INTO `databand_city` VALUES ('3146', 'Hildesheim', 'DEU', 'Niedersachsen');
INSERT INTO `databand_city` VALUES ('3147', 'Witten', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3148', 'Schwerin', 'DEU', 'Mecklenburg-Vorpomme');
INSERT INTO `databand_city` VALUES ('3149', 'Erlangen', 'DEU', 'Baijeri');
INSERT INTO `databand_city` VALUES ('3150', 'Kaiserslautern', 'DEU', 'Rheinland-Pfalz');
INSERT INTO `databand_city` VALUES ('3151', 'Trier', 'DEU', 'Rheinland-Pfalz');
INSERT INTO `databand_city` VALUES ('3152', 'Jena', 'DEU', 'Thüringen');
INSERT INTO `databand_city` VALUES ('3153', 'Iserlohn', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3154', 'Gütersloh', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3155', 'Marl', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3156', 'Lünen', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3157', 'Düren', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3158', 'Ratingen', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3159', 'Velbert', 'DEU', 'Nordrhein-Westfalen');
INSERT INTO `databand_city` VALUES ('3160', 'Esslingen am Neckar', 'DEU', 'Baden-Württemberg');
INSERT INTO `databand_city` VALUES ('3161', 'Honiara', 'SLB', 'Honiara');
INSERT INTO `databand_city` VALUES ('3162', 'Lusaka', 'ZMB', 'Lusaka');
INSERT INTO `databand_city` VALUES ('3163', 'Ndola', 'ZMB', 'Copperbelt');
INSERT INTO `databand_city` VALUES ('3164', 'Kitwe', 'ZMB', 'Copperbelt');
INSERT INTO `databand_city` VALUES ('3165', 'Kabwe', 'ZMB', 'Central');
INSERT INTO `databand_city` VALUES ('3166', 'Chingola', 'ZMB', 'Copperbelt');
INSERT INTO `databand_city` VALUES ('3167', 'Mufulira', 'ZMB', 'Copperbelt');
INSERT INTO `databand_city` VALUES ('3168', 'Luanshya', 'ZMB', 'Copperbelt');
INSERT INTO `databand_city` VALUES ('3169', 'Apia', 'WSM', 'Upolu');
INSERT INTO `databand_city` VALUES ('3170', 'Serravalle', 'SMR', 'Serravalle/Dogano');
INSERT INTO `databand_city` VALUES ('3171', 'San Marino', 'SMR', 'San Marino');
INSERT INTO `databand_city` VALUES ('3172', 'São Tomé', 'STP', 'Aqua Grande');
INSERT INTO `databand_city` VALUES ('3173', 'Riyadh', 'SAU', 'Riyadh');
INSERT INTO `databand_city` VALUES ('3174', 'Jedda', 'SAU', 'Mekka');
INSERT INTO `databand_city` VALUES ('3175', 'Mekka', 'SAU', 'Mekka');
INSERT INTO `databand_city` VALUES ('3176', 'Medina', 'SAU', 'Medina');
INSERT INTO `databand_city` VALUES ('3177', 'al-Dammam', 'SAU', 'al-Sharqiya');
INSERT INTO `databand_city` VALUES ('3178', 'al-Taif', 'SAU', 'Mekka');
INSERT INTO `databand_city` VALUES ('3179', 'Tabuk', 'SAU', 'Tabuk');
INSERT INTO `databand_city` VALUES ('3180', 'Burayda', 'SAU', 'al-Qasim');
INSERT INTO `databand_city` VALUES ('3181', 'al-Hufuf', 'SAU', 'al-Sharqiya');
INSERT INTO `databand_city` VALUES ('3182', 'al-Mubarraz', 'SAU', 'al-Sharqiya');
INSERT INTO `databand_city` VALUES ('3183', 'Khamis Mushayt', 'SAU', 'Asir');
INSERT INTO `databand_city` VALUES ('3184', 'Hail', 'SAU', 'Hail');
INSERT INTO `databand_city` VALUES ('3185', 'al-Kharj', 'SAU', 'Riad');
INSERT INTO `databand_city` VALUES ('3186', 'al-Khubar', 'SAU', 'al-Sharqiya');
INSERT INTO `databand_city` VALUES ('3187', 'Jubayl', 'SAU', 'al-Sharqiya');
INSERT INTO `databand_city` VALUES ('3188', 'Hafar al-Batin', 'SAU', 'al-Sharqiya');
INSERT INTO `databand_city` VALUES ('3189', 'al-Tuqba', 'SAU', 'al-Sharqiya');
INSERT INTO `databand_city` VALUES ('3190', 'Yanbu', 'SAU', 'Medina');
INSERT INTO `databand_city` VALUES ('3191', 'Abha', 'SAU', 'Asir');
INSERT INTO `databand_city` VALUES ('3192', 'Ara´ar', 'SAU', 'al-Khudud al-Samaliy');
INSERT INTO `databand_city` VALUES ('3193', 'al-Qatif', 'SAU', 'al-Sharqiya');
INSERT INTO `databand_city` VALUES ('3194', 'al-Hawiya', 'SAU', 'Mekka');
INSERT INTO `databand_city` VALUES ('3195', 'Unayza', 'SAU', 'Qasim');
INSERT INTO `databand_city` VALUES ('3196', 'Najran', 'SAU', 'Najran');
INSERT INTO `databand_city` VALUES ('3197', 'Pikine', 'SEN', 'Cap-Vert');
INSERT INTO `databand_city` VALUES ('3198', 'Dakar', 'SEN', 'Cap-Vert');
INSERT INTO `databand_city` VALUES ('3199', 'Thiès', 'SEN', 'Thiès');
INSERT INTO `databand_city` VALUES ('3200', 'Kaolack', 'SEN', 'Kaolack');
INSERT INTO `databand_city` VALUES ('3201', 'Ziguinchor', 'SEN', 'Ziguinchor');
INSERT INTO `databand_city` VALUES ('3202', 'Rufisque', 'SEN', 'Cap-Vert');
INSERT INTO `databand_city` VALUES ('3203', 'Saint-Louis', 'SEN', 'Saint-Louis');
INSERT INTO `databand_city` VALUES ('3204', 'Mbour', 'SEN', 'Thiès');
INSERT INTO `databand_city` VALUES ('3205', 'Diourbel', 'SEN', 'Diourbel');
INSERT INTO `databand_city` VALUES ('3206', 'Victoria', 'SYC', 'Mahé');
INSERT INTO `databand_city` VALUES ('3207', 'Freetown', 'SLE', 'Western');
INSERT INTO `databand_city` VALUES ('3208', 'Singapore', 'SGP', '–');
INSERT INTO `databand_city` VALUES ('3209', 'Bratislava', 'SVK', 'Bratislava');
INSERT INTO `databand_city` VALUES ('3210', 'Košice', 'SVK', 'Východné Slovensko');
INSERT INTO `databand_city` VALUES ('3211', 'Prešov', 'SVK', 'Východné Slovensko');
INSERT INTO `databand_city` VALUES ('3212', 'Ljubljana', 'SVN', 'Osrednjeslovenska');
INSERT INTO `databand_city` VALUES ('3213', 'Maribor', 'SVN', 'Podravska');
INSERT INTO `databand_city` VALUES ('3214', 'Mogadishu', 'SOM', 'Banaadir');
INSERT INTO `databand_city` VALUES ('3215', 'Hargeysa', 'SOM', 'Woqooyi Galbeed');
INSERT INTO `databand_city` VALUES ('3216', 'Kismaayo', 'SOM', 'Jubbada Hoose');
INSERT INTO `databand_city` VALUES ('3217', 'Colombo', 'LKA', 'Western');
INSERT INTO `databand_city` VALUES ('3218', 'Dehiwala', 'LKA', 'Western');
INSERT INTO `databand_city` VALUES ('3219', 'Moratuwa', 'LKA', 'Western');
INSERT INTO `databand_city` VALUES ('3220', 'Jaffna', 'LKA', 'Northern');
INSERT INTO `databand_city` VALUES ('3221', 'Kandy', 'LKA', 'Central');
INSERT INTO `databand_city` VALUES ('3222', 'Sri Jayawardenepura Kotte', 'LKA', 'Western');
INSERT INTO `databand_city` VALUES ('3223', 'Negombo', 'LKA', 'Western');
INSERT INTO `databand_city` VALUES ('3224', 'Omdurman', 'SDN', 'Khartum');
INSERT INTO `databand_city` VALUES ('3225', 'Khartum', 'SDN', 'Khartum');
INSERT INTO `databand_city` VALUES ('3226', 'Sharq al-Nil', 'SDN', 'Khartum');
INSERT INTO `databand_city` VALUES ('3227', 'Port Sudan', 'SDN', 'al-Bahr al-Ahmar');
INSERT INTO `databand_city` VALUES ('3228', 'Kassala', 'SDN', 'Kassala');
INSERT INTO `databand_city` VALUES ('3229', 'Obeid', 'SDN', 'Kurdufan al-Shamaliy');
INSERT INTO `databand_city` VALUES ('3230', 'Nyala', 'SDN', 'Darfur al-Janubiya');
INSERT INTO `databand_city` VALUES ('3231', 'Wad Madani', 'SDN', 'al-Jazira');
INSERT INTO `databand_city` VALUES ('3232', 'al-Qadarif', 'SDN', 'al-Qadarif');
INSERT INTO `databand_city` VALUES ('3233', 'Kusti', 'SDN', 'al-Bahr al-Abyad');
INSERT INTO `databand_city` VALUES ('3234', 'al-Fashir', 'SDN', 'Darfur al-Shamaliya');
INSERT INTO `databand_city` VALUES ('3235', 'Juba', 'SDN', 'Bahr al-Jabal');
INSERT INTO `databand_city` VALUES ('3236', 'Helsinki [Helsingfors]', 'FIN', 'Newmaa');
INSERT INTO `databand_city` VALUES ('3237', 'Espoo', 'FIN', 'Newmaa');
INSERT INTO `databand_city` VALUES ('3238', 'Tampere', 'FIN', 'Pirkanmaa');
INSERT INTO `databand_city` VALUES ('3239', 'Vantaa', 'FIN', 'Newmaa');
INSERT INTO `databand_city` VALUES ('3240', 'Turku [Åbo]', 'FIN', 'Varsinais-Suomi');
INSERT INTO `databand_city` VALUES ('3241', 'Oulu', 'FIN', 'Pohjois-Pohjanmaa');
INSERT INTO `databand_city` VALUES ('3242', 'Lahti', 'FIN', 'Päijät-Häme');
INSERT INTO `databand_city` VALUES ('3243', 'Paramaribo', 'SUR', 'Paramaribo');
INSERT INTO `databand_city` VALUES ('3244', 'Mbabane', 'SWZ', 'Hhohho');
INSERT INTO `databand_city` VALUES ('3245', 'Zürich', 'CHE', 'Zürich');
INSERT INTO `databand_city` VALUES ('3246', 'Geneve', 'CHE', 'Geneve');
INSERT INTO `databand_city` VALUES ('3247', 'Basel', 'CHE', 'Basel-Stadt');
INSERT INTO `databand_city` VALUES ('3248', 'Bern', 'CHE', 'Bern');
INSERT INTO `databand_city` VALUES ('3249', 'Lausanne', 'CHE', 'Vaud');
INSERT INTO `databand_city` VALUES ('3250', 'Damascus', 'SYR', 'Damascus');
INSERT INTO `databand_city` VALUES ('3251', 'Aleppo', 'SYR', 'Aleppo');
INSERT INTO `databand_city` VALUES ('3252', 'Hims', 'SYR', 'Hims');
INSERT INTO `databand_city` VALUES ('3253', 'Hama', 'SYR', 'Hama');
INSERT INTO `databand_city` VALUES ('3254', 'Latakia', 'SYR', 'Latakia');
INSERT INTO `databand_city` VALUES ('3255', 'al-Qamishliya', 'SYR', 'al-Hasaka');
INSERT INTO `databand_city` VALUES ('3256', 'Dayr al-Zawr', 'SYR', 'Dayr al-Zawr');
INSERT INTO `databand_city` VALUES ('3257', 'Jaramana', 'SYR', 'Damaskos');
INSERT INTO `databand_city` VALUES ('3258', 'Duma', 'SYR', 'Damaskos');
INSERT INTO `databand_city` VALUES ('3259', 'al-Raqqa', 'SYR', 'al-Raqqa');
INSERT INTO `databand_city` VALUES ('3260', 'Idlib', 'SYR', 'Idlib');
INSERT INTO `databand_city` VALUES ('3261', 'Dushanbe', 'TJK', 'Karotegin');
INSERT INTO `databand_city` VALUES ('3262', 'Khujand', 'TJK', 'Khujand');
INSERT INTO `databand_city` VALUES ('3263', 'Taipei', 'TWN', 'Taipei');
INSERT INTO `databand_city` VALUES ('3264', 'Kaohsiung', 'TWN', 'Kaohsiung');
INSERT INTO `databand_city` VALUES ('3265', 'Taichung', 'TWN', 'Taichung');
INSERT INTO `databand_city` VALUES ('3266', 'Tainan', 'TWN', 'Tainan');
INSERT INTO `databand_city` VALUES ('3267', 'Panchiao', 'TWN', 'Taipei');
INSERT INTO `databand_city` VALUES ('3268', 'Chungho', 'TWN', 'Taipei');
INSERT INTO `databand_city` VALUES ('3269', 'Keelung (Chilung)', 'TWN', 'Keelung');
INSERT INTO `databand_city` VALUES ('3270', 'Sanchung', 'TWN', 'Taipei');
INSERT INTO `databand_city` VALUES ('3271', 'Hsinchuang', 'TWN', 'Taipei');
INSERT INTO `databand_city` VALUES ('3272', 'Hsinchu', 'TWN', 'Hsinchu');
INSERT INTO `databand_city` VALUES ('3273', 'Chungli', 'TWN', 'Taoyuan');
INSERT INTO `databand_city` VALUES ('3274', 'Fengshan', 'TWN', 'Kaohsiung');
INSERT INTO `databand_city` VALUES ('3275', 'Taoyuan', 'TWN', 'Taoyuan');
INSERT INTO `databand_city` VALUES ('3276', 'Chiayi', 'TWN', 'Chiayi');
INSERT INTO `databand_city` VALUES ('3277', 'Hsintien', 'TWN', 'Taipei');
INSERT INTO `databand_city` VALUES ('3278', 'Changhwa', 'TWN', 'Changhwa');
INSERT INTO `databand_city` VALUES ('3279', 'Yungho', 'TWN', 'Taipei');
INSERT INTO `databand_city` VALUES ('3280', 'Tucheng', 'TWN', 'Taipei');
INSERT INTO `databand_city` VALUES ('3281', 'Pingtung', 'TWN', 'Pingtung');
INSERT INTO `databand_city` VALUES ('3282', 'Yungkang', 'TWN', 'Tainan');
INSERT INTO `databand_city` VALUES ('3283', 'Pingchen', 'TWN', 'Taoyuan');
INSERT INTO `databand_city` VALUES ('3284', 'Tali', 'TWN', 'Taichung');
INSERT INTO `databand_city` VALUES ('3285', 'Taiping', 'TWN', '');
INSERT INTO `databand_city` VALUES ('3286', 'Pate', 'TWN', 'Taoyuan');
INSERT INTO `databand_city` VALUES ('3287', 'Fengyuan', 'TWN', 'Taichung');
INSERT INTO `databand_city` VALUES ('3288', 'Luchou', 'TWN', 'Taipei');
INSERT INTO `databand_city` VALUES ('3289', 'Hsichuh', 'TWN', 'Taipei');
INSERT INTO `databand_city` VALUES ('3290', 'Shulin', 'TWN', 'Taipei');
INSERT INTO `databand_city` VALUES ('3291', 'Yuanlin', 'TWN', 'Changhwa');
INSERT INTO `databand_city` VALUES ('3292', 'Yangmei', 'TWN', 'Taoyuan');
INSERT INTO `databand_city` VALUES ('3293', 'Taliao', 'TWN', '');
INSERT INTO `databand_city` VALUES ('3294', 'Kueishan', 'TWN', '');
INSERT INTO `databand_city` VALUES ('3295', 'Tanshui', 'TWN', 'Taipei');
INSERT INTO `databand_city` VALUES ('3296', 'Taitung', 'TWN', 'Taitung');
INSERT INTO `databand_city` VALUES ('3297', 'Hualien', 'TWN', 'Hualien');
INSERT INTO `databand_city` VALUES ('3298', 'Nantou', 'TWN', 'Nantou');
INSERT INTO `databand_city` VALUES ('3299', 'Lungtan', 'TWN', 'Taipei');
INSERT INTO `databand_city` VALUES ('3300', 'Touliu', 'TWN', 'Yünlin');
INSERT INTO `databand_city` VALUES ('3301', 'Tsaotun', 'TWN', 'Nantou');
INSERT INTO `databand_city` VALUES ('3302', 'Kangshan', 'TWN', 'Kaohsiung');
INSERT INTO `databand_city` VALUES ('3303', 'Ilan', 'TWN', 'Ilan');
INSERT INTO `databand_city` VALUES ('3304', 'Miaoli', 'TWN', 'Miaoli');
INSERT INTO `databand_city` VALUES ('3305', 'Dar es Salaam', 'TZA', 'Dar es Salaam');
INSERT INTO `databand_city` VALUES ('3306', 'Dodoma', 'TZA', 'Dodoma');
INSERT INTO `databand_city` VALUES ('3307', 'Mwanza', 'TZA', 'Mwanza');
INSERT INTO `databand_city` VALUES ('3308', 'Zanzibar', 'TZA', 'Zanzibar West');
INSERT INTO `databand_city` VALUES ('3309', 'Tanga', 'TZA', 'Tanga');
INSERT INTO `databand_city` VALUES ('3310', 'Mbeya', 'TZA', 'Mbeya');
INSERT INTO `databand_city` VALUES ('3311', 'Morogoro', 'TZA', 'Morogoro');
INSERT INTO `databand_city` VALUES ('3312', 'Arusha', 'TZA', 'Arusha');
INSERT INTO `databand_city` VALUES ('3313', 'Moshi', 'TZA', 'Kilimanjaro');
INSERT INTO `databand_city` VALUES ('3314', 'Tabora', 'TZA', 'Tabora');
INSERT INTO `databand_city` VALUES ('3315', 'København', 'DNK', 'København');
INSERT INTO `databand_city` VALUES ('3316', 'Århus', 'DNK', 'Århus');
INSERT INTO `databand_city` VALUES ('3317', 'Odense', 'DNK', 'Fyn');
INSERT INTO `databand_city` VALUES ('3318', 'Aalborg', 'DNK', 'Nordjylland');
INSERT INTO `databand_city` VALUES ('3319', 'Frederiksberg', 'DNK', 'Frederiksberg');
INSERT INTO `databand_city` VALUES ('3320', 'Bangkok', 'THA', 'Bangkok');
INSERT INTO `databand_city` VALUES ('3321', 'Nonthaburi', 'THA', 'Nonthaburi');
INSERT INTO `databand_city` VALUES ('3322', 'Nakhon Ratchasima', 'THA', 'Nakhon Ratchasima');
INSERT INTO `databand_city` VALUES ('3323', 'Chiang Mai', 'THA', 'Chiang Mai');
INSERT INTO `databand_city` VALUES ('3324', 'Udon Thani', 'THA', 'Udon Thani');
INSERT INTO `databand_city` VALUES ('3325', 'Hat Yai', 'THA', 'Songkhla');
INSERT INTO `databand_city` VALUES ('3326', 'Khon Kaen', 'THA', 'Khon Kaen');
INSERT INTO `databand_city` VALUES ('3327', 'Pak Kret', 'THA', 'Nonthaburi');
INSERT INTO `databand_city` VALUES ('3328', 'Nakhon Sawan', 'THA', 'Nakhon Sawan');
INSERT INTO `databand_city` VALUES ('3329', 'Ubon Ratchathani', 'THA', 'Ubon Ratchathani');
INSERT INTO `databand_city` VALUES ('3330', 'Songkhla', 'THA', 'Songkhla');
INSERT INTO `databand_city` VALUES ('3331', 'Nakhon Pathom', 'THA', 'Nakhon Pathom');
INSERT INTO `databand_city` VALUES ('3332', 'Lomé', 'TGO', 'Maritime');
INSERT INTO `databand_city` VALUES ('3333', 'Fakaofo', 'TKL', 'Fakaofo');
INSERT INTO `databand_city` VALUES ('3334', 'Nuku´alofa', 'TON', 'Tongatapu');
INSERT INTO `databand_city` VALUES ('3335', 'Chaguanas', 'TTO', 'Caroni');
INSERT INTO `databand_city` VALUES ('3336', 'Port-of-Spain', 'TTO', 'Port-of-Spain');
INSERT INTO `databand_city` VALUES ('3337', 'N´Djaména', 'TCD', 'Chari-Baguirmi');
INSERT INTO `databand_city` VALUES ('3338', 'Moundou', 'TCD', 'Logone Occidental');
INSERT INTO `databand_city` VALUES ('3339', 'Praha', 'CZE', 'Hlavní mesto Praha');
INSERT INTO `databand_city` VALUES ('3340', 'Brno', 'CZE', 'Jizní Morava');
INSERT INTO `databand_city` VALUES ('3341', 'Ostrava', 'CZE', 'Severní Morava');
INSERT INTO `databand_city` VALUES ('3342', 'Plzen', 'CZE', 'Zapadní Cechy');
INSERT INTO `databand_city` VALUES ('3343', 'Olomouc', 'CZE', 'Severní Morava');
INSERT INTO `databand_city` VALUES ('3344', 'Liberec', 'CZE', 'Severní Cechy');
INSERT INTO `databand_city` VALUES ('3345', 'Ceské Budejovice', 'CZE', 'Jizní Cechy');
INSERT INTO `databand_city` VALUES ('3346', 'Hradec Králové', 'CZE', 'Východní Cechy');
INSERT INTO `databand_city` VALUES ('3347', 'Ústí nad Labem', 'CZE', 'Severní Cechy');
INSERT INTO `databand_city` VALUES ('3348', 'Pardubice', 'CZE', 'Východní Cechy');
INSERT INTO `databand_city` VALUES ('3349', 'Tunis', 'TUN', 'Tunis');
INSERT INTO `databand_city` VALUES ('3350', 'Sfax', 'TUN', 'Sfax');
INSERT INTO `databand_city` VALUES ('3351', 'Ariana', 'TUN', 'Ariana');
INSERT INTO `databand_city` VALUES ('3352', 'Ettadhamen', 'TUN', 'Ariana');
INSERT INTO `databand_city` VALUES ('3353', 'Sousse', 'TUN', 'Sousse');
INSERT INTO `databand_city` VALUES ('3354', 'Kairouan', 'TUN', 'Kairouan');
INSERT INTO `databand_city` VALUES ('3355', 'Biserta', 'TUN', 'Biserta');
INSERT INTO `databand_city` VALUES ('3356', 'Gabès', 'TUN', 'Gabès');
INSERT INTO `databand_city` VALUES ('3357', 'Istanbul', 'TUR', 'Istanbul');
INSERT INTO `databand_city` VALUES ('3358', 'Ankara', 'TUR', 'Ankara');
INSERT INTO `databand_city` VALUES ('3359', 'Izmir', 'TUR', 'Izmir');
INSERT INTO `databand_city` VALUES ('3360', 'Adana', 'TUR', 'Adana');
INSERT INTO `databand_city` VALUES ('3361', 'Bursa', 'TUR', 'Bursa');
INSERT INTO `databand_city` VALUES ('3362', 'Gaziantep', 'TUR', 'Gaziantep');
INSERT INTO `databand_city` VALUES ('3363', 'Konya', 'TUR', 'Konya');
INSERT INTO `databand_city` VALUES ('3364', 'Mersin (Içel)', 'TUR', 'Içel');
INSERT INTO `databand_city` VALUES ('3365', 'Antalya', 'TUR', 'Antalya');
INSERT INTO `databand_city` VALUES ('3366', 'Diyarbakir', 'TUR', 'Diyarbakir');
INSERT INTO `databand_city` VALUES ('3367', 'Kayseri', 'TUR', 'Kayseri');
INSERT INTO `databand_city` VALUES ('3368', 'Eskisehir', 'TUR', 'Eskisehir');
INSERT INTO `databand_city` VALUES ('3369', 'Sanliurfa', 'TUR', 'Sanliurfa');
INSERT INTO `databand_city` VALUES ('3370', 'Samsun', 'TUR', 'Samsun');
INSERT INTO `databand_city` VALUES ('3371', 'Malatya', 'TUR', 'Malatya');
INSERT INTO `databand_city` VALUES ('3372', 'Gebze', 'TUR', 'Kocaeli');
INSERT INTO `databand_city` VALUES ('3373', 'Denizli', 'TUR', 'Denizli');
INSERT INTO `databand_city` VALUES ('3374', 'Sivas', 'TUR', 'Sivas');
INSERT INTO `databand_city` VALUES ('3375', 'Erzurum', 'TUR', 'Erzurum');
INSERT INTO `databand_city` VALUES ('3376', 'Tarsus', 'TUR', 'Adana');
INSERT INTO `databand_city` VALUES ('3377', 'Kahramanmaras', 'TUR', 'Kahramanmaras');
INSERT INTO `databand_city` VALUES ('3378', 'Elâzig', 'TUR', 'Elâzig');
INSERT INTO `databand_city` VALUES ('3379', 'Van', 'TUR', 'Van');
INSERT INTO `databand_city` VALUES ('3380', 'Sultanbeyli', 'TUR', 'Istanbul');
INSERT INTO `databand_city` VALUES ('3381', 'Izmit (Kocaeli)', 'TUR', 'Kocaeli');
INSERT INTO `databand_city` VALUES ('3382', 'Manisa', 'TUR', 'Manisa');
INSERT INTO `databand_city` VALUES ('3383', 'Batman', 'TUR', 'Batman');
INSERT INTO `databand_city` VALUES ('3384', 'Balikesir', 'TUR', 'Balikesir');
INSERT INTO `databand_city` VALUES ('3385', 'Sakarya (Adapazari)', 'TUR', 'Sakarya');
INSERT INTO `databand_city` VALUES ('3386', 'Iskenderun', 'TUR', 'Hatay');
INSERT INTO `databand_city` VALUES ('3387', 'Osmaniye', 'TUR', 'Osmaniye');
INSERT INTO `databand_city` VALUES ('3388', 'Çorum', 'TUR', 'Çorum');
INSERT INTO `databand_city` VALUES ('3389', 'Kütahya', 'TUR', 'Kütahya');
INSERT INTO `databand_city` VALUES ('3390', 'Hatay (Antakya)', 'TUR', 'Hatay');
INSERT INTO `databand_city` VALUES ('3391', 'Kirikkale', 'TUR', 'Kirikkale');
INSERT INTO `databand_city` VALUES ('3392', 'Adiyaman', 'TUR', 'Adiyaman');
INSERT INTO `databand_city` VALUES ('3393', 'Trabzon', 'TUR', 'Trabzon');
INSERT INTO `databand_city` VALUES ('3394', 'Ordu', 'TUR', 'Ordu');
INSERT INTO `databand_city` VALUES ('3395', 'Aydin', 'TUR', 'Aydin');
INSERT INTO `databand_city` VALUES ('3396', 'Usak', 'TUR', 'Usak');
INSERT INTO `databand_city` VALUES ('3397', 'Edirne', 'TUR', 'Edirne');
INSERT INTO `databand_city` VALUES ('3398', 'Çorlu', 'TUR', 'Tekirdag');
INSERT INTO `databand_city` VALUES ('3399', 'Isparta', 'TUR', 'Isparta');
INSERT INTO `databand_city` VALUES ('3400', 'Karabük', 'TUR', 'Karabük');
INSERT INTO `databand_city` VALUES ('3401', 'Kilis', 'TUR', 'Kilis');
INSERT INTO `databand_city` VALUES ('3402', 'Alanya', 'TUR', 'Antalya');
INSERT INTO `databand_city` VALUES ('3403', 'Kiziltepe', 'TUR', 'Mardin');
INSERT INTO `databand_city` VALUES ('3404', 'Zonguldak', 'TUR', 'Zonguldak');
INSERT INTO `databand_city` VALUES ('3405', 'Siirt', 'TUR', 'Siirt');
INSERT INTO `databand_city` VALUES ('3406', 'Viransehir', 'TUR', 'Sanliurfa');
INSERT INTO `databand_city` VALUES ('3407', 'Tekirdag', 'TUR', 'Tekirdag');
INSERT INTO `databand_city` VALUES ('3408', 'Karaman', 'TUR', 'Karaman');
INSERT INTO `databand_city` VALUES ('3409', 'Afyon', 'TUR', 'Afyon');
INSERT INTO `databand_city` VALUES ('3410', 'Aksaray', 'TUR', 'Aksaray');
INSERT INTO `databand_city` VALUES ('3411', 'Ceyhan', 'TUR', 'Adana');
INSERT INTO `databand_city` VALUES ('3412', 'Erzincan', 'TUR', 'Erzincan');
INSERT INTO `databand_city` VALUES ('3413', 'Bismil', 'TUR', 'Diyarbakir');
INSERT INTO `databand_city` VALUES ('3414', 'Nazilli', 'TUR', 'Aydin');
INSERT INTO `databand_city` VALUES ('3415', 'Tokat', 'TUR', 'Tokat');
INSERT INTO `databand_city` VALUES ('3416', 'Kars', 'TUR', 'Kars');
INSERT INTO `databand_city` VALUES ('3417', 'Inegöl', 'TUR', 'Bursa');
INSERT INTO `databand_city` VALUES ('3418', 'Bandirma', 'TUR', 'Balikesir');
INSERT INTO `databand_city` VALUES ('3419', 'Ashgabat', 'TKM', 'Ahal');
INSERT INTO `databand_city` VALUES ('3420', 'Chärjew', 'TKM', 'Lebap');
INSERT INTO `databand_city` VALUES ('3421', 'Dashhowuz', 'TKM', 'Dashhowuz');
INSERT INTO `databand_city` VALUES ('3422', 'Mary', 'TKM', 'Mary');
INSERT INTO `databand_city` VALUES ('3423', 'Cockburn Town', 'TCA', 'Grand Turk');
INSERT INTO `databand_city` VALUES ('3424', 'Funafuti', 'TUV', 'Funafuti');
INSERT INTO `databand_city` VALUES ('3425', 'Kampala', 'UGA', 'Central');
INSERT INTO `databand_city` VALUES ('3426', 'Kyiv', 'UKR', 'Kiova');
INSERT INTO `databand_city` VALUES ('3427', 'Harkova [Harkiv]', 'UKR', 'Harkova');
INSERT INTO `databand_city` VALUES ('3428', 'Dnipropetrovsk', 'UKR', 'Dnipropetrovsk');
INSERT INTO `databand_city` VALUES ('3429', 'Donetsk', 'UKR', 'Donetsk');
INSERT INTO `databand_city` VALUES ('3430', 'Odesa', 'UKR', 'Odesa');
INSERT INTO `databand_city` VALUES ('3431', 'Zaporizzja', 'UKR', 'Zaporizzja');
INSERT INTO `databand_city` VALUES ('3432', 'Lviv', 'UKR', 'Lviv');
INSERT INTO `databand_city` VALUES ('3433', 'Kryvyi Rig', 'UKR', 'Dnipropetrovsk');
INSERT INTO `databand_city` VALUES ('3434', 'Mykolajiv', 'UKR', 'Mykolajiv');
INSERT INTO `databand_city` VALUES ('3435', 'Mariupol', 'UKR', 'Donetsk');
INSERT INTO `databand_city` VALUES ('3436', 'Lugansk', 'UKR', 'Lugansk');
INSERT INTO `databand_city` VALUES ('3437', 'Vinnytsja', 'UKR', 'Vinnytsja');
INSERT INTO `databand_city` VALUES ('3438', 'Makijivka', 'UKR', 'Donetsk');
INSERT INTO `databand_city` VALUES ('3439', 'Herson', 'UKR', 'Herson');
INSERT INTO `databand_city` VALUES ('3440', 'Sevastopol', 'UKR', 'Krim');
INSERT INTO `databand_city` VALUES ('3441', 'Simferopol', 'UKR', 'Krim');
INSERT INTO `databand_city` VALUES ('3442', 'Pultava [Poltava]', 'UKR', 'Pultava');
INSERT INTO `databand_city` VALUES ('3443', 'Tšernigiv', 'UKR', 'Tšernigiv');
INSERT INTO `databand_city` VALUES ('3444', 'Tšerkasy', 'UKR', 'Tšerkasy');
INSERT INTO `databand_city` VALUES ('3445', 'Gorlivka', 'UKR', 'Donetsk');
INSERT INTO `databand_city` VALUES ('3446', 'Zytomyr', 'UKR', 'Zytomyr');
INSERT INTO `databand_city` VALUES ('3447', 'Sumy', 'UKR', 'Sumy');
INSERT INTO `databand_city` VALUES ('3448', 'Dniprodzerzynsk', 'UKR', 'Dnipropetrovsk');
INSERT INTO `databand_city` VALUES ('3449', 'Kirovograd', 'UKR', 'Kirovograd');
INSERT INTO `databand_city` VALUES ('3450', 'Hmelnytskyi', 'UKR', 'Hmelnytskyi');
INSERT INTO `databand_city` VALUES ('3451', 'Tšernivtsi', 'UKR', 'Tšernivtsi');
INSERT INTO `databand_city` VALUES ('3452', 'Rivne', 'UKR', 'Rivne');
INSERT INTO `databand_city` VALUES ('3453', 'Krementšuk', 'UKR', 'Pultava');
INSERT INTO `databand_city` VALUES ('3454', 'Ivano-Frankivsk', 'UKR', 'Ivano-Frankivsk');
INSERT INTO `databand_city` VALUES ('3455', 'Ternopil', 'UKR', 'Ternopil');
INSERT INTO `databand_city` VALUES ('3456', 'Lutsk', 'UKR', 'Volynia');
INSERT INTO `databand_city` VALUES ('3457', 'Bila Tserkva', 'UKR', 'Kiova');
INSERT INTO `databand_city` VALUES ('3458', 'Kramatorsk', 'UKR', 'Donetsk');
INSERT INTO `databand_city` VALUES ('3459', 'Melitopol', 'UKR', 'Zaporizzja');
INSERT INTO `databand_city` VALUES ('3460', 'Kertš', 'UKR', 'Krim');
INSERT INTO `databand_city` VALUES ('3461', 'Nikopol', 'UKR', 'Dnipropetrovsk');
INSERT INTO `databand_city` VALUES ('3462', 'Berdjansk', 'UKR', 'Zaporizzja');
INSERT INTO `databand_city` VALUES ('3463', 'Pavlograd', 'UKR', 'Dnipropetrovsk');
INSERT INTO `databand_city` VALUES ('3464', 'Sjeverodonetsk', 'UKR', 'Lugansk');
INSERT INTO `databand_city` VALUES ('3465', 'Slovjansk', 'UKR', 'Donetsk');
INSERT INTO `databand_city` VALUES ('3466', 'Uzgorod', 'UKR', 'Taka-Karpatia');
INSERT INTO `databand_city` VALUES ('3467', 'Altševsk', 'UKR', 'Lugansk');
INSERT INTO `databand_city` VALUES ('3468', 'Lysytšansk', 'UKR', 'Lugansk');
INSERT INTO `databand_city` VALUES ('3469', 'Jevpatorija', 'UKR', 'Krim');
INSERT INTO `databand_city` VALUES ('3470', 'Kamjanets-Podilskyi', 'UKR', 'Hmelnytskyi');
INSERT INTO `databand_city` VALUES ('3471', 'Jenakijeve', 'UKR', 'Donetsk');
INSERT INTO `databand_city` VALUES ('3472', 'Krasnyi Lutš', 'UKR', 'Lugansk');
INSERT INTO `databand_city` VALUES ('3473', 'Stahanov', 'UKR', 'Lugansk');
INSERT INTO `databand_city` VALUES ('3474', 'Oleksandrija', 'UKR', 'Kirovograd');
INSERT INTO `databand_city` VALUES ('3475', 'Konotop', 'UKR', 'Sumy');
INSERT INTO `databand_city` VALUES ('3476', 'Kostjantynivka', 'UKR', 'Donetsk');
INSERT INTO `databand_city` VALUES ('3477', 'Berdytšiv', 'UKR', 'Zytomyr');
INSERT INTO `databand_city` VALUES ('3478', 'Izmajil', 'UKR', 'Odesa');
INSERT INTO `databand_city` VALUES ('3479', 'Šostka', 'UKR', 'Sumy');
INSERT INTO `databand_city` VALUES ('3480', 'Uman', 'UKR', 'Tšerkasy');
INSERT INTO `databand_city` VALUES ('3481', 'Brovary', 'UKR', 'Kiova');
INSERT INTO `databand_city` VALUES ('3482', 'Mukatševe', 'UKR', 'Taka-Karpatia');
INSERT INTO `databand_city` VALUES ('3483', 'Budapest', 'HUN', 'Budapest');
INSERT INTO `databand_city` VALUES ('3484', 'Debrecen', 'HUN', 'Hajdú-Bihar');
INSERT INTO `databand_city` VALUES ('3485', 'Miskolc', 'HUN', 'Borsod-Abaúj-Zemplén');
INSERT INTO `databand_city` VALUES ('3486', 'Szeged', 'HUN', 'Csongrád');
INSERT INTO `databand_city` VALUES ('3487', 'Pécs', 'HUN', 'Baranya');
INSERT INTO `databand_city` VALUES ('3488', 'Györ', 'HUN', 'Györ-Moson-Sopron');
INSERT INTO `databand_city` VALUES ('3489', 'Nyiregyháza', 'HUN', 'Szabolcs-Szatmár-Ber');
INSERT INTO `databand_city` VALUES ('3490', 'Kecskemét', 'HUN', 'Bács-Kiskun');
INSERT INTO `databand_city` VALUES ('3491', 'Székesfehérvár', 'HUN', 'Fejér');
INSERT INTO `databand_city` VALUES ('3492', 'Montevideo', 'URY', 'Montevideo');
INSERT INTO `databand_city` VALUES ('3493', 'Nouméa', 'NCL', '–');
INSERT INTO `databand_city` VALUES ('3494', 'Auckland', 'NZL', 'Auckland');
INSERT INTO `databand_city` VALUES ('3495', 'Christchurch', 'NZL', 'Canterbury');
INSERT INTO `databand_city` VALUES ('3496', 'Manukau', 'NZL', 'Auckland');
INSERT INTO `databand_city` VALUES ('3497', 'North Shore', 'NZL', 'Auckland');
INSERT INTO `databand_city` VALUES ('3498', 'Waitakere', 'NZL', 'Auckland');
INSERT INTO `databand_city` VALUES ('3499', 'Wellington', 'NZL', 'Wellington');
INSERT INTO `databand_city` VALUES ('3500', 'Dunedin', 'NZL', 'Dunedin');
INSERT INTO `databand_city` VALUES ('3501', 'Hamilton', 'NZL', 'Hamilton');
INSERT INTO `databand_city` VALUES ('3502', 'Lower Hutt', 'NZL', 'Wellington');
INSERT INTO `databand_city` VALUES ('3503', 'Toskent', 'UZB', 'Toskent Shahri');
INSERT INTO `databand_city` VALUES ('3504', 'Namangan', 'UZB', 'Namangan');
INSERT INTO `databand_city` VALUES ('3505', 'Samarkand', 'UZB', 'Samarkand');
INSERT INTO `databand_city` VALUES ('3506', 'Andijon', 'UZB', 'Andijon');
INSERT INTO `databand_city` VALUES ('3507', 'Buhoro', 'UZB', 'Buhoro');
INSERT INTO `databand_city` VALUES ('3508', 'Karsi', 'UZB', 'Qashqadaryo');
INSERT INTO `databand_city` VALUES ('3509', 'Nukus', 'UZB', 'Karakalpakistan');
INSERT INTO `databand_city` VALUES ('3510', 'Kükon', 'UZB', 'Fargona');
INSERT INTO `databand_city` VALUES ('3511', 'Fargona', 'UZB', 'Fargona');
INSERT INTO `databand_city` VALUES ('3512', 'Circik', 'UZB', 'Toskent');
INSERT INTO `databand_city` VALUES ('3513', 'Margilon', 'UZB', 'Fargona');
INSERT INTO `databand_city` VALUES ('3514', 'Ürgenc', 'UZB', 'Khorazm');
INSERT INTO `databand_city` VALUES ('3515', 'Angren', 'UZB', 'Toskent');
INSERT INTO `databand_city` VALUES ('3516', 'Cizah', 'UZB', 'Cizah');
INSERT INTO `databand_city` VALUES ('3517', 'Navoi', 'UZB', 'Navoi');
INSERT INTO `databand_city` VALUES ('3518', 'Olmalik', 'UZB', 'Toskent');
INSERT INTO `databand_city` VALUES ('3519', 'Termiz', 'UZB', 'Surkhondaryo');
INSERT INTO `databand_city` VALUES ('3520', 'Minsk', 'BLR', 'Horad Minsk');
INSERT INTO `databand_city` VALUES ('3521', 'Gomel', 'BLR', 'Gomel');
INSERT INTO `databand_city` VALUES ('3522', 'Mogiljov', 'BLR', 'Mogiljov');
INSERT INTO `databand_city` VALUES ('3523', 'Vitebsk', 'BLR', 'Vitebsk');
INSERT INTO `databand_city` VALUES ('3524', 'Grodno', 'BLR', 'Grodno');
INSERT INTO `databand_city` VALUES ('3525', 'Brest', 'BLR', 'Brest');
INSERT INTO `databand_city` VALUES ('3526', 'Bobruisk', 'BLR', 'Mogiljov');
INSERT INTO `databand_city` VALUES ('3527', 'Baranovitši', 'BLR', 'Brest');
INSERT INTO `databand_city` VALUES ('3528', 'Borisov', 'BLR', 'Minsk');
INSERT INTO `databand_city` VALUES ('3529', 'Pinsk', 'BLR', 'Brest');
INSERT INTO `databand_city` VALUES ('3530', 'Orša', 'BLR', 'Vitebsk');
INSERT INTO `databand_city` VALUES ('3531', 'Mozyr', 'BLR', 'Gomel');
INSERT INTO `databand_city` VALUES ('3532', 'Novopolotsk', 'BLR', 'Vitebsk');
INSERT INTO `databand_city` VALUES ('3533', 'Lida', 'BLR', 'Grodno');
INSERT INTO `databand_city` VALUES ('3534', 'Soligorsk', 'BLR', 'Minsk');
INSERT INTO `databand_city` VALUES ('3535', 'Molodetšno', 'BLR', 'Minsk');
INSERT INTO `databand_city` VALUES ('3536', 'Mata-Utu', 'WLF', 'Wallis');
INSERT INTO `databand_city` VALUES ('3537', 'Port-Vila', 'VUT', 'Shefa');
INSERT INTO `databand_city` VALUES ('3538', 'Città del Vaticano', 'VAT', '–');
INSERT INTO `databand_city` VALUES ('3539', 'Caracas', 'VEN', 'Distrito Federal');
INSERT INTO `databand_city` VALUES ('3540', 'Maracaíbo', 'VEN', 'Zulia');
INSERT INTO `databand_city` VALUES ('3541', 'Barquisimeto', 'VEN', 'Lara');
INSERT INTO `databand_city` VALUES ('3542', 'Valencia', 'VEN', 'Carabobo');
INSERT INTO `databand_city` VALUES ('3543', 'Ciudad Guayana', 'VEN', 'Bolívar');
INSERT INTO `databand_city` VALUES ('3544', 'Petare', 'VEN', 'Miranda');
INSERT INTO `databand_city` VALUES ('3545', 'Maracay', 'VEN', 'Aragua');
INSERT INTO `databand_city` VALUES ('3546', 'Barcelona', 'VEN', 'Anzoátegui');
INSERT INTO `databand_city` VALUES ('3547', 'Maturín', 'VEN', 'Monagas');
INSERT INTO `databand_city` VALUES ('3548', 'San Cristóbal', 'VEN', 'Táchira');
INSERT INTO `databand_city` VALUES ('3549', 'Ciudad Bolívar', 'VEN', 'Bolívar');
INSERT INTO `databand_city` VALUES ('3550', 'Cumaná', 'VEN', 'Sucre');
INSERT INTO `databand_city` VALUES ('3551', 'Mérida', 'VEN', 'Mérida');
INSERT INTO `databand_city` VALUES ('3552', 'Cabimas', 'VEN', 'Zulia');
INSERT INTO `databand_city` VALUES ('3553', 'Barinas', 'VEN', 'Barinas');
INSERT INTO `databand_city` VALUES ('3554', 'Turmero', 'VEN', 'Aragua');
INSERT INTO `databand_city` VALUES ('3555', 'Baruta', 'VEN', 'Miranda');
INSERT INTO `databand_city` VALUES ('3556', 'Puerto Cabello', 'VEN', 'Carabobo');
INSERT INTO `databand_city` VALUES ('3557', 'Santa Ana de Coro', 'VEN', 'Falcón');
INSERT INTO `databand_city` VALUES ('3558', 'Los Teques', 'VEN', 'Miranda');
INSERT INTO `databand_city` VALUES ('3559', 'Punto Fijo', 'VEN', 'Falcón');
INSERT INTO `databand_city` VALUES ('3560', 'Guarenas', 'VEN', 'Miranda');
INSERT INTO `databand_city` VALUES ('3561', 'Acarigua', 'VEN', 'Portuguesa');
INSERT INTO `databand_city` VALUES ('3562', 'Puerto La Cruz', 'VEN', 'Anzoátegui');
INSERT INTO `databand_city` VALUES ('3563', 'Ciudad Losada', 'VEN', '');
INSERT INTO `databand_city` VALUES ('3564', 'Guacara', 'VEN', 'Carabobo');
INSERT INTO `databand_city` VALUES ('3565', 'Valera', 'VEN', 'Trujillo');
INSERT INTO `databand_city` VALUES ('3566', 'Guanare', 'VEN', 'Portuguesa');
INSERT INTO `databand_city` VALUES ('3567', 'Carúpano', 'VEN', 'Sucre');
INSERT INTO `databand_city` VALUES ('3568', 'Catia La Mar', 'VEN', 'Distrito Federal');
INSERT INTO `databand_city` VALUES ('3569', 'El Tigre', 'VEN', 'Anzoátegui');
INSERT INTO `databand_city` VALUES ('3570', 'Guatire', 'VEN', 'Miranda');
INSERT INTO `databand_city` VALUES ('3571', 'Calabozo', 'VEN', 'Guárico');
INSERT INTO `databand_city` VALUES ('3572', 'Pozuelos', 'VEN', 'Anzoátegui');
INSERT INTO `databand_city` VALUES ('3573', 'Ciudad Ojeda', 'VEN', 'Zulia');
INSERT INTO `databand_city` VALUES ('3574', 'Ocumare del Tuy', 'VEN', 'Miranda');
INSERT INTO `databand_city` VALUES ('3575', 'Valle de la Pascua', 'VEN', 'Guárico');
INSERT INTO `databand_city` VALUES ('3576', 'Araure', 'VEN', 'Portuguesa');
INSERT INTO `databand_city` VALUES ('3577', 'San Fernando de Apure', 'VEN', 'Apure');
INSERT INTO `databand_city` VALUES ('3578', 'San Felipe', 'VEN', 'Yaracuy');
INSERT INTO `databand_city` VALUES ('3579', 'El Limón', 'VEN', 'Aragua');
INSERT INTO `databand_city` VALUES ('3580', 'Moscow', 'RUS', 'Moscow (City)');
INSERT INTO `databand_city` VALUES ('3581', 'St Petersburg', 'RUS', 'Pietari');
INSERT INTO `databand_city` VALUES ('3582', 'Novosibirsk', 'RUS', 'Novosibirsk');
INSERT INTO `databand_city` VALUES ('3583', 'Nizni Novgorod', 'RUS', 'Nizni Novgorod');
INSERT INTO `databand_city` VALUES ('3584', 'Jekaterinburg', 'RUS', 'Sverdlovsk');
INSERT INTO `databand_city` VALUES ('3585', 'Samara', 'RUS', 'Samara');
INSERT INTO `databand_city` VALUES ('3586', 'Omsk', 'RUS', 'Omsk');
INSERT INTO `databand_city` VALUES ('3587', 'Kazan', 'RUS', 'Tatarstan');
INSERT INTO `databand_city` VALUES ('3588', 'Ufa', 'RUS', 'Baškortostan');
INSERT INTO `databand_city` VALUES ('3589', 'Tšeljabinsk', 'RUS', 'Tšeljabinsk');
INSERT INTO `databand_city` VALUES ('3590', 'Rostov-na-Donu', 'RUS', 'Rostov-na-Donu');
INSERT INTO `databand_city` VALUES ('3591', 'Perm', 'RUS', 'Perm');
INSERT INTO `databand_city` VALUES ('3592', 'Volgograd', 'RUS', 'Volgograd');
INSERT INTO `databand_city` VALUES ('3593', 'Voronez', 'RUS', 'Voronez');
INSERT INTO `databand_city` VALUES ('3594', 'Krasnojarsk', 'RUS', 'Krasnojarsk');
INSERT INTO `databand_city` VALUES ('3595', 'Saratov', 'RUS', 'Saratov');
INSERT INTO `databand_city` VALUES ('3596', 'Toljatti', 'RUS', 'Samara');
INSERT INTO `databand_city` VALUES ('3597', 'Uljanovsk', 'RUS', 'Uljanovsk');
INSERT INTO `databand_city` VALUES ('3598', 'Izevsk', 'RUS', 'Udmurtia');
INSERT INTO `databand_city` VALUES ('3599', 'Krasnodar', 'RUS', 'Krasnodar');
INSERT INTO `databand_city` VALUES ('3600', 'Jaroslavl', 'RUS', 'Jaroslavl');
INSERT INTO `databand_city` VALUES ('3601', 'Habarovsk', 'RUS', 'Habarovsk');
INSERT INTO `databand_city` VALUES ('3602', 'Vladivostok', 'RUS', 'Primorje');
INSERT INTO `databand_city` VALUES ('3603', 'Irkutsk', 'RUS', 'Irkutsk');
INSERT INTO `databand_city` VALUES ('3604', 'Barnaul', 'RUS', 'Altai');
INSERT INTO `databand_city` VALUES ('3605', 'Novokuznetsk', 'RUS', 'Kemerovo');
INSERT INTO `databand_city` VALUES ('3606', 'Penza', 'RUS', 'Penza');
INSERT INTO `databand_city` VALUES ('3607', 'Rjazan', 'RUS', 'Rjazan');
INSERT INTO `databand_city` VALUES ('3608', 'Orenburg', 'RUS', 'Orenburg');
INSERT INTO `databand_city` VALUES ('3609', 'Lipetsk', 'RUS', 'Lipetsk');
INSERT INTO `databand_city` VALUES ('3610', 'Nabereznyje Tšelny', 'RUS', 'Tatarstan');
INSERT INTO `databand_city` VALUES ('3611', 'Tula', 'RUS', 'Tula');
INSERT INTO `databand_city` VALUES ('3612', 'Tjumen', 'RUS', 'Tjumen');
INSERT INTO `databand_city` VALUES ('3613', 'Kemerovo', 'RUS', 'Kemerovo');
INSERT INTO `databand_city` VALUES ('3614', 'Astrahan', 'RUS', 'Astrahan');
INSERT INTO `databand_city` VALUES ('3615', 'Tomsk', 'RUS', 'Tomsk');
INSERT INTO `databand_city` VALUES ('3616', 'Kirov', 'RUS', 'Kirov');
INSERT INTO `databand_city` VALUES ('3617', 'Ivanovo', 'RUS', 'Ivanovo');
INSERT INTO `databand_city` VALUES ('3618', 'Tšeboksary', 'RUS', 'Tšuvassia');
INSERT INTO `databand_city` VALUES ('3619', 'Brjansk', 'RUS', 'Brjansk');
INSERT INTO `databand_city` VALUES ('3620', 'Tver', 'RUS', 'Tver');
INSERT INTO `databand_city` VALUES ('3621', 'Kursk', 'RUS', 'Kursk');
INSERT INTO `databand_city` VALUES ('3622', 'Magnitogorsk', 'RUS', 'Tšeljabinsk');
INSERT INTO `databand_city` VALUES ('3623', 'Kaliningrad', 'RUS', 'Kaliningrad');
INSERT INTO `databand_city` VALUES ('3624', 'Nizni Tagil', 'RUS', 'Sverdlovsk');
INSERT INTO `databand_city` VALUES ('3625', 'Murmansk', 'RUS', 'Murmansk');
INSERT INTO `databand_city` VALUES ('3626', 'Ulan-Ude', 'RUS', 'Burjatia');
INSERT INTO `databand_city` VALUES ('3627', 'Kurgan', 'RUS', 'Kurgan');
INSERT INTO `databand_city` VALUES ('3628', 'Arkangeli', 'RUS', 'Arkangeli');
INSERT INTO `databand_city` VALUES ('3629', 'Sotši', 'RUS', 'Krasnodar');
INSERT INTO `databand_city` VALUES ('3630', 'Smolensk', 'RUS', 'Smolensk');
INSERT INTO `databand_city` VALUES ('3631', 'Orjol', 'RUS', 'Orjol');
INSERT INTO `databand_city` VALUES ('3632', 'Stavropol', 'RUS', 'Stavropol');
INSERT INTO `databand_city` VALUES ('3633', 'Belgorod', 'RUS', 'Belgorod');
INSERT INTO `databand_city` VALUES ('3634', 'Kaluga', 'RUS', 'Kaluga');
INSERT INTO `databand_city` VALUES ('3635', 'Vladimir', 'RUS', 'Vladimir');
INSERT INTO `databand_city` VALUES ('3636', 'Mahatškala', 'RUS', 'Dagestan');
INSERT INTO `databand_city` VALUES ('3637', 'Tšerepovets', 'RUS', 'Vologda');
INSERT INTO `databand_city` VALUES ('3638', 'Saransk', 'RUS', 'Mordva');
INSERT INTO `databand_city` VALUES ('3639', 'Tambov', 'RUS', 'Tambov');
INSERT INTO `databand_city` VALUES ('3640', 'Vladikavkaz', 'RUS', 'North Ossetia-Alania');
INSERT INTO `databand_city` VALUES ('3641', 'Tšita', 'RUS', 'Tšita');
INSERT INTO `databand_city` VALUES ('3642', 'Vologda', 'RUS', 'Vologda');
INSERT INTO `databand_city` VALUES ('3643', 'Veliki Novgorod', 'RUS', 'Novgorod');
INSERT INTO `databand_city` VALUES ('3644', 'Komsomolsk-na-Amure', 'RUS', 'Habarovsk');
INSERT INTO `databand_city` VALUES ('3645', 'Kostroma', 'RUS', 'Kostroma');
INSERT INTO `databand_city` VALUES ('3646', 'Volzski', 'RUS', 'Volgograd');
INSERT INTO `databand_city` VALUES ('3647', 'Taganrog', 'RUS', 'Rostov-na-Donu');
INSERT INTO `databand_city` VALUES ('3648', 'Petroskoi', 'RUS', 'Karjala');
INSERT INTO `databand_city` VALUES ('3649', 'Bratsk', 'RUS', 'Irkutsk');
INSERT INTO `databand_city` VALUES ('3650', 'Dzerzinsk', 'RUS', 'Nizni Novgorod');
INSERT INTO `databand_city` VALUES ('3651', 'Surgut', 'RUS', 'Hanti-Mansia');
INSERT INTO `databand_city` VALUES ('3652', 'Orsk', 'RUS', 'Orenburg');
INSERT INTO `databand_city` VALUES ('3653', 'Sterlitamak', 'RUS', 'Baškortostan');
INSERT INTO `databand_city` VALUES ('3654', 'Angarsk', 'RUS', 'Irkutsk');
INSERT INTO `databand_city` VALUES ('3655', 'Joškar-Ola', 'RUS', 'Marinmaa');
INSERT INTO `databand_city` VALUES ('3656', 'Rybinsk', 'RUS', 'Jaroslavl');
INSERT INTO `databand_city` VALUES ('3657', 'Prokopjevsk', 'RUS', 'Kemerovo');
INSERT INTO `databand_city` VALUES ('3658', 'Niznevartovsk', 'RUS', 'Hanti-Mansia');
INSERT INTO `databand_city` VALUES ('3659', 'Naltšik', 'RUS', 'Kabardi-Balkaria');
INSERT INTO `databand_city` VALUES ('3660', 'Syktyvkar', 'RUS', 'Komi');
INSERT INTO `databand_city` VALUES ('3661', 'Severodvinsk', 'RUS', 'Arkangeli');
INSERT INTO `databand_city` VALUES ('3662', 'Bijsk', 'RUS', 'Altai');
INSERT INTO `databand_city` VALUES ('3663', 'Niznekamsk', 'RUS', 'Tatarstan');
INSERT INTO `databand_city` VALUES ('3664', 'Blagoveštšensk', 'RUS', 'Amur');
INSERT INTO `databand_city` VALUES ('3665', 'Šahty', 'RUS', 'Rostov-na-Donu');
INSERT INTO `databand_city` VALUES ('3666', 'Staryi Oskol', 'RUS', 'Belgorod');
INSERT INTO `databand_city` VALUES ('3667', 'Zelenograd', 'RUS', 'Moscow (City)');
INSERT INTO `databand_city` VALUES ('3668', 'Balakovo', 'RUS', 'Saratov');
INSERT INTO `databand_city` VALUES ('3669', 'Novorossijsk', 'RUS', 'Krasnodar');
INSERT INTO `databand_city` VALUES ('3670', 'Pihkova', 'RUS', 'Pihkova');
INSERT INTO `databand_city` VALUES ('3671', 'Zlatoust', 'RUS', 'Tšeljabinsk');
INSERT INTO `databand_city` VALUES ('3672', 'Jakutsk', 'RUS', 'Saha (Jakutia)');
INSERT INTO `databand_city` VALUES ('3673', 'Podolsk', 'RUS', 'Moskova');
INSERT INTO `databand_city` VALUES ('3674', 'Petropavlovsk-Kamtšatski', 'RUS', 'Kamtšatka');
INSERT INTO `databand_city` VALUES ('3675', 'Kamensk-Uralski', 'RUS', 'Sverdlovsk');
INSERT INTO `databand_city` VALUES ('3676', 'Engels', 'RUS', 'Saratov');
INSERT INTO `databand_city` VALUES ('3677', 'Syzran', 'RUS', 'Samara');
INSERT INTO `databand_city` VALUES ('3678', 'Grozny', 'RUS', 'Tšetšenia');
INSERT INTO `databand_city` VALUES ('3679', 'Novotšerkassk', 'RUS', 'Rostov-na-Donu');
INSERT INTO `databand_city` VALUES ('3680', 'Berezniki', 'RUS', 'Perm');
INSERT INTO `databand_city` VALUES ('3681', 'Juzno-Sahalinsk', 'RUS', 'Sahalin');
INSERT INTO `databand_city` VALUES ('3682', 'Volgodonsk', 'RUS', 'Rostov-na-Donu');
INSERT INTO `databand_city` VALUES ('3683', 'Abakan', 'RUS', 'Hakassia');
INSERT INTO `databand_city` VALUES ('3684', 'Maikop', 'RUS', 'Adygea');
INSERT INTO `databand_city` VALUES ('3685', 'Miass', 'RUS', 'Tšeljabinsk');
INSERT INTO `databand_city` VALUES ('3686', 'Armavir', 'RUS', 'Krasnodar');
INSERT INTO `databand_city` VALUES ('3687', 'Ljubertsy', 'RUS', 'Moskova');
INSERT INTO `databand_city` VALUES ('3688', 'Rubtsovsk', 'RUS', 'Altai');
INSERT INTO `databand_city` VALUES ('3689', 'Kovrov', 'RUS', 'Vladimir');
INSERT INTO `databand_city` VALUES ('3690', 'Nahodka', 'RUS', 'Primorje');
INSERT INTO `databand_city` VALUES ('3691', 'Ussurijsk', 'RUS', 'Primorje');
INSERT INTO `databand_city` VALUES ('3692', 'Salavat', 'RUS', 'Baškortostan');
INSERT INTO `databand_city` VALUES ('3693', 'Mytištši', 'RUS', 'Moskova');
INSERT INTO `databand_city` VALUES ('3694', 'Kolomna', 'RUS', 'Moskova');
INSERT INTO `databand_city` VALUES ('3695', 'Elektrostal', 'RUS', 'Moskova');
INSERT INTO `databand_city` VALUES ('3696', 'Murom', 'RUS', 'Vladimir');
INSERT INTO `databand_city` VALUES ('3697', 'Kolpino', 'RUS', 'Pietari');
INSERT INTO `databand_city` VALUES ('3698', 'Norilsk', 'RUS', 'Krasnojarsk');
INSERT INTO `databand_city` VALUES ('3699', 'Almetjevsk', 'RUS', 'Tatarstan');
INSERT INTO `databand_city` VALUES ('3700', 'Novomoskovsk', 'RUS', 'Tula');
INSERT INTO `databand_city` VALUES ('3701', 'Dimitrovgrad', 'RUS', 'Uljanovsk');
INSERT INTO `databand_city` VALUES ('3702', 'Pervouralsk', 'RUS', 'Sverdlovsk');
INSERT INTO `databand_city` VALUES ('3703', 'Himki', 'RUS', 'Moskova');
INSERT INTO `databand_city` VALUES ('3704', 'Balašiha', 'RUS', 'Moskova');
INSERT INTO `databand_city` VALUES ('3705', 'Nevinnomyssk', 'RUS', 'Stavropol');
INSERT INTO `databand_city` VALUES ('3706', 'Pjatigorsk', 'RUS', 'Stavropol');
INSERT INTO `databand_city` VALUES ('3707', 'Korolev', 'RUS', 'Moskova');
INSERT INTO `databand_city` VALUES ('3708', 'Serpuhov', 'RUS', 'Moskova');
INSERT INTO `databand_city` VALUES ('3709', 'Odintsovo', 'RUS', 'Moskova');
INSERT INTO `databand_city` VALUES ('3710', 'Orehovo-Zujevo', 'RUS', 'Moskova');
INSERT INTO `databand_city` VALUES ('3711', 'Kamyšin', 'RUS', 'Volgograd');
INSERT INTO `databand_city` VALUES ('3712', 'Novotšeboksarsk', 'RUS', 'Tšuvassia');
INSERT INTO `databand_city` VALUES ('3713', 'Tšerkessk', 'RUS', 'Karatšai-Tšerkessia');
INSERT INTO `databand_city` VALUES ('3714', 'Atšinsk', 'RUS', 'Krasnojarsk');
INSERT INTO `databand_city` VALUES ('3715', 'Magadan', 'RUS', 'Magadan');
INSERT INTO `databand_city` VALUES ('3716', 'Mitšurinsk', 'RUS', 'Tambov');
INSERT INTO `databand_city` VALUES ('3717', 'Kislovodsk', 'RUS', 'Stavropol');
INSERT INTO `databand_city` VALUES ('3718', 'Jelets', 'RUS', 'Lipetsk');
INSERT INTO `databand_city` VALUES ('3719', 'Seversk', 'RUS', 'Tomsk');
INSERT INTO `databand_city` VALUES ('3720', 'Noginsk', 'RUS', 'Moskova');
INSERT INTO `databand_city` VALUES ('3721', 'Velikije Luki', 'RUS', 'Pihkova');
INSERT INTO `databand_city` VALUES ('3722', 'Novokuibyševsk', 'RUS', 'Samara');
INSERT INTO `databand_city` VALUES ('3723', 'Neftekamsk', 'RUS', 'Baškortostan');
INSERT INTO `databand_city` VALUES ('3724', 'Leninsk-Kuznetski', 'RUS', 'Kemerovo');
INSERT INTO `databand_city` VALUES ('3725', 'Oktjabrski', 'RUS', 'Baškortostan');
INSERT INTO `databand_city` VALUES ('3726', 'Sergijev Posad', 'RUS', 'Moskova');
INSERT INTO `databand_city` VALUES ('3727', 'Arzamas', 'RUS', 'Nizni Novgorod');
INSERT INTO `databand_city` VALUES ('3728', 'Kiseljovsk', 'RUS', 'Kemerovo');
INSERT INTO `databand_city` VALUES ('3729', 'Novotroitsk', 'RUS', 'Orenburg');
INSERT INTO `databand_city` VALUES ('3730', 'Obninsk', 'RUS', 'Kaluga');
INSERT INTO `databand_city` VALUES ('3731', 'Kansk', 'RUS', 'Krasnojarsk');
INSERT INTO `databand_city` VALUES ('3732', 'Glazov', 'RUS', 'Udmurtia');
INSERT INTO `databand_city` VALUES ('3733', 'Solikamsk', 'RUS', 'Perm');
INSERT INTO `databand_city` VALUES ('3734', 'Sarapul', 'RUS', 'Udmurtia');
INSERT INTO `databand_city` VALUES ('3735', 'Ust-Ilimsk', 'RUS', 'Irkutsk');
INSERT INTO `databand_city` VALUES ('3736', 'Štšolkovo', 'RUS', 'Moskova');
INSERT INTO `databand_city` VALUES ('3737', 'Mezduretšensk', 'RUS', 'Kemerovo');
INSERT INTO `databand_city` VALUES ('3738', 'Usolje-Sibirskoje', 'RUS', 'Irkutsk');
INSERT INTO `databand_city` VALUES ('3739', 'Elista', 'RUS', 'Kalmykia');
INSERT INTO `databand_city` VALUES ('3740', 'Novošahtinsk', 'RUS', 'Rostov-na-Donu');
INSERT INTO `databand_city` VALUES ('3741', 'Votkinsk', 'RUS', 'Udmurtia');
INSERT INTO `databand_city` VALUES ('3742', 'Kyzyl', 'RUS', 'Tyva');
INSERT INTO `databand_city` VALUES ('3743', 'Serov', 'RUS', 'Sverdlovsk');
INSERT INTO `databand_city` VALUES ('3744', 'Zelenodolsk', 'RUS', 'Tatarstan');
INSERT INTO `databand_city` VALUES ('3745', 'Zeleznodoroznyi', 'RUS', 'Moskova');
INSERT INTO `databand_city` VALUES ('3746', 'Kinešma', 'RUS', 'Ivanovo');
INSERT INTO `databand_city` VALUES ('3747', 'Kuznetsk', 'RUS', 'Penza');
INSERT INTO `databand_city` VALUES ('3748', 'Uhta', 'RUS', 'Komi');
INSERT INTO `databand_city` VALUES ('3749', 'Jessentuki', 'RUS', 'Stavropol');
INSERT INTO `databand_city` VALUES ('3750', 'Tobolsk', 'RUS', 'Tjumen');
INSERT INTO `databand_city` VALUES ('3751', 'Neftejugansk', 'RUS', 'Hanti-Mansia');
INSERT INTO `databand_city` VALUES ('3752', 'Bataisk', 'RUS', 'Rostov-na-Donu');
INSERT INTO `databand_city` VALUES ('3753', 'Nojabrsk', 'RUS', 'Yamalin Nenetsia');
INSERT INTO `databand_city` VALUES ('3754', 'Balašov', 'RUS', 'Saratov');
INSERT INTO `databand_city` VALUES ('3755', 'Zeleznogorsk', 'RUS', 'Kursk');
INSERT INTO `databand_city` VALUES ('3756', 'Zukovski', 'RUS', 'Moskova');
INSERT INTO `databand_city` VALUES ('3757', 'Anzero-Sudzensk', 'RUS', 'Kemerovo');
INSERT INTO `databand_city` VALUES ('3758', 'Bugulma', 'RUS', 'Tatarstan');
INSERT INTO `databand_city` VALUES ('3759', 'Zeleznogorsk', 'RUS', 'Krasnojarsk');
INSERT INTO `databand_city` VALUES ('3760', 'Novouralsk', 'RUS', 'Sverdlovsk');
INSERT INTO `databand_city` VALUES ('3761', 'Puškin', 'RUS', 'Pietari');
INSERT INTO `databand_city` VALUES ('3762', 'Vorkuta', 'RUS', 'Komi');
INSERT INTO `databand_city` VALUES ('3763', 'Derbent', 'RUS', 'Dagestan');
INSERT INTO `databand_city` VALUES ('3764', 'Kirovo-Tšepetsk', 'RUS', 'Kirov');
INSERT INTO `databand_city` VALUES ('3765', 'Krasnogorsk', 'RUS', 'Moskova');
INSERT INTO `databand_city` VALUES ('3766', 'Klin', 'RUS', 'Moskova');
INSERT INTO `databand_city` VALUES ('3767', 'Tšaikovski', 'RUS', 'Perm');
INSERT INTO `databand_city` VALUES ('3768', 'Novyi Urengoi', 'RUS', 'Yamalin Nenetsia');
INSERT INTO `databand_city` VALUES ('3769', 'Ho Chi Minh City', 'VNM', 'Ho Chi Minh City');
INSERT INTO `databand_city` VALUES ('3770', 'Hanoi', 'VNM', 'Hanoi');
INSERT INTO `databand_city` VALUES ('3771', 'Haiphong', 'VNM', 'Haiphong');
INSERT INTO `databand_city` VALUES ('3772', 'Da Nang', 'VNM', 'Quang Nam-Da Nang');
INSERT INTO `databand_city` VALUES ('3773', 'Biên Hoa', 'VNM', 'Dong Nai');
INSERT INTO `databand_city` VALUES ('3774', 'Nha Trang', 'VNM', 'Khanh Hoa');
INSERT INTO `databand_city` VALUES ('3775', 'Hue', 'VNM', 'Thua Thien-Hue');
INSERT INTO `databand_city` VALUES ('3776', 'Can Tho', 'VNM', 'Can Tho');
INSERT INTO `databand_city` VALUES ('3777', 'Cam Pha', 'VNM', 'Quang Binh');
INSERT INTO `databand_city` VALUES ('3778', 'Nam Dinh', 'VNM', 'Nam Ha');
INSERT INTO `databand_city` VALUES ('3779', 'Quy Nhon', 'VNM', 'Binh Dinh');
INSERT INTO `databand_city` VALUES ('3780', 'Vung Tau', 'VNM', 'Ba Ria-Vung Tau');
INSERT INTO `databand_city` VALUES ('3781', 'Rach Gia', 'VNM', 'Kien Giang');
INSERT INTO `databand_city` VALUES ('3782', 'Long Xuyen', 'VNM', 'An Giang');
INSERT INTO `databand_city` VALUES ('3783', 'Thai Nguyen', 'VNM', 'Bac Thai');
INSERT INTO `databand_city` VALUES ('3784', 'Hong Gai', 'VNM', 'Quang Ninh');
INSERT INTO `databand_city` VALUES ('3785', 'Phan Thiêt', 'VNM', 'Binh Thuan');
INSERT INTO `databand_city` VALUES ('3786', 'Cam Ranh', 'VNM', 'Khanh Hoa');
INSERT INTO `databand_city` VALUES ('3787', 'Vinh', 'VNM', 'Nghe An');
INSERT INTO `databand_city` VALUES ('3788', 'My Tho', 'VNM', 'Tien Giang');
INSERT INTO `databand_city` VALUES ('3789', 'Da Lat', 'VNM', 'Lam Dong');
INSERT INTO `databand_city` VALUES ('3790', 'Buon Ma Thuot', 'VNM', 'Dac Lac');
INSERT INTO `databand_city` VALUES ('3791', 'Tallinn', 'EST', 'Harjumaa');
INSERT INTO `databand_city` VALUES ('3792', 'Tartu', 'EST', 'Tartumaa');
INSERT INTO `databand_city` VALUES ('3793', 'New York', 'USA', 'New York');
INSERT INTO `databand_city` VALUES ('3794', 'Los Angeles', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3795', 'Chicago', 'USA', 'Illinois');
INSERT INTO `databand_city` VALUES ('3796', 'Houston', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3797', 'Philadelphia', 'USA', 'Pennsylvania');
INSERT INTO `databand_city` VALUES ('3798', 'Phoenix', 'USA', 'Arizona');
INSERT INTO `databand_city` VALUES ('3799', 'San Diego', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3800', 'Dallas', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3801', 'San Antonio', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3802', 'Detroit', 'USA', 'Michigan');
INSERT INTO `databand_city` VALUES ('3803', 'San Jose', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3804', 'Indianapolis', 'USA', 'Indiana');
INSERT INTO `databand_city` VALUES ('3805', 'San Francisco', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3806', 'Jacksonville', 'USA', 'Florida');
INSERT INTO `databand_city` VALUES ('3807', 'Columbus', 'USA', 'Ohio');
INSERT INTO `databand_city` VALUES ('3808', 'Austin', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3809', 'Baltimore', 'USA', 'Maryland');
INSERT INTO `databand_city` VALUES ('3810', 'Memphis', 'USA', 'Tennessee');
INSERT INTO `databand_city` VALUES ('3811', 'Milwaukee', 'USA', 'Wisconsin');
INSERT INTO `databand_city` VALUES ('3812', 'Boston', 'USA', 'Massachusetts');
INSERT INTO `databand_city` VALUES ('3813', 'Washington', 'USA', 'District of Columbia');
INSERT INTO `databand_city` VALUES ('3814', 'Nashville-Davidson', 'USA', 'Tennessee');
INSERT INTO `databand_city` VALUES ('3815', 'El Paso', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3816', 'Seattle', 'USA', 'Washington');
INSERT INTO `databand_city` VALUES ('3817', 'Denver', 'USA', 'Colorado');
INSERT INTO `databand_city` VALUES ('3818', 'Charlotte', 'USA', 'North Carolina');
INSERT INTO `databand_city` VALUES ('3819', 'Fort Worth', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3820', 'Portland', 'USA', 'Oregon');
INSERT INTO `databand_city` VALUES ('3821', 'Oklahoma City', 'USA', 'Oklahoma');
INSERT INTO `databand_city` VALUES ('3822', 'Tucson', 'USA', 'Arizona');
INSERT INTO `databand_city` VALUES ('3823', 'New Orleans', 'USA', 'Louisiana');
INSERT INTO `databand_city` VALUES ('3824', 'Las Vegas', 'USA', 'Nevada');
INSERT INTO `databand_city` VALUES ('3825', 'Cleveland', 'USA', 'Ohio');
INSERT INTO `databand_city` VALUES ('3826', 'Long Beach', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3827', 'Albuquerque', 'USA', 'New Mexico');
INSERT INTO `databand_city` VALUES ('3828', 'Kansas City', 'USA', 'Missouri');
INSERT INTO `databand_city` VALUES ('3829', 'Fresno', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3830', 'Virginia Beach', 'USA', 'Virginia');
INSERT INTO `databand_city` VALUES ('3831', 'Atlanta', 'USA', 'Georgia');
INSERT INTO `databand_city` VALUES ('3832', 'Sacramento', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3833', 'Oakland', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3834', 'Mesa', 'USA', 'Arizona');
INSERT INTO `databand_city` VALUES ('3835', 'Tulsa', 'USA', 'Oklahoma');
INSERT INTO `databand_city` VALUES ('3836', 'Omaha', 'USA', 'Nebraska');
INSERT INTO `databand_city` VALUES ('3837', 'Minneapolis', 'USA', 'Minnesota');
INSERT INTO `databand_city` VALUES ('3838', 'Honolulu', 'USA', 'Hawaii');
INSERT INTO `databand_city` VALUES ('3839', 'Miami', 'USA', 'Florida');
INSERT INTO `databand_city` VALUES ('3840', 'Colorado Springs', 'USA', 'Colorado');
INSERT INTO `databand_city` VALUES ('3841', 'Saint Louis', 'USA', 'Missouri');
INSERT INTO `databand_city` VALUES ('3842', 'Wichita', 'USA', 'Kansas');
INSERT INTO `databand_city` VALUES ('3843', 'Santa Ana', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3844', 'Pittsburgh', 'USA', 'Pennsylvania');
INSERT INTO `databand_city` VALUES ('3845', 'Arlington', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3846', 'Cincinnati', 'USA', 'Ohio');
INSERT INTO `databand_city` VALUES ('3847', 'Anaheim', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3848', 'Toledo', 'USA', 'Ohio');
INSERT INTO `databand_city` VALUES ('3849', 'Tampa', 'USA', 'Florida');
INSERT INTO `databand_city` VALUES ('3850', 'Buffalo', 'USA', 'New York');
INSERT INTO `databand_city` VALUES ('3851', 'Saint Paul', 'USA', 'Minnesota');
INSERT INTO `databand_city` VALUES ('3852', 'Corpus Christi', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3853', 'Aurora', 'USA', 'Colorado');
INSERT INTO `databand_city` VALUES ('3854', 'Raleigh', 'USA', 'North Carolina');
INSERT INTO `databand_city` VALUES ('3855', 'Newark', 'USA', 'New Jersey');
INSERT INTO `databand_city` VALUES ('3856', 'Lexington-Fayette', 'USA', 'Kentucky');
INSERT INTO `databand_city` VALUES ('3857', 'Anchorage', 'USA', 'Alaska');
INSERT INTO `databand_city` VALUES ('3858', 'Louisville', 'USA', 'Kentucky');
INSERT INTO `databand_city` VALUES ('3859', 'Riverside', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3860', 'Saint Petersburg', 'USA', 'Florida');
INSERT INTO `databand_city` VALUES ('3861', 'Bakersfield', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3862', 'Stockton', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3863', 'Birmingham', 'USA', 'Alabama');
INSERT INTO `databand_city` VALUES ('3864', 'Jersey City', 'USA', 'New Jersey');
INSERT INTO `databand_city` VALUES ('3865', 'Norfolk', 'USA', 'Virginia');
INSERT INTO `databand_city` VALUES ('3866', 'Baton Rouge', 'USA', 'Louisiana');
INSERT INTO `databand_city` VALUES ('3867', 'Hialeah', 'USA', 'Florida');
INSERT INTO `databand_city` VALUES ('3868', 'Lincoln', 'USA', 'Nebraska');
INSERT INTO `databand_city` VALUES ('3869', 'Greensboro', 'USA', 'North Carolina');
INSERT INTO `databand_city` VALUES ('3870', 'Plano', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3871', 'Rochester', 'USA', 'New York');
INSERT INTO `databand_city` VALUES ('3872', 'Glendale', 'USA', 'Arizona');
INSERT INTO `databand_city` VALUES ('3873', 'Akron', 'USA', 'Ohio');
INSERT INTO `databand_city` VALUES ('3874', 'Garland', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3875', 'Madison', 'USA', 'Wisconsin');
INSERT INTO `databand_city` VALUES ('3876', 'Fort Wayne', 'USA', 'Indiana');
INSERT INTO `databand_city` VALUES ('3877', 'Fremont', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3878', 'Scottsdale', 'USA', 'Arizona');
INSERT INTO `databand_city` VALUES ('3879', 'Montgomery', 'USA', 'Alabama');
INSERT INTO `databand_city` VALUES ('3880', 'Shreveport', 'USA', 'Louisiana');
INSERT INTO `databand_city` VALUES ('3881', 'Augusta-Richmond County', 'USA', 'Georgia');
INSERT INTO `databand_city` VALUES ('3882', 'Lubbock', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3883', 'Chesapeake', 'USA', 'Virginia');
INSERT INTO `databand_city` VALUES ('3884', 'Mobile', 'USA', 'Alabama');
INSERT INTO `databand_city` VALUES ('3885', 'Des Moines', 'USA', 'Iowa');
INSERT INTO `databand_city` VALUES ('3886', 'Grand Rapids', 'USA', 'Michigan');
INSERT INTO `databand_city` VALUES ('3887', 'Richmond', 'USA', 'Virginia');
INSERT INTO `databand_city` VALUES ('3888', 'Yonkers', 'USA', 'New York');
INSERT INTO `databand_city` VALUES ('3889', 'Spokane', 'USA', 'Washington');
INSERT INTO `databand_city` VALUES ('3890', 'Glendale', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3891', 'Tacoma', 'USA', 'Washington');
INSERT INTO `databand_city` VALUES ('3892', 'Irving', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3893', 'Huntington Beach', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3894', 'Modesto', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3895', 'Durham', 'USA', 'North Carolina');
INSERT INTO `databand_city` VALUES ('3896', 'Columbus', 'USA', 'Georgia');
INSERT INTO `databand_city` VALUES ('3897', 'Orlando', 'USA', 'Florida');
INSERT INTO `databand_city` VALUES ('3898', 'Boise City', 'USA', 'Idaho');
INSERT INTO `databand_city` VALUES ('3899', 'Winston-Salem', 'USA', 'North Carolina');
INSERT INTO `databand_city` VALUES ('3900', 'San Bernardino', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3901', 'Jackson', 'USA', 'Mississippi');
INSERT INTO `databand_city` VALUES ('3902', 'Little Rock', 'USA', 'Arkansas');
INSERT INTO `databand_city` VALUES ('3903', 'Salt Lake City', 'USA', 'Utah');
INSERT INTO `databand_city` VALUES ('3904', 'Reno', 'USA', 'Nevada');
INSERT INTO `databand_city` VALUES ('3905', 'Newport News', 'USA', 'Virginia');
INSERT INTO `databand_city` VALUES ('3906', 'Chandler', 'USA', 'Arizona');
INSERT INTO `databand_city` VALUES ('3907', 'Laredo', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3908', 'Henderson', 'USA', 'Nevada');
INSERT INTO `databand_city` VALUES ('3909', 'Arlington', 'USA', 'Virginia');
INSERT INTO `databand_city` VALUES ('3910', 'Knoxville', 'USA', 'Tennessee');
INSERT INTO `databand_city` VALUES ('3911', 'Amarillo', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3912', 'Providence', 'USA', 'Rhode Island');
INSERT INTO `databand_city` VALUES ('3913', 'Chula Vista', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3914', 'Worcester', 'USA', 'Massachusetts');
INSERT INTO `databand_city` VALUES ('3915', 'Oxnard', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3916', 'Dayton', 'USA', 'Ohio');
INSERT INTO `databand_city` VALUES ('3917', 'Garden Grove', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3918', 'Oceanside', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3919', 'Tempe', 'USA', 'Arizona');
INSERT INTO `databand_city` VALUES ('3920', 'Huntsville', 'USA', 'Alabama');
INSERT INTO `databand_city` VALUES ('3921', 'Ontario', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3922', 'Chattanooga', 'USA', 'Tennessee');
INSERT INTO `databand_city` VALUES ('3923', 'Fort Lauderdale', 'USA', 'Florida');
INSERT INTO `databand_city` VALUES ('3924', 'Springfield', 'USA', 'Massachusetts');
INSERT INTO `databand_city` VALUES ('3925', 'Springfield', 'USA', 'Missouri');
INSERT INTO `databand_city` VALUES ('3926', 'Santa Clarita', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3927', 'Salinas', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3928', 'Tallahassee', 'USA', 'Florida');
INSERT INTO `databand_city` VALUES ('3929', 'Rockford', 'USA', 'Illinois');
INSERT INTO `databand_city` VALUES ('3930', 'Pomona', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3931', 'Metairie', 'USA', 'Louisiana');
INSERT INTO `databand_city` VALUES ('3932', 'Paterson', 'USA', 'New Jersey');
INSERT INTO `databand_city` VALUES ('3933', 'Overland Park', 'USA', 'Kansas');
INSERT INTO `databand_city` VALUES ('3934', 'Santa Rosa', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3935', 'Syracuse', 'USA', 'New York');
INSERT INTO `databand_city` VALUES ('3936', 'Kansas City', 'USA', 'Kansas');
INSERT INTO `databand_city` VALUES ('3937', 'Hampton', 'USA', 'Virginia');
INSERT INTO `databand_city` VALUES ('3938', 'Lakewood', 'USA', 'Colorado');
INSERT INTO `databand_city` VALUES ('3939', 'Vancouver', 'USA', 'Washington');
INSERT INTO `databand_city` VALUES ('3940', 'Irvine', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3941', 'Aurora', 'USA', 'Illinois');
INSERT INTO `databand_city` VALUES ('3942', 'Moreno Valley', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3943', 'Pasadena', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3944', 'Hayward', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3945', 'Brownsville', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3946', 'Bridgeport', 'USA', 'Connecticut');
INSERT INTO `databand_city` VALUES ('3947', 'Hollywood', 'USA', 'Florida');
INSERT INTO `databand_city` VALUES ('3948', 'Warren', 'USA', 'Michigan');
INSERT INTO `databand_city` VALUES ('3949', 'Torrance', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3950', 'Eugene', 'USA', 'Oregon');
INSERT INTO `databand_city` VALUES ('3951', 'Pembroke Pines', 'USA', 'Florida');
INSERT INTO `databand_city` VALUES ('3952', 'Salem', 'USA', 'Oregon');
INSERT INTO `databand_city` VALUES ('3953', 'Pasadena', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3954', 'Escondido', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3955', 'Sunnyvale', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3956', 'Savannah', 'USA', 'Georgia');
INSERT INTO `databand_city` VALUES ('3957', 'Fontana', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3958', 'Orange', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3959', 'Naperville', 'USA', 'Illinois');
INSERT INTO `databand_city` VALUES ('3960', 'Alexandria', 'USA', 'Virginia');
INSERT INTO `databand_city` VALUES ('3961', 'Rancho Cucamonga', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3962', 'Grand Prairie', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3963', 'East Los Angeles', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3964', 'Fullerton', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3965', 'Corona', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3966', 'Flint', 'USA', 'Michigan');
INSERT INTO `databand_city` VALUES ('3967', 'Paradise', 'USA', 'Nevada');
INSERT INTO `databand_city` VALUES ('3968', 'Mesquite', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3969', 'Sterling Heights', 'USA', 'Michigan');
INSERT INTO `databand_city` VALUES ('3970', 'Sioux Falls', 'USA', 'South Dakota');
INSERT INTO `databand_city` VALUES ('3971', 'New Haven', 'USA', 'Connecticut');
INSERT INTO `databand_city` VALUES ('3972', 'Topeka', 'USA', 'Kansas');
INSERT INTO `databand_city` VALUES ('3973', 'Concord', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3974', 'Evansville', 'USA', 'Indiana');
INSERT INTO `databand_city` VALUES ('3975', 'Hartford', 'USA', 'Connecticut');
INSERT INTO `databand_city` VALUES ('3976', 'Fayetteville', 'USA', 'North Carolina');
INSERT INTO `databand_city` VALUES ('3977', 'Cedar Rapids', 'USA', 'Iowa');
INSERT INTO `databand_city` VALUES ('3978', 'Elizabeth', 'USA', 'New Jersey');
INSERT INTO `databand_city` VALUES ('3979', 'Lansing', 'USA', 'Michigan');
INSERT INTO `databand_city` VALUES ('3980', 'Lancaster', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3981', 'Fort Collins', 'USA', 'Colorado');
INSERT INTO `databand_city` VALUES ('3982', 'Coral Springs', 'USA', 'Florida');
INSERT INTO `databand_city` VALUES ('3983', 'Stamford', 'USA', 'Connecticut');
INSERT INTO `databand_city` VALUES ('3984', 'Thousand Oaks', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3985', 'Vallejo', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3986', 'Palmdale', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3987', 'Columbia', 'USA', 'South Carolina');
INSERT INTO `databand_city` VALUES ('3988', 'El Monte', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3989', 'Abilene', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3990', 'North Las Vegas', 'USA', 'Nevada');
INSERT INTO `databand_city` VALUES ('3991', 'Ann Arbor', 'USA', 'Michigan');
INSERT INTO `databand_city` VALUES ('3992', 'Beaumont', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3993', 'Waco', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('3994', 'Macon', 'USA', 'Georgia');
INSERT INTO `databand_city` VALUES ('3995', 'Independence', 'USA', 'Missouri');
INSERT INTO `databand_city` VALUES ('3996', 'Peoria', 'USA', 'Illinois');
INSERT INTO `databand_city` VALUES ('3997', 'Inglewood', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('3998', 'Springfield', 'USA', 'Illinois');
INSERT INTO `databand_city` VALUES ('3999', 'Simi Valley', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4000', 'Lafayette', 'USA', 'Louisiana');
INSERT INTO `databand_city` VALUES ('4001', 'Gilbert', 'USA', 'Arizona');
INSERT INTO `databand_city` VALUES ('4002', 'Carrollton', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('4003', 'Bellevue', 'USA', 'Washington');
INSERT INTO `databand_city` VALUES ('4004', 'West Valley City', 'USA', 'Utah');
INSERT INTO `databand_city` VALUES ('4005', 'Clarksville', 'USA', 'Tennessee');
INSERT INTO `databand_city` VALUES ('4006', 'Costa Mesa', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4007', 'Peoria', 'USA', 'Arizona');
INSERT INTO `databand_city` VALUES ('4008', 'South Bend', 'USA', 'Indiana');
INSERT INTO `databand_city` VALUES ('4009', 'Downey', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4010', 'Waterbury', 'USA', 'Connecticut');
INSERT INTO `databand_city` VALUES ('4011', 'Manchester', 'USA', 'New Hampshire');
INSERT INTO `databand_city` VALUES ('4012', 'Allentown', 'USA', 'Pennsylvania');
INSERT INTO `databand_city` VALUES ('4013', 'McAllen', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('4014', 'Joliet', 'USA', 'Illinois');
INSERT INTO `databand_city` VALUES ('4015', 'Lowell', 'USA', 'Massachusetts');
INSERT INTO `databand_city` VALUES ('4016', 'Provo', 'USA', 'Utah');
INSERT INTO `databand_city` VALUES ('4017', 'West Covina', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4018', 'Wichita Falls', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('4019', 'Erie', 'USA', 'Pennsylvania');
INSERT INTO `databand_city` VALUES ('4020', 'Daly City', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4021', 'Citrus Heights', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4022', 'Norwalk', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4023', 'Gary', 'USA', 'Indiana');
INSERT INTO `databand_city` VALUES ('4024', 'Berkeley', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4025', 'Santa Clara', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4026', 'Green Bay', 'USA', 'Wisconsin');
INSERT INTO `databand_city` VALUES ('4027', 'Cape Coral', 'USA', 'Florida');
INSERT INTO `databand_city` VALUES ('4028', 'Arvada', 'USA', 'Colorado');
INSERT INTO `databand_city` VALUES ('4029', 'Pueblo', 'USA', 'Colorado');
INSERT INTO `databand_city` VALUES ('4030', 'Sandy', 'USA', 'Utah');
INSERT INTO `databand_city` VALUES ('4031', 'Athens-Clarke County', 'USA', 'Georgia');
INSERT INTO `databand_city` VALUES ('4032', 'Cambridge', 'USA', 'Massachusetts');
INSERT INTO `databand_city` VALUES ('4033', 'Westminster', 'USA', 'Colorado');
INSERT INTO `databand_city` VALUES ('4034', 'San Buenaventura', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4035', 'Portsmouth', 'USA', 'Virginia');
INSERT INTO `databand_city` VALUES ('4036', 'Livonia', 'USA', 'Michigan');
INSERT INTO `databand_city` VALUES ('4037', 'Burbank', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4038', 'Clearwater', 'USA', 'Florida');
INSERT INTO `databand_city` VALUES ('4039', 'Midland', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('4040', 'Davenport', 'USA', 'Iowa');
INSERT INTO `databand_city` VALUES ('4041', 'Mission Viejo', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4042', 'Miami Beach', 'USA', 'Florida');
INSERT INTO `databand_city` VALUES ('4043', 'Sunrise Manor', 'USA', 'Nevada');
INSERT INTO `databand_city` VALUES ('4044', 'New Bedford', 'USA', 'Massachusetts');
INSERT INTO `databand_city` VALUES ('4045', 'El Cajon', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4046', 'Norman', 'USA', 'Oklahoma');
INSERT INTO `databand_city` VALUES ('4047', 'Richmond', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4048', 'Albany', 'USA', 'New York');
INSERT INTO `databand_city` VALUES ('4049', 'Brockton', 'USA', 'Massachusetts');
INSERT INTO `databand_city` VALUES ('4050', 'Roanoke', 'USA', 'Virginia');
INSERT INTO `databand_city` VALUES ('4051', 'Billings', 'USA', 'Montana');
INSERT INTO `databand_city` VALUES ('4052', 'Compton', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4053', 'Gainesville', 'USA', 'Florida');
INSERT INTO `databand_city` VALUES ('4054', 'Fairfield', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4055', 'Arden-Arcade', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4056', 'San Mateo', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4057', 'Visalia', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4058', 'Boulder', 'USA', 'Colorado');
INSERT INTO `databand_city` VALUES ('4059', 'Cary', 'USA', 'North Carolina');
INSERT INTO `databand_city` VALUES ('4060', 'Santa Monica', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4061', 'Fall River', 'USA', 'Massachusetts');
INSERT INTO `databand_city` VALUES ('4062', 'Kenosha', 'USA', 'Wisconsin');
INSERT INTO `databand_city` VALUES ('4063', 'Elgin', 'USA', 'Illinois');
INSERT INTO `databand_city` VALUES ('4064', 'Odessa', 'USA', 'Texas');
INSERT INTO `databand_city` VALUES ('4065', 'Carson', 'USA', 'California');
INSERT INTO `databand_city` VALUES ('4066', 'Charleston', 'USA', 'South Carolina');
INSERT INTO `databand_city` VALUES ('4067', 'Charlotte Amalie', 'VIR', 'St Thomas');
INSERT INTO `databand_city` VALUES ('4068', 'Harare', 'ZWE', 'Harare');
INSERT INTO `databand_city` VALUES ('4069', 'Bulawayo', 'ZWE', 'Bulawayo');
INSERT INTO `databand_city` VALUES ('4070', 'Chitungwiza', 'ZWE', 'Harare');
INSERT INTO `databand_city` VALUES ('4071', 'Mount Darwin', 'ZWE', 'Harare');
INSERT INTO `databand_city` VALUES ('4072', 'Mutare', 'ZWE', 'Manicaland');
INSERT INTO `databand_city` VALUES ('4073', 'Gweru', 'ZWE', 'Midlands');
INSERT INTO `databand_city` VALUES ('4074', 'Gaza', 'PSE', 'Gaza');
INSERT INTO `databand_city` VALUES ('4075', 'Khan Yunis', 'PSE', 'Khan Yunis');
INSERT INTO `databand_city` VALUES ('4076', 'Hebron', 'PSE', 'Hebron');
INSERT INTO `databand_city` VALUES ('4077', 'Jabaliya', 'PSE', 'North Gaza');
INSERT INTO `databand_city` VALUES ('4078', 'Nablus', 'PSE', 'Nablus');
INSERT INTO `databand_city` VALUES ('4079', 'Rafah', 'PSE', 'Rafah');

-- ----------------------------
-- Table structure for `databand_country`
-- ----------------------------
DROP TABLE IF EXISTS `databand_country`;
CREATE TABLE `databand_country` (
  `Code` char(3) NOT NULL DEFAULT '',
  `Name` char(52) NOT NULL DEFAULT '',
  `Capital` int(11) DEFAULT NULL,
  `Code2` char(2) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of databand_country
-- ----------------------------
INSERT INTO `databand_country` VALUES ('ABW', 'Aruba', '129', 'AW');
INSERT INTO `databand_country` VALUES ('AFG', 'Afghanistan', '1', 'AF');
INSERT INTO `databand_country` VALUES ('AGO', 'Angola', '56', 'AO');
INSERT INTO `databand_country` VALUES ('AIA', 'Anguilla', '62', 'AI');
INSERT INTO `databand_country` VALUES ('ALB', 'Albania', '34', 'AL');
INSERT INTO `databand_country` VALUES ('AND', 'Andorra', '55', 'AD');
INSERT INTO `databand_country` VALUES ('ANT', 'Netherlands Antilles', '33', 'AN');
INSERT INTO `databand_country` VALUES ('ARE', 'United Arab Emirates', '65', 'AE');
INSERT INTO `databand_country` VALUES ('ARG', 'Argentina', '69', 'AR');
INSERT INTO `databand_country` VALUES ('ARM', 'Armenia', '126', 'AM');
INSERT INTO `databand_country` VALUES ('ASM', 'American Samoa', '54', 'AS');
INSERT INTO `databand_country` VALUES ('ATA', 'Antarctica', null, 'AQ');
INSERT INTO `databand_country` VALUES ('ATF', 'French Southern territories', null, 'TF');
INSERT INTO `databand_country` VALUES ('ATG', 'Antigua and Barbuda', '63', 'AG');
INSERT INTO `databand_country` VALUES ('AUS', 'Australia', '135', 'AU');
INSERT INTO `databand_country` VALUES ('AUT', 'Austria', '1523', 'AT');
INSERT INTO `databand_country` VALUES ('AZE', 'Azerbaijan', '144', 'AZ');
INSERT INTO `databand_country` VALUES ('BDI', 'Burundi', '552', 'BI');
INSERT INTO `databand_country` VALUES ('BEL', 'Belgium', '179', 'BE');
INSERT INTO `databand_country` VALUES ('BEN', 'Benin', '187', 'BJ');
INSERT INTO `databand_country` VALUES ('BFA', 'Burkina Faso', '549', 'BF');
INSERT INTO `databand_country` VALUES ('BGD', 'Bangladesh', '150', 'BD');
INSERT INTO `databand_country` VALUES ('BGR', 'Bulgaria', '539', 'BG');
INSERT INTO `databand_country` VALUES ('BHR', 'Bahrain', '149', 'BH');
INSERT INTO `databand_country` VALUES ('BHS', 'Bahamas', '148', 'BS');
INSERT INTO `databand_country` VALUES ('BIH', 'Bosnia and Herzegovina', '201', 'BA');
INSERT INTO `databand_country` VALUES ('BLR', 'Belarus', '3520', 'BY');
INSERT INTO `databand_country` VALUES ('BLZ', 'Belize', '185', 'BZ');
INSERT INTO `databand_country` VALUES ('BMU', 'Bermuda', '191', 'BM');
INSERT INTO `databand_country` VALUES ('BOL', 'Bolivia', '194', 'BO');
INSERT INTO `databand_country` VALUES ('BRA', 'Brazil', '211', 'BR');
INSERT INTO `databand_country` VALUES ('BRB', 'Barbados', '174', 'BB');
INSERT INTO `databand_country` VALUES ('BRN', 'Brunei', '538', 'BN');
INSERT INTO `databand_country` VALUES ('BTN', 'Bhutan', '192', 'BT');
INSERT INTO `databand_country` VALUES ('BVT', 'Bouvet Island', null, 'BV');
INSERT INTO `databand_country` VALUES ('BWA', 'Botswana', '204', 'BW');
INSERT INTO `databand_country` VALUES ('CAF', 'Central African Republic', '1889', 'CF');
INSERT INTO `databand_country` VALUES ('CAN', 'Canada', '1822', 'CA');
INSERT INTO `databand_country` VALUES ('CCK', 'Cocos (Keeling) Islands', '2317', 'CC');
INSERT INTO `databand_country` VALUES ('CHE', 'Switzerland', '3248', 'CH');
INSERT INTO `databand_country` VALUES ('CHL', 'Chile', '554', 'CL');
INSERT INTO `databand_country` VALUES ('CHN', 'China', '1891', 'CN');
INSERT INTO `databand_country` VALUES ('CIV', 'Côte dIvoire', '2814', 'CI');
INSERT INTO `databand_country` VALUES ('CMR', 'Cameroon', '1804', 'CM');
INSERT INTO `databand_country` VALUES ('COD', 'Congo, The Democratic Republic of the', '2298', 'CD');
INSERT INTO `databand_country` VALUES ('COG', 'Congo', '2296', 'CG');
INSERT INTO `databand_country` VALUES ('COK', 'Cook Islands', '583', 'CK');
INSERT INTO `databand_country` VALUES ('COL', 'Colombia', '2257', 'CO');
INSERT INTO `databand_country` VALUES ('COM', 'Comoros', '2295', 'KM');
INSERT INTO `databand_country` VALUES ('CPV', 'Cape Verde', '1859', 'CV');
INSERT INTO `databand_country` VALUES ('CRI', 'Costa Rica', '584', 'CR');
INSERT INTO `databand_country` VALUES ('CUB', 'Cuba', '2413', 'CU');
INSERT INTO `databand_country` VALUES ('CXR', 'Christmas Island', '1791', 'CX');
INSERT INTO `databand_country` VALUES ('CYM', 'Cayman Islands', '553', 'KY');
INSERT INTO `databand_country` VALUES ('CYP', 'Cyprus', '2430', 'CY');
INSERT INTO `databand_country` VALUES ('CZE', 'Czech Republic', '3339', 'CZ');
INSERT INTO `databand_country` VALUES ('DEU', 'Germany', '3068', 'DE');
INSERT INTO `databand_country` VALUES ('DJI', 'Djibouti', '585', 'DJ');
INSERT INTO `databand_country` VALUES ('DMA', 'Dominica', '586', 'DM');
INSERT INTO `databand_country` VALUES ('DNK', 'Denmark', '3315', 'DK');
INSERT INTO `databand_country` VALUES ('DOM', 'Dominican Republic', '587', 'DO');
INSERT INTO `databand_country` VALUES ('DZA', 'Algeria', '35', 'DZ');
INSERT INTO `databand_country` VALUES ('ECU', 'Ecuador', '594', 'EC');
INSERT INTO `databand_country` VALUES ('EGY', 'Egypt', '608', 'EG');
INSERT INTO `databand_country` VALUES ('ERI', 'Eritrea', '652', 'ER');
INSERT INTO `databand_country` VALUES ('ESH', 'Western Sahara', '2453', 'EH');
INSERT INTO `databand_country` VALUES ('ESP', 'Spain', '653', 'ES');
INSERT INTO `databand_country` VALUES ('EST', 'Estonia', '3791', 'EE');
INSERT INTO `databand_country` VALUES ('ETH', 'Ethiopia', '756', 'ET');
INSERT INTO `databand_country` VALUES ('FIN', 'Finland', '3236', 'FI');
INSERT INTO `databand_country` VALUES ('FJI', 'Fiji Islands', '764', 'FJ');
INSERT INTO `databand_country` VALUES ('FLK', 'Falkland Islands', '763', 'FK');
INSERT INTO `databand_country` VALUES ('FRA', 'France', '2974', 'FR');
INSERT INTO `databand_country` VALUES ('FRO', 'Faroe Islands', '901', 'FO');
INSERT INTO `databand_country` VALUES ('FSM', 'Micronesia, Federated States of', '2689', 'FM');
INSERT INTO `databand_country` VALUES ('GAB', 'Gabon', '902', 'GA');
INSERT INTO `databand_country` VALUES ('GBR', 'United Kingdom', '456', 'GB');
INSERT INTO `databand_country` VALUES ('GEO', 'Georgia', '905', 'GE');
INSERT INTO `databand_country` VALUES ('GHA', 'Ghana', '910', 'GH');
INSERT INTO `databand_country` VALUES ('GIB', 'Gibraltar', '915', 'GI');
INSERT INTO `databand_country` VALUES ('GIN', 'Guinea', '926', 'GN');
INSERT INTO `databand_country` VALUES ('GLP', 'Guadeloupe', '919', 'GP');
INSERT INTO `databand_country` VALUES ('GMB', 'Gambia', '904', 'GM');
INSERT INTO `databand_country` VALUES ('GNB', 'Guinea-Bissau', '927', 'GW');
INSERT INTO `databand_country` VALUES ('GNQ', 'Equatorial Guinea', '2972', 'GQ');
INSERT INTO `databand_country` VALUES ('GRC', 'Greece', '2401', 'GR');
INSERT INTO `databand_country` VALUES ('GRD', 'Grenada', '916', 'GD');
INSERT INTO `databand_country` VALUES ('GRL', 'Greenland', '917', 'GL');
INSERT INTO `databand_country` VALUES ('GTM', 'Guatemala', '922', 'GT');
INSERT INTO `databand_country` VALUES ('GUF', 'French Guiana', '3014', 'GF');
INSERT INTO `databand_country` VALUES ('GUM', 'Guam', '921', 'GU');
INSERT INTO `databand_country` VALUES ('GUY', 'Guyana', '928', 'GY');
INSERT INTO `databand_country` VALUES ('HKG', 'Hong Kong', '937', 'HK');
INSERT INTO `databand_country` VALUES ('HMD', 'Heard Island and McDonald Islands', null, 'HM');
INSERT INTO `databand_country` VALUES ('HND', 'Honduras', '933', 'HN');
INSERT INTO `databand_country` VALUES ('HRV', 'Croatia', '2409', 'HR');
INSERT INTO `databand_country` VALUES ('HTI', 'Haiti', '929', 'HT');
INSERT INTO `databand_country` VALUES ('HUN', 'Hungary', '3483', 'HU');
INSERT INTO `databand_country` VALUES ('IDN', 'Indonesia', '939', 'ID');
INSERT INTO `databand_country` VALUES ('IND', 'India', '1109', 'IN');
INSERT INTO `databand_country` VALUES ('IOT', 'British Indian Ocean Territory', null, 'IO');
INSERT INTO `databand_country` VALUES ('IRL', 'Ireland', '1447', 'IE');
INSERT INTO `databand_country` VALUES ('IRN', 'Iran', '1380', 'IR');
INSERT INTO `databand_country` VALUES ('IRQ', 'Iraq', '1365', 'IQ');
INSERT INTO `databand_country` VALUES ('ISL', 'Iceland', '1449', 'IS');
INSERT INTO `databand_country` VALUES ('ISR', 'Israel', '1450', 'IL');
INSERT INTO `databand_country` VALUES ('ITA', 'Italy', '1464', 'IT');
INSERT INTO `databand_country` VALUES ('JAM', 'Jamaica', '1530', 'JM');
INSERT INTO `databand_country` VALUES ('JOR', 'Jordan', '1786', 'JO');
INSERT INTO `databand_country` VALUES ('JPN', 'Japan', '1532', 'JP');
INSERT INTO `databand_country` VALUES ('KAZ', 'Kazakstan', '1864', 'KZ');
INSERT INTO `databand_country` VALUES ('KEN', 'Kenya', '1881', 'KE');
INSERT INTO `databand_country` VALUES ('KGZ', 'Kyrgyzstan', '2253', 'KG');
INSERT INTO `databand_country` VALUES ('KHM', 'Cambodia', '1800', 'KH');
INSERT INTO `databand_country` VALUES ('KIR', 'Kiribati', '2256', 'KI');
INSERT INTO `databand_country` VALUES ('KNA', 'Saint Kitts and Nevis', '3064', 'KN');
INSERT INTO `databand_country` VALUES ('KOR', 'South Korea', '2331', 'KR');
INSERT INTO `databand_country` VALUES ('KWT', 'Kuwait', '2429', 'KW');
INSERT INTO `databand_country` VALUES ('LAO', 'Laos', '2432', 'LA');
INSERT INTO `databand_country` VALUES ('LBN', 'Lebanon', '2438', 'LB');
INSERT INTO `databand_country` VALUES ('LBR', 'Liberia', '2440', 'LR');
INSERT INTO `databand_country` VALUES ('LBY', 'Libyan Arab Jamahiriya', '2441', 'LY');
INSERT INTO `databand_country` VALUES ('LCA', 'Saint Lucia', '3065', 'LC');
INSERT INTO `databand_country` VALUES ('LIE', 'Liechtenstein', '2446', 'LI');
INSERT INTO `databand_country` VALUES ('LKA', 'Sri Lanka', '3217', 'LK');
INSERT INTO `databand_country` VALUES ('LSO', 'Lesotho', '2437', 'LS');
INSERT INTO `databand_country` VALUES ('LTU', 'Lithuania', '2447', 'LT');
INSERT INTO `databand_country` VALUES ('LUX', 'Luxembourg', '2452', 'LU');
INSERT INTO `databand_country` VALUES ('LVA', 'Latvia', '2434', 'LV');
INSERT INTO `databand_country` VALUES ('MAC', 'Macao', '2454', 'MO');
INSERT INTO `databand_country` VALUES ('MAR', 'Morocco', '2486', 'MA');
INSERT INTO `databand_country` VALUES ('MCO', 'Monaco', '2695', 'MC');
INSERT INTO `databand_country` VALUES ('MDA', 'Moldova', '2690', 'MD');
INSERT INTO `databand_country` VALUES ('MDG', 'Madagascar', '2455', 'MG');
INSERT INTO `databand_country` VALUES ('MDV', 'Maldives', '2463', 'MV');
INSERT INTO `databand_country` VALUES ('MEX', 'Mexico', '2515', 'MX');
INSERT INTO `databand_country` VALUES ('MHL', 'Marshall Islands', '2507', 'MH');
INSERT INTO `databand_country` VALUES ('MKD', 'Macedonia', '2460', 'MK');
INSERT INTO `databand_country` VALUES ('MLI', 'Mali', '2482', 'ML');
INSERT INTO `databand_country` VALUES ('MLT', 'Malta', '2484', 'MT');
INSERT INTO `databand_country` VALUES ('MMR', 'Myanmar', '2710', 'MM');
INSERT INTO `databand_country` VALUES ('MNG', 'Mongolia', '2696', 'MN');
INSERT INTO `databand_country` VALUES ('MNP', 'Northern Mariana Islands', '2913', 'MP');
INSERT INTO `databand_country` VALUES ('MOZ', 'Mozambique', '2698', 'MZ');
INSERT INTO `databand_country` VALUES ('MRT', 'Mauritania', '2509', 'MR');
INSERT INTO `databand_country` VALUES ('MSR', 'Montserrat', '2697', 'MS');
INSERT INTO `databand_country` VALUES ('MTQ', 'Martinique', '2508', 'MQ');
INSERT INTO `databand_country` VALUES ('MUS', 'Mauritius', '2511', 'MU');
INSERT INTO `databand_country` VALUES ('MWI', 'Malawi', '2462', 'MW');
INSERT INTO `databand_country` VALUES ('MYS', 'Malaysia', '2464', 'MY');
INSERT INTO `databand_country` VALUES ('MYT', 'Mayotte', '2514', 'YT');
INSERT INTO `databand_country` VALUES ('NAM', 'Namibia', '2726', 'NA');
INSERT INTO `databand_country` VALUES ('NCL', 'New Caledonia', '3493', 'NC');
INSERT INTO `databand_country` VALUES ('NER', 'Niger', '2738', 'NE');
INSERT INTO `databand_country` VALUES ('NFK', 'Norfolk Island', '2806', 'NF');
INSERT INTO `databand_country` VALUES ('NGA', 'Nigeria', '2754', 'NG');
INSERT INTO `databand_country` VALUES ('NIC', 'Nicaragua', '2734', 'NI');
INSERT INTO `databand_country` VALUES ('NIU', 'Niue', '2805', 'NU');
INSERT INTO `databand_country` VALUES ('NLD', 'Netherlands', '5', 'NL');
INSERT INTO `databand_country` VALUES ('NOR', 'Norway', '2807', 'NO');
INSERT INTO `databand_country` VALUES ('NPL', 'Nepal', '2729', 'NP');
INSERT INTO `databand_country` VALUES ('NRU', 'Nauru', '2728', 'NR');
INSERT INTO `databand_country` VALUES ('NZL', 'New Zealand', '3499', 'NZ');
INSERT INTO `databand_country` VALUES ('OMN', 'Oman', '2821', 'OM');
INSERT INTO `databand_country` VALUES ('PAK', 'Pakistan', '2831', 'PK');
INSERT INTO `databand_country` VALUES ('PAN', 'Panama', '2882', 'PA');
INSERT INTO `databand_country` VALUES ('PCN', 'Pitcairn', '2912', 'PN');
INSERT INTO `databand_country` VALUES ('PER', 'Peru', '2890', 'PE');
INSERT INTO `databand_country` VALUES ('PHL', 'Philippines', '766', 'PH');
INSERT INTO `databand_country` VALUES ('PLW', 'Palau', '2881', 'PW');
INSERT INTO `databand_country` VALUES ('PNG', 'Papua New Guinea', '2884', 'PG');
INSERT INTO `databand_country` VALUES ('POL', 'Poland', '2928', 'PL');
INSERT INTO `databand_country` VALUES ('PRI', 'Puerto Rico', '2919', 'PR');
INSERT INTO `databand_country` VALUES ('PRK', 'North Korea', '2318', 'KP');
INSERT INTO `databand_country` VALUES ('PRT', 'Portugal', '2914', 'PT');
INSERT INTO `databand_country` VALUES ('PRY', 'Paraguay', '2885', 'PY');
INSERT INTO `databand_country` VALUES ('PSE', 'Palestine', '4074', 'PS');
INSERT INTO `databand_country` VALUES ('PYF', 'French Polynesia', '3016', 'PF');
INSERT INTO `databand_country` VALUES ('QAT', 'Qatar', '2973', 'QA');
INSERT INTO `databand_country` VALUES ('REU', 'Réunion', '3017', 'RE');
INSERT INTO `databand_country` VALUES ('ROM', 'Romania', '3018', 'RO');
INSERT INTO `databand_country` VALUES ('RUS', 'Russian Federation', '3580', 'RU');
INSERT INTO `databand_country` VALUES ('RWA', 'Rwanda', '3047', 'RW');
INSERT INTO `databand_country` VALUES ('SAU', 'Saudi Arabia', '3173', 'SA');
INSERT INTO `databand_country` VALUES ('SDN', 'Sudan', '3225', 'SD');
INSERT INTO `databand_country` VALUES ('SEN', 'Senegal', '3198', 'SN');
INSERT INTO `databand_country` VALUES ('SGP', 'Singapore', '3208', 'SG');
INSERT INTO `databand_country` VALUES ('SGS', 'South Georgia and the South Sandwich Islands', null, 'GS');
INSERT INTO `databand_country` VALUES ('SHN', 'Saint Helena', '3063', 'SH');
INSERT INTO `databand_country` VALUES ('SJM', 'Svalbard and Jan Mayen', '938', 'SJ');
INSERT INTO `databand_country` VALUES ('SLB', 'Solomon Islands', '3161', 'SB');
INSERT INTO `databand_country` VALUES ('SLE', 'Sierra Leone', '3207', 'SL');
INSERT INTO `databand_country` VALUES ('SLV', 'El Salvador', '645', 'SV');
INSERT INTO `databand_country` VALUES ('SMR', 'San Marino', '3171', 'SM');
INSERT INTO `databand_country` VALUES ('SOM', 'Somalia', '3214', 'SO');
INSERT INTO `databand_country` VALUES ('SPM', 'Saint Pierre and Miquelon', '3067', 'PM');
INSERT INTO `databand_country` VALUES ('STP', 'Sao Tome and Principe', '3172', 'ST');
INSERT INTO `databand_country` VALUES ('SUR', 'Suriname', '3243', 'SR');
INSERT INTO `databand_country` VALUES ('SVK', 'Slovakia', '3209', 'SK');
INSERT INTO `databand_country` VALUES ('SVN', 'Slovenia', '3212', 'SI');
INSERT INTO `databand_country` VALUES ('SWE', 'Sweden', '3048', 'SE');
INSERT INTO `databand_country` VALUES ('SWZ', 'Swaziland', '3244', 'SZ');
INSERT INTO `databand_country` VALUES ('SYC', 'Seychelles', '3206', 'SC');
INSERT INTO `databand_country` VALUES ('SYR', 'Syria', '3250', 'SY');
INSERT INTO `databand_country` VALUES ('TCA', 'Turks and Caicos Islands', '3423', 'TC');
INSERT INTO `databand_country` VALUES ('TCD', 'Chad', '3337', 'TD');
INSERT INTO `databand_country` VALUES ('TGO', 'Togo', '3332', 'TG');
INSERT INTO `databand_country` VALUES ('THA', 'Thailand', '3320', 'TH');
INSERT INTO `databand_country` VALUES ('TJK', 'Tajikistan', '3261', 'TJ');
INSERT INTO `databand_country` VALUES ('TKL', 'Tokelau', '3333', 'TK');
INSERT INTO `databand_country` VALUES ('TKM', 'Turkmenistan', '3419', 'TM');
INSERT INTO `databand_country` VALUES ('TMP', 'East Timor', '1522', 'TP');
INSERT INTO `databand_country` VALUES ('TON', 'Tonga', '3334', 'TO');
INSERT INTO `databand_country` VALUES ('TTO', 'Trinidad and Tobago', '3336', 'TT');
INSERT INTO `databand_country` VALUES ('TUN', 'Tunisia', '3349', 'TN');
INSERT INTO `databand_country` VALUES ('TUR', 'Turkey', '3358', 'TR');
INSERT INTO `databand_country` VALUES ('TUV', 'Tuvalu', '3424', 'TV');
INSERT INTO `databand_country` VALUES ('TWN', 'Taiwan', '3263', 'TW');
INSERT INTO `databand_country` VALUES ('TZA', 'Tanzania', '3306', 'TZ');
INSERT INTO `databand_country` VALUES ('UGA', 'Uganda', '3425', 'UG');
INSERT INTO `databand_country` VALUES ('UKR', 'Ukraine', '3426', 'UA');
INSERT INTO `databand_country` VALUES ('UMI', 'United States Minor Outlying Islands', null, 'UM');
INSERT INTO `databand_country` VALUES ('URY', 'Uruguay', '3492', 'UY');
INSERT INTO `databand_country` VALUES ('USA', 'United States', '3813', 'US');
INSERT INTO `databand_country` VALUES ('UZB', 'Uzbekistan', '3503', 'UZ');
INSERT INTO `databand_country` VALUES ('VAT', 'Holy See (Vatican City State)', '3538', 'VA');
INSERT INTO `databand_country` VALUES ('VCT', 'Saint Vincent and the Grenadines', '3066', 'VC');
INSERT INTO `databand_country` VALUES ('VEN', 'Venezuela', '3539', 'VE');
INSERT INTO `databand_country` VALUES ('VGB', 'Virgin Islands, British', '537', 'VG');
INSERT INTO `databand_country` VALUES ('VIR', 'Virgin Islands, U.S.', '4067', 'VI');
INSERT INTO `databand_country` VALUES ('VNM', 'Vietnam', '3770', 'VN');
INSERT INTO `databand_country` VALUES ('VUT', 'Vanuatu', '3537', 'VU');
INSERT INTO `databand_country` VALUES ('WLF', 'Wallis and Futuna', '3536', 'WF');
INSERT INTO `databand_country` VALUES ('WSM', 'Samoa', '3169', 'WS');
INSERT INTO `databand_country` VALUES ('YEM', 'Yemen', '1780', 'YE');
INSERT INTO `databand_country` VALUES ('YUG', 'Yugoslavia', '1792', 'YU');
INSERT INTO `databand_country` VALUES ('ZAF', 'South Africa', '716', 'ZA');
INSERT INTO `databand_country` VALUES ('ZMB', 'Zambia', '3162', 'ZM');
INSERT INTO `databand_country` VALUES ('ZWE', 'Zimbabwe', '4068', 'ZW');

-- ----------------------------
-- Table structure for `databand_countryinfo`
-- ----------------------------
DROP TABLE IF EXISTS `databand_countryinfo`;
CREATE TABLE `databand_countryinfo` (
  `doc` json DEFAULT NULL,
  `_id` varbinary(32) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,_utf8mb4'$._id'))) STORED NOT NULL,
  `_json_schema` json GENERATED ALWAYS AS (_utf8mb4'{"type":"object"}') VIRTUAL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of databand_countryinfo
-- ----------------------------
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 828, \"_id\": \"00005de917d80000000000000000\", \"Code\": \"ABW\", \"Name\": \"Aruba\", \"IndepYear\": null, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 193}, \"government\": {\"HeadOfState\": \"Beatrix\", \"GovernmentForm\": \"Nonmetropolitan Territory of The Netherlands\"}, \"demographics\": {\"Population\": 103000, \"LifeExpectancy\": 78.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 5976, \"_id\": \"00005de917d80000000000000001\", \"Code\": \"AFG\", \"Name\": \"Afghanistan\", \"IndepYear\": 1919, \"geography\": {\"Region\": \"Southern and Central Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 652090}, \"government\": {\"HeadOfState\": \"Mohammad Omar\", \"GovernmentForm\": \"Islamic Emirate\"}, \"demographics\": {\"Population\": 22720000, \"LifeExpectancy\": 45.900001525878906}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 6648, \"_id\": \"00005de917d80000000000000002\", \"Code\": \"AGO\", \"Name\": \"Angola\", \"IndepYear\": 1975, \"geography\": {\"Region\": \"Central Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 1246700}, \"government\": {\"HeadOfState\": \"José Eduardo dos Santos\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 12878000, \"LifeExpectancy\": 38.29999923706055}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 63.20000076293945, \"_id\": \"00005de917d80000000000000003\", \"Code\": \"AIA\", \"Name\": \"Anguilla\", \"IndepYear\": null, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 96}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Dependent Territory of the UK\"}, \"demographics\": {\"Population\": 8000, \"LifeExpectancy\": 76.0999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 3205, \"_id\": \"00005de917d80000000000000004\", \"Code\": \"ALB\", \"Name\": \"Albania\", \"IndepYear\": 1912, \"geography\": {\"Region\": \"Southern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 28748}, \"government\": {\"HeadOfState\": \"Rexhep Mejdani\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 3401200, \"LifeExpectancy\": 71.5999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1630, \"_id\": \"00005de917d80000000000000005\", \"Code\": \"AND\", \"Name\": \"Andorra\", \"IndepYear\": 1278, \"geography\": {\"Region\": \"Southern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 468}, \"government\": {\"HeadOfState\": \"\", \"GovernmentForm\": \"Parliamentary Coprincipality\"}, \"demographics\": {\"Population\": 78000, \"LifeExpectancy\": 83.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1941, \"_id\": \"00005de917d80000000000000006\", \"Code\": \"ANT\", \"Name\": \"Netherlands Antilles\", \"IndepYear\": null, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 800}, \"government\": {\"HeadOfState\": \"Beatrix\", \"GovernmentForm\": \"Nonmetropolitan Territory of The Netherlands\"}, \"demographics\": {\"Population\": 217000, \"LifeExpectancy\": 74.69999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 37966, \"_id\": \"00005de917d80000000000000007\", \"Code\": \"ARE\", \"Name\": \"United Arab Emirates\", \"IndepYear\": 1971, \"geography\": {\"Region\": \"Middle East\", \"Continent\": \"Asia\", \"SurfaceArea\": 83600}, \"government\": {\"HeadOfState\": \"Zayid bin Sultan al-Nahayan\", \"GovernmentForm\": \"Emirate Federation\"}, \"demographics\": {\"Population\": 2441000, \"LifeExpectancy\": 74.0999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 340238, \"_id\": \"00005de917d80000000000000008\", \"Code\": \"ARG\", \"Name\": \"Argentina\", \"IndepYear\": 1816, \"geography\": {\"Region\": \"South America\", \"Continent\": \"South America\", \"SurfaceArea\": 2780400}, \"government\": {\"HeadOfState\": \"Fernando de la Rúa\", \"GovernmentForm\": \"Federal Republic\"}, \"demographics\": {\"Population\": 37032000, \"LifeExpectancy\": 75.0999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1813, \"_id\": \"00005de917d80000000000000009\", \"Code\": \"ARM\", \"Name\": \"Armenia\", \"IndepYear\": 1991, \"geography\": {\"Region\": \"Middle East\", \"Continent\": \"Asia\", \"SurfaceArea\": 29800}, \"government\": {\"HeadOfState\": \"Robert Kotšarjan\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 3520000, \"LifeExpectancy\": 66.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 334, \"_id\": \"00005de917d8000000000000000a\", \"Code\": \"ASM\", \"Name\": \"American Samoa\", \"IndepYear\": null, \"geography\": {\"Region\": \"Polynesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 199}, \"government\": {\"HeadOfState\": \"George W. Bush\", \"GovernmentForm\": \"US Territory\"}, \"demographics\": {\"Population\": 68000, \"LifeExpectancy\": 75.0999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d8000000000000000b\", \"Code\": \"ATA\", \"Name\": \"Antarctica\", \"IndepYear\": null, \"geography\": {\"Region\": \"Antarctica\", \"Continent\": \"Antarctica\", \"SurfaceArea\": 13120000}, \"government\": {\"HeadOfState\": \"\", \"GovernmentForm\": \"Co-administrated\"}, \"demographics\": {\"Population\": 0, \"LifeExpectancy\": null}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d8000000000000000c\", \"Code\": \"ATF\", \"Name\": \"French Southern territories\", \"IndepYear\": null, \"geography\": {\"Region\": \"Antarctica\", \"Continent\": \"Antarctica\", \"SurfaceArea\": 7780}, \"government\": {\"HeadOfState\": \"Jacques Chirac\", \"GovernmentForm\": \"Nonmetropolitan Territory of France\"}, \"demographics\": {\"Population\": 0, \"LifeExpectancy\": null}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 612, \"_id\": \"00005de917d8000000000000000d\", \"Code\": \"ATG\", \"Name\": \"Antigua and Barbuda\", \"IndepYear\": 1981, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 442}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 68000, \"LifeExpectancy\": 70.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 351182, \"_id\": \"00005de917d8000000000000000e\", \"Code\": \"AUS\", \"Name\": \"Australia\", \"IndepYear\": 1901, \"geography\": {\"Region\": \"Australia and New Zealand\", \"Continent\": \"Oceania\", \"SurfaceArea\": 7741220}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Constitutional Monarchy, Federation\"}, \"demographics\": {\"Population\": 18886000, \"LifeExpectancy\": 79.80000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 211860, \"_id\": \"00005de917d8000000000000000f\", \"Code\": \"AUT\", \"Name\": \"Austria\", \"IndepYear\": 1918, \"geography\": {\"Region\": \"Western Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 83859}, \"government\": {\"HeadOfState\": \"Thomas Klestil\", \"GovernmentForm\": \"Federal Republic\"}, \"demographics\": {\"Population\": 8091800, \"LifeExpectancy\": 77.69999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 4127, \"_id\": \"00005de917d80000000000000010\", \"Code\": \"AZE\", \"Name\": \"Azerbaijan\", \"IndepYear\": 1991, \"geography\": {\"Region\": \"Middle East\", \"Continent\": \"Asia\", \"SurfaceArea\": 86600}, \"government\": {\"HeadOfState\": \"Heydär Äliyev\", \"GovernmentForm\": \"Federal Republic\"}, \"demographics\": {\"Population\": 7734000, \"LifeExpectancy\": 62.900001525878906}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 903, \"_id\": \"00005de917d80000000000000011\", \"Code\": \"BDI\", \"Name\": \"Burundi\", \"IndepYear\": 1962, \"geography\": {\"Region\": \"Eastern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 27834}, \"government\": {\"HeadOfState\": \"Pierre Buyoya\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 6695000, \"LifeExpectancy\": 46.20000076293945}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 249704, \"_id\": \"00005de917d80000000000000012\", \"Code\": \"BEL\", \"Name\": \"Belgium\", \"IndepYear\": 1830, \"geography\": {\"Region\": \"Western Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 30518}, \"government\": {\"HeadOfState\": \"Albert II\", \"GovernmentForm\": \"Constitutional Monarchy, Federation\"}, \"demographics\": {\"Population\": 10239000, \"LifeExpectancy\": 77.80000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 2357, \"_id\": \"00005de917d80000000000000013\", \"Code\": \"BEN\", \"Name\": \"Benin\", \"IndepYear\": 1960, \"geography\": {\"Region\": \"Western Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 112622}, \"government\": {\"HeadOfState\": \"Mathieu Kérékou\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 6097000, \"LifeExpectancy\": 50.20000076293945}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 2425, \"_id\": \"00005de917d80000000000000014\", \"Code\": \"BFA\", \"Name\": \"Burkina Faso\", \"IndepYear\": 1960, \"geography\": {\"Region\": \"Western Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 274000}, \"government\": {\"HeadOfState\": \"Blaise Compaoré\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 11937000, \"LifeExpectancy\": 46.70000076293945}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 32852, \"_id\": \"00005de917d80000000000000015\", \"Code\": \"BGD\", \"Name\": \"Bangladesh\", \"IndepYear\": 1971, \"geography\": {\"Region\": \"Southern and Central Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 143998}, \"government\": {\"HeadOfState\": \"Shahabuddin Ahmad\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 129155000, \"LifeExpectancy\": 60.20000076293945}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 12178, \"_id\": \"00005de917d80000000000000016\", \"Code\": \"BGR\", \"Name\": \"Bulgaria\", \"IndepYear\": 1908, \"geography\": {\"Region\": \"Eastern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 110994}, \"government\": {\"HeadOfState\": \"Petar Stojanov\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 8190900, \"LifeExpectancy\": 70.9000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 6366, \"_id\": \"00005de917d80000000000000017\", \"Code\": \"BHR\", \"Name\": \"Bahrain\", \"IndepYear\": 1971, \"geography\": {\"Region\": \"Middle East\", \"Continent\": \"Asia\", \"SurfaceArea\": 694}, \"government\": {\"HeadOfState\": \"Hamad ibn Isa al-Khalifa\", \"GovernmentForm\": \"Monarchy (Emirate)\"}, \"demographics\": {\"Population\": 617000, \"LifeExpectancy\": 73}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 3527, \"_id\": \"00005de917d80000000000000018\", \"Code\": \"BHS\", \"Name\": \"Bahamas\", \"IndepYear\": 1973, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 13878}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 307000, \"LifeExpectancy\": 71.0999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 2841, \"_id\": \"00005de917d80000000000000019\", \"Code\": \"BIH\", \"Name\": \"Bosnia and Herzegovina\", \"IndepYear\": 1992, \"geography\": {\"Region\": \"Southern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 51197}, \"government\": {\"HeadOfState\": \"Ante Jelavic\", \"GovernmentForm\": \"Federal Republic\"}, \"demographics\": {\"Population\": 3972000, \"LifeExpectancy\": 71.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 13714, \"_id\": \"00005de917d8000000000000001a\", \"Code\": \"BLR\", \"Name\": \"Belarus\", \"IndepYear\": 1991, \"geography\": {\"Region\": \"Eastern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 207600}, \"government\": {\"HeadOfState\": \"Aljaksandr Lukašenka\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 10236000, \"LifeExpectancy\": 68}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 630, \"_id\": \"00005de917d8000000000000001b\", \"Code\": \"BLZ\", \"Name\": \"Belize\", \"IndepYear\": 1981, \"geography\": {\"Region\": \"Central America\", \"Continent\": \"North America\", \"SurfaceArea\": 22696}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 241000, \"LifeExpectancy\": 70.9000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 2328, \"_id\": \"00005de917d8000000000000001c\", \"Code\": \"BMU\", \"Name\": \"Bermuda\", \"IndepYear\": null, \"geography\": {\"Region\": \"North America\", \"Continent\": \"North America\", \"SurfaceArea\": 53}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Dependent Territory of the UK\"}, \"demographics\": {\"Population\": 65000, \"LifeExpectancy\": 76.9000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 8571, \"_id\": \"00005de917d8000000000000001d\", \"Code\": \"BOL\", \"Name\": \"Bolivia\", \"IndepYear\": 1825, \"geography\": {\"Region\": \"South America\", \"Continent\": \"South America\", \"SurfaceArea\": 1098581}, \"government\": {\"HeadOfState\": \"Hugo Bánzer Suárez\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 8329000, \"LifeExpectancy\": 63.70000076293945}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 776739, \"_id\": \"00005de917d8000000000000001e\", \"Code\": \"BRA\", \"Name\": \"Brazil\", \"IndepYear\": 1822, \"geography\": {\"Region\": \"South America\", \"Continent\": \"South America\", \"SurfaceArea\": 8547403}, \"government\": {\"HeadOfState\": \"Fernando Henrique Cardoso\", \"GovernmentForm\": \"Federal Republic\"}, \"demographics\": {\"Population\": 170115000, \"LifeExpectancy\": 62.900001525878906}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 2223, \"_id\": \"00005de917d8000000000000001f\", \"Code\": \"BRB\", \"Name\": \"Barbados\", \"IndepYear\": 1966, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 430}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 270000, \"LifeExpectancy\": 73}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 11705, \"_id\": \"00005de917d80000000000000020\", \"Code\": \"BRN\", \"Name\": \"Brunei\", \"IndepYear\": 1984, \"geography\": {\"Region\": \"Southeast Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 5765}, \"government\": {\"HeadOfState\": \"Haji Hassan al-Bolkiah\", \"GovernmentForm\": \"Monarchy (Sultanate)\"}, \"demographics\": {\"Population\": 328000, \"LifeExpectancy\": 73.5999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 372, \"_id\": \"00005de917d80000000000000021\", \"Code\": \"BTN\", \"Name\": \"Bhutan\", \"IndepYear\": 1910, \"geography\": {\"Region\": \"Southern and Central Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 47000}, \"government\": {\"HeadOfState\": \"Jigme Singye Wangchuk\", \"GovernmentForm\": \"Monarchy\"}, \"demographics\": {\"Population\": 2124000, \"LifeExpectancy\": 52.400001525878906}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d80000000000000022\", \"Code\": \"BVT\", \"Name\": \"Bouvet Island\", \"IndepYear\": null, \"geography\": {\"Region\": \"Antarctica\", \"Continent\": \"Antarctica\", \"SurfaceArea\": 59}, \"government\": {\"HeadOfState\": \"Harald V\", \"GovernmentForm\": \"Dependent Territory of Norway\"}, \"demographics\": {\"Population\": 0, \"LifeExpectancy\": null}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 4834, \"_id\": \"00005de917d80000000000000023\", \"Code\": \"BWA\", \"Name\": \"Botswana\", \"IndepYear\": 1966, \"geography\": {\"Region\": \"Southern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 581730}, \"government\": {\"HeadOfState\": \"Festus G. Mogae\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 1622000, \"LifeExpectancy\": 39.29999923706055}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1054, \"_id\": \"00005de917d80000000000000024\", \"Code\": \"CAF\", \"Name\": \"Central African Republic\", \"IndepYear\": 1960, \"geography\": {\"Region\": \"Central Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 622984}, \"government\": {\"HeadOfState\": \"Ange-Félix Patassé\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 3615000, \"LifeExpectancy\": 44}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 598862, \"_id\": \"00005de917d80000000000000025\", \"Code\": \"CAN\", \"Name\": \"Canada\", \"IndepYear\": 1867, \"geography\": {\"Region\": \"North America\", \"Continent\": \"North America\", \"SurfaceArea\": 9970610}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Constitutional Monarchy, Federation\"}, \"demographics\": {\"Population\": 31147000, \"LifeExpectancy\": 79.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d80000000000000026\", \"Code\": \"CCK\", \"Name\": \"Cocos (Keeling) Islands\", \"IndepYear\": null, \"geography\": {\"Region\": \"Australia and New Zealand\", \"Continent\": \"Oceania\", \"SurfaceArea\": 14}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Territory of Australia\"}, \"demographics\": {\"Population\": 600, \"LifeExpectancy\": null}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 264478, \"_id\": \"00005de917d80000000000000027\", \"Code\": \"CHE\", \"Name\": \"Switzerland\", \"IndepYear\": 1499, \"geography\": {\"Region\": \"Western Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 41284}, \"government\": {\"HeadOfState\": \"Adolf Ogi\", \"GovernmentForm\": \"Federation\"}, \"demographics\": {\"Population\": 7160400, \"LifeExpectancy\": 79.5999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 72949, \"_id\": \"00005de917d80000000000000028\", \"Code\": \"CHL\", \"Name\": \"Chile\", \"IndepYear\": 1810, \"geography\": {\"Region\": \"South America\", \"Continent\": \"South America\", \"SurfaceArea\": 756626}, \"government\": {\"HeadOfState\": \"Ricardo Lagos Escobar\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 15211000, \"LifeExpectancy\": 75.69999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 982268, \"_id\": \"00005de917d80000000000000029\", \"Code\": \"CHN\", \"Name\": \"China\", \"IndepYear\": -1523, \"geography\": {\"Region\": \"Eastern Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 9572900}, \"government\": {\"HeadOfState\": \"Jiang Zemin\", \"GovernmentForm\": \"People\'sRepublic\"}, \"demographics\": {\"Population\": 1277558000, \"LifeExpectancy\": 71.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 11345, \"_id\": \"00005de917d8000000000000002a\", \"Code\": \"CIV\", \"Name\": \"Côte dIvoire\", \"IndepYear\": 1960, \"geography\": {\"Region\": \"Western Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 322463}, \"government\": {\"HeadOfState\": \"Laurent Gbagbo\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 14786000, \"LifeExpectancy\": 45.20000076293945}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 9174, \"_id\": \"00005de917d8000000000000002b\", \"Code\": \"CMR\", \"Name\": \"Cameroon\", \"IndepYear\": 1960, \"geography\": {\"Region\": \"Central Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 475442}, \"government\": {\"HeadOfState\": \"Paul Biya\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 15085000, \"LifeExpectancy\": 54.79999923706055}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 6964, \"_id\": \"00005de917d8000000000000002c\", \"Code\": \"COD\", \"Name\": \"Congo, The Democratic Republic of the\", \"IndepYear\": 1960, \"geography\": {\"Region\": \"Central Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 2344858}, \"government\": {\"HeadOfState\": \"Joseph Kabila\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 51654000, \"LifeExpectancy\": 48.79999923706055}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 2108, \"_id\": \"00005de917d8000000000000002d\", \"Code\": \"COG\", \"Name\": \"Congo\", \"IndepYear\": 1960, \"geography\": {\"Region\": \"Central Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 342000}, \"government\": {\"HeadOfState\": \"Denis Sassou-Nguesso\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 2943000, \"LifeExpectancy\": 47.400001525878906}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 100, \"_id\": \"00005de917d8000000000000002e\", \"Code\": \"COK\", \"Name\": \"Cook Islands\", \"IndepYear\": null, \"geography\": {\"Region\": \"Polynesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 236}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Nonmetropolitan Territory of New Zealand\"}, \"demographics\": {\"Population\": 20000, \"LifeExpectancy\": 71.0999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 102896, \"_id\": \"00005de917d8000000000000002f\", \"Code\": \"COL\", \"Name\": \"Colombia\", \"IndepYear\": 1810, \"geography\": {\"Region\": \"South America\", \"Continent\": \"South America\", \"SurfaceArea\": 1138914}, \"government\": {\"HeadOfState\": \"Andrés Pastrana Arango\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 42321000, \"LifeExpectancy\": 70.30000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 4401, \"_id\": \"00005de917d80000000000000030\", \"Code\": \"COM\", \"Name\": \"Comoros\", \"IndepYear\": 1975, \"geography\": {\"Region\": \"Eastern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 1862}, \"government\": {\"HeadOfState\": \"Azali Assoumani\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 578000, \"LifeExpectancy\": 60}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 435, \"_id\": \"00005de917d80000000000000031\", \"Code\": \"CPV\", \"Name\": \"Cape Verde\", \"IndepYear\": 1975, \"geography\": {\"Region\": \"Western Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 4033}, \"government\": {\"HeadOfState\": \"António Mascarenhas Monteiro\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 428000, \"LifeExpectancy\": 68.9000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 10226, \"_id\": \"00005de917d80000000000000032\", \"Code\": \"CRI\", \"Name\": \"Costa Rica\", \"IndepYear\": 1821, \"geography\": {\"Region\": \"Central America\", \"Continent\": \"North America\", \"SurfaceArea\": 51100}, \"government\": {\"HeadOfState\": \"Miguel Ángel Rodríguez Echeverría\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 4023000, \"LifeExpectancy\": 75.80000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 17843, \"_id\": \"00005de917d80000000000000033\", \"Code\": \"CUB\", \"Name\": \"Cuba\", \"IndepYear\": 1902, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 110861}, \"government\": {\"HeadOfState\": \"Fidel Castro Ruz\", \"GovernmentForm\": \"Socialistic Republic\"}, \"demographics\": {\"Population\": 11201000, \"LifeExpectancy\": 76.19999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d80000000000000034\", \"Code\": \"CXR\", \"Name\": \"Christmas Island\", \"IndepYear\": null, \"geography\": {\"Region\": \"Australia and New Zealand\", \"Continent\": \"Oceania\", \"SurfaceArea\": 135}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Territory of Australia\"}, \"demographics\": {\"Population\": 2500, \"LifeExpectancy\": null}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1263, \"_id\": \"00005de917d80000000000000035\", \"Code\": \"CYM\", \"Name\": \"Cayman Islands\", \"IndepYear\": null, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 264}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Dependent Territory of the UK\"}, \"demographics\": {\"Population\": 38000, \"LifeExpectancy\": 78.9000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 9333, \"_id\": \"00005de917d80000000000000036\", \"Code\": \"CYP\", \"Name\": \"Cyprus\", \"IndepYear\": 1960, \"geography\": {\"Region\": \"Middle East\", \"Continent\": \"Asia\", \"SurfaceArea\": 9251}, \"government\": {\"HeadOfState\": \"Glafkos Klerides\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 754700, \"LifeExpectancy\": 76.69999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 55017, \"_id\": \"00005de917d80000000000000037\", \"Code\": \"CZE\", \"Name\": \"Czech Republic\", \"IndepYear\": 1993, \"geography\": {\"Region\": \"Eastern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 78866}, \"government\": {\"HeadOfState\": \"Václav Havel\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 10278100, \"LifeExpectancy\": 74.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 2133367, \"_id\": \"00005de917d80000000000000038\", \"Code\": \"DEU\", \"Name\": \"Germany\", \"IndepYear\": 1955, \"geography\": {\"Region\": \"Western Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 357022}, \"government\": {\"HeadOfState\": \"Johannes Rau\", \"GovernmentForm\": \"Federal Republic\"}, \"demographics\": {\"Population\": 82164700, \"LifeExpectancy\": 77.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 382, \"_id\": \"00005de917d80000000000000039\", \"Code\": \"DJI\", \"Name\": \"Djibouti\", \"IndepYear\": 1977, \"geography\": {\"Region\": \"Eastern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 23200}, \"government\": {\"HeadOfState\": \"Ismail Omar Guelleh\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 638000, \"LifeExpectancy\": 50.79999923706055}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 256, \"_id\": \"00005de917d8000000000000003a\", \"Code\": \"DMA\", \"Name\": \"Dominica\", \"IndepYear\": 1978, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 751}, \"government\": {\"HeadOfState\": \"Vernon Shaw\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 71000, \"LifeExpectancy\": 73.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 174099, \"_id\": \"00005de917d8000000000000003b\", \"Code\": \"DNK\", \"Name\": \"Denmark\", \"IndepYear\": 800, \"geography\": {\"Region\": \"Nordic Countries\", \"Continent\": \"Europe\", \"SurfaceArea\": 43094}, \"government\": {\"HeadOfState\": \"Margrethe II\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 5330000, \"LifeExpectancy\": 76.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 15846, \"_id\": \"00005de917d8000000000000003c\", \"Code\": \"DOM\", \"Name\": \"Dominican Republic\", \"IndepYear\": 1844, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 48511}, \"government\": {\"HeadOfState\": \"Hipólito Mejía Domínguez\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 8495000, \"LifeExpectancy\": 73.19999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 49982, \"_id\": \"00005de917d8000000000000003d\", \"Code\": \"DZA\", \"Name\": \"Algeria\", \"IndepYear\": 1962, \"geography\": {\"Region\": \"Northern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 2381741}, \"government\": {\"HeadOfState\": \"Abdelaziz Bouteflika\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 31471000, \"LifeExpectancy\": 69.69999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 19770, \"_id\": \"00005de917d8000000000000003e\", \"Code\": \"ECU\", \"Name\": \"Ecuador\", \"IndepYear\": 1822, \"geography\": {\"Region\": \"South America\", \"Continent\": \"South America\", \"SurfaceArea\": 283561}, \"government\": {\"HeadOfState\": \"Gustavo Noboa Bejarano\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 12646000, \"LifeExpectancy\": 71.0999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 82710, \"_id\": \"00005de917d8000000000000003f\", \"Code\": \"EGY\", \"Name\": \"Egypt\", \"IndepYear\": 1922, \"geography\": {\"Region\": \"Northern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 1001449}, \"government\": {\"HeadOfState\": \"Hosni Mubarak\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 68470000, \"LifeExpectancy\": 63.29999923706055}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 650, \"_id\": \"00005de917d80000000000000040\", \"Code\": \"ERI\", \"Name\": \"Eritrea\", \"IndepYear\": 1993, \"geography\": {\"Region\": \"Eastern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 117600}, \"government\": {\"HeadOfState\": \"Isayas Afewerki [Isaias Afwerki]\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 3850000, \"LifeExpectancy\": 55.79999923706055}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 60, \"_id\": \"00005de917d80000000000000041\", \"Code\": \"ESH\", \"Name\": \"Western Sahara\", \"IndepYear\": null, \"geography\": {\"Region\": \"Northern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 266000}, \"government\": {\"HeadOfState\": \"Mohammed Abdel Aziz\", \"GovernmentForm\": \"Occupied by Marocco\"}, \"demographics\": {\"Population\": 293000, \"LifeExpectancy\": 49.79999923706055}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 553233, \"_id\": \"00005de917d80000000000000042\", \"Code\": \"ESP\", \"Name\": \"Spain\", \"IndepYear\": 1492, \"geography\": {\"Region\": \"Southern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 505992}, \"government\": {\"HeadOfState\": \"Juan Carlos I\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 39441700, \"LifeExpectancy\": 78.80000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 5328, \"_id\": \"00005de917d80000000000000043\", \"Code\": \"EST\", \"Name\": \"Estonia\", \"IndepYear\": 1991, \"geography\": {\"Region\": \"Baltic Countries\", \"Continent\": \"Europe\", \"SurfaceArea\": 45227}, \"government\": {\"HeadOfState\": \"Lennart Meri\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 1439200, \"LifeExpectancy\": 69.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 6353, \"_id\": \"00005de917d80000000000000044\", \"Code\": \"ETH\", \"Name\": \"Ethiopia\", \"IndepYear\": -1000, \"geography\": {\"Region\": \"Eastern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 1104300}, \"government\": {\"HeadOfState\": \"Negasso Gidada\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 62565000, \"LifeExpectancy\": 45.20000076293945}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 121914, \"_id\": \"00005de917d80000000000000045\", \"Code\": \"FIN\", \"Name\": \"Finland\", \"IndepYear\": 1917, \"geography\": {\"Region\": \"Nordic Countries\", \"Continent\": \"Europe\", \"SurfaceArea\": 338145}, \"government\": {\"HeadOfState\": \"Tarja Halonen\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 5171300, \"LifeExpectancy\": 77.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1536, \"_id\": \"00005de917d80000000000000046\", \"Code\": \"FJI\", \"Name\": \"Fiji Islands\", \"IndepYear\": 1970, \"geography\": {\"Region\": \"Melanesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 18274}, \"government\": {\"HeadOfState\": \"Josefa Iloilo\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 817000, \"LifeExpectancy\": 67.9000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d80000000000000047\", \"Code\": \"FLK\", \"Name\": \"Falkland Islands\", \"IndepYear\": null, \"geography\": {\"Region\": \"South America\", \"Continent\": \"South America\", \"SurfaceArea\": 12173}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Dependent Territory of the UK\"}, \"demographics\": {\"Population\": 2000, \"LifeExpectancy\": null}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1424285, \"_id\": \"00005de917d80000000000000048\", \"Code\": \"FRA\", \"Name\": \"France\", \"IndepYear\": 843, \"geography\": {\"Region\": \"Western Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 551500}, \"government\": {\"HeadOfState\": \"Jacques Chirac\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 59225700, \"LifeExpectancy\": 78.80000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d80000000000000049\", \"Code\": \"FRO\", \"Name\": \"Faroe Islands\", \"IndepYear\": null, \"geography\": {\"Region\": \"Nordic Countries\", \"Continent\": \"Europe\", \"SurfaceArea\": 1399}, \"government\": {\"HeadOfState\": \"Margrethe II\", \"GovernmentForm\": \"Part of Denmark\"}, \"demographics\": {\"Population\": 43000, \"LifeExpectancy\": 78.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 212, \"_id\": \"00005de917d8000000000000004a\", \"Code\": \"FSM\", \"Name\": \"Micronesia, Federated States of\", \"IndepYear\": 1990, \"geography\": {\"Region\": \"Micronesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 702}, \"government\": {\"HeadOfState\": \"Leo A. Falcam\", \"GovernmentForm\": \"Federal Republic\"}, \"demographics\": {\"Population\": 119000, \"LifeExpectancy\": 68.5999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 5493, \"_id\": \"00005de917d8000000000000004b\", \"Code\": \"GAB\", \"Name\": \"Gabon\", \"IndepYear\": 1960, \"geography\": {\"Region\": \"Central Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 267668}, \"government\": {\"HeadOfState\": \"Omar Bongo\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 1226000, \"LifeExpectancy\": 50.099998474121094}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1378330, \"_id\": \"00005de917d8000000000000004c\", \"Code\": \"GBR\", \"Name\": \"United Kingdom\", \"IndepYear\": 1066, \"geography\": {\"Region\": \"British Islands\", \"Continent\": \"Europe\", \"SurfaceArea\": 242900}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 59623400, \"LifeExpectancy\": 77.69999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 6064, \"_id\": \"00005de917d8000000000000004d\", \"Code\": \"GEO\", \"Name\": \"Georgia\", \"IndepYear\": 1991, \"geography\": {\"Region\": \"Middle East\", \"Continent\": \"Asia\", \"SurfaceArea\": 69700}, \"government\": {\"HeadOfState\": \"Eduard Ševardnadze\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 4968000, \"LifeExpectancy\": 64.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 7137, \"_id\": \"00005de917d8000000000000004e\", \"Code\": \"GHA\", \"Name\": \"Ghana\", \"IndepYear\": 1957, \"geography\": {\"Region\": \"Western Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 238533}, \"government\": {\"HeadOfState\": \"John Kufuor\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 20212000, \"LifeExpectancy\": 57.400001525878906}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 258, \"_id\": \"00005de917d8000000000000004f\", \"Code\": \"GIB\", \"Name\": \"Gibraltar\", \"IndepYear\": null, \"geography\": {\"Region\": \"Southern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 6}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Dependent Territory of the UK\"}, \"demographics\": {\"Population\": 25000, \"LifeExpectancy\": 79}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 2352, \"_id\": \"00005de917d80000000000000050\", \"Code\": \"GIN\", \"Name\": \"Guinea\", \"IndepYear\": 1958, \"geography\": {\"Region\": \"Western Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 245857}, \"government\": {\"HeadOfState\": \"Lansana Conté\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 7430000, \"LifeExpectancy\": 45.599998474121094}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 3501, \"_id\": \"00005de917d80000000000000051\", \"Code\": \"GLP\", \"Name\": \"Guadeloupe\", \"IndepYear\": null, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 1705}, \"government\": {\"HeadOfState\": \"Jacques Chirac\", \"GovernmentForm\": \"Overseas Department of France\"}, \"demographics\": {\"Population\": 456000, \"LifeExpectancy\": 77}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 320, \"_id\": \"00005de917d80000000000000052\", \"Code\": \"GMB\", \"Name\": \"Gambia\", \"IndepYear\": 1965, \"geography\": {\"Region\": \"Western Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 11295}, \"government\": {\"HeadOfState\": \"Yahya Jammeh\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 1305000, \"LifeExpectancy\": 53.20000076293945}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 293, \"_id\": \"00005de917d80000000000000053\", \"Code\": \"GNB\", \"Name\": \"Guinea-Bissau\", \"IndepYear\": 1974, \"geography\": {\"Region\": \"Western Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 36125}, \"government\": {\"HeadOfState\": \"Kumba Ialá\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 1213000, \"LifeExpectancy\": 49}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 283, \"_id\": \"00005de917d80000000000000054\", \"Code\": \"GNQ\", \"Name\": \"Equatorial Guinea\", \"IndepYear\": 1968, \"geography\": {\"Region\": \"Central Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 28051}, \"government\": {\"HeadOfState\": \"Teodoro Obiang Nguema Mbasogo\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 453000, \"LifeExpectancy\": 53.599998474121094}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 120724, \"_id\": \"00005de917d80000000000000055\", \"Code\": \"GRC\", \"Name\": \"Greece\", \"IndepYear\": 1830, \"geography\": {\"Region\": \"Southern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 131626}, \"government\": {\"HeadOfState\": \"Kostis Stefanopoulos\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 10545700, \"LifeExpectancy\": 78.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 318, \"_id\": \"00005de917d80000000000000056\", \"Code\": \"GRD\", \"Name\": \"Grenada\", \"IndepYear\": 1974, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 344}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 94000, \"LifeExpectancy\": 64.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d80000000000000057\", \"Code\": \"GRL\", \"Name\": \"Greenland\", \"IndepYear\": null, \"geography\": {\"Region\": \"North America\", \"Continent\": \"North America\", \"SurfaceArea\": 2166090}, \"government\": {\"HeadOfState\": \"Margrethe II\", \"GovernmentForm\": \"Part of Denmark\"}, \"demographics\": {\"Population\": 56000, \"LifeExpectancy\": 68.0999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 19008, \"_id\": \"00005de917d80000000000000058\", \"Code\": \"GTM\", \"Name\": \"Guatemala\", \"IndepYear\": 1821, \"geography\": {\"Region\": \"Central America\", \"Continent\": \"North America\", \"SurfaceArea\": 108889}, \"government\": {\"HeadOfState\": \"Alfonso Portillo Cabrera\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 11385000, \"LifeExpectancy\": 66.19999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 681, \"_id\": \"00005de917d80000000000000059\", \"Code\": \"GUF\", \"Name\": \"French Guiana\", \"IndepYear\": null, \"geography\": {\"Region\": \"South America\", \"Continent\": \"South America\", \"SurfaceArea\": 90000}, \"government\": {\"HeadOfState\": \"Jacques Chirac\", \"GovernmentForm\": \"Overseas Department of France\"}, \"demographics\": {\"Population\": 181000, \"LifeExpectancy\": 76.0999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1197, \"_id\": \"00005de917d8000000000000005a\", \"Code\": \"GUM\", \"Name\": \"Guam\", \"IndepYear\": null, \"geography\": {\"Region\": \"Micronesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 549}, \"government\": {\"HeadOfState\": \"George W. Bush\", \"GovernmentForm\": \"US Territory\"}, \"demographics\": {\"Population\": 168000, \"LifeExpectancy\": 77.80000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 722, \"_id\": \"00005de917d8000000000000005b\", \"Code\": \"GUY\", \"Name\": \"Guyana\", \"IndepYear\": 1966, \"geography\": {\"Region\": \"South America\", \"Continent\": \"South America\", \"SurfaceArea\": 214969}, \"government\": {\"HeadOfState\": \"Bharrat Jagdeo\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 861000, \"LifeExpectancy\": 64}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 166448, \"_id\": \"00005de917d8000000000000005c\", \"Code\": \"HKG\", \"Name\": \"Hong Kong\", \"IndepYear\": null, \"geography\": {\"Region\": \"Eastern Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 1075}, \"government\": {\"HeadOfState\": \"Jiang Zemin\", \"GovernmentForm\": \"Special Administrative Region of China\"}, \"demographics\": {\"Population\": 6782000, \"LifeExpectancy\": 79.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d8000000000000005d\", \"Code\": \"HMD\", \"Name\": \"Heard Island and McDonald Islands\", \"IndepYear\": null, \"geography\": {\"Region\": \"Antarctica\", \"Continent\": \"Antarctica\", \"SurfaceArea\": 359}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Territory of Australia\"}, \"demographics\": {\"Population\": 0, \"LifeExpectancy\": null}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 5333, \"_id\": \"00005de917d8000000000000005e\", \"Code\": \"HND\", \"Name\": \"Honduras\", \"IndepYear\": 1838, \"geography\": {\"Region\": \"Central America\", \"Continent\": \"North America\", \"SurfaceArea\": 112088}, \"government\": {\"HeadOfState\": \"Carlos Roberto Flores Facussé\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 6485000, \"LifeExpectancy\": 69.9000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 20208, \"_id\": \"00005de917d8000000000000005f\", \"Code\": \"HRV\", \"Name\": \"Croatia\", \"IndepYear\": 1991, \"geography\": {\"Region\": \"Southern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 56538}, \"government\": {\"HeadOfState\": \"Štipe Mesic\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 4473000, \"LifeExpectancy\": 73.69999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 3459, \"_id\": \"00005de917d80000000000000060\", \"Code\": \"HTI\", \"Name\": \"Haiti\", \"IndepYear\": 1804, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 27750}, \"government\": {\"HeadOfState\": \"Jean-Bertrand Aristide\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 8222000, \"LifeExpectancy\": 49.20000076293945}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 48267, \"_id\": \"00005de917d80000000000000061\", \"Code\": \"HUN\", \"Name\": \"Hungary\", \"IndepYear\": 1918, \"geography\": {\"Region\": \"Eastern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 93030}, \"government\": {\"HeadOfState\": \"Ferenc Mádl\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 10043200, \"LifeExpectancy\": 71.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 84982, \"_id\": \"00005de917d80000000000000062\", \"Code\": \"IDN\", \"Name\": \"Indonesia\", \"IndepYear\": 1945, \"geography\": {\"Region\": \"Southeast Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 1904569}, \"government\": {\"HeadOfState\": \"Abdurrahman Wahid\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 212107000, \"LifeExpectancy\": 68}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 447114, \"_id\": \"00005de917d80000000000000063\", \"Code\": \"IND\", \"Name\": \"India\", \"IndepYear\": 1947, \"geography\": {\"Region\": \"Southern and Central Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 3287263}, \"government\": {\"HeadOfState\": \"Kocheril Raman Narayanan\", \"GovernmentForm\": \"Federal Republic\"}, \"demographics\": {\"Population\": 1013662000, \"LifeExpectancy\": 62.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d80000000000000064\", \"Code\": \"IOT\", \"Name\": \"British Indian Ocean Territory\", \"IndepYear\": null, \"geography\": {\"Region\": \"Eastern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 78}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Dependent Territory of the UK\"}, \"demographics\": {\"Population\": 0, \"LifeExpectancy\": null}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 75921, \"_id\": \"00005de917d80000000000000065\", \"Code\": \"IRL\", \"Name\": \"Ireland\", \"IndepYear\": 1921, \"geography\": {\"Region\": \"British Islands\", \"Continent\": \"Europe\", \"SurfaceArea\": 70273}, \"government\": {\"HeadOfState\": \"Mary McAleese\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 3775100, \"LifeExpectancy\": 76.80000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 195746, \"_id\": \"00005de917d80000000000000066\", \"Code\": \"IRN\", \"Name\": \"Iran\", \"IndepYear\": 1906, \"geography\": {\"Region\": \"Southern and Central Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 1648195}, \"government\": {\"HeadOfState\": \"Ali Mohammad Khatami-Ardakani\", \"GovernmentForm\": \"Islamic Republic\"}, \"demographics\": {\"Population\": 67702000, \"LifeExpectancy\": 69.69999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 11500, \"_id\": \"00005de917d80000000000000067\", \"Code\": \"IRQ\", \"Name\": \"Iraq\", \"IndepYear\": 1932, \"geography\": {\"Region\": \"Middle East\", \"Continent\": \"Asia\", \"SurfaceArea\": 438317}, \"government\": {\"HeadOfState\": \"Saddam Hussein al-Takriti\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 23115000, \"LifeExpectancy\": 66.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 8255, \"_id\": \"00005de917d80000000000000068\", \"Code\": \"ISL\", \"Name\": \"Iceland\", \"IndepYear\": 1944, \"geography\": {\"Region\": \"Nordic Countries\", \"Continent\": \"Europe\", \"SurfaceArea\": 103000}, \"government\": {\"HeadOfState\": \"Ólafur Ragnar Grímsson\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 279000, \"LifeExpectancy\": 79.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 97477, \"_id\": \"00005de917d80000000000000069\", \"Code\": \"ISR\", \"Name\": \"Israel\", \"IndepYear\": 1948, \"geography\": {\"Region\": \"Middle East\", \"Continent\": \"Asia\", \"SurfaceArea\": 21056}, \"government\": {\"HeadOfState\": \"Moshe Katzav\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 6217000, \"LifeExpectancy\": 78.5999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1161755, \"_id\": \"00005de917d8000000000000006a\", \"Code\": \"ITA\", \"Name\": \"Italy\", \"IndepYear\": 1861, \"geography\": {\"Region\": \"Southern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 301316}, \"government\": {\"HeadOfState\": \"Carlo Azeglio Ciampi\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 57680000, \"LifeExpectancy\": 79}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 6871, \"_id\": \"00005de917d8000000000000006b\", \"Code\": \"JAM\", \"Name\": \"Jamaica\", \"IndepYear\": 1962, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 10990}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 2583000, \"LifeExpectancy\": 75.19999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 7526, \"_id\": \"00005de917d8000000000000006c\", \"Code\": \"JOR\", \"Name\": \"Jordan\", \"IndepYear\": 1946, \"geography\": {\"Region\": \"Middle East\", \"Continent\": \"Asia\", \"SurfaceArea\": 88946}, \"government\": {\"HeadOfState\": \"Abdullah II\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 5083000, \"LifeExpectancy\": 77.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 3787042, \"_id\": \"00005de917d8000000000000006d\", \"Code\": \"JPN\", \"Name\": \"Japan\", \"IndepYear\": -660, \"geography\": {\"Region\": \"Eastern Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 377829}, \"government\": {\"HeadOfState\": \"Akihito\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 126714000, \"LifeExpectancy\": 80.69999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 24375, \"_id\": \"00005de917d8000000000000006e\", \"Code\": \"KAZ\", \"Name\": \"Kazakstan\", \"IndepYear\": 1991, \"geography\": {\"Region\": \"Southern and Central Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 2724900}, \"government\": {\"HeadOfState\": \"Nursultan Nazarbajev\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 16223000, \"LifeExpectancy\": 63.20000076293945}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 9217, \"_id\": \"00005de917d8000000000000006f\", \"Code\": \"KEN\", \"Name\": \"Kenya\", \"IndepYear\": 1963, \"geography\": {\"Region\": \"Eastern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 580367}, \"government\": {\"HeadOfState\": \"Daniel arap Moi\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 30080000, \"LifeExpectancy\": 48}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1626, \"_id\": \"00005de917d80000000000000070\", \"Code\": \"KGZ\", \"Name\": \"Kyrgyzstan\", \"IndepYear\": 1991, \"geography\": {\"Region\": \"Southern and Central Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 199900}, \"government\": {\"HeadOfState\": \"Askar Akajev\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 4699000, \"LifeExpectancy\": 63.400001525878906}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 5121, \"_id\": \"00005de917d80000000000000071\", \"Code\": \"KHM\", \"Name\": \"Cambodia\", \"IndepYear\": 1953, \"geography\": {\"Region\": \"Southeast Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 181035}, \"government\": {\"HeadOfState\": \"Norodom Sihanouk\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 11168000, \"LifeExpectancy\": 56.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 40.70000076293945, \"_id\": \"00005de917d80000000000000072\", \"Code\": \"KIR\", \"Name\": \"Kiribati\", \"IndepYear\": 1979, \"geography\": {\"Region\": \"Micronesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 726}, \"government\": {\"HeadOfState\": \"Teburoro Tito\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 83000, \"LifeExpectancy\": 59.79999923706055}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 299, \"_id\": \"00005de917d80000000000000073\", \"Code\": \"KNA\", \"Name\": \"Saint Kitts and Nevis\", \"IndepYear\": 1983, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 261}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 38000, \"LifeExpectancy\": 70.69999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 320749, \"_id\": \"00005de917d80000000000000074\", \"Code\": \"KOR\", \"Name\": \"South Korea\", \"IndepYear\": 1948, \"geography\": {\"Region\": \"Eastern Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 99434}, \"government\": {\"HeadOfState\": \"Kim Dae-jung\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 46844000, \"LifeExpectancy\": 74.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 27037, \"_id\": \"00005de917d80000000000000075\", \"Code\": \"KWT\", \"Name\": \"Kuwait\", \"IndepYear\": 1961, \"geography\": {\"Region\": \"Middle East\", \"Continent\": \"Asia\", \"SurfaceArea\": 17818}, \"government\": {\"HeadOfState\": \"Jabir al-Ahmad al-Jabir al-Sabah\", \"GovernmentForm\": \"Constitutional Monarchy (Emirate)\"}, \"demographics\": {\"Population\": 1972000, \"LifeExpectancy\": 76.0999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1292, \"_id\": \"00005de917d80000000000000076\", \"Code\": \"LAO\", \"Name\": \"Laos\", \"IndepYear\": 1953, \"geography\": {\"Region\": \"Southeast Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 236800}, \"government\": {\"HeadOfState\": \"Khamtay Siphandone\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 5433000, \"LifeExpectancy\": 53.099998474121094}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 17121, \"_id\": \"00005de917d80000000000000077\", \"Code\": \"LBN\", \"Name\": \"Lebanon\", \"IndepYear\": 1941, \"geography\": {\"Region\": \"Middle East\", \"Continent\": \"Asia\", \"SurfaceArea\": 10400}, \"government\": {\"HeadOfState\": \"Émile Lahoud\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 3282000, \"LifeExpectancy\": 71.30000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 2012, \"_id\": \"00005de917d80000000000000078\", \"Code\": \"LBR\", \"Name\": \"Liberia\", \"IndepYear\": 1847, \"geography\": {\"Region\": \"Western Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 111369}, \"government\": {\"HeadOfState\": \"Charles Taylor\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 3154000, \"LifeExpectancy\": 51}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 44806, \"_id\": \"00005de917d80000000000000079\", \"Code\": \"LBY\", \"Name\": \"Libyan Arab Jamahiriya\", \"IndepYear\": 1951, \"geography\": {\"Region\": \"Northern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 1759540}, \"government\": {\"HeadOfState\": \"Muammar al-Qadhafi\", \"GovernmentForm\": \"Socialistic State\"}, \"demographics\": {\"Population\": 5605000, \"LifeExpectancy\": 75.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 571, \"_id\": \"00005de917d8000000000000007a\", \"Code\": \"LCA\", \"Name\": \"Saint Lucia\", \"IndepYear\": 1979, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 622}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 154000, \"LifeExpectancy\": 72.30000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1119, \"_id\": \"00005de917d8000000000000007b\", \"Code\": \"LIE\", \"Name\": \"Liechtenstein\", \"IndepYear\": 1806, \"geography\": {\"Region\": \"Western Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 160}, \"government\": {\"HeadOfState\": \"Hans-Adam II\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 32300, \"LifeExpectancy\": 78.80000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 15706, \"_id\": \"00005de917d8000000000000007c\", \"Code\": \"LKA\", \"Name\": \"Sri Lanka\", \"IndepYear\": 1948, \"geography\": {\"Region\": \"Southern and Central Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 65610}, \"government\": {\"HeadOfState\": \"Chandrika Kumaratunga\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 18827000, \"LifeExpectancy\": 71.80000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1061, \"_id\": \"00005de917d8000000000000007d\", \"Code\": \"LSO\", \"Name\": \"Lesotho\", \"IndepYear\": 1966, \"geography\": {\"Region\": \"Southern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 30355}, \"government\": {\"HeadOfState\": \"Letsie III\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 2153000, \"LifeExpectancy\": 50.79999923706055}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 10692, \"_id\": \"00005de917d8000000000000007e\", \"Code\": \"LTU\", \"Name\": \"Lithuania\", \"IndepYear\": 1991, \"geography\": {\"Region\": \"Baltic Countries\", \"Continent\": \"Europe\", \"SurfaceArea\": 65301}, \"government\": {\"HeadOfState\": \"Valdas Adamkus\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 3698500, \"LifeExpectancy\": 69.0999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 16321, \"_id\": \"00005de917d8000000000000007f\", \"Code\": \"LUX\", \"Name\": \"Luxembourg\", \"IndepYear\": 1867, \"geography\": {\"Region\": \"Western Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 2586}, \"government\": {\"HeadOfState\": \"Henri\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 435700, \"LifeExpectancy\": 77.0999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 6398, \"_id\": \"00005de917d80000000000000080\", \"Code\": \"LVA\", \"Name\": \"Latvia\", \"IndepYear\": 1991, \"geography\": {\"Region\": \"Baltic Countries\", \"Continent\": \"Europe\", \"SurfaceArea\": 64589}, \"government\": {\"HeadOfState\": \"Vaira Vike-Freiberga\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 2424200, \"LifeExpectancy\": 68.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 5749, \"_id\": \"00005de917d80000000000000081\", \"Code\": \"MAC\", \"Name\": \"Macao\", \"IndepYear\": null, \"geography\": {\"Region\": \"Eastern Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 18}, \"government\": {\"HeadOfState\": \"Jiang Zemin\", \"GovernmentForm\": \"Special Administrative Region of China\"}, \"demographics\": {\"Population\": 473000, \"LifeExpectancy\": 81.5999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 36124, \"_id\": \"00005de917d80000000000000082\", \"Code\": \"MAR\", \"Name\": \"Morocco\", \"IndepYear\": 1956, \"geography\": {\"Region\": \"Northern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 446550}, \"government\": {\"HeadOfState\": \"Mohammed VI\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 28351000, \"LifeExpectancy\": 69.0999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 776, \"_id\": \"00005de917d80000000000000083\", \"Code\": \"MCO\", \"Name\": \"Monaco\", \"IndepYear\": 1861, \"geography\": {\"Region\": \"Western Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 1.5}, \"government\": {\"HeadOfState\": \"Rainier III\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 34000, \"LifeExpectancy\": 78.80000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1579, \"_id\": \"00005de917d80000000000000084\", \"Code\": \"MDA\", \"Name\": \"Moldova\", \"IndepYear\": 1991, \"geography\": {\"Region\": \"Eastern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 33851}, \"government\": {\"HeadOfState\": \"Vladimir Voronin\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 4380000, \"LifeExpectancy\": 64.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 3750, \"_id\": \"00005de917d80000000000000085\", \"Code\": \"MDG\", \"Name\": \"Madagascar\", \"IndepYear\": 1960, \"geography\": {\"Region\": \"Eastern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 587041}, \"government\": {\"HeadOfState\": \"Didier Ratsiraka\", \"GovernmentForm\": \"Federal Republic\"}, \"demographics\": {\"Population\": 15942000, \"LifeExpectancy\": 55}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 199, \"_id\": \"00005de917d80000000000000086\", \"Code\": \"MDV\", \"Name\": \"Maldives\", \"IndepYear\": 1965, \"geography\": {\"Region\": \"Southern and Central Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 298}, \"government\": {\"HeadOfState\": \"Maumoon Abdul Gayoom\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 286000, \"LifeExpectancy\": 62.20000076293945}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 414972, \"_id\": \"00005de917d80000000000000087\", \"Code\": \"MEX\", \"Name\": \"Mexico\", \"IndepYear\": 1810, \"geography\": {\"Region\": \"Central America\", \"Continent\": \"North America\", \"SurfaceArea\": 1958201}, \"government\": {\"HeadOfState\": \"Vicente Fox Quesada\", \"GovernmentForm\": \"Federal Republic\"}, \"demographics\": {\"Population\": 98881000, \"LifeExpectancy\": 71.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 97, \"_id\": \"00005de917d80000000000000088\", \"Code\": \"MHL\", \"Name\": \"Marshall Islands\", \"IndepYear\": 1990, \"geography\": {\"Region\": \"Micronesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 181}, \"government\": {\"HeadOfState\": \"Kessai Note\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 64000, \"LifeExpectancy\": 65.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1694, \"_id\": \"00005de917d80000000000000089\", \"Code\": \"MKD\", \"Name\": \"Macedonia\", \"IndepYear\": 1991, \"geography\": {\"Region\": \"Southern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 25713}, \"government\": {\"HeadOfState\": \"Boris Trajkovski\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 2024000, \"LifeExpectancy\": 73.80000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 2642, \"_id\": \"00005de917d8000000000000008a\", \"Code\": \"MLI\", \"Name\": \"Mali\", \"IndepYear\": 1960, \"geography\": {\"Region\": \"Western Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 1240192}, \"government\": {\"HeadOfState\": \"Alpha Oumar Konaré\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 11234000, \"LifeExpectancy\": 46.70000076293945}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 3512, \"_id\": \"00005de917d8000000000000008b\", \"Code\": \"MLT\", \"Name\": \"Malta\", \"IndepYear\": 1964, \"geography\": {\"Region\": \"Southern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 316}, \"government\": {\"HeadOfState\": \"Guido de Marco\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 380200, \"LifeExpectancy\": 77.9000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 180375, \"_id\": \"00005de917d8000000000000008c\", \"Code\": \"MMR\", \"Name\": \"Myanmar\", \"IndepYear\": 1948, \"geography\": {\"Region\": \"Southeast Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 676578}, \"government\": {\"HeadOfState\": \"kenraali Than Shwe\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 45611000, \"LifeExpectancy\": 54.900001525878906}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1043, \"_id\": \"00005de917d8000000000000008d\", \"Code\": \"MNG\", \"Name\": \"Mongolia\", \"IndepYear\": 1921, \"geography\": {\"Region\": \"Eastern Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 1566500}, \"government\": {\"HeadOfState\": \"Natsagiin Bagabandi\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 2662000, \"LifeExpectancy\": 67.30000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d8000000000000008e\", \"Code\": \"MNP\", \"Name\": \"Northern Mariana Islands\", \"IndepYear\": null, \"geography\": {\"Region\": \"Micronesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 464}, \"government\": {\"HeadOfState\": \"George W. Bush\", \"GovernmentForm\": \"Commonwealth of the US\"}, \"demographics\": {\"Population\": 78000, \"LifeExpectancy\": 75.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 2891, \"_id\": \"00005de917d8000000000000008f\", \"Code\": \"MOZ\", \"Name\": \"Mozambique\", \"IndepYear\": 1975, \"geography\": {\"Region\": \"Eastern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 801590}, \"government\": {\"HeadOfState\": \"Joaquím A. Chissano\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 19680000, \"LifeExpectancy\": 37.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 998, \"_id\": \"00005de917d80000000000000090\", \"Code\": \"MRT\", \"Name\": \"Mauritania\", \"IndepYear\": 1960, \"geography\": {\"Region\": \"Western Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 1025520}, \"government\": {\"HeadOfState\": \"Maaouiya Ould Sid´Ahmad Taya\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 2670000, \"LifeExpectancy\": 50.79999923706055}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 109, \"_id\": \"00005de917d80000000000000091\", \"Code\": \"MSR\", \"Name\": \"Montserrat\", \"IndepYear\": null, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 102}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Dependent Territory of the UK\"}, \"demographics\": {\"Population\": 11000, \"LifeExpectancy\": 78}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 2731, \"_id\": \"00005de917d80000000000000092\", \"Code\": \"MTQ\", \"Name\": \"Martinique\", \"IndepYear\": null, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 1102}, \"government\": {\"HeadOfState\": \"Jacques Chirac\", \"GovernmentForm\": \"Overseas Department of France\"}, \"demographics\": {\"Population\": 395000, \"LifeExpectancy\": 78.30000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 4251, \"_id\": \"00005de917d80000000000000093\", \"Code\": \"MUS\", \"Name\": \"Mauritius\", \"IndepYear\": 1968, \"geography\": {\"Region\": \"Eastern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 2040}, \"government\": {\"HeadOfState\": \"Cassam Uteem\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 1158000, \"LifeExpectancy\": 71}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1687, \"_id\": \"00005de917d80000000000000094\", \"Code\": \"MWI\", \"Name\": \"Malawi\", \"IndepYear\": 1964, \"geography\": {\"Region\": \"Eastern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 118484}, \"government\": {\"HeadOfState\": \"Bakili Muluzi\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 10925000, \"LifeExpectancy\": 37.599998474121094}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 69213, \"_id\": \"00005de917d80000000000000095\", \"Code\": \"MYS\", \"Name\": \"Malaysia\", \"IndepYear\": 1957, \"geography\": {\"Region\": \"Southeast Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 329758}, \"government\": {\"HeadOfState\": \"Salahuddin Abdul Aziz Shah Alhaj\", \"GovernmentForm\": \"Constitutional Monarchy, Federation\"}, \"demographics\": {\"Population\": 22244000, \"LifeExpectancy\": 70.80000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d80000000000000096\", \"Code\": \"MYT\", \"Name\": \"Mayotte\", \"IndepYear\": null, \"geography\": {\"Region\": \"Eastern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 373}, \"government\": {\"HeadOfState\": \"Jacques Chirac\", \"GovernmentForm\": \"Territorial Collectivity of France\"}, \"demographics\": {\"Population\": 149000, \"LifeExpectancy\": 59.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 3101, \"_id\": \"00005de917d80000000000000097\", \"Code\": \"NAM\", \"Name\": \"Namibia\", \"IndepYear\": 1990, \"geography\": {\"Region\": \"Southern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 824292}, \"government\": {\"HeadOfState\": \"Sam Nujoma\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 1726000, \"LifeExpectancy\": 42.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 3563, \"_id\": \"00005de917d80000000000000098\", \"Code\": \"NCL\", \"Name\": \"New Caledonia\", \"IndepYear\": null, \"geography\": {\"Region\": \"Melanesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 18575}, \"government\": {\"HeadOfState\": \"Jacques Chirac\", \"GovernmentForm\": \"Nonmetropolitan Territory of France\"}, \"demographics\": {\"Population\": 214000, \"LifeExpectancy\": 72.80000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1706, \"_id\": \"00005de917d80000000000000099\", \"Code\": \"NER\", \"Name\": \"Niger\", \"IndepYear\": 1960, \"geography\": {\"Region\": \"Western Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 1267000}, \"government\": {\"HeadOfState\": \"Mamadou Tandja\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 10730000, \"LifeExpectancy\": 41.29999923706055}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d8000000000000009a\", \"Code\": \"NFK\", \"Name\": \"Norfolk Island\", \"IndepYear\": null, \"geography\": {\"Region\": \"Australia and New Zealand\", \"Continent\": \"Oceania\", \"SurfaceArea\": 36}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Territory of Australia\"}, \"demographics\": {\"Population\": 2000, \"LifeExpectancy\": null}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 65707, \"_id\": \"00005de917d8000000000000009b\", \"Code\": \"NGA\", \"Name\": \"Nigeria\", \"IndepYear\": 1960, \"geography\": {\"Region\": \"Western Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 923768}, \"government\": {\"HeadOfState\": \"Olusegun Obasanjo\", \"GovernmentForm\": \"Federal Republic\"}, \"demographics\": {\"Population\": 111506000, \"LifeExpectancy\": 51.599998474121094}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1988, \"_id\": \"00005de917d8000000000000009c\", \"Code\": \"NIC\", \"Name\": \"Nicaragua\", \"IndepYear\": 1838, \"geography\": {\"Region\": \"Central America\", \"Continent\": \"North America\", \"SurfaceArea\": 130000}, \"government\": {\"HeadOfState\": \"Arnoldo Alemán Lacayo\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 5074000, \"LifeExpectancy\": 68.69999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d8000000000000009d\", \"Code\": \"NIU\", \"Name\": \"Niue\", \"IndepYear\": null, \"geography\": {\"Region\": \"Polynesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 260}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Nonmetropolitan Territory of New Zealand\"}, \"demographics\": {\"Population\": 2000, \"LifeExpectancy\": null}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 371362, \"_id\": \"00005de917d8000000000000009e\", \"Code\": \"NLD\", \"Name\": \"Netherlands\", \"IndepYear\": 1581, \"geography\": {\"Region\": \"Western Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 41526}, \"government\": {\"HeadOfState\": \"Beatrix\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 15864000, \"LifeExpectancy\": 78.30000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 145895, \"_id\": \"00005de917d8000000000000009f\", \"Code\": \"NOR\", \"Name\": \"Norway\", \"IndepYear\": 1905, \"geography\": {\"Region\": \"Nordic Countries\", \"Continent\": \"Europe\", \"SurfaceArea\": 323877}, \"government\": {\"HeadOfState\": \"Harald V\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 4478500, \"LifeExpectancy\": 78.69999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 4768, \"_id\": \"00005de917d800000000000000a0\", \"Code\": \"NPL\", \"Name\": \"Nepal\", \"IndepYear\": 1769, \"geography\": {\"Region\": \"Southern and Central Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 147181}, \"government\": {\"HeadOfState\": \"Gyanendra Bir Bikram\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 23930000, \"LifeExpectancy\": 57.79999923706055}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 197, \"_id\": \"00005de917d800000000000000a1\", \"Code\": \"NRU\", \"Name\": \"Nauru\", \"IndepYear\": 1968, \"geography\": {\"Region\": \"Micronesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 21}, \"government\": {\"HeadOfState\": \"Bernard Dowiyogo\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 12000, \"LifeExpectancy\": 60.79999923706055}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 54669, \"_id\": \"00005de917d800000000000000a2\", \"Code\": \"NZL\", \"Name\": \"New Zealand\", \"IndepYear\": 1907, \"geography\": {\"Region\": \"Australia and New Zealand\", \"Continent\": \"Oceania\", \"SurfaceArea\": 270534}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 3862000, \"LifeExpectancy\": 77.80000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 16904, \"_id\": \"00005de917d800000000000000a3\", \"Code\": \"OMN\", \"Name\": \"Oman\", \"IndepYear\": 1951, \"geography\": {\"Region\": \"Middle East\", \"Continent\": \"Asia\", \"SurfaceArea\": 309500}, \"government\": {\"HeadOfState\": \"Qabus ibn Sa´id\", \"GovernmentForm\": \"Monarchy (Sultanate)\"}, \"demographics\": {\"Population\": 2542000, \"LifeExpectancy\": 71.80000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 61289, \"_id\": \"00005de917d800000000000000a4\", \"Code\": \"PAK\", \"Name\": \"Pakistan\", \"IndepYear\": 1947, \"geography\": {\"Region\": \"Southern and Central Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 796095}, \"government\": {\"HeadOfState\": \"Mohammad Rafiq Tarar\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 156483000, \"LifeExpectancy\": 61.099998474121094}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 9131, \"_id\": \"00005de917d800000000000000a5\", \"Code\": \"PAN\", \"Name\": \"Panama\", \"IndepYear\": 1903, \"geography\": {\"Region\": \"Central America\", \"Continent\": \"North America\", \"SurfaceArea\": 75517}, \"government\": {\"HeadOfState\": \"Mireya Elisa Moscoso Rodríguez\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 2856000, \"LifeExpectancy\": 75.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d800000000000000a6\", \"Code\": \"PCN\", \"Name\": \"Pitcairn\", \"IndepYear\": null, \"geography\": {\"Region\": \"Polynesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 49}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Dependent Territory of the UK\"}, \"demographics\": {\"Population\": 50, \"LifeExpectancy\": null}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 64140, \"_id\": \"00005de917d800000000000000a7\", \"Code\": \"PER\", \"Name\": \"Peru\", \"IndepYear\": 1821, \"geography\": {\"Region\": \"South America\", \"Continent\": \"South America\", \"SurfaceArea\": 1285216}, \"government\": {\"HeadOfState\": \"Valentin Paniagua Corazao\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 25662000, \"LifeExpectancy\": 70}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 65107, \"_id\": \"00005de917d800000000000000a8\", \"Code\": \"PHL\", \"Name\": \"Philippines\", \"IndepYear\": 1946, \"geography\": {\"Region\": \"Southeast Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 300000}, \"government\": {\"HeadOfState\": \"Gloria Macapagal-Arroyo\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 75967000, \"LifeExpectancy\": 67.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 105, \"_id\": \"00005de917d800000000000000a9\", \"Code\": \"PLW\", \"Name\": \"Palau\", \"IndepYear\": 1994, \"geography\": {\"Region\": \"Micronesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 459}, \"government\": {\"HeadOfState\": \"Kuniwo Nakamura\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 19000, \"LifeExpectancy\": 68.5999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 4988, \"_id\": \"00005de917d800000000000000aa\", \"Code\": \"PNG\", \"Name\": \"Papua New Guinea\", \"IndepYear\": 1975, \"geography\": {\"Region\": \"Melanesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 462840}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 4807000, \"LifeExpectancy\": 63.099998474121094}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 151697, \"_id\": \"00005de917d800000000000000ab\", \"Code\": \"POL\", \"Name\": \"Poland\", \"IndepYear\": 1918, \"geography\": {\"Region\": \"Eastern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 323250}, \"government\": {\"HeadOfState\": \"Aleksander Kwasniewski\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 38653600, \"LifeExpectancy\": 73.19999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 34100, \"_id\": \"00005de917d800000000000000ac\", \"Code\": \"PRI\", \"Name\": \"Puerto Rico\", \"IndepYear\": null, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 8875}, \"government\": {\"HeadOfState\": \"George W. Bush\", \"GovernmentForm\": \"Commonwealth of the US\"}, \"demographics\": {\"Population\": 3869000, \"LifeExpectancy\": 75.5999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 5332, \"_id\": \"00005de917d800000000000000ad\", \"Code\": \"PRK\", \"Name\": \"North Korea\", \"IndepYear\": 1948, \"geography\": {\"Region\": \"Eastern Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 120538}, \"government\": {\"HeadOfState\": \"Kim Jong-il\", \"GovernmentForm\": \"Socialistic Republic\"}, \"demographics\": {\"Population\": 24039000, \"LifeExpectancy\": 70.69999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 105954, \"_id\": \"00005de917d800000000000000ae\", \"Code\": \"PRT\", \"Name\": \"Portugal\", \"IndepYear\": 1143, \"geography\": {\"Region\": \"Southern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 91982}, \"government\": {\"HeadOfState\": \"Jorge Sampãio\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 9997600, \"LifeExpectancy\": 75.80000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 8444, \"_id\": \"00005de917d800000000000000af\", \"Code\": \"PRY\", \"Name\": \"Paraguay\", \"IndepYear\": 1811, \"geography\": {\"Region\": \"South America\", \"Continent\": \"South America\", \"SurfaceArea\": 406752}, \"government\": {\"HeadOfState\": \"Luis Ángel González Macchi\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 5496000, \"LifeExpectancy\": 73.69999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 4173, \"_id\": \"00005de917d800000000000000b0\", \"Code\": \"PSE\", \"Name\": \"Palestine\", \"IndepYear\": null, \"geography\": {\"Region\": \"Middle East\", \"Continent\": \"Asia\", \"SurfaceArea\": 6257}, \"government\": {\"HeadOfState\": \"Yasser (Yasir) Arafat\", \"GovernmentForm\": \"Autonomous Area\"}, \"demographics\": {\"Population\": 3101000, \"LifeExpectancy\": 71.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 818, \"_id\": \"00005de917d800000000000000b1\", \"Code\": \"PYF\", \"Name\": \"French Polynesia\", \"IndepYear\": null, \"geography\": {\"Region\": \"Polynesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 4000}, \"government\": {\"HeadOfState\": \"Jacques Chirac\", \"GovernmentForm\": \"Nonmetropolitan Territory of France\"}, \"demographics\": {\"Population\": 235000, \"LifeExpectancy\": 74.80000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 9472, \"_id\": \"00005de917d800000000000000b2\", \"Code\": \"QAT\", \"Name\": \"Qatar\", \"IndepYear\": 1971, \"geography\": {\"Region\": \"Middle East\", \"Continent\": \"Asia\", \"SurfaceArea\": 11000}, \"government\": {\"HeadOfState\": \"Hamad ibn Khalifa al-Thani\", \"GovernmentForm\": \"Monarchy\"}, \"demographics\": {\"Population\": 599000, \"LifeExpectancy\": 72.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 8287, \"_id\": \"00005de917d800000000000000b3\", \"Code\": \"REU\", \"Name\": \"Réunion\", \"IndepYear\": null, \"geography\": {\"Region\": \"Eastern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 2510}, \"government\": {\"HeadOfState\": \"Jacques Chirac\", \"GovernmentForm\": \"Overseas Department of France\"}, \"demographics\": {\"Population\": 699000, \"LifeExpectancy\": 72.69999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 38158, \"_id\": \"00005de917d800000000000000b4\", \"Code\": \"ROM\", \"Name\": \"Romania\", \"IndepYear\": 1878, \"geography\": {\"Region\": \"Eastern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 238391}, \"government\": {\"HeadOfState\": \"Ion Iliescu\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 22455500, \"LifeExpectancy\": 69.9000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 276608, \"_id\": \"00005de917d800000000000000b5\", \"Code\": \"RUS\", \"Name\": \"Russian Federation\", \"IndepYear\": 1991, \"geography\": {\"Region\": \"Eastern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 17075400}, \"government\": {\"HeadOfState\": \"Vladimir Putin\", \"GovernmentForm\": \"Federal Republic\"}, \"demographics\": {\"Population\": 146934000, \"LifeExpectancy\": 67.19999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 2036, \"_id\": \"00005de917d800000000000000b6\", \"Code\": \"RWA\", \"Name\": \"Rwanda\", \"IndepYear\": 1962, \"geography\": {\"Region\": \"Eastern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 26338}, \"government\": {\"HeadOfState\": \"Paul Kagame\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 7733000, \"LifeExpectancy\": 39.29999923706055}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 137635, \"_id\": \"00005de917d800000000000000b7\", \"Code\": \"SAU\", \"Name\": \"Saudi Arabia\", \"IndepYear\": 1932, \"geography\": {\"Region\": \"Middle East\", \"Continent\": \"Asia\", \"SurfaceArea\": 2149690}, \"government\": {\"HeadOfState\": \"Fahd ibn Abdul-Aziz al-Sa´ud\", \"GovernmentForm\": \"Monarchy\"}, \"demographics\": {\"Population\": 21607000, \"LifeExpectancy\": 67.80000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 10162, \"_id\": \"00005de917d800000000000000b8\", \"Code\": \"SDN\", \"Name\": \"Sudan\", \"IndepYear\": 1956, \"geography\": {\"Region\": \"Northern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 2505813}, \"government\": {\"HeadOfState\": \"Omar Hassan Ahmad al-Bashir\", \"GovernmentForm\": \"Islamic Republic\"}, \"demographics\": {\"Population\": 29490000, \"LifeExpectancy\": 56.599998474121094}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 4787, \"_id\": \"00005de917d800000000000000b9\", \"Code\": \"SEN\", \"Name\": \"Senegal\", \"IndepYear\": 1960, \"geography\": {\"Region\": \"Western Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 196722}, \"government\": {\"HeadOfState\": \"Abdoulaye Wade\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 9481000, \"LifeExpectancy\": 62.20000076293945}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 86503, \"_id\": \"00005de917d800000000000000ba\", \"Code\": \"SGP\", \"Name\": \"Singapore\", \"IndepYear\": 1965, \"geography\": {\"Region\": \"Southeast Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 618}, \"government\": {\"HeadOfState\": \"Sellapan Rama Nathan\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 3567000, \"LifeExpectancy\": 80.0999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d800000000000000bb\", \"Code\": \"SGS\", \"Name\": \"South Georgia and the South Sandwich Islands\", \"IndepYear\": null, \"geography\": {\"Region\": \"Antarctica\", \"Continent\": \"Antarctica\", \"SurfaceArea\": 3903}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Dependent Territory of the UK\"}, \"demographics\": {\"Population\": 0, \"LifeExpectancy\": null}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d800000000000000bc\", \"Code\": \"SHN\", \"Name\": \"Saint Helena\", \"IndepYear\": null, \"geography\": {\"Region\": \"Western Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 314}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Dependent Territory of the UK\"}, \"demographics\": {\"Population\": 6000, \"LifeExpectancy\": 76.80000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d800000000000000bd\", \"Code\": \"SJM\", \"Name\": \"Svalbard and Jan Mayen\", \"IndepYear\": null, \"geography\": {\"Region\": \"Nordic Countries\", \"Continent\": \"Europe\", \"SurfaceArea\": 62422}, \"government\": {\"HeadOfState\": \"Harald V\", \"GovernmentForm\": \"Dependent Territory of Norway\"}, \"demographics\": {\"Population\": 3200, \"LifeExpectancy\": null}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 182, \"_id\": \"00005de917d800000000000000be\", \"Code\": \"SLB\", \"Name\": \"Solomon Islands\", \"IndepYear\": 1978, \"geography\": {\"Region\": \"Melanesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 28896}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 444000, \"LifeExpectancy\": 71.30000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 746, \"_id\": \"00005de917d800000000000000bf\", \"Code\": \"SLE\", \"Name\": \"Sierra Leone\", \"IndepYear\": 1961, \"geography\": {\"Region\": \"Western Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 71740}, \"government\": {\"HeadOfState\": \"Ahmed Tejan Kabbah\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 4854000, \"LifeExpectancy\": 45.29999923706055}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 11863, \"_id\": \"00005de917d800000000000000c0\", \"Code\": \"SLV\", \"Name\": \"El Salvador\", \"IndepYear\": 1841, \"geography\": {\"Region\": \"Central America\", \"Continent\": \"North America\", \"SurfaceArea\": 21041}, \"government\": {\"HeadOfState\": \"Francisco Guillermo Flores Pérez\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 6276000, \"LifeExpectancy\": 69.69999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 510, \"_id\": \"00005de917d800000000000000c1\", \"Code\": \"SMR\", \"Name\": \"San Marino\", \"IndepYear\": 885, \"geography\": {\"Region\": \"Southern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 61}, \"government\": {\"HeadOfState\": null, \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 27000, \"LifeExpectancy\": 81.0999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 935, \"_id\": \"00005de917d800000000000000c2\", \"Code\": \"SOM\", \"Name\": \"Somalia\", \"IndepYear\": 1960, \"geography\": {\"Region\": \"Eastern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 637657}, \"government\": {\"HeadOfState\": \"Abdiqassim Salad Hassan\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 10097000, \"LifeExpectancy\": 46.20000076293945}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d800000000000000c3\", \"Code\": \"SPM\", \"Name\": \"Saint Pierre and Miquelon\", \"IndepYear\": null, \"geography\": {\"Region\": \"North America\", \"Continent\": \"North America\", \"SurfaceArea\": 242}, \"government\": {\"HeadOfState\": \"Jacques Chirac\", \"GovernmentForm\": \"Territorial Collectivity of France\"}, \"demographics\": {\"Population\": 7000, \"LifeExpectancy\": 77.5999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 6, \"_id\": \"00005de917d800000000000000c4\", \"Code\": \"STP\", \"Name\": \"Sao Tome and Principe\", \"IndepYear\": 1975, \"geography\": {\"Region\": \"Central Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 964}, \"government\": {\"HeadOfState\": \"Miguel Trovoada\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 147000, \"LifeExpectancy\": 65.30000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 870, \"_id\": \"00005de917d800000000000000c5\", \"Code\": \"SUR\", \"Name\": \"Suriname\", \"IndepYear\": 1975, \"geography\": {\"Region\": \"South America\", \"Continent\": \"South America\", \"SurfaceArea\": 163265}, \"government\": {\"HeadOfState\": \"Ronald Venetiaan\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 417000, \"LifeExpectancy\": 71.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 20594, \"_id\": \"00005de917d800000000000000c6\", \"Code\": \"SVK\", \"Name\": \"Slovakia\", \"IndepYear\": 1993, \"geography\": {\"Region\": \"Eastern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 49012}, \"government\": {\"HeadOfState\": \"Rudolf Schuster\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 5398700, \"LifeExpectancy\": 73.69999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 19756, \"_id\": \"00005de917d800000000000000c7\", \"Code\": \"SVN\", \"Name\": \"Slovenia\", \"IndepYear\": 1991, \"geography\": {\"Region\": \"Southern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 20256}, \"government\": {\"HeadOfState\": \"Milan Kucan\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 1987800, \"LifeExpectancy\": 74.9000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 226492, \"_id\": \"00005de917d800000000000000c8\", \"Code\": \"SWE\", \"Name\": \"Sweden\", \"IndepYear\": 836, \"geography\": {\"Region\": \"Nordic Countries\", \"Continent\": \"Europe\", \"SurfaceArea\": 449964}, \"government\": {\"HeadOfState\": \"Carl XVI Gustaf\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 8861400, \"LifeExpectancy\": 79.5999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1206, \"_id\": \"00005de917d800000000000000c9\", \"Code\": \"SWZ\", \"Name\": \"Swaziland\", \"IndepYear\": 1968, \"geography\": {\"Region\": \"Southern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 17364}, \"government\": {\"HeadOfState\": \"Mswati III\", \"GovernmentForm\": \"Monarchy\"}, \"demographics\": {\"Population\": 1008000, \"LifeExpectancy\": 40.400001525878906}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 536, \"_id\": \"00005de917d800000000000000ca\", \"Code\": \"SYC\", \"Name\": \"Seychelles\", \"IndepYear\": 1976, \"geography\": {\"Region\": \"Eastern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 455}, \"government\": {\"HeadOfState\": \"France-Albert René\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 77000, \"LifeExpectancy\": 70.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 65984, \"_id\": \"00005de917d800000000000000cb\", \"Code\": \"SYR\", \"Name\": \"Syria\", \"IndepYear\": 1941, \"geography\": {\"Region\": \"Middle East\", \"Continent\": \"Asia\", \"SurfaceArea\": 185180}, \"government\": {\"HeadOfState\": \"Bashar al-Assad\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 16125000, \"LifeExpectancy\": 68.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 96, \"_id\": \"00005de917d800000000000000cc\", \"Code\": \"TCA\", \"Name\": \"Turks and Caicos Islands\", \"IndepYear\": null, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 430}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Dependent Territory of the UK\"}, \"demographics\": {\"Population\": 17000, \"LifeExpectancy\": 73.30000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1208, \"_id\": \"00005de917d800000000000000cd\", \"Code\": \"TCD\", \"Name\": \"Chad\", \"IndepYear\": 1960, \"geography\": {\"Region\": \"Central Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 1284000}, \"government\": {\"HeadOfState\": \"Idriss Déby\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 7651000, \"LifeExpectancy\": 50.5}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1449, \"_id\": \"00005de917d800000000000000ce\", \"Code\": \"TGO\", \"Name\": \"Togo\", \"IndepYear\": 1960, \"geography\": {\"Region\": \"Western Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 56785}, \"government\": {\"HeadOfState\": \"Gnassingbé Eyadéma\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 4629000, \"LifeExpectancy\": 54.70000076293945}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 116416, \"_id\": \"00005de917d800000000000000cf\", \"Code\": \"THA\", \"Name\": \"Thailand\", \"IndepYear\": 1350, \"geography\": {\"Region\": \"Southeast Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 513115}, \"government\": {\"HeadOfState\": \"Bhumibol Adulyadej\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 61399000, \"LifeExpectancy\": 68.5999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 1990, \"_id\": \"00005de917d800000000000000d0\", \"Code\": \"TJK\", \"Name\": \"Tajikistan\", \"IndepYear\": 1991, \"geography\": {\"Region\": \"Southern and Central Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 143100}, \"government\": {\"HeadOfState\": \"Emomali Rahmonov\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 6188000, \"LifeExpectancy\": 64.0999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d800000000000000d1\", \"Code\": \"TKL\", \"Name\": \"Tokelau\", \"IndepYear\": null, \"geography\": {\"Region\": \"Polynesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 12}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Nonmetropolitan Territory of New Zealand\"}, \"demographics\": {\"Population\": 2000, \"LifeExpectancy\": null}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 4397, \"_id\": \"00005de917d800000000000000d2\", \"Code\": \"TKM\", \"Name\": \"Turkmenistan\", \"IndepYear\": 1991, \"geography\": {\"Region\": \"Southern and Central Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 488100}, \"government\": {\"HeadOfState\": \"Saparmurad Nijazov\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 4459000, \"LifeExpectancy\": 60.900001525878906}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d800000000000000d3\", \"Code\": \"TMP\", \"Name\": \"East Timor\", \"IndepYear\": null, \"geography\": {\"Region\": \"Southeast Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 14874}, \"government\": {\"HeadOfState\": \"José Alexandre Gusmão\", \"GovernmentForm\": \"Administrated by the UN\"}, \"demographics\": {\"Population\": 885000, \"LifeExpectancy\": 46}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 146, \"_id\": \"00005de917d800000000000000d4\", \"Code\": \"TON\", \"Name\": \"Tonga\", \"IndepYear\": 1970, \"geography\": {\"Region\": \"Polynesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 650}, \"government\": {\"HeadOfState\": \"Taufa\'ahau Tupou IV\", \"GovernmentForm\": \"Monarchy\"}, \"demographics\": {\"Population\": 99000, \"LifeExpectancy\": 67.9000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 6232, \"_id\": \"00005de917d800000000000000d5\", \"Code\": \"TTO\", \"Name\": \"Trinidad and Tobago\", \"IndepYear\": 1962, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 5130}, \"government\": {\"HeadOfState\": \"Arthur N. R. Robinson\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 1295000, \"LifeExpectancy\": 68}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 20026, \"_id\": \"00005de917d800000000000000d6\", \"Code\": \"TUN\", \"Name\": \"Tunisia\", \"IndepYear\": 1956, \"geography\": {\"Region\": \"Northern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 163610}, \"government\": {\"HeadOfState\": \"Zine al-Abidine Ben Ali\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 9586000, \"LifeExpectancy\": 73.69999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 210721, \"_id\": \"00005de917d800000000000000d7\", \"Code\": \"TUR\", \"Name\": \"Turkey\", \"IndepYear\": 1923, \"geography\": {\"Region\": \"Middle East\", \"Continent\": \"Asia\", \"SurfaceArea\": 774815}, \"government\": {\"HeadOfState\": \"Ahmet Necdet Sezer\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 66591000, \"LifeExpectancy\": 71}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 6, \"_id\": \"00005de917d800000000000000d8\", \"Code\": \"TUV\", \"Name\": \"Tuvalu\", \"IndepYear\": 1978, \"geography\": {\"Region\": \"Polynesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 26}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 12000, \"LifeExpectancy\": 66.30000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 256254, \"_id\": \"00005de917d800000000000000d9\", \"Code\": \"TWN\", \"Name\": \"Taiwan\", \"IndepYear\": 1945, \"geography\": {\"Region\": \"Eastern Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 36188}, \"government\": {\"HeadOfState\": \"Chen Shui-bian\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 22256000, \"LifeExpectancy\": 76.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 8005, \"_id\": \"00005de917d800000000000000da\", \"Code\": \"TZA\", \"Name\": \"Tanzania\", \"IndepYear\": 1961, \"geography\": {\"Region\": \"Eastern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 883749}, \"government\": {\"HeadOfState\": \"Benjamin William Mkapa\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 33517000, \"LifeExpectancy\": 52.29999923706055}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 6313, \"_id\": \"00005de917d800000000000000db\", \"Code\": \"UGA\", \"Name\": \"Uganda\", \"IndepYear\": 1962, \"geography\": {\"Region\": \"Eastern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 241038}, \"government\": {\"HeadOfState\": \"Yoweri Museveni\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 21778000, \"LifeExpectancy\": 42.900001525878906}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 42168, \"_id\": \"00005de917d800000000000000dc\", \"Code\": \"UKR\", \"Name\": \"Ukraine\", \"IndepYear\": 1991, \"geography\": {\"Region\": \"Eastern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 603700}, \"government\": {\"HeadOfState\": \"Leonid Kutšma\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 50456000, \"LifeExpectancy\": 66}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d800000000000000dd\", \"Code\": \"UMI\", \"Name\": \"United States Minor Outlying Islands\", \"IndepYear\": null, \"geography\": {\"Region\": \"Micronesia/Caribbean\", \"Continent\": \"Oceania\", \"SurfaceArea\": 16}, \"government\": {\"HeadOfState\": \"George W. Bush\", \"GovernmentForm\": \"Dependent Territory of the US\"}, \"demographics\": {\"Population\": 0, \"LifeExpectancy\": null}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 20831, \"_id\": \"00005de917d800000000000000de\", \"Code\": \"URY\", \"Name\": \"Uruguay\", \"IndepYear\": 1828, \"geography\": {\"Region\": \"South America\", \"Continent\": \"South America\", \"SurfaceArea\": 175016}, \"government\": {\"HeadOfState\": \"Jorge Batlle Ibáñez\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 3337000, \"LifeExpectancy\": 75.19999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 8510700, \"_id\": \"00005de917d800000000000000df\", \"Code\": \"USA\", \"Name\": \"United States\", \"IndepYear\": 1776, \"geography\": {\"Region\": \"North America\", \"Continent\": \"North America\", \"SurfaceArea\": 9363520}, \"government\": {\"HeadOfState\": \"George W. Bush\", \"GovernmentForm\": \"Federal Republic\"}, \"demographics\": {\"Population\": 278357000, \"LifeExpectancy\": 77.0999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 14194, \"_id\": \"00005de917d800000000000000e0\", \"Code\": \"UZB\", \"Name\": \"Uzbekistan\", \"IndepYear\": 1991, \"geography\": {\"Region\": \"Southern and Central Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 447400}, \"government\": {\"HeadOfState\": \"Islam Karimov\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 24318000, \"LifeExpectancy\": 63.70000076293945}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 9, \"_id\": \"00005de917d800000000000000e1\", \"Code\": \"VAT\", \"Name\": \"Holy See (Vatican City State)\", \"IndepYear\": 1929, \"geography\": {\"Region\": \"Southern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 0.4000000059604645}, \"government\": {\"HeadOfState\": \"Johannes Paavali II\", \"GovernmentForm\": \"Independent Church State\"}, \"demographics\": {\"Population\": 1000, \"LifeExpectancy\": null}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 285, \"_id\": \"00005de917d800000000000000e2\", \"Code\": \"VCT\", \"Name\": \"Saint Vincent and the Grenadines\", \"IndepYear\": 1979, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 388}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Constitutional Monarchy\"}, \"demographics\": {\"Population\": 114000, \"LifeExpectancy\": 72.30000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 95023, \"_id\": \"00005de917d800000000000000e3\", \"Code\": \"VEN\", \"Name\": \"Venezuela\", \"IndepYear\": 1811, \"geography\": {\"Region\": \"South America\", \"Continent\": \"South America\", \"SurfaceArea\": 912050}, \"government\": {\"HeadOfState\": \"Hugo Chávez Frías\", \"GovernmentForm\": \"Federal Republic\"}, \"demographics\": {\"Population\": 24170000, \"LifeExpectancy\": 73.0999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 612, \"_id\": \"00005de917d800000000000000e4\", \"Code\": \"VGB\", \"Name\": \"Virgin Islands, British\", \"IndepYear\": null, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 151}, \"government\": {\"HeadOfState\": \"Elizabeth II\", \"GovernmentForm\": \"Dependent Territory of the UK\"}, \"demographics\": {\"Population\": 21000, \"LifeExpectancy\": 75.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d800000000000000e5\", \"Code\": \"VIR\", \"Name\": \"Virgin Islands, U.S.\", \"IndepYear\": null, \"geography\": {\"Region\": \"Caribbean\", \"Continent\": \"North America\", \"SurfaceArea\": 347}, \"government\": {\"HeadOfState\": \"George W. Bush\", \"GovernmentForm\": \"US Territory\"}, \"demographics\": {\"Population\": 93000, \"LifeExpectancy\": 78.0999984741211}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 21929, \"_id\": \"00005de917d800000000000000e6\", \"Code\": \"VNM\", \"Name\": \"Vietnam\", \"IndepYear\": 1945, \"geography\": {\"Region\": \"Southeast Asia\", \"Continent\": \"Asia\", \"SurfaceArea\": 331689}, \"government\": {\"HeadOfState\": \"Trân Duc Luong\", \"GovernmentForm\": \"Socialistic Republic\"}, \"demographics\": {\"Population\": 79832000, \"LifeExpectancy\": 69.30000305175781}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 261, \"_id\": \"00005de917d800000000000000e7\", \"Code\": \"VUT\", \"Name\": \"Vanuatu\", \"IndepYear\": 1980, \"geography\": {\"Region\": \"Melanesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 12189}, \"government\": {\"HeadOfState\": \"John Bani\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 190000, \"LifeExpectancy\": 60.599998474121094}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 0, \"_id\": \"00005de917d800000000000000e8\", \"Code\": \"WLF\", \"Name\": \"Wallis and Futuna\", \"IndepYear\": null, \"geography\": {\"Region\": \"Polynesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 200}, \"government\": {\"HeadOfState\": \"Jacques Chirac\", \"GovernmentForm\": \"Nonmetropolitan Territory of France\"}, \"demographics\": {\"Population\": 15000, \"LifeExpectancy\": null}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 141, \"_id\": \"00005de917d800000000000000e9\", \"Code\": \"WSM\", \"Name\": \"Samoa\", \"IndepYear\": 1962, \"geography\": {\"Region\": \"Polynesia\", \"Continent\": \"Oceania\", \"SurfaceArea\": 2831}, \"government\": {\"HeadOfState\": \"Malietoa Tanumafili II\", \"GovernmentForm\": \"Parlementary Monarchy\"}, \"demographics\": {\"Population\": 180000, \"LifeExpectancy\": 69.19999694824219}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 6041, \"_id\": \"00005de917d800000000000000ea\", \"Code\": \"YEM\", \"Name\": \"Yemen\", \"IndepYear\": 1918, \"geography\": {\"Region\": \"Middle East\", \"Continent\": \"Asia\", \"SurfaceArea\": 527968}, \"government\": {\"HeadOfState\": \"Ali Abdallah Salih\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 18112000, \"LifeExpectancy\": 59.79999923706055}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 17000, \"_id\": \"00005de917d800000000000000eb\", \"Code\": \"YUG\", \"Name\": \"Yugoslavia\", \"IndepYear\": 1918, \"geography\": {\"Region\": \"Southern Europe\", \"Continent\": \"Europe\", \"SurfaceArea\": 102173}, \"government\": {\"HeadOfState\": \"Vojislav Koštunica\", \"GovernmentForm\": \"Federal Republic\"}, \"demographics\": {\"Population\": 10640000, \"LifeExpectancy\": 72.4000015258789}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 116729, \"_id\": \"00005de917d800000000000000ec\", \"Code\": \"ZAF\", \"Name\": \"South Africa\", \"IndepYear\": 1910, \"geography\": {\"Region\": \"Southern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 1221037}, \"government\": {\"HeadOfState\": \"Thabo Mbeki\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 40377000, \"LifeExpectancy\": 51.099998474121094}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 3377, \"_id\": \"00005de917d800000000000000ed\", \"Code\": \"ZMB\", \"Name\": \"Zambia\", \"IndepYear\": 1964, \"geography\": {\"Region\": \"Eastern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 752618}, \"government\": {\"HeadOfState\": \"Frederick Chiluba\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 9169000, \"LifeExpectancy\": 37.20000076293945}}');
INSERT INTO `databand_countryinfo` VALUES ('{\"GNP\": 5951, \"_id\": \"00005de917d800000000000000ee\", \"Code\": \"ZWE\", \"Name\": \"Zimbabwe\", \"IndepYear\": 1980, \"geography\": {\"Region\": \"Eastern Africa\", \"Continent\": \"Africa\", \"SurfaceArea\": 390757}, \"government\": {\"HeadOfState\": \"Robert G. Mugabe\", \"GovernmentForm\": \"Republic\"}, \"demographics\": {\"Population\": 11669000, \"LifeExpectancy\": 37.79999923706055}}');

-- ----------------------------
-- Table structure for `databand_countrylanguage`
-- ----------------------------
DROP TABLE IF EXISTS `databand_countrylanguage`;
CREATE TABLE `databand_countrylanguage` (
  `CountryCode` char(3) NOT NULL DEFAULT '',
  `Language` char(30) NOT NULL DEFAULT '',
  `IsOfficial` enum('T','F') NOT NULL DEFAULT 'F',
  `Percentage` decimal(4,1) NOT NULL DEFAULT '0.0',
  PRIMARY KEY (`CountryCode`,`Language`),
  KEY `CountryCode` (`CountryCode`),
  CONSTRAINT `databand_countrylanguage_ibfk_1` FOREIGN KEY (`CountryCode`) REFERENCES `databand_country` (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of databand_countrylanguage
-- ----------------------------
INSERT INTO `databand_countrylanguage` VALUES ('ABW', 'Dutch', 'T', '5.3');
INSERT INTO `databand_countrylanguage` VALUES ('ABW', 'English', 'F', '9.5');
INSERT INTO `databand_countrylanguage` VALUES ('ABW', 'Papiamento', 'F', '76.7');
INSERT INTO `databand_countrylanguage` VALUES ('ABW', 'Spanish', 'F', '7.4');
INSERT INTO `databand_countrylanguage` VALUES ('AFG', 'Balochi', 'F', '0.9');
INSERT INTO `databand_countrylanguage` VALUES ('AFG', 'Dari', 'T', '32.1');
INSERT INTO `databand_countrylanguage` VALUES ('AFG', 'Pashto', 'T', '52.4');
INSERT INTO `databand_countrylanguage` VALUES ('AFG', 'Turkmenian', 'F', '1.9');
INSERT INTO `databand_countrylanguage` VALUES ('AFG', 'Uzbek', 'F', '8.8');
INSERT INTO `databand_countrylanguage` VALUES ('AGO', 'Ambo', 'F', '2.4');
INSERT INTO `databand_countrylanguage` VALUES ('AGO', 'Chokwe', 'F', '4.2');
INSERT INTO `databand_countrylanguage` VALUES ('AGO', 'Kongo', 'F', '13.2');
INSERT INTO `databand_countrylanguage` VALUES ('AGO', 'Luchazi', 'F', '2.4');
INSERT INTO `databand_countrylanguage` VALUES ('AGO', 'Luimbe-nganguela', 'F', '5.4');
INSERT INTO `databand_countrylanguage` VALUES ('AGO', 'Luvale', 'F', '3.6');
INSERT INTO `databand_countrylanguage` VALUES ('AGO', 'Mbundu', 'F', '21.6');
INSERT INTO `databand_countrylanguage` VALUES ('AGO', 'Nyaneka-nkhumbi', 'F', '5.4');
INSERT INTO `databand_countrylanguage` VALUES ('AGO', 'Ovimbundu', 'F', '37.2');
INSERT INTO `databand_countrylanguage` VALUES ('AIA', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('ALB', 'Albaniana', 'T', '97.9');
INSERT INTO `databand_countrylanguage` VALUES ('ALB', 'Greek', 'F', '1.8');
INSERT INTO `databand_countrylanguage` VALUES ('ALB', 'Macedonian', 'F', '0.1');
INSERT INTO `databand_countrylanguage` VALUES ('AND', 'Catalan', 'T', '32.3');
INSERT INTO `databand_countrylanguage` VALUES ('AND', 'French', 'F', '6.2');
INSERT INTO `databand_countrylanguage` VALUES ('AND', 'Portuguese', 'F', '10.8');
INSERT INTO `databand_countrylanguage` VALUES ('AND', 'Spanish', 'F', '44.6');
INSERT INTO `databand_countrylanguage` VALUES ('ANT', 'Dutch', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('ANT', 'English', 'F', '7.8');
INSERT INTO `databand_countrylanguage` VALUES ('ANT', 'Papiamento', 'T', '86.2');
INSERT INTO `databand_countrylanguage` VALUES ('ARE', 'Arabic', 'T', '42.0');
INSERT INTO `databand_countrylanguage` VALUES ('ARE', 'Hindi', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('ARG', 'Indian Languages', 'F', '0.3');
INSERT INTO `databand_countrylanguage` VALUES ('ARG', 'Italian', 'F', '1.7');
INSERT INTO `databand_countrylanguage` VALUES ('ARG', 'Spanish', 'T', '96.8');
INSERT INTO `databand_countrylanguage` VALUES ('ARM', 'Armenian', 'T', '93.4');
INSERT INTO `databand_countrylanguage` VALUES ('ARM', 'Azerbaijani', 'F', '2.6');
INSERT INTO `databand_countrylanguage` VALUES ('ASM', 'English', 'T', '3.1');
INSERT INTO `databand_countrylanguage` VALUES ('ASM', 'Samoan', 'T', '90.6');
INSERT INTO `databand_countrylanguage` VALUES ('ASM', 'Tongan', 'F', '3.1');
INSERT INTO `databand_countrylanguage` VALUES ('ATG', 'Creole English', 'F', '95.7');
INSERT INTO `databand_countrylanguage` VALUES ('ATG', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('AUS', 'Arabic', 'F', '1.0');
INSERT INTO `databand_countrylanguage` VALUES ('AUS', 'Canton Chinese', 'F', '1.1');
INSERT INTO `databand_countrylanguage` VALUES ('AUS', 'English', 'T', '81.2');
INSERT INTO `databand_countrylanguage` VALUES ('AUS', 'German', 'F', '0.6');
INSERT INTO `databand_countrylanguage` VALUES ('AUS', 'Greek', 'F', '1.6');
INSERT INTO `databand_countrylanguage` VALUES ('AUS', 'Italian', 'F', '2.2');
INSERT INTO `databand_countrylanguage` VALUES ('AUS', 'Serbo-Croatian', 'F', '0.6');
INSERT INTO `databand_countrylanguage` VALUES ('AUS', 'Vietnamese', 'F', '0.8');
INSERT INTO `databand_countrylanguage` VALUES ('AUT', 'Czech', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('AUT', 'German', 'T', '92.0');
INSERT INTO `databand_countrylanguage` VALUES ('AUT', 'Hungarian', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('AUT', 'Polish', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('AUT', 'Romanian', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('AUT', 'Serbo-Croatian', 'F', '2.2');
INSERT INTO `databand_countrylanguage` VALUES ('AUT', 'Slovene', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('AUT', 'Turkish', 'F', '1.5');
INSERT INTO `databand_countrylanguage` VALUES ('AZE', 'Armenian', 'F', '2.0');
INSERT INTO `databand_countrylanguage` VALUES ('AZE', 'Azerbaijani', 'T', '89.0');
INSERT INTO `databand_countrylanguage` VALUES ('AZE', 'Lezgian', 'F', '2.3');
INSERT INTO `databand_countrylanguage` VALUES ('AZE', 'Russian', 'F', '3.0');
INSERT INTO `databand_countrylanguage` VALUES ('BDI', 'French', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('BDI', 'Kirundi', 'T', '98.1');
INSERT INTO `databand_countrylanguage` VALUES ('BDI', 'Swahili', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('BEL', 'Arabic', 'F', '1.6');
INSERT INTO `databand_countrylanguage` VALUES ('BEL', 'Dutch', 'T', '59.2');
INSERT INTO `databand_countrylanguage` VALUES ('BEL', 'French', 'T', '32.6');
INSERT INTO `databand_countrylanguage` VALUES ('BEL', 'German', 'T', '1.0');
INSERT INTO `databand_countrylanguage` VALUES ('BEL', 'Italian', 'F', '2.4');
INSERT INTO `databand_countrylanguage` VALUES ('BEL', 'Turkish', 'F', '0.9');
INSERT INTO `databand_countrylanguage` VALUES ('BEN', 'Adja', 'F', '11.1');
INSERT INTO `databand_countrylanguage` VALUES ('BEN', 'Aizo', 'F', '8.7');
INSERT INTO `databand_countrylanguage` VALUES ('BEN', 'Bariba', 'F', '8.7');
INSERT INTO `databand_countrylanguage` VALUES ('BEN', 'Fon', 'F', '39.8');
INSERT INTO `databand_countrylanguage` VALUES ('BEN', 'Ful', 'F', '5.6');
INSERT INTO `databand_countrylanguage` VALUES ('BEN', 'Joruba', 'F', '12.2');
INSERT INTO `databand_countrylanguage` VALUES ('BEN', 'Somba', 'F', '6.7');
INSERT INTO `databand_countrylanguage` VALUES ('BFA', 'Busansi', 'F', '3.5');
INSERT INTO `databand_countrylanguage` VALUES ('BFA', 'Dagara', 'F', '3.1');
INSERT INTO `databand_countrylanguage` VALUES ('BFA', 'Dyula', 'F', '2.6');
INSERT INTO `databand_countrylanguage` VALUES ('BFA', 'Ful', 'F', '9.7');
INSERT INTO `databand_countrylanguage` VALUES ('BFA', 'Gurma', 'F', '5.7');
INSERT INTO `databand_countrylanguage` VALUES ('BFA', 'Mossi', 'F', '50.2');
INSERT INTO `databand_countrylanguage` VALUES ('BGD', 'Bengali', 'T', '97.7');
INSERT INTO `databand_countrylanguage` VALUES ('BGD', 'Chakma', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('BGD', 'Garo', 'F', '0.1');
INSERT INTO `databand_countrylanguage` VALUES ('BGD', 'Khasi', 'F', '0.1');
INSERT INTO `databand_countrylanguage` VALUES ('BGD', 'Marma', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('BGD', 'Santhali', 'F', '0.1');
INSERT INTO `databand_countrylanguage` VALUES ('BGD', 'Tripuri', 'F', '0.1');
INSERT INTO `databand_countrylanguage` VALUES ('BGR', 'Bulgariana', 'T', '83.2');
INSERT INTO `databand_countrylanguage` VALUES ('BGR', 'Macedonian', 'F', '2.6');
INSERT INTO `databand_countrylanguage` VALUES ('BGR', 'Romani', 'F', '3.7');
INSERT INTO `databand_countrylanguage` VALUES ('BGR', 'Turkish', 'F', '9.4');
INSERT INTO `databand_countrylanguage` VALUES ('BHR', 'Arabic', 'T', '67.7');
INSERT INTO `databand_countrylanguage` VALUES ('BHR', 'English', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('BHS', 'Creole English', 'F', '89.7');
INSERT INTO `databand_countrylanguage` VALUES ('BHS', 'Creole French', 'F', '10.3');
INSERT INTO `databand_countrylanguage` VALUES ('BIH', 'Serbo-Croatian', 'T', '99.2');
INSERT INTO `databand_countrylanguage` VALUES ('BLR', 'Belorussian', 'T', '65.6');
INSERT INTO `databand_countrylanguage` VALUES ('BLR', 'Polish', 'F', '0.6');
INSERT INTO `databand_countrylanguage` VALUES ('BLR', 'Russian', 'T', '32.0');
INSERT INTO `databand_countrylanguage` VALUES ('BLR', 'Ukrainian', 'F', '1.3');
INSERT INTO `databand_countrylanguage` VALUES ('BLZ', 'English', 'T', '50.8');
INSERT INTO `databand_countrylanguage` VALUES ('BLZ', 'Garifuna', 'F', '6.8');
INSERT INTO `databand_countrylanguage` VALUES ('BLZ', 'Maya Languages', 'F', '9.6');
INSERT INTO `databand_countrylanguage` VALUES ('BLZ', 'Spanish', 'F', '31.6');
INSERT INTO `databand_countrylanguage` VALUES ('BMU', 'English', 'T', '100.0');
INSERT INTO `databand_countrylanguage` VALUES ('BOL', 'Aimará', 'T', '3.2');
INSERT INTO `databand_countrylanguage` VALUES ('BOL', 'Guaraní', 'F', '0.1');
INSERT INTO `databand_countrylanguage` VALUES ('BOL', 'Ketšua', 'T', '8.1');
INSERT INTO `databand_countrylanguage` VALUES ('BOL', 'Spanish', 'T', '87.7');
INSERT INTO `databand_countrylanguage` VALUES ('BRA', 'German', 'F', '0.5');
INSERT INTO `databand_countrylanguage` VALUES ('BRA', 'Indian Languages', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('BRA', 'Italian', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('BRA', 'Japanese', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('BRA', 'Portuguese', 'T', '97.5');
INSERT INTO `databand_countrylanguage` VALUES ('BRB', 'Bajan', 'F', '95.1');
INSERT INTO `databand_countrylanguage` VALUES ('BRB', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('BRN', 'Chinese', 'F', '9.3');
INSERT INTO `databand_countrylanguage` VALUES ('BRN', 'English', 'F', '3.1');
INSERT INTO `databand_countrylanguage` VALUES ('BRN', 'Malay', 'T', '45.5');
INSERT INTO `databand_countrylanguage` VALUES ('BRN', 'Malay-English', 'F', '28.8');
INSERT INTO `databand_countrylanguage` VALUES ('BTN', 'Asami', 'F', '15.2');
INSERT INTO `databand_countrylanguage` VALUES ('BTN', 'Dzongkha', 'T', '50.0');
INSERT INTO `databand_countrylanguage` VALUES ('BTN', 'Nepali', 'F', '34.8');
INSERT INTO `databand_countrylanguage` VALUES ('BWA', 'Khoekhoe', 'F', '2.5');
INSERT INTO `databand_countrylanguage` VALUES ('BWA', 'Ndebele', 'F', '1.3');
INSERT INTO `databand_countrylanguage` VALUES ('BWA', 'San', 'F', '3.5');
INSERT INTO `databand_countrylanguage` VALUES ('BWA', 'Shona', 'F', '12.3');
INSERT INTO `databand_countrylanguage` VALUES ('BWA', 'Tswana', 'F', '75.5');
INSERT INTO `databand_countrylanguage` VALUES ('CAF', 'Banda', 'F', '23.5');
INSERT INTO `databand_countrylanguage` VALUES ('CAF', 'Gbaya', 'F', '23.8');
INSERT INTO `databand_countrylanguage` VALUES ('CAF', 'Mandjia', 'F', '14.8');
INSERT INTO `databand_countrylanguage` VALUES ('CAF', 'Mbum', 'F', '6.4');
INSERT INTO `databand_countrylanguage` VALUES ('CAF', 'Ngbaka', 'F', '7.5');
INSERT INTO `databand_countrylanguage` VALUES ('CAF', 'Sara', 'F', '6.4');
INSERT INTO `databand_countrylanguage` VALUES ('CAN', 'Chinese', 'F', '2.5');
INSERT INTO `databand_countrylanguage` VALUES ('CAN', 'Dutch', 'F', '0.5');
INSERT INTO `databand_countrylanguage` VALUES ('CAN', 'English', 'T', '60.4');
INSERT INTO `databand_countrylanguage` VALUES ('CAN', 'Eskimo Languages', 'F', '0.1');
INSERT INTO `databand_countrylanguage` VALUES ('CAN', 'French', 'T', '23.4');
INSERT INTO `databand_countrylanguage` VALUES ('CAN', 'German', 'F', '1.6');
INSERT INTO `databand_countrylanguage` VALUES ('CAN', 'Italian', 'F', '1.7');
INSERT INTO `databand_countrylanguage` VALUES ('CAN', 'Polish', 'F', '0.7');
INSERT INTO `databand_countrylanguage` VALUES ('CAN', 'Portuguese', 'F', '0.7');
INSERT INTO `databand_countrylanguage` VALUES ('CAN', 'Punjabi', 'F', '0.7');
INSERT INTO `databand_countrylanguage` VALUES ('CAN', 'Spanish', 'F', '0.7');
INSERT INTO `databand_countrylanguage` VALUES ('CAN', 'Ukrainian', 'F', '0.6');
INSERT INTO `databand_countrylanguage` VALUES ('CCK', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('CCK', 'Malay', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('CHE', 'French', 'T', '19.2');
INSERT INTO `databand_countrylanguage` VALUES ('CHE', 'German', 'T', '63.6');
INSERT INTO `databand_countrylanguage` VALUES ('CHE', 'Italian', 'T', '7.7');
INSERT INTO `databand_countrylanguage` VALUES ('CHE', 'Romansh', 'T', '0.6');
INSERT INTO `databand_countrylanguage` VALUES ('CHL', 'Aimará', 'F', '0.5');
INSERT INTO `databand_countrylanguage` VALUES ('CHL', 'Araucan', 'F', '9.6');
INSERT INTO `databand_countrylanguage` VALUES ('CHL', 'Rapa nui', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('CHL', 'Spanish', 'T', '89.7');
INSERT INTO `databand_countrylanguage` VALUES ('CHN', 'Chinese', 'T', '92.0');
INSERT INTO `databand_countrylanguage` VALUES ('CHN', 'Dong', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('CHN', 'Hui', 'F', '0.8');
INSERT INTO `databand_countrylanguage` VALUES ('CHN', 'Mantšu', 'F', '0.9');
INSERT INTO `databand_countrylanguage` VALUES ('CHN', 'Miao', 'F', '0.7');
INSERT INTO `databand_countrylanguage` VALUES ('CHN', 'Mongolian', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('CHN', 'Puyi', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('CHN', 'Tibetan', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('CHN', 'Tujia', 'F', '0.5');
INSERT INTO `databand_countrylanguage` VALUES ('CHN', 'Uighur', 'F', '0.6');
INSERT INTO `databand_countrylanguage` VALUES ('CHN', 'Yi', 'F', '0.6');
INSERT INTO `databand_countrylanguage` VALUES ('CHN', 'Zhuang', 'F', '1.4');
INSERT INTO `databand_countrylanguage` VALUES ('CIV', 'Akan', 'F', '30.0');
INSERT INTO `databand_countrylanguage` VALUES ('CIV', 'Gur', 'F', '11.7');
INSERT INTO `databand_countrylanguage` VALUES ('CIV', 'Kru', 'F', '10.5');
INSERT INTO `databand_countrylanguage` VALUES ('CIV', 'Malinke', 'F', '11.4');
INSERT INTO `databand_countrylanguage` VALUES ('CIV', '[South]Mande', 'F', '7.7');
INSERT INTO `databand_countrylanguage` VALUES ('CMR', 'Bamileke-bamum', 'F', '18.6');
INSERT INTO `databand_countrylanguage` VALUES ('CMR', 'Duala', 'F', '10.9');
INSERT INTO `databand_countrylanguage` VALUES ('CMR', 'Fang', 'F', '19.7');
INSERT INTO `databand_countrylanguage` VALUES ('CMR', 'Ful', 'F', '9.6');
INSERT INTO `databand_countrylanguage` VALUES ('CMR', 'Maka', 'F', '4.9');
INSERT INTO `databand_countrylanguage` VALUES ('CMR', 'Mandara', 'F', '5.7');
INSERT INTO `databand_countrylanguage` VALUES ('CMR', 'Masana', 'F', '3.9');
INSERT INTO `databand_countrylanguage` VALUES ('CMR', 'Tikar', 'F', '7.4');
INSERT INTO `databand_countrylanguage` VALUES ('COD', 'Boa', 'F', '2.3');
INSERT INTO `databand_countrylanguage` VALUES ('COD', 'Chokwe', 'F', '1.8');
INSERT INTO `databand_countrylanguage` VALUES ('COD', 'Kongo', 'F', '16.0');
INSERT INTO `databand_countrylanguage` VALUES ('COD', 'Luba', 'F', '18.0');
INSERT INTO `databand_countrylanguage` VALUES ('COD', 'Mongo', 'F', '13.5');
INSERT INTO `databand_countrylanguage` VALUES ('COD', 'Ngala and Bangi', 'F', '5.8');
INSERT INTO `databand_countrylanguage` VALUES ('COD', 'Rundi', 'F', '3.8');
INSERT INTO `databand_countrylanguage` VALUES ('COD', 'Rwanda', 'F', '10.3');
INSERT INTO `databand_countrylanguage` VALUES ('COD', 'Teke', 'F', '2.7');
INSERT INTO `databand_countrylanguage` VALUES ('COD', 'Zande', 'F', '6.1');
INSERT INTO `databand_countrylanguage` VALUES ('COG', 'Kongo', 'F', '51.5');
INSERT INTO `databand_countrylanguage` VALUES ('COG', 'Mbete', 'F', '4.8');
INSERT INTO `databand_countrylanguage` VALUES ('COG', 'Mboshi', 'F', '11.4');
INSERT INTO `databand_countrylanguage` VALUES ('COG', 'Punu', 'F', '2.9');
INSERT INTO `databand_countrylanguage` VALUES ('COG', 'Sango', 'F', '2.6');
INSERT INTO `databand_countrylanguage` VALUES ('COG', 'Teke', 'F', '17.3');
INSERT INTO `databand_countrylanguage` VALUES ('COK', 'English', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('COK', 'Maori', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('COL', 'Arawakan', 'F', '0.1');
INSERT INTO `databand_countrylanguage` VALUES ('COL', 'Caribbean', 'F', '0.1');
INSERT INTO `databand_countrylanguage` VALUES ('COL', 'Chibcha', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('COL', 'Creole English', 'F', '0.1');
INSERT INTO `databand_countrylanguage` VALUES ('COL', 'Spanish', 'T', '99.0');
INSERT INTO `databand_countrylanguage` VALUES ('COM', 'Comorian', 'T', '75.0');
INSERT INTO `databand_countrylanguage` VALUES ('COM', 'Comorian-Arabic', 'F', '1.6');
INSERT INTO `databand_countrylanguage` VALUES ('COM', 'Comorian-French', 'F', '12.9');
INSERT INTO `databand_countrylanguage` VALUES ('COM', 'Comorian-madagassi', 'F', '5.5');
INSERT INTO `databand_countrylanguage` VALUES ('COM', 'Comorian-Swahili', 'F', '0.5');
INSERT INTO `databand_countrylanguage` VALUES ('CPV', 'Crioulo', 'F', '100.0');
INSERT INTO `databand_countrylanguage` VALUES ('CPV', 'Portuguese', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('CRI', 'Chibcha', 'F', '0.3');
INSERT INTO `databand_countrylanguage` VALUES ('CRI', 'Chinese', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('CRI', 'Creole English', 'F', '2.0');
INSERT INTO `databand_countrylanguage` VALUES ('CRI', 'Spanish', 'T', '97.5');
INSERT INTO `databand_countrylanguage` VALUES ('CUB', 'Spanish', 'T', '100.0');
INSERT INTO `databand_countrylanguage` VALUES ('CXR', 'Chinese', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('CXR', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('CYM', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('CYP', 'Greek', 'T', '74.1');
INSERT INTO `databand_countrylanguage` VALUES ('CYP', 'Turkish', 'T', '22.4');
INSERT INTO `databand_countrylanguage` VALUES ('CZE', 'Czech', 'T', '81.2');
INSERT INTO `databand_countrylanguage` VALUES ('CZE', 'German', 'F', '0.5');
INSERT INTO `databand_countrylanguage` VALUES ('CZE', 'Hungarian', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('CZE', 'Moravian', 'F', '12.9');
INSERT INTO `databand_countrylanguage` VALUES ('CZE', 'Polish', 'F', '0.6');
INSERT INTO `databand_countrylanguage` VALUES ('CZE', 'Romani', 'F', '0.3');
INSERT INTO `databand_countrylanguage` VALUES ('CZE', 'Silesiana', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('CZE', 'Slovak', 'F', '3.1');
INSERT INTO `databand_countrylanguage` VALUES ('DEU', 'German', 'T', '91.3');
INSERT INTO `databand_countrylanguage` VALUES ('DEU', 'Greek', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('DEU', 'Italian', 'F', '0.7');
INSERT INTO `databand_countrylanguage` VALUES ('DEU', 'Polish', 'F', '0.3');
INSERT INTO `databand_countrylanguage` VALUES ('DEU', 'Southern Slavic Languages', 'F', '1.4');
INSERT INTO `databand_countrylanguage` VALUES ('DEU', 'Turkish', 'F', '2.6');
INSERT INTO `databand_countrylanguage` VALUES ('DJI', 'Afar', 'F', '34.8');
INSERT INTO `databand_countrylanguage` VALUES ('DJI', 'Arabic', 'T', '10.6');
INSERT INTO `databand_countrylanguage` VALUES ('DJI', 'Somali', 'F', '43.9');
INSERT INTO `databand_countrylanguage` VALUES ('DMA', 'Creole English', 'F', '100.0');
INSERT INTO `databand_countrylanguage` VALUES ('DMA', 'Creole French', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('DNK', 'Arabic', 'F', '0.7');
INSERT INTO `databand_countrylanguage` VALUES ('DNK', 'Danish', 'T', '93.5');
INSERT INTO `databand_countrylanguage` VALUES ('DNK', 'English', 'F', '0.3');
INSERT INTO `databand_countrylanguage` VALUES ('DNK', 'German', 'F', '0.5');
INSERT INTO `databand_countrylanguage` VALUES ('DNK', 'Norwegian', 'F', '0.3');
INSERT INTO `databand_countrylanguage` VALUES ('DNK', 'Swedish', 'F', '0.3');
INSERT INTO `databand_countrylanguage` VALUES ('DNK', 'Turkish', 'F', '0.8');
INSERT INTO `databand_countrylanguage` VALUES ('DOM', 'Creole French', 'F', '2.0');
INSERT INTO `databand_countrylanguage` VALUES ('DOM', 'Spanish', 'T', '98.0');
INSERT INTO `databand_countrylanguage` VALUES ('DZA', 'Arabic', 'T', '86.0');
INSERT INTO `databand_countrylanguage` VALUES ('DZA', 'Berberi', 'F', '14.0');
INSERT INTO `databand_countrylanguage` VALUES ('ECU', 'Ketšua', 'F', '7.0');
INSERT INTO `databand_countrylanguage` VALUES ('ECU', 'Spanish', 'T', '93.0');
INSERT INTO `databand_countrylanguage` VALUES ('EGY', 'Arabic', 'T', '98.8');
INSERT INTO `databand_countrylanguage` VALUES ('EGY', 'Sinaberberi', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('ERI', 'Afar', 'F', '4.3');
INSERT INTO `databand_countrylanguage` VALUES ('ERI', 'Bilin', 'F', '3.0');
INSERT INTO `databand_countrylanguage` VALUES ('ERI', 'Hadareb', 'F', '3.8');
INSERT INTO `databand_countrylanguage` VALUES ('ERI', 'Saho', 'F', '3.0');
INSERT INTO `databand_countrylanguage` VALUES ('ERI', 'Tigre', 'F', '31.7');
INSERT INTO `databand_countrylanguage` VALUES ('ERI', 'Tigrinja', 'T', '49.1');
INSERT INTO `databand_countrylanguage` VALUES ('ESH', 'Arabic', 'T', '100.0');
INSERT INTO `databand_countrylanguage` VALUES ('ESP', 'Basque', 'F', '1.6');
INSERT INTO `databand_countrylanguage` VALUES ('ESP', 'Catalan', 'F', '16.9');
INSERT INTO `databand_countrylanguage` VALUES ('ESP', 'Galecian', 'F', '6.4');
INSERT INTO `databand_countrylanguage` VALUES ('ESP', 'Spanish', 'T', '74.4');
INSERT INTO `databand_countrylanguage` VALUES ('EST', 'Belorussian', 'F', '1.4');
INSERT INTO `databand_countrylanguage` VALUES ('EST', 'Estonian', 'T', '65.3');
INSERT INTO `databand_countrylanguage` VALUES ('EST', 'Finnish', 'F', '0.7');
INSERT INTO `databand_countrylanguage` VALUES ('EST', 'Russian', 'F', '27.8');
INSERT INTO `databand_countrylanguage` VALUES ('EST', 'Ukrainian', 'F', '2.8');
INSERT INTO `databand_countrylanguage` VALUES ('ETH', 'Amhara', 'F', '30.0');
INSERT INTO `databand_countrylanguage` VALUES ('ETH', 'Gurage', 'F', '4.7');
INSERT INTO `databand_countrylanguage` VALUES ('ETH', 'Oromo', 'F', '31.0');
INSERT INTO `databand_countrylanguage` VALUES ('ETH', 'Sidamo', 'F', '3.2');
INSERT INTO `databand_countrylanguage` VALUES ('ETH', 'Somali', 'F', '4.1');
INSERT INTO `databand_countrylanguage` VALUES ('ETH', 'Tigrinja', 'F', '7.2');
INSERT INTO `databand_countrylanguage` VALUES ('ETH', 'Walaita', 'F', '2.8');
INSERT INTO `databand_countrylanguage` VALUES ('FIN', 'Estonian', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('FIN', 'Finnish', 'T', '92.7');
INSERT INTO `databand_countrylanguage` VALUES ('FIN', 'Russian', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('FIN', 'Saame', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('FIN', 'Swedish', 'T', '5.7');
INSERT INTO `databand_countrylanguage` VALUES ('FJI', 'Fijian', 'T', '50.8');
INSERT INTO `databand_countrylanguage` VALUES ('FJI', 'Hindi', 'F', '43.7');
INSERT INTO `databand_countrylanguage` VALUES ('FLK', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('FRA', 'Arabic', 'F', '2.5');
INSERT INTO `databand_countrylanguage` VALUES ('FRA', 'French', 'T', '93.6');
INSERT INTO `databand_countrylanguage` VALUES ('FRA', 'Italian', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('FRA', 'Portuguese', 'F', '1.2');
INSERT INTO `databand_countrylanguage` VALUES ('FRA', 'Spanish', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('FRA', 'Turkish', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('FRO', 'Danish', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('FRO', 'Faroese', 'T', '100.0');
INSERT INTO `databand_countrylanguage` VALUES ('FSM', 'Kosrean', 'F', '7.3');
INSERT INTO `databand_countrylanguage` VALUES ('FSM', 'Mortlock', 'F', '7.6');
INSERT INTO `databand_countrylanguage` VALUES ('FSM', 'Pohnpei', 'F', '23.8');
INSERT INTO `databand_countrylanguage` VALUES ('FSM', 'Trukese', 'F', '41.6');
INSERT INTO `databand_countrylanguage` VALUES ('FSM', 'Wolea', 'F', '3.7');
INSERT INTO `databand_countrylanguage` VALUES ('FSM', 'Yap', 'F', '5.8');
INSERT INTO `databand_countrylanguage` VALUES ('GAB', 'Fang', 'F', '35.8');
INSERT INTO `databand_countrylanguage` VALUES ('GAB', 'Mbete', 'F', '13.8');
INSERT INTO `databand_countrylanguage` VALUES ('GAB', 'Mpongwe', 'F', '14.6');
INSERT INTO `databand_countrylanguage` VALUES ('GAB', 'Punu-sira-nzebi', 'F', '17.1');
INSERT INTO `databand_countrylanguage` VALUES ('GBR', 'English', 'T', '97.3');
INSERT INTO `databand_countrylanguage` VALUES ('GBR', 'Gaeli', 'F', '0.1');
INSERT INTO `databand_countrylanguage` VALUES ('GBR', 'Kymri', 'F', '0.9');
INSERT INTO `databand_countrylanguage` VALUES ('GEO', 'Abhyasi', 'F', '1.7');
INSERT INTO `databand_countrylanguage` VALUES ('GEO', 'Armenian', 'F', '6.8');
INSERT INTO `databand_countrylanguage` VALUES ('GEO', 'Azerbaijani', 'F', '5.5');
INSERT INTO `databand_countrylanguage` VALUES ('GEO', 'Georgiana', 'T', '71.7');
INSERT INTO `databand_countrylanguage` VALUES ('GEO', 'Osseetti', 'F', '2.4');
INSERT INTO `databand_countrylanguage` VALUES ('GEO', 'Russian', 'F', '8.8');
INSERT INTO `databand_countrylanguage` VALUES ('GHA', 'Akan', 'F', '52.4');
INSERT INTO `databand_countrylanguage` VALUES ('GHA', 'Ewe', 'F', '11.9');
INSERT INTO `databand_countrylanguage` VALUES ('GHA', 'Ga-adangme', 'F', '7.8');
INSERT INTO `databand_countrylanguage` VALUES ('GHA', 'Gurma', 'F', '3.3');
INSERT INTO `databand_countrylanguage` VALUES ('GHA', 'Joruba', 'F', '1.3');
INSERT INTO `databand_countrylanguage` VALUES ('GHA', 'Mossi', 'F', '15.8');
INSERT INTO `databand_countrylanguage` VALUES ('GIB', 'Arabic', 'F', '7.4');
INSERT INTO `databand_countrylanguage` VALUES ('GIB', 'English', 'T', '88.9');
INSERT INTO `databand_countrylanguage` VALUES ('GIN', 'Ful', 'F', '38.6');
INSERT INTO `databand_countrylanguage` VALUES ('GIN', 'Kissi', 'F', '6.0');
INSERT INTO `databand_countrylanguage` VALUES ('GIN', 'Kpelle', 'F', '4.6');
INSERT INTO `databand_countrylanguage` VALUES ('GIN', 'Loma', 'F', '2.3');
INSERT INTO `databand_countrylanguage` VALUES ('GIN', 'Malinke', 'F', '23.2');
INSERT INTO `databand_countrylanguage` VALUES ('GIN', 'Susu', 'F', '11.0');
INSERT INTO `databand_countrylanguage` VALUES ('GIN', 'Yalunka', 'F', '2.9');
INSERT INTO `databand_countrylanguage` VALUES ('GLP', 'Creole French', 'F', '95.0');
INSERT INTO `databand_countrylanguage` VALUES ('GLP', 'French', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('GMB', 'Diola', 'F', '9.2');
INSERT INTO `databand_countrylanguage` VALUES ('GMB', 'Ful', 'F', '16.2');
INSERT INTO `databand_countrylanguage` VALUES ('GMB', 'Malinke', 'F', '34.1');
INSERT INTO `databand_countrylanguage` VALUES ('GMB', 'Soninke', 'F', '7.6');
INSERT INTO `databand_countrylanguage` VALUES ('GMB', 'Wolof', 'F', '12.6');
INSERT INTO `databand_countrylanguage` VALUES ('GNB', 'Balante', 'F', '14.6');
INSERT INTO `databand_countrylanguage` VALUES ('GNB', 'Crioulo', 'F', '36.4');
INSERT INTO `databand_countrylanguage` VALUES ('GNB', 'Ful', 'F', '16.6');
INSERT INTO `databand_countrylanguage` VALUES ('GNB', 'Malinke', 'F', '6.9');
INSERT INTO `databand_countrylanguage` VALUES ('GNB', 'Mandyako', 'F', '4.9');
INSERT INTO `databand_countrylanguage` VALUES ('GNB', 'Portuguese', 'T', '8.1');
INSERT INTO `databand_countrylanguage` VALUES ('GNQ', 'Bubi', 'F', '8.7');
INSERT INTO `databand_countrylanguage` VALUES ('GNQ', 'Fang', 'F', '84.8');
INSERT INTO `databand_countrylanguage` VALUES ('GRC', 'Greek', 'T', '98.5');
INSERT INTO `databand_countrylanguage` VALUES ('GRC', 'Turkish', 'F', '0.9');
INSERT INTO `databand_countrylanguage` VALUES ('GRD', 'Creole English', 'F', '100.0');
INSERT INTO `databand_countrylanguage` VALUES ('GRL', 'Danish', 'T', '12.5');
INSERT INTO `databand_countrylanguage` VALUES ('GRL', 'Greenlandic', 'T', '87.5');
INSERT INTO `databand_countrylanguage` VALUES ('GTM', 'Cakchiquel', 'F', '8.9');
INSERT INTO `databand_countrylanguage` VALUES ('GTM', 'Kekchí', 'F', '4.9');
INSERT INTO `databand_countrylanguage` VALUES ('GTM', 'Mam', 'F', '2.7');
INSERT INTO `databand_countrylanguage` VALUES ('GTM', 'Quiché', 'F', '10.1');
INSERT INTO `databand_countrylanguage` VALUES ('GTM', 'Spanish', 'T', '64.7');
INSERT INTO `databand_countrylanguage` VALUES ('GUF', 'Creole French', 'F', '94.3');
INSERT INTO `databand_countrylanguage` VALUES ('GUF', 'Indian Languages', 'F', '1.9');
INSERT INTO `databand_countrylanguage` VALUES ('GUM', 'Chamorro', 'T', '29.6');
INSERT INTO `databand_countrylanguage` VALUES ('GUM', 'English', 'T', '37.5');
INSERT INTO `databand_countrylanguage` VALUES ('GUM', 'Japanese', 'F', '2.0');
INSERT INTO `databand_countrylanguage` VALUES ('GUM', 'Korean', 'F', '3.3');
INSERT INTO `databand_countrylanguage` VALUES ('GUM', 'Philippene Languages', 'F', '19.7');
INSERT INTO `databand_countrylanguage` VALUES ('GUY', 'Arawakan', 'F', '1.4');
INSERT INTO `databand_countrylanguage` VALUES ('GUY', 'Caribbean', 'F', '2.2');
INSERT INTO `databand_countrylanguage` VALUES ('GUY', 'Creole English', 'F', '96.4');
INSERT INTO `databand_countrylanguage` VALUES ('HKG', 'Canton Chinese', 'F', '88.7');
INSERT INTO `databand_countrylanguage` VALUES ('HKG', 'Chiu chau', 'F', '1.4');
INSERT INTO `databand_countrylanguage` VALUES ('HKG', 'English', 'T', '2.2');
INSERT INTO `databand_countrylanguage` VALUES ('HKG', 'Fukien', 'F', '1.9');
INSERT INTO `databand_countrylanguage` VALUES ('HKG', 'Hakka', 'F', '1.6');
INSERT INTO `databand_countrylanguage` VALUES ('HND', 'Creole English', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('HND', 'Garifuna', 'F', '1.3');
INSERT INTO `databand_countrylanguage` VALUES ('HND', 'Miskito', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('HND', 'Spanish', 'T', '97.2');
INSERT INTO `databand_countrylanguage` VALUES ('HRV', 'Serbo-Croatian', 'T', '95.9');
INSERT INTO `databand_countrylanguage` VALUES ('HRV', 'Slovene', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('HTI', 'French', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('HTI', 'Haiti Creole', 'F', '100.0');
INSERT INTO `databand_countrylanguage` VALUES ('HUN', 'German', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('HUN', 'Hungarian', 'T', '98.5');
INSERT INTO `databand_countrylanguage` VALUES ('HUN', 'Romani', 'F', '0.5');
INSERT INTO `databand_countrylanguage` VALUES ('HUN', 'Romanian', 'F', '0.1');
INSERT INTO `databand_countrylanguage` VALUES ('HUN', 'Serbo-Croatian', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('HUN', 'Slovak', 'F', '0.1');
INSERT INTO `databand_countrylanguage` VALUES ('IDN', 'Bali', 'F', '1.7');
INSERT INTO `databand_countrylanguage` VALUES ('IDN', 'Banja', 'F', '1.8');
INSERT INTO `databand_countrylanguage` VALUES ('IDN', 'Batakki', 'F', '2.2');
INSERT INTO `databand_countrylanguage` VALUES ('IDN', 'Bugi', 'F', '2.2');
INSERT INTO `databand_countrylanguage` VALUES ('IDN', 'Javanese', 'F', '39.4');
INSERT INTO `databand_countrylanguage` VALUES ('IDN', 'Madura', 'F', '4.3');
INSERT INTO `databand_countrylanguage` VALUES ('IDN', 'Malay', 'T', '12.1');
INSERT INTO `databand_countrylanguage` VALUES ('IDN', 'Minangkabau', 'F', '2.4');
INSERT INTO `databand_countrylanguage` VALUES ('IDN', 'Sunda', 'F', '15.8');
INSERT INTO `databand_countrylanguage` VALUES ('IND', 'Asami', 'F', '1.5');
INSERT INTO `databand_countrylanguage` VALUES ('IND', 'Bengali', 'F', '8.2');
INSERT INTO `databand_countrylanguage` VALUES ('IND', 'Gujarati', 'F', '4.8');
INSERT INTO `databand_countrylanguage` VALUES ('IND', 'Hindi', 'T', '39.9');
INSERT INTO `databand_countrylanguage` VALUES ('IND', 'Kannada', 'F', '3.9');
INSERT INTO `databand_countrylanguage` VALUES ('IND', 'Malajalam', 'F', '3.6');
INSERT INTO `databand_countrylanguage` VALUES ('IND', 'Marathi', 'F', '7.4');
INSERT INTO `databand_countrylanguage` VALUES ('IND', 'Orija', 'F', '3.3');
INSERT INTO `databand_countrylanguage` VALUES ('IND', 'Punjabi', 'F', '2.8');
INSERT INTO `databand_countrylanguage` VALUES ('IND', 'Tamil', 'F', '6.3');
INSERT INTO `databand_countrylanguage` VALUES ('IND', 'Telugu', 'F', '7.8');
INSERT INTO `databand_countrylanguage` VALUES ('IND', 'Urdu', 'F', '5.1');
INSERT INTO `databand_countrylanguage` VALUES ('IRL', 'English', 'T', '98.4');
INSERT INTO `databand_countrylanguage` VALUES ('IRL', 'Irish', 'T', '1.6');
INSERT INTO `databand_countrylanguage` VALUES ('IRN', 'Arabic', 'F', '2.2');
INSERT INTO `databand_countrylanguage` VALUES ('IRN', 'Azerbaijani', 'F', '16.8');
INSERT INTO `databand_countrylanguage` VALUES ('IRN', 'Bakhtyari', 'F', '1.7');
INSERT INTO `databand_countrylanguage` VALUES ('IRN', 'Balochi', 'F', '2.3');
INSERT INTO `databand_countrylanguage` VALUES ('IRN', 'Gilaki', 'F', '5.3');
INSERT INTO `databand_countrylanguage` VALUES ('IRN', 'Kurdish', 'F', '9.1');
INSERT INTO `databand_countrylanguage` VALUES ('IRN', 'Luri', 'F', '4.3');
INSERT INTO `databand_countrylanguage` VALUES ('IRN', 'Mazandarani', 'F', '3.6');
INSERT INTO `databand_countrylanguage` VALUES ('IRN', 'Persian', 'T', '45.7');
INSERT INTO `databand_countrylanguage` VALUES ('IRN', 'Turkmenian', 'F', '1.6');
INSERT INTO `databand_countrylanguage` VALUES ('IRQ', 'Arabic', 'T', '77.2');
INSERT INTO `databand_countrylanguage` VALUES ('IRQ', 'Assyrian', 'F', '0.8');
INSERT INTO `databand_countrylanguage` VALUES ('IRQ', 'Azerbaijani', 'F', '1.7');
INSERT INTO `databand_countrylanguage` VALUES ('IRQ', 'Kurdish', 'F', '19.0');
INSERT INTO `databand_countrylanguage` VALUES ('IRQ', 'Persian', 'F', '0.8');
INSERT INTO `databand_countrylanguage` VALUES ('ISL', 'English', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('ISL', 'Icelandic', 'T', '95.7');
INSERT INTO `databand_countrylanguage` VALUES ('ISR', 'Arabic', 'T', '18.0');
INSERT INTO `databand_countrylanguage` VALUES ('ISR', 'Hebrew', 'T', '63.1');
INSERT INTO `databand_countrylanguage` VALUES ('ISR', 'Russian', 'F', '8.9');
INSERT INTO `databand_countrylanguage` VALUES ('ITA', 'Albaniana', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('ITA', 'French', 'F', '0.5');
INSERT INTO `databand_countrylanguage` VALUES ('ITA', 'Friuli', 'F', '1.2');
INSERT INTO `databand_countrylanguage` VALUES ('ITA', 'German', 'F', '0.5');
INSERT INTO `databand_countrylanguage` VALUES ('ITA', 'Italian', 'T', '94.1');
INSERT INTO `databand_countrylanguage` VALUES ('ITA', 'Romani', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('ITA', 'Sardinian', 'F', '2.7');
INSERT INTO `databand_countrylanguage` VALUES ('ITA', 'Slovene', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('JAM', 'Creole English', 'F', '94.2');
INSERT INTO `databand_countrylanguage` VALUES ('JAM', 'Hindi', 'F', '1.9');
INSERT INTO `databand_countrylanguage` VALUES ('JOR', 'Arabic', 'T', '97.9');
INSERT INTO `databand_countrylanguage` VALUES ('JOR', 'Armenian', 'F', '1.0');
INSERT INTO `databand_countrylanguage` VALUES ('JOR', 'Circassian', 'F', '1.0');
INSERT INTO `databand_countrylanguage` VALUES ('JPN', 'Ainu', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('JPN', 'Chinese', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('JPN', 'English', 'F', '0.1');
INSERT INTO `databand_countrylanguage` VALUES ('JPN', 'Japanese', 'T', '99.1');
INSERT INTO `databand_countrylanguage` VALUES ('JPN', 'Korean', 'F', '0.5');
INSERT INTO `databand_countrylanguage` VALUES ('JPN', 'Philippene Languages', 'F', '0.1');
INSERT INTO `databand_countrylanguage` VALUES ('KAZ', 'German', 'F', '3.1');
INSERT INTO `databand_countrylanguage` VALUES ('KAZ', 'Kazakh', 'T', '46.0');
INSERT INTO `databand_countrylanguage` VALUES ('KAZ', 'Russian', 'F', '34.7');
INSERT INTO `databand_countrylanguage` VALUES ('KAZ', 'Tatar', 'F', '2.0');
INSERT INTO `databand_countrylanguage` VALUES ('KAZ', 'Ukrainian', 'F', '5.0');
INSERT INTO `databand_countrylanguage` VALUES ('KAZ', 'Uzbek', 'F', '2.3');
INSERT INTO `databand_countrylanguage` VALUES ('KEN', 'Gusii', 'F', '6.1');
INSERT INTO `databand_countrylanguage` VALUES ('KEN', 'Kalenjin', 'F', '10.8');
INSERT INTO `databand_countrylanguage` VALUES ('KEN', 'Kamba', 'F', '11.2');
INSERT INTO `databand_countrylanguage` VALUES ('KEN', 'Kikuyu', 'F', '20.9');
INSERT INTO `databand_countrylanguage` VALUES ('KEN', 'Luhya', 'F', '13.8');
INSERT INTO `databand_countrylanguage` VALUES ('KEN', 'Luo', 'F', '12.8');
INSERT INTO `databand_countrylanguage` VALUES ('KEN', 'Masai', 'F', '1.6');
INSERT INTO `databand_countrylanguage` VALUES ('KEN', 'Meru', 'F', '5.5');
INSERT INTO `databand_countrylanguage` VALUES ('KEN', 'Nyika', 'F', '4.8');
INSERT INTO `databand_countrylanguage` VALUES ('KEN', 'Turkana', 'F', '1.4');
INSERT INTO `databand_countrylanguage` VALUES ('KGZ', 'Kazakh', 'F', '0.8');
INSERT INTO `databand_countrylanguage` VALUES ('KGZ', 'Kirgiz', 'T', '59.7');
INSERT INTO `databand_countrylanguage` VALUES ('KGZ', 'Russian', 'T', '16.2');
INSERT INTO `databand_countrylanguage` VALUES ('KGZ', 'Tadzhik', 'F', '0.8');
INSERT INTO `databand_countrylanguage` VALUES ('KGZ', 'Tatar', 'F', '1.3');
INSERT INTO `databand_countrylanguage` VALUES ('KGZ', 'Ukrainian', 'F', '1.7');
INSERT INTO `databand_countrylanguage` VALUES ('KGZ', 'Uzbek', 'F', '14.1');
INSERT INTO `databand_countrylanguage` VALUES ('KHM', 'Chinese', 'F', '3.1');
INSERT INTO `databand_countrylanguage` VALUES ('KHM', 'Khmer', 'T', '88.6');
INSERT INTO `databand_countrylanguage` VALUES ('KHM', 'Tšam', 'F', '2.4');
INSERT INTO `databand_countrylanguage` VALUES ('KHM', 'Vietnamese', 'F', '5.5');
INSERT INTO `databand_countrylanguage` VALUES ('KIR', 'Kiribati', 'T', '98.9');
INSERT INTO `databand_countrylanguage` VALUES ('KIR', 'Tuvalu', 'F', '0.5');
INSERT INTO `databand_countrylanguage` VALUES ('KNA', 'Creole English', 'F', '100.0');
INSERT INTO `databand_countrylanguage` VALUES ('KNA', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('KOR', 'Chinese', 'F', '0.1');
INSERT INTO `databand_countrylanguage` VALUES ('KOR', 'Korean', 'T', '99.9');
INSERT INTO `databand_countrylanguage` VALUES ('KWT', 'Arabic', 'T', '78.1');
INSERT INTO `databand_countrylanguage` VALUES ('KWT', 'English', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('LAO', 'Lao', 'T', '67.2');
INSERT INTO `databand_countrylanguage` VALUES ('LAO', 'Lao-Soung', 'F', '5.2');
INSERT INTO `databand_countrylanguage` VALUES ('LAO', 'Mon-khmer', 'F', '16.5');
INSERT INTO `databand_countrylanguage` VALUES ('LAO', 'Thai', 'F', '7.8');
INSERT INTO `databand_countrylanguage` VALUES ('LBN', 'Arabic', 'T', '93.0');
INSERT INTO `databand_countrylanguage` VALUES ('LBN', 'Armenian', 'F', '5.9');
INSERT INTO `databand_countrylanguage` VALUES ('LBN', 'French', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('LBR', 'Bassa', 'F', '13.7');
INSERT INTO `databand_countrylanguage` VALUES ('LBR', 'Gio', 'F', '7.9');
INSERT INTO `databand_countrylanguage` VALUES ('LBR', 'Grebo', 'F', '8.9');
INSERT INTO `databand_countrylanguage` VALUES ('LBR', 'Kpelle', 'F', '19.5');
INSERT INTO `databand_countrylanguage` VALUES ('LBR', 'Kru', 'F', '7.2');
INSERT INTO `databand_countrylanguage` VALUES ('LBR', 'Loma', 'F', '5.8');
INSERT INTO `databand_countrylanguage` VALUES ('LBR', 'Malinke', 'F', '5.1');
INSERT INTO `databand_countrylanguage` VALUES ('LBR', 'Mano', 'F', '7.2');
INSERT INTO `databand_countrylanguage` VALUES ('LBY', 'Arabic', 'T', '96.0');
INSERT INTO `databand_countrylanguage` VALUES ('LBY', 'Berberi', 'F', '1.0');
INSERT INTO `databand_countrylanguage` VALUES ('LCA', 'Creole French', 'F', '80.0');
INSERT INTO `databand_countrylanguage` VALUES ('LCA', 'English', 'T', '20.0');
INSERT INTO `databand_countrylanguage` VALUES ('LIE', 'German', 'T', '89.0');
INSERT INTO `databand_countrylanguage` VALUES ('LIE', 'Italian', 'F', '2.5');
INSERT INTO `databand_countrylanguage` VALUES ('LIE', 'Turkish', 'F', '2.5');
INSERT INTO `databand_countrylanguage` VALUES ('LKA', 'Mixed Languages', 'F', '19.6');
INSERT INTO `databand_countrylanguage` VALUES ('LKA', 'Singali', 'T', '60.3');
INSERT INTO `databand_countrylanguage` VALUES ('LKA', 'Tamil', 'T', '19.6');
INSERT INTO `databand_countrylanguage` VALUES ('LSO', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('LSO', 'Sotho', 'T', '85.0');
INSERT INTO `databand_countrylanguage` VALUES ('LSO', 'Zulu', 'F', '15.0');
INSERT INTO `databand_countrylanguage` VALUES ('LTU', 'Belorussian', 'F', '1.4');
INSERT INTO `databand_countrylanguage` VALUES ('LTU', 'Lithuanian', 'T', '81.6');
INSERT INTO `databand_countrylanguage` VALUES ('LTU', 'Polish', 'F', '7.0');
INSERT INTO `databand_countrylanguage` VALUES ('LTU', 'Russian', 'F', '8.1');
INSERT INTO `databand_countrylanguage` VALUES ('LTU', 'Ukrainian', 'F', '1.1');
INSERT INTO `databand_countrylanguage` VALUES ('LUX', 'French', 'T', '4.2');
INSERT INTO `databand_countrylanguage` VALUES ('LUX', 'German', 'T', '2.3');
INSERT INTO `databand_countrylanguage` VALUES ('LUX', 'Italian', 'F', '4.6');
INSERT INTO `databand_countrylanguage` VALUES ('LUX', 'Luxembourgish', 'T', '64.4');
INSERT INTO `databand_countrylanguage` VALUES ('LUX', 'Portuguese', 'F', '13.0');
INSERT INTO `databand_countrylanguage` VALUES ('LVA', 'Belorussian', 'F', '4.1');
INSERT INTO `databand_countrylanguage` VALUES ('LVA', 'Latvian', 'T', '55.1');
INSERT INTO `databand_countrylanguage` VALUES ('LVA', 'Lithuanian', 'F', '1.2');
INSERT INTO `databand_countrylanguage` VALUES ('LVA', 'Polish', 'F', '2.1');
INSERT INTO `databand_countrylanguage` VALUES ('LVA', 'Russian', 'F', '32.5');
INSERT INTO `databand_countrylanguage` VALUES ('LVA', 'Ukrainian', 'F', '2.9');
INSERT INTO `databand_countrylanguage` VALUES ('MAC', 'Canton Chinese', 'F', '85.6');
INSERT INTO `databand_countrylanguage` VALUES ('MAC', 'English', 'F', '0.5');
INSERT INTO `databand_countrylanguage` VALUES ('MAC', 'Mandarin Chinese', 'F', '1.2');
INSERT INTO `databand_countrylanguage` VALUES ('MAC', 'Portuguese', 'T', '2.3');
INSERT INTO `databand_countrylanguage` VALUES ('MAR', 'Arabic', 'T', '65.0');
INSERT INTO `databand_countrylanguage` VALUES ('MAR', 'Berberi', 'F', '33.0');
INSERT INTO `databand_countrylanguage` VALUES ('MCO', 'English', 'F', '6.5');
INSERT INTO `databand_countrylanguage` VALUES ('MCO', 'French', 'T', '41.9');
INSERT INTO `databand_countrylanguage` VALUES ('MCO', 'Italian', 'F', '16.1');
INSERT INTO `databand_countrylanguage` VALUES ('MCO', 'Monegasque', 'F', '16.1');
INSERT INTO `databand_countrylanguage` VALUES ('MDA', 'Bulgariana', 'F', '1.6');
INSERT INTO `databand_countrylanguage` VALUES ('MDA', 'Gagauzi', 'F', '3.2');
INSERT INTO `databand_countrylanguage` VALUES ('MDA', 'Romanian', 'T', '61.9');
INSERT INTO `databand_countrylanguage` VALUES ('MDA', 'Russian', 'F', '23.2');
INSERT INTO `databand_countrylanguage` VALUES ('MDA', 'Ukrainian', 'F', '8.6');
INSERT INTO `databand_countrylanguage` VALUES ('MDG', 'French', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('MDG', 'Malagasy', 'T', '98.9');
INSERT INTO `databand_countrylanguage` VALUES ('MDV', 'Dhivehi', 'T', '100.0');
INSERT INTO `databand_countrylanguage` VALUES ('MDV', 'English', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('MEX', 'Mixtec', 'F', '0.6');
INSERT INTO `databand_countrylanguage` VALUES ('MEX', 'Náhuatl', 'F', '1.8');
INSERT INTO `databand_countrylanguage` VALUES ('MEX', 'Otomí', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('MEX', 'Spanish', 'T', '92.1');
INSERT INTO `databand_countrylanguage` VALUES ('MEX', 'Yucatec', 'F', '1.1');
INSERT INTO `databand_countrylanguage` VALUES ('MEX', 'Zapotec', 'F', '0.6');
INSERT INTO `databand_countrylanguage` VALUES ('MHL', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('MHL', 'Marshallese', 'T', '96.8');
INSERT INTO `databand_countrylanguage` VALUES ('MKD', 'Albaniana', 'F', '22.9');
INSERT INTO `databand_countrylanguage` VALUES ('MKD', 'Macedonian', 'T', '66.5');
INSERT INTO `databand_countrylanguage` VALUES ('MKD', 'Romani', 'F', '2.3');
INSERT INTO `databand_countrylanguage` VALUES ('MKD', 'Serbo-Croatian', 'F', '2.0');
INSERT INTO `databand_countrylanguage` VALUES ('MKD', 'Turkish', 'F', '4.0');
INSERT INTO `databand_countrylanguage` VALUES ('MLI', 'Bambara', 'F', '31.8');
INSERT INTO `databand_countrylanguage` VALUES ('MLI', 'Ful', 'F', '13.9');
INSERT INTO `databand_countrylanguage` VALUES ('MLI', 'Senufo and Minianka', 'F', '12.0');
INSERT INTO `databand_countrylanguage` VALUES ('MLI', 'Songhai', 'F', '6.9');
INSERT INTO `databand_countrylanguage` VALUES ('MLI', 'Soninke', 'F', '8.7');
INSERT INTO `databand_countrylanguage` VALUES ('MLI', 'Tamashek', 'F', '7.3');
INSERT INTO `databand_countrylanguage` VALUES ('MLT', 'English', 'T', '2.1');
INSERT INTO `databand_countrylanguage` VALUES ('MLT', 'Maltese', 'T', '95.8');
INSERT INTO `databand_countrylanguage` VALUES ('MMR', 'Burmese', 'T', '69.0');
INSERT INTO `databand_countrylanguage` VALUES ('MMR', 'Chin', 'F', '2.2');
INSERT INTO `databand_countrylanguage` VALUES ('MMR', 'Kachin', 'F', '1.4');
INSERT INTO `databand_countrylanguage` VALUES ('MMR', 'Karen', 'F', '6.2');
INSERT INTO `databand_countrylanguage` VALUES ('MMR', 'Kayah', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('MMR', 'Mon', 'F', '2.4');
INSERT INTO `databand_countrylanguage` VALUES ('MMR', 'Rakhine', 'F', '4.5');
INSERT INTO `databand_countrylanguage` VALUES ('MMR', 'Shan', 'F', '8.5');
INSERT INTO `databand_countrylanguage` VALUES ('MNG', 'Bajad', 'F', '1.9');
INSERT INTO `databand_countrylanguage` VALUES ('MNG', 'Buryat', 'F', '1.7');
INSERT INTO `databand_countrylanguage` VALUES ('MNG', 'Dariganga', 'F', '1.4');
INSERT INTO `databand_countrylanguage` VALUES ('MNG', 'Dorbet', 'F', '2.7');
INSERT INTO `databand_countrylanguage` VALUES ('MNG', 'Kazakh', 'F', '5.9');
INSERT INTO `databand_countrylanguage` VALUES ('MNG', 'Mongolian', 'T', '78.8');
INSERT INTO `databand_countrylanguage` VALUES ('MNP', 'Carolinian', 'F', '4.8');
INSERT INTO `databand_countrylanguage` VALUES ('MNP', 'Chamorro', 'F', '30.0');
INSERT INTO `databand_countrylanguage` VALUES ('MNP', 'Chinese', 'F', '7.1');
INSERT INTO `databand_countrylanguage` VALUES ('MNP', 'English', 'T', '4.8');
INSERT INTO `databand_countrylanguage` VALUES ('MNP', 'Korean', 'F', '6.5');
INSERT INTO `databand_countrylanguage` VALUES ('MNP', 'Philippene Languages', 'F', '34.1');
INSERT INTO `databand_countrylanguage` VALUES ('MOZ', 'Chuabo', 'F', '5.7');
INSERT INTO `databand_countrylanguage` VALUES ('MOZ', 'Lomwe', 'F', '7.8');
INSERT INTO `databand_countrylanguage` VALUES ('MOZ', 'Makua', 'F', '27.8');
INSERT INTO `databand_countrylanguage` VALUES ('MOZ', 'Marendje', 'F', '3.5');
INSERT INTO `databand_countrylanguage` VALUES ('MOZ', 'Nyanja', 'F', '3.3');
INSERT INTO `databand_countrylanguage` VALUES ('MOZ', 'Ronga', 'F', '3.7');
INSERT INTO `databand_countrylanguage` VALUES ('MOZ', 'Sena', 'F', '9.4');
INSERT INTO `databand_countrylanguage` VALUES ('MOZ', 'Shona', 'F', '6.5');
INSERT INTO `databand_countrylanguage` VALUES ('MOZ', 'Tsonga', 'F', '12.4');
INSERT INTO `databand_countrylanguage` VALUES ('MOZ', 'Tswa', 'F', '6.0');
INSERT INTO `databand_countrylanguage` VALUES ('MRT', 'Ful', 'F', '1.2');
INSERT INTO `databand_countrylanguage` VALUES ('MRT', 'Hassaniya', 'F', '81.7');
INSERT INTO `databand_countrylanguage` VALUES ('MRT', 'Soninke', 'F', '2.7');
INSERT INTO `databand_countrylanguage` VALUES ('MRT', 'Tukulor', 'F', '5.4');
INSERT INTO `databand_countrylanguage` VALUES ('MRT', 'Wolof', 'F', '6.6');
INSERT INTO `databand_countrylanguage` VALUES ('MRT', 'Zenaga', 'F', '1.2');
INSERT INTO `databand_countrylanguage` VALUES ('MSR', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('MTQ', 'Creole French', 'F', '96.6');
INSERT INTO `databand_countrylanguage` VALUES ('MTQ', 'French', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('MUS', 'Bhojpuri', 'F', '21.1');
INSERT INTO `databand_countrylanguage` VALUES ('MUS', 'Creole French', 'F', '70.6');
INSERT INTO `databand_countrylanguage` VALUES ('MUS', 'French', 'F', '3.4');
INSERT INTO `databand_countrylanguage` VALUES ('MUS', 'Hindi', 'F', '1.2');
INSERT INTO `databand_countrylanguage` VALUES ('MUS', 'Marathi', 'F', '0.7');
INSERT INTO `databand_countrylanguage` VALUES ('MUS', 'Tamil', 'F', '0.8');
INSERT INTO `databand_countrylanguage` VALUES ('MWI', 'Chichewa', 'T', '58.3');
INSERT INTO `databand_countrylanguage` VALUES ('MWI', 'Lomwe', 'F', '18.4');
INSERT INTO `databand_countrylanguage` VALUES ('MWI', 'Ngoni', 'F', '6.7');
INSERT INTO `databand_countrylanguage` VALUES ('MWI', 'Yao', 'F', '13.2');
INSERT INTO `databand_countrylanguage` VALUES ('MYS', 'Chinese', 'F', '9.0');
INSERT INTO `databand_countrylanguage` VALUES ('MYS', 'Dusun', 'F', '1.1');
INSERT INTO `databand_countrylanguage` VALUES ('MYS', 'English', 'F', '1.6');
INSERT INTO `databand_countrylanguage` VALUES ('MYS', 'Iban', 'F', '2.8');
INSERT INTO `databand_countrylanguage` VALUES ('MYS', 'Malay', 'T', '58.4');
INSERT INTO `databand_countrylanguage` VALUES ('MYS', 'Tamil', 'F', '3.9');
INSERT INTO `databand_countrylanguage` VALUES ('MYT', 'French', 'T', '20.3');
INSERT INTO `databand_countrylanguage` VALUES ('MYT', 'Mahoré', 'F', '41.9');
INSERT INTO `databand_countrylanguage` VALUES ('MYT', 'Malagasy', 'F', '16.1');
INSERT INTO `databand_countrylanguage` VALUES ('NAM', 'Afrikaans', 'F', '9.5');
INSERT INTO `databand_countrylanguage` VALUES ('NAM', 'Caprivi', 'F', '4.7');
INSERT INTO `databand_countrylanguage` VALUES ('NAM', 'German', 'F', '0.9');
INSERT INTO `databand_countrylanguage` VALUES ('NAM', 'Herero', 'F', '8.0');
INSERT INTO `databand_countrylanguage` VALUES ('NAM', 'Kavango', 'F', '9.7');
INSERT INTO `databand_countrylanguage` VALUES ('NAM', 'Nama', 'F', '12.4');
INSERT INTO `databand_countrylanguage` VALUES ('NAM', 'Ovambo', 'F', '50.7');
INSERT INTO `databand_countrylanguage` VALUES ('NAM', 'San', 'F', '1.9');
INSERT INTO `databand_countrylanguage` VALUES ('NCL', 'French', 'T', '34.3');
INSERT INTO `databand_countrylanguage` VALUES ('NCL', 'Malenasian Languages', 'F', '45.4');
INSERT INTO `databand_countrylanguage` VALUES ('NCL', 'Polynesian Languages', 'F', '11.6');
INSERT INTO `databand_countrylanguage` VALUES ('NER', 'Ful', 'F', '9.7');
INSERT INTO `databand_countrylanguage` VALUES ('NER', 'Hausa', 'F', '53.1');
INSERT INTO `databand_countrylanguage` VALUES ('NER', 'Kanuri', 'F', '4.4');
INSERT INTO `databand_countrylanguage` VALUES ('NER', 'Songhai-zerma', 'F', '21.2');
INSERT INTO `databand_countrylanguage` VALUES ('NER', 'Tamashek', 'F', '10.4');
INSERT INTO `databand_countrylanguage` VALUES ('NFK', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('NGA', 'Bura', 'F', '1.6');
INSERT INTO `databand_countrylanguage` VALUES ('NGA', 'Edo', 'F', '3.3');
INSERT INTO `databand_countrylanguage` VALUES ('NGA', 'Ful', 'F', '11.3');
INSERT INTO `databand_countrylanguage` VALUES ('NGA', 'Hausa', 'F', '21.1');
INSERT INTO `databand_countrylanguage` VALUES ('NGA', 'Ibibio', 'F', '5.6');
INSERT INTO `databand_countrylanguage` VALUES ('NGA', 'Ibo', 'F', '18.1');
INSERT INTO `databand_countrylanguage` VALUES ('NGA', 'Ijo', 'F', '1.8');
INSERT INTO `databand_countrylanguage` VALUES ('NGA', 'Joruba', 'F', '21.4');
INSERT INTO `databand_countrylanguage` VALUES ('NGA', 'Kanuri', 'F', '4.1');
INSERT INTO `databand_countrylanguage` VALUES ('NGA', 'Tiv', 'F', '2.3');
INSERT INTO `databand_countrylanguage` VALUES ('NIC', 'Creole English', 'F', '0.5');
INSERT INTO `databand_countrylanguage` VALUES ('NIC', 'Miskito', 'F', '1.6');
INSERT INTO `databand_countrylanguage` VALUES ('NIC', 'Spanish', 'T', '97.6');
INSERT INTO `databand_countrylanguage` VALUES ('NIC', 'Sumo', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('NIU', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('NIU', 'Niue', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('NLD', 'Arabic', 'F', '0.9');
INSERT INTO `databand_countrylanguage` VALUES ('NLD', 'Dutch', 'T', '95.6');
INSERT INTO `databand_countrylanguage` VALUES ('NLD', 'Fries', 'F', '3.7');
INSERT INTO `databand_countrylanguage` VALUES ('NLD', 'Turkish', 'F', '0.8');
INSERT INTO `databand_countrylanguage` VALUES ('NOR', 'Danish', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('NOR', 'English', 'F', '0.5');
INSERT INTO `databand_countrylanguage` VALUES ('NOR', 'Norwegian', 'T', '96.6');
INSERT INTO `databand_countrylanguage` VALUES ('NOR', 'Saame', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('NOR', 'Swedish', 'F', '0.3');
INSERT INTO `databand_countrylanguage` VALUES ('NPL', 'Bhojpuri', 'F', '7.5');
INSERT INTO `databand_countrylanguage` VALUES ('NPL', 'Hindi', 'F', '3.0');
INSERT INTO `databand_countrylanguage` VALUES ('NPL', 'Maithili', 'F', '11.9');
INSERT INTO `databand_countrylanguage` VALUES ('NPL', 'Nepali', 'T', '50.4');
INSERT INTO `databand_countrylanguage` VALUES ('NPL', 'Newari', 'F', '3.7');
INSERT INTO `databand_countrylanguage` VALUES ('NPL', 'Tamang', 'F', '4.9');
INSERT INTO `databand_countrylanguage` VALUES ('NPL', 'Tharu', 'F', '5.4');
INSERT INTO `databand_countrylanguage` VALUES ('NRU', 'Chinese', 'F', '8.5');
INSERT INTO `databand_countrylanguage` VALUES ('NRU', 'English', 'T', '7.5');
INSERT INTO `databand_countrylanguage` VALUES ('NRU', 'Kiribati', 'F', '17.9');
INSERT INTO `databand_countrylanguage` VALUES ('NRU', 'Nauru', 'T', '57.5');
INSERT INTO `databand_countrylanguage` VALUES ('NRU', 'Tuvalu', 'F', '8.5');
INSERT INTO `databand_countrylanguage` VALUES ('NZL', 'English', 'T', '87.0');
INSERT INTO `databand_countrylanguage` VALUES ('NZL', 'Maori', 'F', '4.3');
INSERT INTO `databand_countrylanguage` VALUES ('OMN', 'Arabic', 'T', '76.7');
INSERT INTO `databand_countrylanguage` VALUES ('OMN', 'Balochi', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('PAK', 'Balochi', 'F', '3.0');
INSERT INTO `databand_countrylanguage` VALUES ('PAK', 'Brahui', 'F', '1.2');
INSERT INTO `databand_countrylanguage` VALUES ('PAK', 'Hindko', 'F', '2.4');
INSERT INTO `databand_countrylanguage` VALUES ('PAK', 'Pashto', 'F', '13.1');
INSERT INTO `databand_countrylanguage` VALUES ('PAK', 'Punjabi', 'F', '48.2');
INSERT INTO `databand_countrylanguage` VALUES ('PAK', 'Saraiki', 'F', '9.8');
INSERT INTO `databand_countrylanguage` VALUES ('PAK', 'Sindhi', 'F', '11.8');
INSERT INTO `databand_countrylanguage` VALUES ('PAK', 'Urdu', 'T', '7.6');
INSERT INTO `databand_countrylanguage` VALUES ('PAN', 'Arabic', 'F', '0.6');
INSERT INTO `databand_countrylanguage` VALUES ('PAN', 'Creole English', 'F', '14.0');
INSERT INTO `databand_countrylanguage` VALUES ('PAN', 'Cuna', 'F', '2.0');
INSERT INTO `databand_countrylanguage` VALUES ('PAN', 'Embera', 'F', '0.6');
INSERT INTO `databand_countrylanguage` VALUES ('PAN', 'Guaymí', 'F', '5.3');
INSERT INTO `databand_countrylanguage` VALUES ('PAN', 'Spanish', 'T', '76.8');
INSERT INTO `databand_countrylanguage` VALUES ('PCN', 'Pitcairnese', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('PER', 'Aimará', 'T', '2.3');
INSERT INTO `databand_countrylanguage` VALUES ('PER', 'Ketšua', 'T', '16.4');
INSERT INTO `databand_countrylanguage` VALUES ('PER', 'Spanish', 'T', '79.8');
INSERT INTO `databand_countrylanguage` VALUES ('PHL', 'Bicol', 'F', '5.7');
INSERT INTO `databand_countrylanguage` VALUES ('PHL', 'Cebuano', 'F', '23.3');
INSERT INTO `databand_countrylanguage` VALUES ('PHL', 'Hiligaynon', 'F', '9.1');
INSERT INTO `databand_countrylanguage` VALUES ('PHL', 'Ilocano', 'F', '9.3');
INSERT INTO `databand_countrylanguage` VALUES ('PHL', 'Maguindanao', 'F', '1.4');
INSERT INTO `databand_countrylanguage` VALUES ('PHL', 'Maranao', 'F', '1.3');
INSERT INTO `databand_countrylanguage` VALUES ('PHL', 'Pampango', 'F', '3.0');
INSERT INTO `databand_countrylanguage` VALUES ('PHL', 'Pangasinan', 'F', '1.8');
INSERT INTO `databand_countrylanguage` VALUES ('PHL', 'Pilipino', 'T', '29.3');
INSERT INTO `databand_countrylanguage` VALUES ('PHL', 'Waray-waray', 'F', '3.8');
INSERT INTO `databand_countrylanguage` VALUES ('PLW', 'Chinese', 'F', '1.6');
INSERT INTO `databand_countrylanguage` VALUES ('PLW', 'English', 'T', '3.2');
INSERT INTO `databand_countrylanguage` VALUES ('PLW', 'Palau', 'T', '82.2');
INSERT INTO `databand_countrylanguage` VALUES ('PLW', 'Philippene Languages', 'F', '9.2');
INSERT INTO `databand_countrylanguage` VALUES ('PNG', 'Malenasian Languages', 'F', '20.0');
INSERT INTO `databand_countrylanguage` VALUES ('PNG', 'Papuan Languages', 'F', '78.1');
INSERT INTO `databand_countrylanguage` VALUES ('POL', 'Belorussian', 'F', '0.5');
INSERT INTO `databand_countrylanguage` VALUES ('POL', 'German', 'F', '1.3');
INSERT INTO `databand_countrylanguage` VALUES ('POL', 'Polish', 'T', '97.6');
INSERT INTO `databand_countrylanguage` VALUES ('POL', 'Ukrainian', 'F', '0.6');
INSERT INTO `databand_countrylanguage` VALUES ('PRI', 'English', 'F', '47.4');
INSERT INTO `databand_countrylanguage` VALUES ('PRI', 'Spanish', 'T', '51.3');
INSERT INTO `databand_countrylanguage` VALUES ('PRK', 'Chinese', 'F', '0.1');
INSERT INTO `databand_countrylanguage` VALUES ('PRK', 'Korean', 'T', '99.9');
INSERT INTO `databand_countrylanguage` VALUES ('PRT', 'Portuguese', 'T', '99.0');
INSERT INTO `databand_countrylanguage` VALUES ('PRY', 'German', 'F', '0.9');
INSERT INTO `databand_countrylanguage` VALUES ('PRY', 'Guaraní', 'T', '40.1');
INSERT INTO `databand_countrylanguage` VALUES ('PRY', 'Portuguese', 'F', '3.2');
INSERT INTO `databand_countrylanguage` VALUES ('PRY', 'Spanish', 'T', '55.1');
INSERT INTO `databand_countrylanguage` VALUES ('PSE', 'Arabic', 'F', '95.9');
INSERT INTO `databand_countrylanguage` VALUES ('PSE', 'Hebrew', 'F', '4.1');
INSERT INTO `databand_countrylanguage` VALUES ('PYF', 'Chinese', 'F', '2.9');
INSERT INTO `databand_countrylanguage` VALUES ('PYF', 'French', 'T', '40.8');
INSERT INTO `databand_countrylanguage` VALUES ('PYF', 'Tahitian', 'F', '46.4');
INSERT INTO `databand_countrylanguage` VALUES ('QAT', 'Arabic', 'T', '40.7');
INSERT INTO `databand_countrylanguage` VALUES ('QAT', 'Urdu', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('REU', 'Chinese', 'F', '2.8');
INSERT INTO `databand_countrylanguage` VALUES ('REU', 'Comorian', 'F', '2.8');
INSERT INTO `databand_countrylanguage` VALUES ('REU', 'Creole French', 'F', '91.5');
INSERT INTO `databand_countrylanguage` VALUES ('REU', 'Malagasy', 'F', '1.4');
INSERT INTO `databand_countrylanguage` VALUES ('REU', 'Tamil', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('ROM', 'German', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('ROM', 'Hungarian', 'F', '7.2');
INSERT INTO `databand_countrylanguage` VALUES ('ROM', 'Romani', 'T', '0.7');
INSERT INTO `databand_countrylanguage` VALUES ('ROM', 'Romanian', 'T', '90.7');
INSERT INTO `databand_countrylanguage` VALUES ('ROM', 'Serbo-Croatian', 'F', '0.1');
INSERT INTO `databand_countrylanguage` VALUES ('ROM', 'Ukrainian', 'F', '0.3');
INSERT INTO `databand_countrylanguage` VALUES ('RUS', 'Avarian', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('RUS', 'Bashkir', 'F', '0.7');
INSERT INTO `databand_countrylanguage` VALUES ('RUS', 'Belorussian', 'F', '0.3');
INSERT INTO `databand_countrylanguage` VALUES ('RUS', 'Chechen', 'F', '0.6');
INSERT INTO `databand_countrylanguage` VALUES ('RUS', 'Chuvash', 'F', '0.9');
INSERT INTO `databand_countrylanguage` VALUES ('RUS', 'Kazakh', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('RUS', 'Mari', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('RUS', 'Mordva', 'F', '0.5');
INSERT INTO `databand_countrylanguage` VALUES ('RUS', 'Russian', 'T', '86.6');
INSERT INTO `databand_countrylanguage` VALUES ('RUS', 'Tatar', 'F', '3.2');
INSERT INTO `databand_countrylanguage` VALUES ('RUS', 'Udmur', 'F', '0.3');
INSERT INTO `databand_countrylanguage` VALUES ('RUS', 'Ukrainian', 'F', '1.3');
INSERT INTO `databand_countrylanguage` VALUES ('RWA', 'French', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('RWA', 'Rwanda', 'T', '100.0');
INSERT INTO `databand_countrylanguage` VALUES ('SAU', 'Arabic', 'T', '95.0');
INSERT INTO `databand_countrylanguage` VALUES ('SDN', 'Arabic', 'T', '49.4');
INSERT INTO `databand_countrylanguage` VALUES ('SDN', 'Bari', 'F', '2.5');
INSERT INTO `databand_countrylanguage` VALUES ('SDN', 'Beja', 'F', '6.4');
INSERT INTO `databand_countrylanguage` VALUES ('SDN', 'Chilluk', 'F', '1.7');
INSERT INTO `databand_countrylanguage` VALUES ('SDN', 'Dinka', 'F', '11.5');
INSERT INTO `databand_countrylanguage` VALUES ('SDN', 'Fur', 'F', '2.1');
INSERT INTO `databand_countrylanguage` VALUES ('SDN', 'Lotuko', 'F', '1.5');
INSERT INTO `databand_countrylanguage` VALUES ('SDN', 'Nubian Languages', 'F', '8.1');
INSERT INTO `databand_countrylanguage` VALUES ('SDN', 'Nuer', 'F', '4.9');
INSERT INTO `databand_countrylanguage` VALUES ('SDN', 'Zande', 'F', '2.7');
INSERT INTO `databand_countrylanguage` VALUES ('SEN', 'Diola', 'F', '5.0');
INSERT INTO `databand_countrylanguage` VALUES ('SEN', 'Ful', 'F', '21.7');
INSERT INTO `databand_countrylanguage` VALUES ('SEN', 'Malinke', 'F', '3.8');
INSERT INTO `databand_countrylanguage` VALUES ('SEN', 'Serer', 'F', '12.5');
INSERT INTO `databand_countrylanguage` VALUES ('SEN', 'Soninke', 'F', '1.3');
INSERT INTO `databand_countrylanguage` VALUES ('SEN', 'Wolof', 'T', '48.1');
INSERT INTO `databand_countrylanguage` VALUES ('SGP', 'Chinese', 'T', '77.1');
INSERT INTO `databand_countrylanguage` VALUES ('SGP', 'Malay', 'T', '14.1');
INSERT INTO `databand_countrylanguage` VALUES ('SGP', 'Tamil', 'T', '7.4');
INSERT INTO `databand_countrylanguage` VALUES ('SHN', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('SJM', 'Norwegian', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('SJM', 'Russian', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('SLB', 'Malenasian Languages', 'F', '85.6');
INSERT INTO `databand_countrylanguage` VALUES ('SLB', 'Papuan Languages', 'F', '8.6');
INSERT INTO `databand_countrylanguage` VALUES ('SLB', 'Polynesian Languages', 'F', '3.8');
INSERT INTO `databand_countrylanguage` VALUES ('SLE', 'Bullom-sherbro', 'F', '3.8');
INSERT INTO `databand_countrylanguage` VALUES ('SLE', 'Ful', 'F', '3.8');
INSERT INTO `databand_countrylanguage` VALUES ('SLE', 'Kono-vai', 'F', '5.1');
INSERT INTO `databand_countrylanguage` VALUES ('SLE', 'Kuranko', 'F', '3.4');
INSERT INTO `databand_countrylanguage` VALUES ('SLE', 'Limba', 'F', '8.3');
INSERT INTO `databand_countrylanguage` VALUES ('SLE', 'Mende', 'F', '34.8');
INSERT INTO `databand_countrylanguage` VALUES ('SLE', 'Temne', 'F', '31.8');
INSERT INTO `databand_countrylanguage` VALUES ('SLE', 'Yalunka', 'F', '3.4');
INSERT INTO `databand_countrylanguage` VALUES ('SLV', 'Nahua', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('SLV', 'Spanish', 'T', '100.0');
INSERT INTO `databand_countrylanguage` VALUES ('SMR', 'Italian', 'T', '100.0');
INSERT INTO `databand_countrylanguage` VALUES ('SOM', 'Arabic', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('SOM', 'Somali', 'T', '98.3');
INSERT INTO `databand_countrylanguage` VALUES ('SPM', 'French', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('STP', 'Crioulo', 'F', '86.3');
INSERT INTO `databand_countrylanguage` VALUES ('STP', 'French', 'F', '0.7');
INSERT INTO `databand_countrylanguage` VALUES ('SUR', 'Hindi', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('SUR', 'Sranantonga', 'F', '81.0');
INSERT INTO `databand_countrylanguage` VALUES ('SVK', 'Czech and Moravian', 'F', '1.1');
INSERT INTO `databand_countrylanguage` VALUES ('SVK', 'Hungarian', 'F', '10.5');
INSERT INTO `databand_countrylanguage` VALUES ('SVK', 'Romani', 'F', '1.7');
INSERT INTO `databand_countrylanguage` VALUES ('SVK', 'Slovak', 'T', '85.6');
INSERT INTO `databand_countrylanguage` VALUES ('SVK', 'Ukrainian and Russian', 'F', '0.6');
INSERT INTO `databand_countrylanguage` VALUES ('SVN', 'Hungarian', 'F', '0.5');
INSERT INTO `databand_countrylanguage` VALUES ('SVN', 'Serbo-Croatian', 'F', '7.9');
INSERT INTO `databand_countrylanguage` VALUES ('SVN', 'Slovene', 'T', '87.9');
INSERT INTO `databand_countrylanguage` VALUES ('SWE', 'Arabic', 'F', '0.8');
INSERT INTO `databand_countrylanguage` VALUES ('SWE', 'Finnish', 'F', '2.4');
INSERT INTO `databand_countrylanguage` VALUES ('SWE', 'Norwegian', 'F', '0.5');
INSERT INTO `databand_countrylanguage` VALUES ('SWE', 'Southern Slavic Languages', 'F', '1.3');
INSERT INTO `databand_countrylanguage` VALUES ('SWE', 'Spanish', 'F', '0.6');
INSERT INTO `databand_countrylanguage` VALUES ('SWE', 'Swedish', 'T', '89.5');
INSERT INTO `databand_countrylanguage` VALUES ('SWZ', 'Swazi', 'T', '89.9');
INSERT INTO `databand_countrylanguage` VALUES ('SWZ', 'Zulu', 'F', '2.0');
INSERT INTO `databand_countrylanguage` VALUES ('SYC', 'English', 'T', '3.8');
INSERT INTO `databand_countrylanguage` VALUES ('SYC', 'French', 'T', '1.3');
INSERT INTO `databand_countrylanguage` VALUES ('SYC', 'Seselwa', 'F', '91.3');
INSERT INTO `databand_countrylanguage` VALUES ('SYR', 'Arabic', 'T', '90.0');
INSERT INTO `databand_countrylanguage` VALUES ('SYR', 'Kurdish', 'F', '9.0');
INSERT INTO `databand_countrylanguage` VALUES ('TCA', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('TCD', 'Arabic', 'T', '12.3');
INSERT INTO `databand_countrylanguage` VALUES ('TCD', 'Gorane', 'F', '6.2');
INSERT INTO `databand_countrylanguage` VALUES ('TCD', 'Hadjarai', 'F', '6.7');
INSERT INTO `databand_countrylanguage` VALUES ('TCD', 'Kanem-bornu', 'F', '9.0');
INSERT INTO `databand_countrylanguage` VALUES ('TCD', 'Mayo-kebbi', 'F', '11.5');
INSERT INTO `databand_countrylanguage` VALUES ('TCD', 'Ouaddai', 'F', '8.7');
INSERT INTO `databand_countrylanguage` VALUES ('TCD', 'Sara', 'F', '27.7');
INSERT INTO `databand_countrylanguage` VALUES ('TCD', 'Tandjile', 'F', '6.5');
INSERT INTO `databand_countrylanguage` VALUES ('TGO', 'Ane', 'F', '5.7');
INSERT INTO `databand_countrylanguage` VALUES ('TGO', 'Ewe', 'T', '23.2');
INSERT INTO `databand_countrylanguage` VALUES ('TGO', 'Gurma', 'F', '3.4');
INSERT INTO `databand_countrylanguage` VALUES ('TGO', 'Kabyé', 'T', '13.8');
INSERT INTO `databand_countrylanguage` VALUES ('TGO', 'Kotokoli', 'F', '5.7');
INSERT INTO `databand_countrylanguage` VALUES ('TGO', 'Moba', 'F', '5.4');
INSERT INTO `databand_countrylanguage` VALUES ('TGO', 'Naudemba', 'F', '4.1');
INSERT INTO `databand_countrylanguage` VALUES ('TGO', 'Watyi', 'F', '10.3');
INSERT INTO `databand_countrylanguage` VALUES ('THA', 'Chinese', 'F', '12.1');
INSERT INTO `databand_countrylanguage` VALUES ('THA', 'Khmer', 'F', '1.3');
INSERT INTO `databand_countrylanguage` VALUES ('THA', 'Kuy', 'F', '1.1');
INSERT INTO `databand_countrylanguage` VALUES ('THA', 'Lao', 'F', '26.9');
INSERT INTO `databand_countrylanguage` VALUES ('THA', 'Malay', 'F', '3.6');
INSERT INTO `databand_countrylanguage` VALUES ('THA', 'Thai', 'T', '52.6');
INSERT INTO `databand_countrylanguage` VALUES ('TJK', 'Russian', 'F', '9.7');
INSERT INTO `databand_countrylanguage` VALUES ('TJK', 'Tadzhik', 'T', '62.2');
INSERT INTO `databand_countrylanguage` VALUES ('TJK', 'Uzbek', 'F', '23.2');
INSERT INTO `databand_countrylanguage` VALUES ('TKL', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('TKL', 'Tokelau', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('TKM', 'Kazakh', 'F', '2.0');
INSERT INTO `databand_countrylanguage` VALUES ('TKM', 'Russian', 'F', '6.7');
INSERT INTO `databand_countrylanguage` VALUES ('TKM', 'Turkmenian', 'T', '76.7');
INSERT INTO `databand_countrylanguage` VALUES ('TKM', 'Uzbek', 'F', '9.2');
INSERT INTO `databand_countrylanguage` VALUES ('TMP', 'Portuguese', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('TMP', 'Sunda', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('TON', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('TON', 'Tongan', 'T', '98.3');
INSERT INTO `databand_countrylanguage` VALUES ('TTO', 'Creole English', 'F', '2.9');
INSERT INTO `databand_countrylanguage` VALUES ('TTO', 'English', 'F', '93.5');
INSERT INTO `databand_countrylanguage` VALUES ('TTO', 'Hindi', 'F', '3.4');
INSERT INTO `databand_countrylanguage` VALUES ('TUN', 'Arabic', 'T', '69.9');
INSERT INTO `databand_countrylanguage` VALUES ('TUN', 'Arabic-French', 'F', '26.3');
INSERT INTO `databand_countrylanguage` VALUES ('TUN', 'Arabic-French-English', 'F', '3.2');
INSERT INTO `databand_countrylanguage` VALUES ('TUR', 'Arabic', 'F', '1.4');
INSERT INTO `databand_countrylanguage` VALUES ('TUR', 'Kurdish', 'F', '10.6');
INSERT INTO `databand_countrylanguage` VALUES ('TUR', 'Turkish', 'T', '87.6');
INSERT INTO `databand_countrylanguage` VALUES ('TUV', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('TUV', 'Kiribati', 'F', '7.5');
INSERT INTO `databand_countrylanguage` VALUES ('TUV', 'Tuvalu', 'T', '92.5');
INSERT INTO `databand_countrylanguage` VALUES ('TWN', 'Ami', 'F', '0.6');
INSERT INTO `databand_countrylanguage` VALUES ('TWN', 'Atayal', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('TWN', 'Hakka', 'F', '11.0');
INSERT INTO `databand_countrylanguage` VALUES ('TWN', 'Mandarin Chinese', 'T', '20.1');
INSERT INTO `databand_countrylanguage` VALUES ('TWN', 'Min', 'F', '66.7');
INSERT INTO `databand_countrylanguage` VALUES ('TWN', 'Paiwan', 'F', '0.3');
INSERT INTO `databand_countrylanguage` VALUES ('TZA', 'Chaga and Pare', 'F', '4.9');
INSERT INTO `databand_countrylanguage` VALUES ('TZA', 'Gogo', 'F', '3.9');
INSERT INTO `databand_countrylanguage` VALUES ('TZA', 'Ha', 'F', '3.5');
INSERT INTO `databand_countrylanguage` VALUES ('TZA', 'Haya', 'F', '5.9');
INSERT INTO `databand_countrylanguage` VALUES ('TZA', 'Hehet', 'F', '6.9');
INSERT INTO `databand_countrylanguage` VALUES ('TZA', 'Luguru', 'F', '4.9');
INSERT INTO `databand_countrylanguage` VALUES ('TZA', 'Makonde', 'F', '5.9');
INSERT INTO `databand_countrylanguage` VALUES ('TZA', 'Nyakusa', 'F', '5.4');
INSERT INTO `databand_countrylanguage` VALUES ('TZA', 'Nyamwesi', 'F', '21.1');
INSERT INTO `databand_countrylanguage` VALUES ('TZA', 'Shambala', 'F', '4.3');
INSERT INTO `databand_countrylanguage` VALUES ('TZA', 'Swahili', 'T', '8.8');
INSERT INTO `databand_countrylanguage` VALUES ('UGA', 'Acholi', 'F', '4.4');
INSERT INTO `databand_countrylanguage` VALUES ('UGA', 'Ganda', 'F', '18.1');
INSERT INTO `databand_countrylanguage` VALUES ('UGA', 'Gisu', 'F', '4.5');
INSERT INTO `databand_countrylanguage` VALUES ('UGA', 'Kiga', 'F', '8.3');
INSERT INTO `databand_countrylanguage` VALUES ('UGA', 'Lango', 'F', '5.9');
INSERT INTO `databand_countrylanguage` VALUES ('UGA', 'Lugbara', 'F', '4.7');
INSERT INTO `databand_countrylanguage` VALUES ('UGA', 'Nkole', 'F', '10.7');
INSERT INTO `databand_countrylanguage` VALUES ('UGA', 'Rwanda', 'F', '3.2');
INSERT INTO `databand_countrylanguage` VALUES ('UGA', 'Soga', 'F', '8.2');
INSERT INTO `databand_countrylanguage` VALUES ('UGA', 'Teso', 'F', '6.0');
INSERT INTO `databand_countrylanguage` VALUES ('UKR', 'Belorussian', 'F', '0.3');
INSERT INTO `databand_countrylanguage` VALUES ('UKR', 'Bulgariana', 'F', '0.3');
INSERT INTO `databand_countrylanguage` VALUES ('UKR', 'Hungarian', 'F', '0.3');
INSERT INTO `databand_countrylanguage` VALUES ('UKR', 'Polish', 'F', '0.1');
INSERT INTO `databand_countrylanguage` VALUES ('UKR', 'Romanian', 'F', '0.7');
INSERT INTO `databand_countrylanguage` VALUES ('UKR', 'Russian', 'F', '32.9');
INSERT INTO `databand_countrylanguage` VALUES ('UKR', 'Ukrainian', 'T', '64.7');
INSERT INTO `databand_countrylanguage` VALUES ('UMI', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('URY', 'Spanish', 'T', '95.7');
INSERT INTO `databand_countrylanguage` VALUES ('USA', 'Chinese', 'F', '0.6');
INSERT INTO `databand_countrylanguage` VALUES ('USA', 'English', 'T', '86.2');
INSERT INTO `databand_countrylanguage` VALUES ('USA', 'French', 'F', '0.7');
INSERT INTO `databand_countrylanguage` VALUES ('USA', 'German', 'F', '0.7');
INSERT INTO `databand_countrylanguage` VALUES ('USA', 'Italian', 'F', '0.6');
INSERT INTO `databand_countrylanguage` VALUES ('USA', 'Japanese', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('USA', 'Korean', 'F', '0.3');
INSERT INTO `databand_countrylanguage` VALUES ('USA', 'Polish', 'F', '0.3');
INSERT INTO `databand_countrylanguage` VALUES ('USA', 'Portuguese', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('USA', 'Spanish', 'F', '7.5');
INSERT INTO `databand_countrylanguage` VALUES ('USA', 'Tagalog', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('USA', 'Vietnamese', 'F', '0.2');
INSERT INTO `databand_countrylanguage` VALUES ('UZB', 'Karakalpak', 'F', '2.0');
INSERT INTO `databand_countrylanguage` VALUES ('UZB', 'Kazakh', 'F', '3.8');
INSERT INTO `databand_countrylanguage` VALUES ('UZB', 'Russian', 'F', '10.9');
INSERT INTO `databand_countrylanguage` VALUES ('UZB', 'Tadzhik', 'F', '4.4');
INSERT INTO `databand_countrylanguage` VALUES ('UZB', 'Tatar', 'F', '1.8');
INSERT INTO `databand_countrylanguage` VALUES ('UZB', 'Uzbek', 'T', '72.6');
INSERT INTO `databand_countrylanguage` VALUES ('VAT', 'Italian', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('VCT', 'Creole English', 'F', '99.1');
INSERT INTO `databand_countrylanguage` VALUES ('VCT', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('VEN', 'Goajiro', 'F', '0.4');
INSERT INTO `databand_countrylanguage` VALUES ('VEN', 'Spanish', 'T', '96.9');
INSERT INTO `databand_countrylanguage` VALUES ('VEN', 'Warrau', 'F', '0.1');
INSERT INTO `databand_countrylanguage` VALUES ('VGB', 'English', 'T', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('VIR', 'English', 'T', '81.7');
INSERT INTO `databand_countrylanguage` VALUES ('VIR', 'French', 'F', '2.5');
INSERT INTO `databand_countrylanguage` VALUES ('VIR', 'Spanish', 'F', '13.3');
INSERT INTO `databand_countrylanguage` VALUES ('VNM', 'Chinese', 'F', '1.4');
INSERT INTO `databand_countrylanguage` VALUES ('VNM', 'Khmer', 'F', '1.4');
INSERT INTO `databand_countrylanguage` VALUES ('VNM', 'Man', 'F', '0.7');
INSERT INTO `databand_countrylanguage` VALUES ('VNM', 'Miao', 'F', '0.9');
INSERT INTO `databand_countrylanguage` VALUES ('VNM', 'Muong', 'F', '1.5');
INSERT INTO `databand_countrylanguage` VALUES ('VNM', 'Nung', 'F', '1.1');
INSERT INTO `databand_countrylanguage` VALUES ('VNM', 'Thai', 'F', '1.6');
INSERT INTO `databand_countrylanguage` VALUES ('VNM', 'Tho', 'F', '1.8');
INSERT INTO `databand_countrylanguage` VALUES ('VNM', 'Vietnamese', 'T', '86.8');
INSERT INTO `databand_countrylanguage` VALUES ('VUT', 'Bislama', 'T', '56.6');
INSERT INTO `databand_countrylanguage` VALUES ('VUT', 'English', 'T', '28.3');
INSERT INTO `databand_countrylanguage` VALUES ('VUT', 'French', 'T', '14.2');
INSERT INTO `databand_countrylanguage` VALUES ('WLF', 'Futuna', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('WLF', 'Wallis', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('WSM', 'English', 'T', '0.6');
INSERT INTO `databand_countrylanguage` VALUES ('WSM', 'Samoan', 'T', '47.5');
INSERT INTO `databand_countrylanguage` VALUES ('WSM', 'Samoan-English', 'F', '52.0');
INSERT INTO `databand_countrylanguage` VALUES ('YEM', 'Arabic', 'T', '99.6');
INSERT INTO `databand_countrylanguage` VALUES ('YEM', 'Soqutri', 'F', '0.0');
INSERT INTO `databand_countrylanguage` VALUES ('YUG', 'Albaniana', 'F', '16.5');
INSERT INTO `databand_countrylanguage` VALUES ('YUG', 'Hungarian', 'F', '3.4');
INSERT INTO `databand_countrylanguage` VALUES ('YUG', 'Macedonian', 'F', '0.5');
INSERT INTO `databand_countrylanguage` VALUES ('YUG', 'Romani', 'F', '1.4');
INSERT INTO `databand_countrylanguage` VALUES ('YUG', 'Serbo-Croatian', 'T', '75.2');
INSERT INTO `databand_countrylanguage` VALUES ('YUG', 'Slovak', 'F', '0.7');
INSERT INTO `databand_countrylanguage` VALUES ('ZAF', 'Afrikaans', 'T', '14.3');
INSERT INTO `databand_countrylanguage` VALUES ('ZAF', 'English', 'T', '8.5');
INSERT INTO `databand_countrylanguage` VALUES ('ZAF', 'Ndebele', 'F', '1.5');
INSERT INTO `databand_countrylanguage` VALUES ('ZAF', 'Northsotho', 'F', '9.1');
INSERT INTO `databand_countrylanguage` VALUES ('ZAF', 'Southsotho', 'F', '7.6');
INSERT INTO `databand_countrylanguage` VALUES ('ZAF', 'Swazi', 'F', '2.5');
INSERT INTO `databand_countrylanguage` VALUES ('ZAF', 'Tsonga', 'F', '4.3');
INSERT INTO `databand_countrylanguage` VALUES ('ZAF', 'Tswana', 'F', '8.1');
INSERT INTO `databand_countrylanguage` VALUES ('ZAF', 'Venda', 'F', '2.2');
INSERT INTO `databand_countrylanguage` VALUES ('ZAF', 'Xhosa', 'T', '17.7');
INSERT INTO `databand_countrylanguage` VALUES ('ZAF', 'Zulu', 'T', '22.7');
INSERT INTO `databand_countrylanguage` VALUES ('ZMB', 'Bemba', 'F', '29.7');
INSERT INTO `databand_countrylanguage` VALUES ('ZMB', 'Chewa', 'F', '5.7');
INSERT INTO `databand_countrylanguage` VALUES ('ZMB', 'Lozi', 'F', '6.4');
INSERT INTO `databand_countrylanguage` VALUES ('ZMB', 'Nsenga', 'F', '4.3');
INSERT INTO `databand_countrylanguage` VALUES ('ZMB', 'Nyanja', 'F', '7.8');
INSERT INTO `databand_countrylanguage` VALUES ('ZMB', 'Tongan', 'F', '11.0');
INSERT INTO `databand_countrylanguage` VALUES ('ZWE', 'English', 'T', '2.2');
INSERT INTO `databand_countrylanguage` VALUES ('ZWE', 'Ndebele', 'F', '16.2');
INSERT INTO `databand_countrylanguage` VALUES ('ZWE', 'Nyanja', 'F', '2.2');
INSERT INTO `databand_countrylanguage` VALUES ('ZWE', 'Shona', 'F', '72.1');

-- ----------------------------
-- Table structure for `databand_fromhive`
-- ----------------------------
DROP TABLE IF EXISTS `databand_fromhive`;
CREATE TABLE `databand_fromhive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `buycount` int(11) DEFAULT NULL,
  `citycode` varchar(50) DEFAULT NULL,
  `saledatetime` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of databand_fromhive
-- ----------------------------
INSERT INTO `databand_fromhive` VALUES ('1', '100', '广州', '2010-10-10');
INSERT INTO `databand_fromhive` VALUES ('2', '11', 'aa', 'bb');
INSERT INTO `databand_fromhive` VALUES ('3', '0', 'CITYCODE', 'SALEDATETIME');
INSERT INTO `databand_fromhive` VALUES ('4', '1', '深圳', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('5', '2', '广州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('6', '1', '郑州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('7', '1', '杭州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('8', '1', '南京', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('9', '1', '深圳', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('10', '1', '郑州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('11', '1', '北京', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('12', '1', '深圳', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('13', '0', 'CITYCODE', 'SALEDATETIME');
INSERT INTO `databand_fromhive` VALUES ('14', '1', '深圳', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('15', '2', '广州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('16', '1', '郑州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('17', '1', '杭州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('18', '1', '南京', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('19', '1', '深圳', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('20', '1', '郑州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('21', '1', '北京', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('22', '1', '深圳', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('23', '0', 'CITYCODE', 'SALEDATETIME');
INSERT INTO `databand_fromhive` VALUES ('24', '1', '深圳', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('25', '2', '广州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('26', '1', '郑州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('27', '1', '杭州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('28', '1', '南京', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('29', '1', '深圳', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('30', '1', '郑州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('31', '1', '北京', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('32', '1', '深圳', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('33', '0', 'CITYCODE', 'SALEDATETIME');
INSERT INTO `databand_fromhive` VALUES ('34', '1', '深圳', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('35', '2', '广州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('36', '1', '郑州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('37', '1', '杭州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('38', '1', '南京', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('39', '1', '深圳', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('40', '1', '郑州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('41', '1', '北京', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('42', '1', '深圳', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('43', '0', 'CITYCODE', 'SALEDATETIME');
INSERT INTO `databand_fromhive` VALUES ('44', '1', '深圳', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('45', '2', '广州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('46', '1', '郑州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('47', '1', '杭州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('48', '1', '南京', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('49', '1', '深圳', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('50', '1', '郑州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('51', '1', '北京', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('52', '1', '深圳', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('53', '0', 'CITYCODE', 'SALEDATETIME');
INSERT INTO `databand_fromhive` VALUES ('54', '1', '深圳', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('55', '2', '广州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('56', '1', '郑州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('57', '1', '杭州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('58', '1', '南京', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('59', '1', '深圳', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('60', '1', '郑州', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('61', '1', '北京', '2020-10-1');
INSERT INTO `databand_fromhive` VALUES ('62', '1', '深圳', '2020-10-1');

-- ----------------------------
-- Table structure for `databand_mockinstances`
-- ----------------------------
DROP TABLE IF EXISTS `databand_mockinstances`;
CREATE TABLE `databand_mockinstances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descrb` varchar(300) DEFAULT NULL,
  `method` varchar(10) DEFAULT NULL,
  `path` varchar(200) DEFAULT NULL,
  `path_parameters` varchar(300) DEFAULT NULL,
  `query_string_parameters` varchar(3000) DEFAULT NULL,
  `req_cookie` varchar(300) DEFAULT NULL,
  `req_headers` varchar(500) DEFAULT NULL,
  `req_jsonbody` text,
  `resp_statuscode` varchar(50) DEFAULT NULL,
  `resp_cookie` varchar(300) DEFAULT NULL,
  `resp_headers` varchar(500) DEFAULT NULL,
  `resp_body` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of databand_mockinstances
-- ----------------------------
INSERT INTO `databand_mockinstances` VALUES ('1', 'GET路径：/mypath/2/ANYCHARS', 'GET', '/mypath/{id}/{type}', '{id<=>2}$$${type<=>[A-Z0-9\\\\\\\\-]+}', null, null, null, '', '200', '{Session:97d43b1e-fe03-4855-926a-f448eddac32f}', '{Content-Type:text/html;charset=utf-8},{other:othervalue}', '{\r\n  \"id\" : 1,\r\n  \"name\" : \"姓名\",\r\n  \"price\" : \"123\",\r\n  \"price2\" : \"121\",\r\n  \"enabled\" : \"true\",\r\n  \"tags\" : [ \"tag1\", \"tag2数组项\" ]\r\n}');
INSERT INTO `databand_mockinstances` VALUES ('2', 'GET路径：/mypath2?month=10&userid=ANYCHARS', 'GET', '/mypath2', null, '{month<=>10}$$${userid<=>[A-Z0-9\\\\\\\\-]+}', null, null, '', '200', null, '{Content-Type:text/html;charset=utf-8},{other:othervalue}', '{\r\n  \"id\" : \"97d43b1e-fe03-4855-926a-f448eddac32f\",\r\n  \"name\" : \"姓名\",\r\n  \"year\" : \"2019\",\r\n  \"month\" : \"10\",\r\n  \"userid\" : \"id\",\r\n  \"tags\" : [ \"tag3\", \"tag4\" ]\r\n}');
INSERT INTO `databand_mockinstances` VALUES ('3', 'GET路径：/get', 'GET', '/get', null, null, null, null, '{}', '200', null, null, '{\r\n  \"id\" : 1,\r\n  \"name\" : \"姓名\",\r\n  \"price\" : \"123\",\r\n  \"price2\" : \"121\",\r\n  \"enabled\" : \"true\",\r\n  \"tags\" : [ \"home\", \"green\" ]\r\n}');
INSERT INTO `databand_mockinstances` VALUES ('4', 'POST路径：/submitForm', 'POST', '/submitForm', null, null, null, null, '{\"username\": \"user\",  \"password\": \"mypass123\"}', '200', null, null, '{\r\n  \"id\" : 123,\r\n  \"name\" : \"post ok\"\r\n}');

-- ----------------------------
-- Table structure for `databand_report`
-- ----------------------------
DROP TABLE IF EXISTS `databand_report`;
CREATE TABLE `databand_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL COMMENT '报表标题',
  `descri` varchar(300) DEFAULT NULL COMMENT '描述',
  `templateid` int(11) DEFAULT NULL,
  `reporturl` varchar(300) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `subtitle` varchar(100) DEFAULT NULL COMMENT '二级标题',
  `daterange` varchar(100) DEFAULT NULL COMMENT '时间日期范围',
  `options` varchar(200) DEFAULT NULL COMMENT '下拉选项(字典项)',
  `sql` varchar(1000) DEFAULT NULL COMMENT '报表SQL',
  `listsql` varchar(1000) DEFAULT NULL COMMENT '列表展示SQL',
  `sourceid` int(50) DEFAULT NULL COMMENT '数据源ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='报表';

-- ----------------------------
-- Records of databand_report
-- ----------------------------
INSERT INTO `databand_report` VALUES ('2', '测试报表', '', null, '/demoreport', null, '测试报表子标题', '2', '', null, null, null);

-- ----------------------------
-- Table structure for `databand_reporthistroy`
-- ----------------------------
DROP TABLE IF EXISTS `databand_reporthistroy`;
CREATE TABLE `databand_reporthistroy` (
  `id` int(11) DEFAULT NULL,
  `reportid` int(11) DEFAULT NULL,
  `obj` text,
  `updatime` datetime DEFAULT NULL,
  `updateby` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='报表版本';

-- ----------------------------
-- Records of databand_reporthistroy
-- ----------------------------

-- ----------------------------
-- Table structure for `databand_reporttab`
-- ----------------------------
DROP TABLE IF EXISTS `databand_reporttab`;
CREATE TABLE `databand_reporttab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reportid` int(11) DEFAULT NULL COMMENT '所属报表',
  `title` varchar(50) DEFAULT NULL,
  `sql` varchar(1000) DEFAULT NULL,
  `listsql` varchar(1000) DEFAULT NULL,
  `sortnum` int(11) DEFAULT NULL COMMENT '页签排序',
  `sourceid` int(11) DEFAULT NULL,
  `apiurl` varchar(1000) DEFAULT NULL COMMENT '数据接口',
  `apiparam` varchar(200) DEFAULT NULL,
  `apitag` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='报表页';

-- ----------------------------
-- Records of databand_reporttab
-- ----------------------------
INSERT INTO `databand_reporttab` VALUES ('1', '2', '报表页一', '', '', '1', '2', null, null, null);
INSERT INTO `databand_reporttab` VALUES ('2', '2', '报表页二', '', '', '2', '2', null, null, null);
INSERT INTO `databand_reporttab` VALUES ('3', '2', '页三', '', '', '3', '2', '/getdata', 'productcate={productcate}&city={city}', '');

-- ----------------------------
-- Table structure for `databand_scheduletask`
-- ----------------------------
DROP TABLE IF EXISTS `databand_scheduletask`;
CREATE TABLE `databand_scheduletask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jobtype` varchar(128) NOT NULL COMMENT '任务类型',
  `jobcode` varchar(128) NOT NULL COMMENT '任务关键key，任务实例id',
  `beaname` varchar(128) DEFAULT NULL COMMENT 'bean名称，只在java型任务有效',
  `methodname` varchar(128) DEFAULT NULL COMMENT '方法名称，只在java型任务有效',
  `methodparams` varchar(128) DEFAULT NULL COMMENT '方法参数，只在java型任务有效',
  `descri` varchar(128) DEFAULT NULL COMMENT '任务描述',
  `cron` varchar(128) NOT NULL COMMENT 'cron任务表达式',
  `ext` varchar(500) DEFAULT NULL COMMENT '扩展字段',
  `status` int(2) NOT NULL DEFAULT '-1' COMMENT '成功 1 是 0 否',
  `startflag` int(2) DEFAULT '0',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniqu_jobcode` (`jobcode`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='批处理计划';

-- ----------------------------
-- Records of databand_scheduletask
-- ----------------------------
INSERT INTO `databand_scheduletask` VALUES ('1', 'HdfsToJdbcSqoop1', 'HdfsToJdbcSqoop1Job', '-', '-', '-', '每日数据导入:产品各分类订单数', '0/45 * * * * ?', '{}', '-1', '0', '2020-12-14 10:46:37', '2020-12-17 15:58:05');
INSERT INTO `databand_scheduletask` VALUES ('6', 'cmd', 'WindowsDir1', '-', '-', '-', '命令行：dir', '0/10 * * * * ?', '{}', '-1', '0', '2020-12-16 20:50:45', '2020-12-16 20:58:53');
INSERT INTO `databand_scheduletask` VALUES ('7', 'cmd', 'WindowsIP1', '-', '-', '-', '命令行：ipconfig', '0/25 * * * * ?', '{}', '-1', '0', '2020-12-16 20:51:36', '2020-12-16 20:59:10');
INSERT INTO `databand_scheduletask` VALUES ('8', 'HdfsBackup', 'hdfs_product2020', '-', '-', '-', 'HDFS产品数据每日备份，v2020版', '0/35 * * * * ?', '{}', '-1', '0', '2020-12-17 14:57:13', '2020-12-17 14:57:39');
INSERT INTO `databand_scheduletask` VALUES ('9', 'HdfsToLocalFile', 'hdfs_toLocal2020', '-', '-', '-', 'HDFS定时复制文件到文件系统', '0/15 * * * * ?', '{}', '-1', '0', '2020-12-17 15:44:21', '2020-12-17 15:59:59');
INSERT INTO `databand_scheduletask` VALUES ('11', 'HdfsToLocalFile', 'hdfs_toLocal2020_1', '-', '-', '-', 'HDFS定时复制文件到文件系统的另一个实例', '0/20 * * * * ?', '{}', '-1', '0', '2020-12-17 15:59:53', '2020-12-17 15:59:53');
INSERT INTO `databand_scheduletask` VALUES ('12', 'HiveSqlExecute', 'hiveSqlExecuteJob1', '-', '-', '-', 'hive执行型任务', '0/35 * * * * ?', '{}', '-1', '0', '2020-12-21 17:53:41', '2020-12-21 17:53:41');
INSERT INTO `databand_scheduletask` VALUES ('13', 'HiveSqlQueryJob', 'hiveSqlQueryJob1', '-', '-', '-', 'hive数据查询并且sink导出', '0/35 * * * * ?', '{}', '-1', '0', '2020-12-23 16:44:56', '2020-12-23 16:44:56');

-- ----------------------------
-- Table structure for `databand_site`
-- ----------------------------
DROP TABLE IF EXISTS `databand_site`;
CREATE TABLE `databand_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '站点编码',
  `code` varchar(30) DEFAULT NULL,
  `title` varchar(30) DEFAULT NULL COMMENT '站点名称',
  `deptid` int(11) DEFAULT NULL COMMENT '所属部门',
  `roleid` int(11) DEFAULT NULL COMMENT '所属角色',
  `templateid` int(11) DEFAULT NULL COMMENT '模板id',
  `url` varchar(300) DEFAULT NULL COMMENT '站点地址',
  `homepage` varchar(50) DEFAULT NULL COMMENT '首页',
  `descri` varchar(300) DEFAULT NULL COMMENT '描述',
  `action` varchar(20) DEFAULT NULL COMMENT '动作',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='站点';

-- ----------------------------
-- Records of databand_site
-- ----------------------------
INSERT INTO `databand_site` VALUES ('1', 'demo', 'demo站点', null, null, null, '/demo', null, '', null, 'fa fa-address-book');

-- ----------------------------
-- Table structure for `databand_sitehistroy`
-- ----------------------------
DROP TABLE IF EXISTS `databand_sitehistroy`;
CREATE TABLE `databand_sitehistroy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteid` int(11) DEFAULT NULL COMMENT '站点ID',
  `obj` text COMMENT '对象值',
  `updatime` datetime DEFAULT NULL COMMENT '更新时间',
  `updateby` int(11) DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='站点历史';

-- ----------------------------
-- Records of databand_sitehistroy
-- ----------------------------

-- ----------------------------
-- Table structure for `databand_sitemenu`
-- ----------------------------
DROP TABLE IF EXISTS `databand_sitemenu`;
CREATE TABLE `databand_sitemenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteid` int(11) DEFAULT NULL COMMENT '站点id',
  `menuname` varchar(30) DEFAULT NULL COMMENT '站点菜单',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `parentid` int(11) DEFAULT NULL COMMENT '父栏目',
  `reportid` int(11) DEFAULT NULL COMMENT '关联报表',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  `nodetype` bit(1) DEFAULT NULL COMMENT '节点类型,1:根；2：菜单；3：叶子，可关联报表',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='站点菜单';

-- ----------------------------
-- Records of databand_sitemenu
-- ----------------------------
INSERT INTO `databand_sitemenu` VALUES ('1', '1', '产品销售分析', '1', null, '2', 'fa fa-area-chart', '');

-- ----------------------------
-- Table structure for `databand_source`
-- ----------------------------
DROP TABLE IF EXISTS `databand_source`;
CREATE TABLE `databand_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL COMMENT '数据源标题',
  `drive` varchar(50) DEFAULT NULL COMMENT '驱动',
  `conn` varchar(200) DEFAULT NULL COMMENT '数据源连接',
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='数据源';

-- ----------------------------
-- Records of databand_source
-- ----------------------------
INSERT INTO `databand_source` VALUES ('2', '产品分析源-mysql', 'com.mysql.jdbc.Driver', 'jdbc:mysql://localhost:3306/databand?useUnicode=true&characterEncoding=utf8&useSSL=true', 'root', '');

-- ----------------------------
-- Table structure for `databand_summary`
-- ----------------------------
DROP TABLE IF EXISTS `databand_summary`;
CREATE TABLE `databand_summary` (
  `id` bigint(20) NOT NULL,
  `org` varchar(100) DEFAULT NULL,
  `dept` varchar(100) DEFAULT NULL,
  `group` varchar(100) DEFAULT NULL,
  `key` varchar(100) DEFAULT NULL,
  `valueint` int(11) DEFAULT NULL,
  `valuedecemal` decimal(10,0) DEFAULT NULL,
  `bussdate` varchar(20) DEFAULT NULL,
  `insertdate` varchar(20) DEFAULT NULL,
  `ext` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `keyindex` (`key`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of databand_summary
-- ----------------------------

-- ----------------------------
-- Table structure for `databand_testtable`
-- ----------------------------
DROP TABLE IF EXISTS `databand_testtable`;
CREATE TABLE `databand_testtable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `v1` int(10) DEFAULT NULL,
  `v2` int(11) DEFAULT NULL,
  `update_time` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22013 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of databand_testtable
-- ----------------------------
INSERT INTO `databand_testtable` VALUES ('22003', '女:董珍', '47', '热河大厦11号-18-3', '13100538246', '59', null, '2020-06-03');
INSERT INTO `databand_testtable` VALUES ('22004', '男:伍安祥', '39', '华山路37号-16-8', '13402855868', '26', null, '2020-09-11');
INSERT INTO `databand_testtable` VALUES ('22005', '男:殷以峰', '77', '冠县支广场75号-12-7', '13903735343', '39', null, '2020-10-22');
INSERT INTO `databand_testtable` VALUES ('22006', '女:酆蓉芳', '44', '吴兴一广场71号-15-9', '15200416283', '75', null, '2020-04-04');
INSERT INTO `databand_testtable` VALUES ('22007', '女:康萍', '48', '济南街104号-18-8', '15801880212', '34', null, '2020-03-29');
INSERT INTO `databand_testtable` VALUES ('22008', '男:韦宁山', '72', '台西纬四街131号-3-6', '15500736715', '49', null, '2020-06-18');
INSERT INTO `databand_testtable` VALUES ('22009', '女:倪馨梦', '98', '驼峰路107号-20-4', '13605681446', '98', null, '2020-03-15');
INSERT INTO `databand_testtable` VALUES ('22010', '女:臧静柔', '33', '城武大厦64号-9-3', '15504628502', '28', null, '2020-04-08');
INSERT INTO `databand_testtable` VALUES ('22011', '男:袁朗', '83', '仰口街120号-6-10', '15202377021', '74', null, '2020-01-29');
INSERT INTO `databand_testtable` VALUES ('22012', '男:冯时', '94', '滕县路99号-8-8', '15100404423', '83', null, '2020-10-21');

-- ----------------------------
-- Table structure for `databand_user`
-- ----------------------------
DROP TABLE IF EXISTS `databand_user`;
CREATE TABLE `databand_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of databand_user
-- ----------------------------
INSERT INTO `databand_user` VALUES ('1', '张三', '22');
INSERT INTO `databand_user` VALUES ('2', '李四', '53');
INSERT INTO `databand_user` VALUES ('3', '王五', '42');

-- ----------------------------
-- Table structure for `databand_usermoney`
-- ----------------------------
DROP TABLE IF EXISTS `databand_usermoney`;
CREATE TABLE `databand_usermoney` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) DEFAULT NULL,
  `bonus` decimal(10,0) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of databand_usermoney
-- ----------------------------

-- ----------------------------
-- Table structure for `databand_video`
-- ----------------------------
DROP TABLE IF EXISTS `databand_video`;
CREATE TABLE `databand_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(512) NOT NULL COMMENT '标题',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  `state` int(11) DEFAULT NULL COMMENT '0-有效，1-无效',
  `modify_time` varchar(512) DEFAULT NULL COMMENT '修改时间',
  `cover_id` varchar(512) DEFAULT NULL COMMENT '专辑id',
  `vid` varchar(512) DEFAULT NULL COMMENT '视频id',
  `url` varchar(512) DEFAULT NULL COMMENT '播放url',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `tx_updatetime` datetime DEFAULT NULL COMMENT '更新时间',
  `c_link_flag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of databand_video
-- ----------------------------
INSERT INTO `databand_video` VALUES ('1', '测试视频1', '1', '2', null, '11235', '34234', 'http://xxxxx.mp4', null, null, null);
INSERT INTO `databand_video` VALUES ('2', 'a', '2', '4', null, '22341', '23451', 'http://xxxxxrt.mp4', null, null, null);
INSERT INTO `databand_video` VALUES ('100', '测试视频1', '1', '2', null, '11235', '34234', 'http://xxxxx.mp4', null, null, null);
INSERT INTO `databand_video` VALUES ('101', 'a', '2', '4', null, '22341', '23451', 'http://xxxxxrt.mp4', null, null, null);

-- ----------------------------
-- Table structure for `gen_table`
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table` (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作 sub主子表操作）',
  `package_name` varchar(100) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='代码生成业务表';

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES ('1', 'databand_source', '数据源', null, null, 'DatabandSource', 'crud', 'com.ruoyi.web', 'web', 'source', '数据源', 'databand', '0', '/', null, 'admin', '2020-12-30 16:24:02', '', null, null);
INSERT INTO `gen_table` VALUES ('2', 'databand_report', '报表', null, null, 'DatabandReport', 'crud', 'com.ruoyi.web', 'web', 'report', '报', 'databand', '0', '/', null, 'admin', '2020-12-31 10:34:26', '', null, null);
INSERT INTO `gen_table` VALUES ('3', 'databand_reporthistroy', '报表版本', null, null, 'DatabandReporthistroy', 'crud', 'com.ruoyi.web', 'web', 'reporthistroy', '报版本', 'databand', '0', '/', null, 'admin', '2020-12-31 10:34:26', '', null, null);
INSERT INTO `gen_table` VALUES ('4', 'databand_reporttab', '报表页', null, null, 'DatabandReporttab', 'crud', 'com.ruoyi.web', 'web', 'reporttab', '报页', 'databand', '0', '/', null, 'admin', '2020-12-31 10:34:26', '', null, null);
INSERT INTO `gen_table` VALUES ('5', 'databand_scheduletask', '批处理计划', null, null, 'DatabandScheduletask', 'crud', 'com.ruoyi.web', 'web', 'scheduletask', '批处理计划', 'databand', '0', '/', null, 'admin', '2020-12-31 10:34:26', '', null, null);
INSERT INTO `gen_table` VALUES ('6', 'databand_site', '站点', null, null, 'DatabandSite', 'crud', 'com.ruoyi.web', 'web', 'site', '站点', 'databand', '0', '/', null, 'admin', '2020-12-31 10:34:27', '', null, null);
INSERT INTO `gen_table` VALUES ('7', 'databand_sitehistroy', '站点历史', null, null, 'DatabandSitehistroy', 'crud', 'com.ruoyi.web', 'web', 'sitehistroy', '站点历史', 'databand', '0', '/', null, 'admin', '2020-12-31 10:34:27', '', null, null);
INSERT INTO `gen_table` VALUES ('8', 'databand_sitemenu', '站点菜单', null, null, 'DatabandSitemenu', 'crud', 'com.ruoyi.web', 'web', 'sitemenu', '站点菜单', 'databand', '0', '/', null, 'admin', '2020-12-31 10:34:27', '', null, null);

-- ----------------------------
-- Table structure for `gen_table_column`
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column` (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) DEFAULT '' COMMENT '字典类型',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COMMENT='代码生成业务表字段';

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES ('1', '1', 'id', null, 'int(11)', 'Long', 'id', '1', '0', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2020-12-30 16:24:02', '', null);
INSERT INTO `gen_table_column` VALUES ('2', '1', 'title', '数据源标题', 'varchar(50)', 'String', 'title', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '2', 'admin', '2020-12-30 16:24:02', '', null);
INSERT INTO `gen_table_column` VALUES ('3', '1', 'drive', '驱动', 'varchar(50)', 'String', 'drive', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '3', 'admin', '2020-12-30 16:24:02', '', null);
INSERT INTO `gen_table_column` VALUES ('4', '1', 'conn', '数据源连接', 'varchar(200)', 'String', 'conn', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '4', 'admin', '2020-12-30 16:24:02', '', null);
INSERT INTO `gen_table_column` VALUES ('5', '1', 'username', '用户名', 'varchar(50)', 'String', 'username', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '5', 'admin', '2020-12-30 16:24:02', '', null);
INSERT INTO `gen_table_column` VALUES ('6', '1', 'password', '密码', 'varchar(50)', 'String', 'password', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '6', 'admin', '2020-12-30 16:24:02', '', null);
INSERT INTO `gen_table_column` VALUES ('7', '2', 'id', null, 'int(11)', 'Long', 'id', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('8', '2', 'title', '报表标题', 'varchar(50)', 'String', 'title', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '2', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('9', '2', 'descri', '描述', 'varchar(300)', 'String', 'descri', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '3', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('10', '2', 'templateid', null, 'int(11)', 'Long', 'templateid', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '4', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('11', '2', 'reporturl', null, 'varchar(300)', 'String', 'reporturl', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '5', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('12', '2', 'icon', null, 'varchar(50)', 'String', 'icon', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '6', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('13', '2', 'subtitle', '二级标题', 'varchar(100)', 'String', 'subtitle', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '7', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('14', '2', 'daterange', '时间日期范围', 'varchar(100)', 'String', 'daterange', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '8', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('15', '2', 'options', '下拉选项(字典项)', 'varchar(200)', 'String', 'options', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '9', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('16', '2', 'sql', '报表SQL', 'varchar(1000)', 'String', 'sql', '0', '0', null, '1', '1', '1', '1', 'EQ', 'textarea', '', '10', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('17', '2', 'listsql', '列表展示SQL', 'varchar(1000)', 'String', 'listsql', '0', '0', null, '1', '1', '1', '1', 'EQ', 'textarea', '', '11', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('18', '2', 'sourceid', '数据源ID', 'int(50)', 'Long', 'sourceid', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '12', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('19', '3', 'id', null, 'int(11)', 'Long', 'id', '0', '0', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('20', '3', 'reportid', null, 'int(11)', 'Long', 'reportid', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '2', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('21', '3', 'obj', null, 'text', 'String', 'obj', '0', '0', null, '1', '1', '1', '1', 'EQ', 'textarea', '', '3', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('22', '3', 'updatime', null, 'datetime', 'Date', 'updatime', '0', '0', null, '1', '1', '1', '1', 'EQ', 'datetime', '', '4', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('23', '3', 'updateby', null, 'varchar(50)', 'String', 'updateby', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '5', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('24', '4', 'id', null, 'int(11)', 'Long', 'id', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('25', '4', 'reportid', '所属报表', 'int(11)', 'Long', 'reportid', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '2', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('26', '4', 'title', null, 'varchar(50)', 'String', 'title', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '3', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('27', '4', 'sql', null, 'varchar(1000)', 'String', 'sql', '0', '0', null, '1', '1', '1', '1', 'EQ', 'textarea', '', '4', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('28', '4', 'listsql', null, 'varchar(1000)', 'String', 'listsql', '0', '0', null, '1', '1', '1', '1', 'EQ', 'textarea', '', '5', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('29', '4', 'sortnum', '页签排序', 'int(11)', 'Long', 'sortnum', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '6', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('30', '4', 'sourceid', null, 'int(11)', 'Long', 'sourceid', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '7', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('31', '5', 'id', null, 'int(11)', 'Long', 'id', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('32', '5', 'jobtype', '任务类型', 'varchar(128)', 'String', 'jobtype', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', '', '2', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('33', '5', 'jobcode', '任务关键key，任务实例id', 'varchar(128)', 'String', 'jobcode', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', '3', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('34', '5', 'beaname', 'bean名称，只在java型任务有效', 'varchar(128)', 'String', 'beaname', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '4', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('35', '5', 'methodname', '方法名称，只在java型任务有效', 'varchar(128)', 'String', 'methodname', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '5', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('36', '5', 'methodparams', '方法参数，只在java型任务有效', 'varchar(128)', 'String', 'methodparams', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '6', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('37', '5', 'descri', '任务描述', 'varchar(128)', 'String', 'descri', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '7', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('38', '5', 'cron', 'cron任务表达式', 'varchar(128)', 'String', 'cron', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', '8', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('39', '5', 'ext', '扩展字段', 'varchar(500)', 'String', 'ext', '0', '0', null, '1', '1', '1', '1', 'EQ', 'textarea', '', '9', 'admin', '2020-12-31 10:34:26', '', null);
INSERT INTO `gen_table_column` VALUES ('40', '5', 'status', '成功 1 是 0 否', 'int(2)', 'Integer', 'status', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'radio', '', '10', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('41', '5', 'startflag', null, 'int(2)', 'Integer', 'startflag', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '11', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('42', '5', 'create_time', '创建时间', 'timestamp', 'Date', 'createTime', '0', '0', null, '1', null, null, null, 'EQ', 'datetime', '', '12', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('43', '5', 'update_time', '更新时间', 'timestamp', 'Date', 'updateTime', '0', '0', null, '1', '1', null, null, 'EQ', 'datetime', '', '13', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('44', '6', 'id', '站点编码', 'int(11)', 'Long', 'id', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('45', '6', 'code', null, 'varchar(30)', 'String', 'code', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '2', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('46', '6', 'title', '站点名称', 'varchar(30)', 'String', 'title', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '3', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('47', '6', 'deptid', '所属部门', 'int(11)', 'Long', 'deptid', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '4', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('48', '6', 'roleid', '所属角色', 'int(11)', 'Long', 'roleid', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '5', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('49', '6', 'templateid', '模板id', 'int(11)', 'Long', 'templateid', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '6', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('50', '6', 'url', '站点地址', 'varchar(300)', 'String', 'url', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '7', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('51', '6', 'homepage', '首页', 'varchar(50)', 'String', 'homepage', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '8', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('52', '6', 'descri', '描述', 'varchar(300)', 'String', 'descri', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '9', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('53', '6', 'action', '动作', 'varchar(20)', 'String', 'action', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '10', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('54', '6', 'icon', '图标地址', 'varchar(50)', 'String', 'icon', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '11', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('55', '7', 'id', null, 'int(11)', 'Long', 'id', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('56', '7', 'siteid', '站点ID', 'int(11)', 'Long', 'siteid', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '2', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('57', '7', 'obj', '对象值', 'text', 'String', 'obj', '0', '0', null, '1', '1', '1', '1', 'EQ', 'textarea', '', '3', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('58', '7', 'updatime', null, 'datetime', 'Date', 'updatime', '0', '0', null, '1', '1', '1', '1', 'EQ', 'datetime', '', '4', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('59', '7', 'updateby', '更新者', 'int(11)', 'Long', 'updateby', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '5', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('60', '8', 'id', null, 'int(11)', 'Long', 'id', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('61', '8', 'siteid', '站点id', 'int(11)', 'Long', 'siteid', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '2', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('62', '8', 'menuname', '站点菜单', 'varchar(30)', 'String', 'menuname', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '3', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('63', '8', 'sort', '排序', 'int(11)', 'Long', 'sort', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '4', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('64', '8', 'parentid', '父栏目', 'int(11)', 'Long', 'parentid', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '5', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('65', '8', 'reportid', '关联报表', 'int(11)', 'Long', 'reportid', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '6', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('66', '8', 'icon', '图标', 'varchar(50)', 'String', 'icon', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '7', 'admin', '2020-12-31 10:34:27', '', null);
INSERT INTO `gen_table_column` VALUES ('67', '8', 'nodetype', '节点类型,1:根；2：菜单；3：叶子，可关联报表', 'bit(1)', 'Integer', 'nodetype', '0', '0', null, '1', '1', '1', '1', 'EQ', 'select', '', '8', 'admin', '2020-12-31 10:34:27', '', null);

-- ----------------------------
-- Table structure for `qrtz_blob_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `blob_data` blob,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_calendars`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `sched_name` varchar(120) NOT NULL,
  `calendar_name` varchar(200) NOT NULL,
  `calendar` blob NOT NULL,
  PRIMARY KEY (`sched_name`,`calendar_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_cron_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `cron_expression` varchar(200) NOT NULL,
  `time_zone_id` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for `qrtz_fired_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `entry_id` varchar(95) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `instance_name` varchar(200) NOT NULL,
  `fired_time` bigint(13) NOT NULL,
  `sched_time` bigint(13) NOT NULL,
  `priority` int(11) NOT NULL,
  `state` varchar(16) NOT NULL,
  `job_name` varchar(200) DEFAULT NULL,
  `job_group` varchar(200) DEFAULT NULL,
  `is_nonconcurrent` varchar(1) DEFAULT NULL,
  `requests_recovery` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_job_details`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `sched_name` varchar(120) NOT NULL,
  `job_name` varchar(200) NOT NULL,
  `job_group` varchar(200) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `job_class_name` varchar(250) NOT NULL,
  `is_durable` varchar(1) NOT NULL,
  `is_nonconcurrent` varchar(1) NOT NULL,
  `is_update_data` varchar(1) NOT NULL,
  `requests_recovery` varchar(1) NOT NULL,
  `job_data` blob,
  PRIMARY KEY (`sched_name`,`job_name`,`job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', null, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000176A89F801078707400007070707400013174000E302F3130202A202A202A202A203F74001172795461736B2E72794E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', null, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000176A89F801078707400007070707400013174000E302F3135202A202A202A202A203F74001572795461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', null, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000176A89F801078707400007070707400013174000E302F3230202A202A202A202A203F74003872795461736B2E72794D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC8974000133740001317800);

-- ----------------------------
-- Table structure for `qrtz_locks`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `sched_name` varchar(120) NOT NULL,
  `lock_name` varchar(40) NOT NULL,
  PRIMARY KEY (`sched_name`,`lock_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for `qrtz_paused_trigger_grps`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  PRIMARY KEY (`sched_name`,`trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_scheduler_state`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `sched_name` varchar(120) NOT NULL,
  `instance_name` varchar(200) NOT NULL,
  `last_checkin_time` bigint(13) NOT NULL,
  `checkin_interval` bigint(13) NOT NULL,
  PRIMARY KEY (`sched_name`,`instance_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RuoyiScheduler', 'WIN7-18030717311609986825693', '1609987713427', '15000');

-- ----------------------------
-- Table structure for `qrtz_simple_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `repeat_count` bigint(7) NOT NULL,
  `repeat_interval` bigint(12) NOT NULL,
  `times_triggered` bigint(10) NOT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_simprop_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `str_prop_1` varchar(512) DEFAULT NULL,
  `str_prop_2` varchar(512) DEFAULT NULL,
  `str_prop_3` varchar(512) DEFAULT NULL,
  `int_prop_1` int(11) DEFAULT NULL,
  `int_prop_2` int(11) DEFAULT NULL,
  `long_prop_1` bigint(20) DEFAULT NULL,
  `long_prop_2` bigint(20) DEFAULT NULL,
  `dec_prop_1` decimal(13,4) DEFAULT NULL,
  `dec_prop_2` decimal(13,4) DEFAULT NULL,
  `bool_prop_1` varchar(1) DEFAULT NULL,
  `bool_prop_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `qrtz_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `sched_name` varchar(120) NOT NULL,
  `trigger_name` varchar(200) NOT NULL,
  `trigger_group` varchar(200) NOT NULL,
  `job_name` varchar(200) NOT NULL,
  `job_group` varchar(200) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `next_fire_time` bigint(13) DEFAULT NULL,
  `prev_fire_time` bigint(13) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `trigger_state` varchar(16) NOT NULL,
  `trigger_type` varchar(8) NOT NULL,
  `start_time` bigint(13) NOT NULL,
  `end_time` bigint(13) DEFAULT NULL,
  `calendar_name` varchar(200) DEFAULT NULL,
  `misfire_instr` smallint(2) DEFAULT NULL,
  `job_data` blob,
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  KEY `sched_name` (`sched_name`,`job_name`,`job_group`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', null, '1609986830000', '-1', '5', 'PAUSED', 'CRON', '1609986826000', '0', null, '2', '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', null, '1609986840000', '-1', '5', 'PAUSED', 'CRON', '1609986826000', '0', null, '2', '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', null, '1609986840000', '-1', '5', 'PAUSED', 'CRON', '1609986826000', '0', null, '2', '');

-- ----------------------------
-- Table structure for `sys_config`
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COMMENT='参数配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2020-12-28 17:13:13', '', null, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES ('2', '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2020-12-28 17:13:13', '', null, '初始化密码 123456');
INSERT INTO `sys_config` VALUES ('3', '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2020-12-28 17:13:13', '', null, '深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue');
INSERT INTO `sys_config` VALUES ('4', '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2020-12-28 17:13:13', '', null, '是否开启注册用户功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES ('5', '用户管理-密码字符范围', 'sys.account.chrtype', '0', 'Y', 'admin', '2020-12-28 17:13:13', '', null, '默认任意字符范围，0任意（密码可以输入任意字符），1数字（密码只能为0-9数字），2英文字母（密码只能为a-z和A-Z字母），3字母和数字（密码必须包含字母，数字）,4字母数字和特殊字符（目前支持的特殊字符包括：~!@#$%^&*()-=_+）');
INSERT INTO `sys_config` VALUES ('6', '用户管理-初始密码修改策略', 'sys.account.initPasswordModify', '0', 'Y', 'admin', '2020-12-28 17:13:13', '', null, '0：初始密码修改策略关闭，没有任何提示，1：提醒用户，如果未修改初始密码，则在登录时就会提醒修改密码对话框');
INSERT INTO `sys_config` VALUES ('7', '用户管理-账号密码更新周期', 'sys.account.passwordValidateDays', '0', 'Y', 'admin', '2020-12-28 17:13:13', '', null, '密码更新周期（填写数字，数据初始化值为0不限制，若修改必须为大于0小于365的正整数），如果超过这个周期登录系统时，则在登录时就会提醒修改密码对话框');
INSERT INTO `sys_config` VALUES ('8', '主框架页-菜单导航显示风格', 'sys.index.menuStyle', 'default', 'Y', 'admin', '2020-12-28 17:13:13', '', null, '菜单导航显示风格（default为左侧导航菜单，topnav为顶部导航菜单）');
INSERT INTO `sys_config` VALUES ('9', '主框架页-是否开启页脚', 'sys.index.ignoreFooter', 'true', 'Y', 'admin', '2020-12-28 17:13:13', '', null, '是否开启底部页脚显示（true显示，false隐藏）');

-- ----------------------------
-- Table structure for `sys_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(50) DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `leader` varchar(20) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `status` char(1) DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8mb4 COMMENT='部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('100', '0', '0', '总集团', '0', '', '', '', '0', '0', 'admin', '2020-12-28 17:12:56', 'admin', '2020-12-31 15:16:47');
INSERT INTO `sys_dept` VALUES ('101', '100', '0,100', '深圳总公司', '1', '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2020-12-28 17:12:56', '', null);
INSERT INTO `sys_dept` VALUES ('102', '100', '0,100', '长沙分公司', '2', '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2020-12-28 17:12:56', '', null);
INSERT INTO `sys_dept` VALUES ('103', '101', '0,100,101', '研发部门', '1', '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2020-12-28 17:12:56', '', null);
INSERT INTO `sys_dept` VALUES ('104', '101', '0,100,101', '市场部门', '2', '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2020-12-28 17:12:56', '', null);
INSERT INTO `sys_dept` VALUES ('105', '101', '0,100,101', '测试部门', '3', '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2020-12-28 17:12:56', '', null);
INSERT INTO `sys_dept` VALUES ('106', '101', '0,100,101', '财务部门', '4', '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2020-12-28 17:12:56', '', null);
INSERT INTO `sys_dept` VALUES ('107', '101', '0,100,101', '运维部门', '5', '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2020-12-28 17:12:56', '', null);
INSERT INTO `sys_dept` VALUES ('108', '102', '0,100,102', '市场部门', '1', '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2020-12-28 17:12:56', '', null);
INSERT INTO `sys_dept` VALUES ('109', '102', '0,100,102', '财务部门', '2', '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2020-12-28 17:12:57', '', null);

-- ----------------------------
-- Table structure for `sys_dict_data`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) DEFAULT '0' COMMENT '字典排序',
  `dict_label` varchar(100) DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COMMENT='字典数据表';

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES ('1', '1', '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2020-12-28 17:13:11', '', null, '性别男');
INSERT INTO `sys_dict_data` VALUES ('2', '2', '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2020-12-28 17:13:11', '', null, '性别女');
INSERT INTO `sys_dict_data` VALUES ('3', '3', '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2020-12-28 17:13:11', '', null, '性别未知');
INSERT INTO `sys_dict_data` VALUES ('4', '1', '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2020-12-28 17:13:11', '', null, '显示菜单');
INSERT INTO `sys_dict_data` VALUES ('5', '2', '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2020-12-28 17:13:12', '', null, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES ('6', '1', '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2020-12-28 17:13:12', '', null, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('7', '2', '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2020-12-28 17:13:12', '', null, '停用状态');
INSERT INTO `sys_dict_data` VALUES ('8', '1', '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2020-12-28 17:13:12', '', null, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('9', '2', '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2020-12-28 17:13:12', '', null, '停用状态');
INSERT INTO `sys_dict_data` VALUES ('10', '1', '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2020-12-28 17:13:12', '', null, '默认分组');
INSERT INTO `sys_dict_data` VALUES ('11', '2', '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2020-12-28 17:13:12', '', null, '系统分组');
INSERT INTO `sys_dict_data` VALUES ('12', '1', '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2020-12-28 17:13:12', '', null, '系统默认是');
INSERT INTO `sys_dict_data` VALUES ('13', '2', '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2020-12-28 17:13:12', '', null, '系统默认否');
INSERT INTO `sys_dict_data` VALUES ('14', '1', '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2020-12-28 17:13:12', '', null, '通知');
INSERT INTO `sys_dict_data` VALUES ('15', '2', '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2020-12-28 17:13:12', '', null, '公告');
INSERT INTO `sys_dict_data` VALUES ('16', '1', '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2020-12-28 17:13:12', '', null, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('17', '2', '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2020-12-28 17:13:12', '', null, '关闭状态');
INSERT INTO `sys_dict_data` VALUES ('18', '99', '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2020-12-28 17:13:12', '', null, '其他操作');
INSERT INTO `sys_dict_data` VALUES ('19', '1', '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2020-12-28 17:13:12', '', null, '新增操作');
INSERT INTO `sys_dict_data` VALUES ('20', '2', '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2020-12-28 17:13:12', '', null, '修改操作');
INSERT INTO `sys_dict_data` VALUES ('21', '3', '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2020-12-28 17:13:12', '', null, '删除操作');
INSERT INTO `sys_dict_data` VALUES ('22', '4', '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2020-12-28 17:13:12', '', null, '授权操作');
INSERT INTO `sys_dict_data` VALUES ('23', '5', '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2020-12-28 17:13:12', '', null, '导出操作');
INSERT INTO `sys_dict_data` VALUES ('24', '6', '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2020-12-28 17:13:12', '', null, '导入操作');
INSERT INTO `sys_dict_data` VALUES ('25', '7', '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2020-12-28 17:13:13', '', null, '强退操作');
INSERT INTO `sys_dict_data` VALUES ('26', '8', '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2020-12-28 17:13:13', '', null, '生成操作');
INSERT INTO `sys_dict_data` VALUES ('27', '9', '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2020-12-28 17:13:13', '', null, '清空操作');
INSERT INTO `sys_dict_data` VALUES ('28', '1', '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2020-12-28 17:13:13', '', null, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('29', '2', '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2020-12-28 17:13:13', '', null, '停用状态');
INSERT INTO `sys_dict_data` VALUES ('30', '1', 'PC', 'PC', 'productcate', null, 'success', 'Y', '0', 'admin', '2021-01-06 09:33:38', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('31', '2', '手机', 'phone', 'productcate', null, 'info', 'Y', '0', 'admin', '2021-01-06 09:34:00', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('32', '1', '1-500', '1', 'salerange', null, null, 'Y', '0', 'admin', '2021-01-06 09:34:33', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('33', '2', '500-2000', '2', 'salerange', null, null, 'Y', '0', 'admin', '2021-01-06 09:34:44', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('34', '3', '2000-5000', '3', 'salerange', null, null, 'Y', '0', 'admin', '2021-01-06 09:34:57', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('35', '4', '5000以上', '4', 'salerange', null, null, 'Y', '0', 'admin', '2021-01-06 09:35:12', '', null, null);

-- ----------------------------
-- Table structure for `sys_dict_type`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type` (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`),
  UNIQUE KEY `dict_type` (`dict_type`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COMMENT='字典类型表';

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES ('1', '用户性别', 'sys_user_sex', '0', 'admin', '2020-12-28 17:13:10', '', null, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES ('2', '菜单状态', 'sys_show_hide', '0', 'admin', '2020-12-28 17:13:10', '', null, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES ('3', '系统开关', 'sys_normal_disable', '0', 'admin', '2020-12-28 17:13:10', '', null, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES ('4', '任务状态', 'sys_job_status', '0', 'admin', '2020-12-28 17:13:11', '', null, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES ('5', '任务分组', 'sys_job_group', '0', 'admin', '2020-12-28 17:13:11', '', null, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES ('6', '系统是否', 'sys_yes_no', '0', 'admin', '2020-12-28 17:13:11', '', null, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES ('7', '通知类型', 'sys_notice_type', '0', 'admin', '2020-12-28 17:13:11', '', null, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES ('8', '通知状态', 'sys_notice_status', '0', 'admin', '2020-12-28 17:13:11', '', null, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES ('9', '操作类型', 'sys_oper_type', '0', 'admin', '2020-12-28 17:13:11', '', null, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES ('10', '系统状态', 'sys_common_status', '0', 'admin', '2020-12-28 17:13:11', '', null, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES ('11', '产品类型', 'productcate', '0', 'admin', '2021-01-06 09:32:23', '', null, '报表页类型');
INSERT INTO `sys_dict_type` VALUES ('12', '销售额', 'salerange', '0', 'admin', '2021-01-06 09:33:14', '', null, '报表页类型');

-- ----------------------------
-- Table structure for `sys_job`
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`,`job_name`,`job_group`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='定时任务调度表';

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES ('1', '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2020-12-28 17:13:14', '', null, '');
INSERT INTO `sys_job` VALUES ('2', '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2020-12-28 17:13:14', '', null, '');
INSERT INTO `sys_job` VALUES ('3', '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2020-12-28 17:13:14', '', null, '');

-- ----------------------------
-- Table structure for `sys_job_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log` (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) DEFAULT NULL COMMENT '日志信息',
  `status` char(1) DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) DEFAULT '' COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='定时任务调度日志表';

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_logininfor`
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor` (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `login_name` varchar(50) DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(50) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` char(1) DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) DEFAULT '' COMMENT '提示消息',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8mb4 COMMENT='系统访问记录';

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES ('100', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '1', '密码输入错误1次', '2020-12-28 17:25:44');
INSERT INTO `sys_logininfor` VALUES ('101', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '1', '验证码错误', '2020-12-28 17:27:37');
INSERT INTO `sys_logininfor` VALUES ('102', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2020-12-28 17:27:41');
INSERT INTO `sys_logininfor` VALUES ('103', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '退出成功', '2020-12-31 11:41:34');
INSERT INTO `sys_logininfor` VALUES ('104', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '1', '验证码错误', '2020-12-31 14:20:58');
INSERT INTO `sys_logininfor` VALUES ('105', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2020-12-31 14:21:02');
INSERT INTO `sys_logininfor` VALUES ('106', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '退出成功', '2021-01-06 09:22:13');
INSERT INTO `sys_logininfor` VALUES ('107', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-06 09:22:22');
INSERT INTO `sys_logininfor` VALUES ('108', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-06 09:24:46');
INSERT INTO `sys_logininfor` VALUES ('109', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-06 09:55:14');
INSERT INTO `sys_logininfor` VALUES ('110', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '1', '验证码错误', '2021-01-06 09:58:51');
INSERT INTO `sys_logininfor` VALUES ('111', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-06 09:58:55');
INSERT INTO `sys_logininfor` VALUES ('112', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-06 10:05:31');
INSERT INTO `sys_logininfor` VALUES ('113', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-06 10:13:25');
INSERT INTO `sys_logininfor` VALUES ('114', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '1', '验证码错误', '2021-01-06 10:14:55');
INSERT INTO `sys_logininfor` VALUES ('115', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-06 10:14:57');
INSERT INTO `sys_logininfor` VALUES ('116', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-06 10:15:52');
INSERT INTO `sys_logininfor` VALUES ('117', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-06 10:17:39');
INSERT INTO `sys_logininfor` VALUES ('118', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-06 10:25:37');
INSERT INTO `sys_logininfor` VALUES ('119', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-06 10:28:30');
INSERT INTO `sys_logininfor` VALUES ('120', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-06 10:33:57');
INSERT INTO `sys_logininfor` VALUES ('121', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-06 10:34:49');
INSERT INTO `sys_logininfor` VALUES ('122', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-06 10:40:05');
INSERT INTO `sys_logininfor` VALUES ('123', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-06 10:44:37');
INSERT INTO `sys_logininfor` VALUES ('124', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-06 10:47:37');
INSERT INTO `sys_logininfor` VALUES ('125', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-06 10:53:25');
INSERT INTO `sys_logininfor` VALUES ('126', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-06 11:07:03');
INSERT INTO `sys_logininfor` VALUES ('127', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-06 17:20:50');
INSERT INTO `sys_logininfor` VALUES ('128', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-07 09:16:18');
INSERT INTO `sys_logininfor` VALUES ('129', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-07 09:26:48');
INSERT INTO `sys_logininfor` VALUES ('130', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-07 10:28:30');
INSERT INTO `sys_logininfor` VALUES ('131', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-07 10:32:40');
INSERT INTO `sys_logininfor` VALUES ('132', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '1', '验证码错误', '2021-01-07 10:35:37');
INSERT INTO `sys_logininfor` VALUES ('133', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2021-01-07 10:35:43');

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `url` varchar(200) DEFAULT '#' COMMENT '请求地址',
  `target` varchar(20) DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
  `menu_type` char(1) DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `is_refresh` char(1) DEFAULT '1' COMMENT '是否刷新（0刷新 1不刷新）',
  `perms` varchar(100) DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2064 DEFAULT CHARSET=utf8mb4 COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '系统管理', '0', '20', '#', 'menuItem', 'M', '0', '1', '', 'fa fa-gear', 'admin', '2020-12-28 17:12:58', 'admin', '2020-12-31 11:06:31', '系统管理目录');
INSERT INTO `sys_menu` VALUES ('2', '系统监控', '0', '20', '#', 'menuItem', 'M', '0', '1', '', 'fa fa-video-camera', 'admin', '2020-12-28 17:12:59', 'admin', '2020-12-31 11:06:38', '系统监控目录');
INSERT INTO `sys_menu` VALUES ('3', '系统工具', '0', '30', '#', 'menuItem', 'M', '0', '1', '', 'fa fa-bars', 'admin', '2020-12-28 17:12:59', 'admin', '2020-12-31 11:06:43', '系统工具目录');
INSERT INTO `sys_menu` VALUES ('4', '若依官网', '0', '40', 'http://ruoyi.vip', 'menuBlank', 'C', '0', '1', '', 'fa fa-location-arrow', 'admin', '2020-12-28 17:12:59', 'admin', '2020-12-31 11:06:50', '若依官网地址');
INSERT INTO `sys_menu` VALUES ('100', '用户管理', '1', '1', '/system/user', '', 'C', '0', '1', 'system:user:view', 'fa fa-user-o', 'admin', '2020-12-28 17:12:59', '', null, '用户管理菜单');
INSERT INTO `sys_menu` VALUES ('101', '角色管理', '1', '2', '/system/role', '', 'C', '0', '1', 'system:role:view', 'fa fa-user-secret', 'admin', '2020-12-28 17:12:59', '', null, '角色管理菜单');
INSERT INTO `sys_menu` VALUES ('102', '菜单管理', '1', '3', '/system/menu', '', 'C', '0', '1', 'system:menu:view', 'fa fa-th-list', 'admin', '2020-12-28 17:12:59', '', null, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES ('103', '部门管理', '1', '4', '/system/dept', '', 'C', '0', '1', 'system:dept:view', 'fa fa-outdent', 'admin', '2020-12-28 17:12:59', '', null, '部门管理菜单');
INSERT INTO `sys_menu` VALUES ('104', '岗位管理', '1', '5', '/system/post', '', 'C', '0', '1', 'system:post:view', 'fa fa-address-card-o', 'admin', '2020-12-28 17:12:59', '', null, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES ('105', '字典管理', '1', '6', '/system/dict', '', 'C', '0', '1', 'system:dict:view', 'fa fa-bookmark-o', 'admin', '2020-12-28 17:12:59', '', null, '字典管理菜单');
INSERT INTO `sys_menu` VALUES ('106', '参数设置', '1', '7', '/system/config', '', 'C', '0', '1', 'system:config:view', 'fa fa-sun-o', 'admin', '2020-12-28 17:12:59', '', null, '参数设置菜单');
INSERT INTO `sys_menu` VALUES ('107', '通知公告', '1', '8', '/system/notice', '', 'C', '0', '1', 'system:notice:view', 'fa fa-bullhorn', 'admin', '2020-12-28 17:12:59', '', null, '通知公告菜单');
INSERT INTO `sys_menu` VALUES ('108', '日志管理', '1', '9', '#', '', 'M', '0', '1', '', 'fa fa-pencil-square-o', 'admin', '2020-12-28 17:12:59', '', null, '日志管理菜单');
INSERT INTO `sys_menu` VALUES ('109', '在线用户', '2', '1', '/monitor/online', '', 'C', '0', '1', 'monitor:online:view', 'fa fa-user-circle', 'admin', '2020-12-28 17:12:59', '', null, '在线用户菜单');
INSERT INTO `sys_menu` VALUES ('110', '定时任务', '2', '2', '/monitor/job', '', 'C', '0', '1', 'monitor:job:view', 'fa fa-tasks', 'admin', '2020-12-28 17:12:59', '', null, '定时任务菜单');
INSERT INTO `sys_menu` VALUES ('111', '数据监控', '2', '3', '/monitor/data', '', 'C', '0', '1', 'monitor:data:view', 'fa fa-bug', 'admin', '2020-12-28 17:12:59', '', null, '数据监控菜单');
INSERT INTO `sys_menu` VALUES ('112', '服务监控', '2', '4', '/monitor/server', '', 'C', '0', '1', 'monitor:server:view', 'fa fa-server', 'admin', '2020-12-28 17:12:59', '', null, '服务监控菜单');
INSERT INTO `sys_menu` VALUES ('113', '缓存监控', '2', '5', '/monitor/cache', '', 'C', '0', '1', 'monitor:cache:view', 'fa fa-cube', 'admin', '2020-12-28 17:12:59', '', null, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES ('114', '表单构建', '3', '1', '/tool/build', '', 'C', '0', '1', 'tool:build:view', 'fa fa-wpforms', 'admin', '2020-12-28 17:13:00', '', null, '表单构建菜单');
INSERT INTO `sys_menu` VALUES ('115', '代码生成', '3', '2', '/tool/gen', '', 'C', '0', '1', 'tool:gen:view', 'fa fa-code', 'admin', '2020-12-28 17:13:00', '', null, '代码生成菜单');
INSERT INTO `sys_menu` VALUES ('116', '系统接口', '3', '3', '/tool/swagger', '', 'C', '0', '1', 'tool:swagger:view', 'fa fa-gg', 'admin', '2020-12-28 17:13:00', '', null, '系统接口菜单');
INSERT INTO `sys_menu` VALUES ('500', '操作日志', '108', '1', '/monitor/operlog', '', 'C', '0', '1', 'monitor:operlog:view', 'fa fa-address-book', 'admin', '2020-12-28 17:13:00', '', null, '操作日志菜单');
INSERT INTO `sys_menu` VALUES ('501', '登录日志', '108', '2', '/monitor/logininfor', '', 'C', '0', '1', 'monitor:logininfor:view', 'fa fa-file-image-o', 'admin', '2020-12-28 17:13:00', '', null, '登录日志菜单');
INSERT INTO `sys_menu` VALUES ('1000', '用户查询', '100', '1', '#', '', 'F', '0', '1', 'system:user:list', '#', 'admin', '2020-12-28 17:13:00', '', null, '');
INSERT INTO `sys_menu` VALUES ('1001', '用户新增', '100', '2', '#', '', 'F', '0', '1', 'system:user:add', '#', 'admin', '2020-12-28 17:13:00', '', null, '');
INSERT INTO `sys_menu` VALUES ('1002', '用户修改', '100', '3', '#', '', 'F', '0', '1', 'system:user:edit', '#', 'admin', '2020-12-28 17:13:00', '', null, '');
INSERT INTO `sys_menu` VALUES ('1003', '用户删除', '100', '4', '#', '', 'F', '0', '1', 'system:user:remove', '#', 'admin', '2020-12-28 17:13:00', '', null, '');
INSERT INTO `sys_menu` VALUES ('1004', '用户导出', '100', '5', '#', '', 'F', '0', '1', 'system:user:export', '#', 'admin', '2020-12-28 17:13:00', '', null, '');
INSERT INTO `sys_menu` VALUES ('1005', '用户导入', '100', '6', '#', '', 'F', '0', '1', 'system:user:import', '#', 'admin', '2020-12-28 17:13:00', '', null, '');
INSERT INTO `sys_menu` VALUES ('1006', '重置密码', '100', '7', '#', '', 'F', '0', '1', 'system:user:resetPwd', '#', 'admin', '2020-12-28 17:13:00', '', null, '');
INSERT INTO `sys_menu` VALUES ('1007', '角色查询', '101', '1', '#', '', 'F', '0', '1', 'system:role:list', '#', 'admin', '2020-12-28 17:13:00', '', null, '');
INSERT INTO `sys_menu` VALUES ('1008', '角色新增', '101', '2', '#', '', 'F', '0', '1', 'system:role:add', '#', 'admin', '2020-12-28 17:13:00', '', null, '');
INSERT INTO `sys_menu` VALUES ('1009', '角色修改', '101', '3', '#', '', 'F', '0', '1', 'system:role:edit', '#', 'admin', '2020-12-28 17:13:00', '', null, '');
INSERT INTO `sys_menu` VALUES ('1010', '角色删除', '101', '4', '#', '', 'F', '0', '1', 'system:role:remove', '#', 'admin', '2020-12-28 17:13:00', '', null, '');
INSERT INTO `sys_menu` VALUES ('1011', '角色导出', '101', '5', '#', '', 'F', '0', '1', 'system:role:export', '#', 'admin', '2020-12-28 17:13:00', '', null, '');
INSERT INTO `sys_menu` VALUES ('1012', '菜单查询', '102', '1', '#', '', 'F', '0', '1', 'system:menu:list', '#', 'admin', '2020-12-28 17:13:01', '', null, '');
INSERT INTO `sys_menu` VALUES ('1013', '菜单新增', '102', '2', '#', '', 'F', '0', '1', 'system:menu:add', '#', 'admin', '2020-12-28 17:13:01', '', null, '');
INSERT INTO `sys_menu` VALUES ('1014', '菜单修改', '102', '3', '#', '', 'F', '0', '1', 'system:menu:edit', '#', 'admin', '2020-12-28 17:13:01', '', null, '');
INSERT INTO `sys_menu` VALUES ('1015', '菜单删除', '102', '4', '#', '', 'F', '0', '1', 'system:menu:remove', '#', 'admin', '2020-12-28 17:13:01', '', null, '');
INSERT INTO `sys_menu` VALUES ('1016', '部门查询', '103', '1', '#', '', 'F', '0', '1', 'system:dept:list', '#', 'admin', '2020-12-28 17:13:01', '', null, '');
INSERT INTO `sys_menu` VALUES ('1017', '部门新增', '103', '2', '#', '', 'F', '0', '1', 'system:dept:add', '#', 'admin', '2020-12-28 17:13:01', '', null, '');
INSERT INTO `sys_menu` VALUES ('1018', '部门修改', '103', '3', '#', '', 'F', '0', '1', 'system:dept:edit', '#', 'admin', '2020-12-28 17:13:01', '', null, '');
INSERT INTO `sys_menu` VALUES ('1019', '部门删除', '103', '4', '#', '', 'F', '0', '1', 'system:dept:remove', '#', 'admin', '2020-12-28 17:13:01', '', null, '');
INSERT INTO `sys_menu` VALUES ('1020', '岗位查询', '104', '1', '#', '', 'F', '0', '1', 'system:post:list', '#', 'admin', '2020-12-28 17:13:01', '', null, '');
INSERT INTO `sys_menu` VALUES ('1021', '岗位新增', '104', '2', '#', '', 'F', '0', '1', 'system:post:add', '#', 'admin', '2020-12-28 17:13:01', '', null, '');
INSERT INTO `sys_menu` VALUES ('1022', '岗位修改', '104', '3', '#', '', 'F', '0', '1', 'system:post:edit', '#', 'admin', '2020-12-28 17:13:01', '', null, '');
INSERT INTO `sys_menu` VALUES ('1023', '岗位删除', '104', '4', '#', '', 'F', '0', '1', 'system:post:remove', '#', 'admin', '2020-12-28 17:13:01', '', null, '');
INSERT INTO `sys_menu` VALUES ('1024', '岗位导出', '104', '5', '#', '', 'F', '0', '1', 'system:post:export', '#', 'admin', '2020-12-28 17:13:01', '', null, '');
INSERT INTO `sys_menu` VALUES ('1025', '字典查询', '105', '1', '#', '', 'F', '0', '1', 'system:dict:list', '#', 'admin', '2020-12-28 17:13:02', '', null, '');
INSERT INTO `sys_menu` VALUES ('1026', '字典新增', '105', '2', '#', '', 'F', '0', '1', 'system:dict:add', '#', 'admin', '2020-12-28 17:13:02', '', null, '');
INSERT INTO `sys_menu` VALUES ('1027', '字典修改', '105', '3', '#', '', 'F', '0', '1', 'system:dict:edit', '#', 'admin', '2020-12-28 17:13:02', '', null, '');
INSERT INTO `sys_menu` VALUES ('1028', '字典删除', '105', '4', '#', '', 'F', '0', '1', 'system:dict:remove', '#', 'admin', '2020-12-28 17:13:02', '', null, '');
INSERT INTO `sys_menu` VALUES ('1029', '字典导出', '105', '5', '#', '', 'F', '0', '1', 'system:dict:export', '#', 'admin', '2020-12-28 17:13:02', '', null, '');
INSERT INTO `sys_menu` VALUES ('1030', '参数查询', '106', '1', '#', '', 'F', '0', '1', 'system:config:list', '#', 'admin', '2020-12-28 17:13:02', '', null, '');
INSERT INTO `sys_menu` VALUES ('1031', '参数新增', '106', '2', '#', '', 'F', '0', '1', 'system:config:add', '#', 'admin', '2020-12-28 17:13:02', '', null, '');
INSERT INTO `sys_menu` VALUES ('1032', '参数修改', '106', '3', '#', '', 'F', '0', '1', 'system:config:edit', '#', 'admin', '2020-12-28 17:13:02', '', null, '');
INSERT INTO `sys_menu` VALUES ('1033', '参数删除', '106', '4', '#', '', 'F', '0', '1', 'system:config:remove', '#', 'admin', '2020-12-28 17:13:02', '', null, '');
INSERT INTO `sys_menu` VALUES ('1034', '参数导出', '106', '5', '#', '', 'F', '0', '1', 'system:config:export', '#', 'admin', '2020-12-28 17:13:02', '', null, '');
INSERT INTO `sys_menu` VALUES ('1035', '公告查询', '107', '1', '#', '', 'F', '0', '1', 'system:notice:list', '#', 'admin', '2020-12-28 17:13:02', '', null, '');
INSERT INTO `sys_menu` VALUES ('1036', '公告新增', '107', '2', '#', '', 'F', '0', '1', 'system:notice:add', '#', 'admin', '2020-12-28 17:13:02', '', null, '');
INSERT INTO `sys_menu` VALUES ('1037', '公告修改', '107', '3', '#', '', 'F', '0', '1', 'system:notice:edit', '#', 'admin', '2020-12-28 17:13:02', '', null, '');
INSERT INTO `sys_menu` VALUES ('1038', '公告删除', '107', '4', '#', '', 'F', '0', '1', 'system:notice:remove', '#', 'admin', '2020-12-28 17:13:02', '', null, '');
INSERT INTO `sys_menu` VALUES ('1039', '操作查询', '500', '1', '#', '', 'F', '0', '1', 'monitor:operlog:list', '#', 'admin', '2020-12-28 17:13:02', '', null, '');
INSERT INTO `sys_menu` VALUES ('1040', '操作删除', '500', '2', '#', '', 'F', '0', '1', 'monitor:operlog:remove', '#', 'admin', '2020-12-28 17:13:02', '', null, '');
INSERT INTO `sys_menu` VALUES ('1041', '详细信息', '500', '3', '#', '', 'F', '0', '1', 'monitor:operlog:detail', '#', 'admin', '2020-12-28 17:13:02', '', null, '');
INSERT INTO `sys_menu` VALUES ('1042', '日志导出', '500', '4', '#', '', 'F', '0', '1', 'monitor:operlog:export', '#', 'admin', '2020-12-28 17:13:02', '', null, '');
INSERT INTO `sys_menu` VALUES ('1043', '登录查询', '501', '1', '#', '', 'F', '0', '1', 'monitor:logininfor:list', '#', 'admin', '2020-12-28 17:13:03', '', null, '');
INSERT INTO `sys_menu` VALUES ('1044', '登录删除', '501', '2', '#', '', 'F', '0', '1', 'monitor:logininfor:remove', '#', 'admin', '2020-12-28 17:13:03', '', null, '');
INSERT INTO `sys_menu` VALUES ('1045', '日志导出', '501', '3', '#', '', 'F', '0', '1', 'monitor:logininfor:export', '#', 'admin', '2020-12-28 17:13:03', '', null, '');
INSERT INTO `sys_menu` VALUES ('1046', '账户解锁', '501', '4', '#', '', 'F', '0', '1', 'monitor:logininfor:unlock', '#', 'admin', '2020-12-28 17:13:03', '', null, '');
INSERT INTO `sys_menu` VALUES ('1047', '在线查询', '109', '1', '#', '', 'F', '0', '1', 'monitor:online:list', '#', 'admin', '2020-12-28 17:13:03', '', null, '');
INSERT INTO `sys_menu` VALUES ('1048', '批量强退', '109', '2', '#', '', 'F', '0', '1', 'monitor:online:batchForceLogout', '#', 'admin', '2020-12-28 17:13:03', '', null, '');
INSERT INTO `sys_menu` VALUES ('1049', '单条强退', '109', '3', '#', '', 'F', '0', '1', 'monitor:online:forceLogout', '#', 'admin', '2020-12-28 17:13:03', '', null, '');
INSERT INTO `sys_menu` VALUES ('1050', '任务查询', '110', '1', '#', '', 'F', '0', '1', 'monitor:job:list', '#', 'admin', '2020-12-28 17:13:03', '', null, '');
INSERT INTO `sys_menu` VALUES ('1051', '任务新增', '110', '2', '#', '', 'F', '0', '1', 'monitor:job:add', '#', 'admin', '2020-12-28 17:13:03', '', null, '');
INSERT INTO `sys_menu` VALUES ('1052', '任务修改', '110', '3', '#', '', 'F', '0', '1', 'monitor:job:edit', '#', 'admin', '2020-12-28 17:13:03', '', null, '');
INSERT INTO `sys_menu` VALUES ('1053', '任务删除', '110', '4', '#', '', 'F', '0', '1', 'monitor:job:remove', '#', 'admin', '2020-12-28 17:13:03', '', null, '');
INSERT INTO `sys_menu` VALUES ('1054', '状态修改', '110', '5', '#', '', 'F', '0', '1', 'monitor:job:changeStatus', '#', 'admin', '2020-12-28 17:13:03', '', null, '');
INSERT INTO `sys_menu` VALUES ('1055', '任务详细', '110', '6', '#', '', 'F', '0', '1', 'monitor:job:detail', '#', 'admin', '2020-12-28 17:13:03', '', null, '');
INSERT INTO `sys_menu` VALUES ('1056', '任务导出', '110', '7', '#', '', 'F', '0', '1', 'monitor:job:export', '#', 'admin', '2020-12-28 17:13:03', '', null, '');
INSERT INTO `sys_menu` VALUES ('1057', '生成查询', '115', '1', '#', '', 'F', '0', '1', 'tool:gen:list', '#', 'admin', '2020-12-28 17:13:03', '', null, '');
INSERT INTO `sys_menu` VALUES ('1058', '生成修改', '115', '2', '#', '', 'F', '0', '1', 'tool:gen:edit', '#', 'admin', '2020-12-28 17:13:03', '', null, '');
INSERT INTO `sys_menu` VALUES ('1059', '生成删除', '115', '3', '#', '', 'F', '0', '1', 'tool:gen:remove', '#', 'admin', '2020-12-28 17:13:03', '', null, '');
INSERT INTO `sys_menu` VALUES ('1060', '预览代码', '115', '4', '#', '', 'F', '0', '1', 'tool:gen:preview', '#', 'admin', '2020-12-28 17:13:03', '', null, '');
INSERT INTO `sys_menu` VALUES ('1061', '生成代码', '115', '5', '#', '', 'F', '0', '1', 'tool:gen:code', '#', 'admin', '2020-12-28 17:13:03', '', null, '');
INSERT INTO `sys_menu` VALUES ('2000', '数据源', '2060', '50', '/web/source', '', 'C', '0', '1', 'web:source:view', '#', 'admin', '2020-12-30 16:25:52', '', null, '数据源菜单');
INSERT INTO `sys_menu` VALUES ('2001', '数据源查询', '2000', '1', '#', '', 'F', '0', '1', 'web:source:list', '#', 'admin', '2020-12-30 16:25:53', '', null, '');
INSERT INTO `sys_menu` VALUES ('2002', '数据源新增', '2000', '2', '#', '', 'F', '0', '1', 'web:source:add', '#', 'admin', '2020-12-30 16:25:53', '', null, '');
INSERT INTO `sys_menu` VALUES ('2003', '数据源修改', '2000', '3', '#', '', 'F', '0', '1', 'web:source:edit', '#', 'admin', '2020-12-30 16:25:53', '', null, '');
INSERT INTO `sys_menu` VALUES ('2004', '数据源删除', '2000', '4', '#', '', 'F', '0', '1', 'web:source:remove', '#', 'admin', '2020-12-30 16:25:53', '', null, '');
INSERT INTO `sys_menu` VALUES ('2005', '数据源导出', '2000', '5', '#', '', 'F', '0', '1', 'web:source:export', '#', 'admin', '2020-12-30 16:25:53', '', null, '');
INSERT INTO `sys_menu` VALUES ('2006', '报表版本', '2062', '20', '/web/reporthistroy', '', 'C', '0', '1', 'web:reporthistroy:view', '#', 'admin', '2020-12-31 10:35:33', '', null, '报版本菜单');
INSERT INTO `sys_menu` VALUES ('2007', '报表版本查询', '2006', '1', '#', '', 'F', '0', '1', 'web:reporthistroy:list', '#', 'admin', '2020-12-31 10:35:33', '', null, '');
INSERT INTO `sys_menu` VALUES ('2008', '报表版本新增', '2006', '2', '#', '', 'F', '0', '1', 'web:reporthistroy:add', '#', 'admin', '2020-12-31 10:35:33', '', null, '');
INSERT INTO `sys_menu` VALUES ('2009', '报表版本修改', '2006', '3', '#', '', 'F', '0', '1', 'web:reporthistroy:edit', '#', 'admin', '2020-12-31 10:35:33', '', null, '');
INSERT INTO `sys_menu` VALUES ('2010', '报表版本删除', '2006', '4', '#', '', 'F', '0', '1', 'web:reporthistroy:remove', '#', 'admin', '2020-12-31 10:35:33', '', null, '');
INSERT INTO `sys_menu` VALUES ('2011', '报表版本导出', '2006', '5', '#', '', 'F', '0', '1', 'web:reporthistroy:export', '#', 'admin', '2020-12-31 10:35:33', '', null, '');
INSERT INTO `sys_menu` VALUES ('2012', '报表', '2062', '1', '/web/report', '', 'C', '0', '1', 'web:report:view', '#', 'admin', '2020-12-31 10:35:37', '', null, '报菜单');
INSERT INTO `sys_menu` VALUES ('2013', '报表查询', '2012', '1', '#', '', 'F', '0', '1', 'web:report:list', '#', 'admin', '2020-12-31 10:35:37', '', null, '');
INSERT INTO `sys_menu` VALUES ('2014', '报表新增', '2012', '2', '#', '', 'F', '0', '1', 'web:report:add', '#', 'admin', '2020-12-31 10:35:37', '', null, '');
INSERT INTO `sys_menu` VALUES ('2015', '报表修改', '2012', '3', '#', '', 'F', '0', '1', 'web:report:edit', '#', 'admin', '2020-12-31 10:35:37', '', null, '');
INSERT INTO `sys_menu` VALUES ('2016', '报表表删除', '2012', '4', '#', '', 'F', '0', '1', 'web:report:remove', '#', 'admin', '2020-12-31 10:35:37', '', null, '');
INSERT INTO `sys_menu` VALUES ('2017', '报表表导出', '2012', '5', '#', '', 'F', '0', '1', 'web:report:export', '#', 'admin', '2020-12-31 10:35:37', '', null, '');
INSERT INTO `sys_menu` VALUES ('2018', '报表页', '2062', '10', '/web/reporttab', '', 'C', '0', '1', 'web:reporttab:view', '#', 'admin', '2020-12-31 10:35:40', '', null, '报页菜单');
INSERT INTO `sys_menu` VALUES ('2019', '报表页查询', '2018', '1', '#', '', 'F', '0', '1', 'web:reporttab:list', '#', 'admin', '2020-12-31 10:35:40', '', null, '');
INSERT INTO `sys_menu` VALUES ('2020', '报表页新增', '2018', '2', '#', '', 'F', '0', '1', 'web:reporttab:add', '#', 'admin', '2020-12-31 10:35:40', '', null, '');
INSERT INTO `sys_menu` VALUES ('2021', '报表页修改', '2018', '3', '#', '', 'F', '0', '1', 'web:reporttab:edit', '#', 'admin', '2020-12-31 10:35:40', '', null, '');
INSERT INTO `sys_menu` VALUES ('2022', '报表页删除', '2018', '4', '#', '', 'F', '0', '1', 'web:reporttab:remove', '#', 'admin', '2020-12-31 10:35:40', '', null, '');
INSERT INTO `sys_menu` VALUES ('2023', '报页导出', '2018', '5', '#', '', 'F', '0', '1', 'web:reporttab:export', '#', 'admin', '2020-12-31 10:35:40', '', null, '');
INSERT INTO `sys_menu` VALUES ('2024', '批处理计划', '2063', '1', '/web/scheduletask', '', 'C', '0', '1', 'web:scheduletask:view', '#', 'admin', '2020-12-31 10:35:43', '', null, '批处理计划菜单');
INSERT INTO `sys_menu` VALUES ('2025', '批处理计划查询', '2024', '1', '#', '', 'F', '0', '1', 'web:scheduletask:list', '#', 'admin', '2020-12-31 10:35:44', '', null, '');
INSERT INTO `sys_menu` VALUES ('2026', '批处理计划新增', '2024', '2', '#', '', 'F', '0', '1', 'web:scheduletask:add', '#', 'admin', '2020-12-31 10:35:44', '', null, '');
INSERT INTO `sys_menu` VALUES ('2027', '批处理计划修改', '2024', '3', '#', '', 'F', '0', '1', 'web:scheduletask:edit', '#', 'admin', '2020-12-31 10:35:44', '', null, '');
INSERT INTO `sys_menu` VALUES ('2028', '批处理计划删除', '2024', '4', '#', '', 'F', '0', '1', 'web:scheduletask:remove', '#', 'admin', '2020-12-31 10:35:44', '', null, '');
INSERT INTO `sys_menu` VALUES ('2029', '批处理计划导出', '2024', '5', '#', '', 'F', '0', '1', 'web:scheduletask:export', '#', 'admin', '2020-12-31 10:35:44', '', null, '');
INSERT INTO `sys_menu` VALUES ('2030', '站点历史', '2060', '20', '/web/sitehistroy', '', 'C', '0', '1', 'web:sitehistroy:view', '#', 'admin', '2020-12-31 10:35:47', '', null, '站点历史菜单');
INSERT INTO `sys_menu` VALUES ('2031', '站点历史查询', '2030', '1', '#', '', 'F', '0', '1', 'web:sitehistroy:list', '#', 'admin', '2020-12-31 10:35:47', '', null, '');
INSERT INTO `sys_menu` VALUES ('2032', '站点历史新增', '2030', '2', '#', '', 'F', '0', '1', 'web:sitehistroy:add', '#', 'admin', '2020-12-31 10:35:48', '', null, '');
INSERT INTO `sys_menu` VALUES ('2033', '站点历史修改', '2030', '3', '#', '', 'F', '0', '1', 'web:sitehistroy:edit', '#', 'admin', '2020-12-31 10:35:48', '', null, '');
INSERT INTO `sys_menu` VALUES ('2034', '站点历史删除', '2030', '4', '#', '', 'F', '0', '1', 'web:sitehistroy:remove', '#', 'admin', '2020-12-31 10:35:48', '', null, '');
INSERT INTO `sys_menu` VALUES ('2035', '站点历史导出', '2030', '5', '#', '', 'F', '0', '1', 'web:sitehistroy:export', '#', 'admin', '2020-12-31 10:35:48', '', null, '');
INSERT INTO `sys_menu` VALUES ('2036', '站点', '2060', '1', '/web/site', '', 'C', '0', '1', 'web:site:view', '#', 'admin', '2020-12-31 10:35:52', '', null, '站点菜单');
INSERT INTO `sys_menu` VALUES ('2037', '站点查询', '2036', '1', '#', '', 'F', '0', '1', 'web:site:list', '#', 'admin', '2020-12-31 10:35:52', '', null, '');
INSERT INTO `sys_menu` VALUES ('2038', '站点新增', '2036', '2', '#', '', 'F', '0', '1', 'web:site:add', '#', 'admin', '2020-12-31 10:35:52', '', null, '');
INSERT INTO `sys_menu` VALUES ('2039', '站点修改', '2036', '3', '#', '', 'F', '0', '1', 'web:site:edit', '#', 'admin', '2020-12-31 10:35:52', '', null, '');
INSERT INTO `sys_menu` VALUES ('2040', '站点删除', '2036', '4', '#', '', 'F', '0', '1', 'web:site:remove', '#', 'admin', '2020-12-31 10:35:52', '', null, '');
INSERT INTO `sys_menu` VALUES ('2041', '站点导出', '2036', '5', '#', '', 'F', '0', '1', 'web:site:export', '#', 'admin', '2020-12-31 10:35:52', '', null, '');
INSERT INTO `sys_menu` VALUES ('2048', '站点菜单', '2060', '10', '/web/sitemenu', 'menuItem', 'C', '1', '1', 'web:sitemenu:view', '#', 'admin', '2020-12-31 10:35:58', 'admin', '2021-01-07 09:31:43', '站点菜单菜单');
INSERT INTO `sys_menu` VALUES ('2049', '站点菜单查询', '2048', '1', '#', '', 'F', '0', '1', 'web:sitemenu:list', '#', 'admin', '2020-12-31 10:35:58', '', null, '');
INSERT INTO `sys_menu` VALUES ('2050', '站点菜单新增', '2048', '2', '#', '', 'F', '0', '1', 'web:sitemenu:add', '#', 'admin', '2020-12-31 10:35:58', '', null, '');
INSERT INTO `sys_menu` VALUES ('2051', '站点菜单修改', '2048', '3', '#', '', 'F', '0', '1', 'web:sitemenu:edit', '#', 'admin', '2020-12-31 10:35:58', '', null, '');
INSERT INTO `sys_menu` VALUES ('2052', '站点菜单删除', '2048', '4', '#', '', 'F', '0', '1', 'web:sitemenu:remove', '#', 'admin', '2020-12-31 10:35:58', '', null, '');
INSERT INTO `sys_menu` VALUES ('2053', '站点菜单导出', '2048', '5', '#', '', 'F', '0', '1', 'web:sitemenu:export', '#', 'admin', '2020-12-31 10:35:58', '', null, '');
INSERT INTO `sys_menu` VALUES ('2060', '数据站点', '0', '1', '#', 'menuItem', 'M', '0', '1', null, 'fa fa-university', 'admin', '2020-12-31 11:06:01', '', null, '');
INSERT INTO `sys_menu` VALUES ('2061', 'databand站点', '0', '39', 'https://gitee.com/475660/databand', 'menuBlank', 'C', '0', '1', null, 'fa fa-code-fork', 'admin', '2020-12-31 11:08:24', '', null, '');
INSERT INTO `sys_menu` VALUES ('2062', '数据报表', '0', '2', '#', 'menuItem', 'M', '0', '1', null, 'fa fa-newspaper-o', 'admin', '2020-12-31 11:12:20', '', null, '');
INSERT INTO `sys_menu` VALUES ('2063', '任务中心', '0', '10', '#', 'menuItem', 'M', '0', '1', '', 'fa fa-calendar', 'admin', '2020-12-31 11:13:06', 'admin', '2020-12-31 11:15:24', '');

-- ----------------------------
-- Table structure for `sys_notice`
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) NOT NULL COMMENT '公告标题',
  `notice_type` char(1) NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` varchar(2000) DEFAULT NULL COMMENT '公告内容',
  `status` char(1) DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='通知公告表';

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES ('1', '温馨提醒：2018-07-01 若依新版本发布啦', '2', '新版本内容', '0', 'admin', '2020-12-28 17:13:15', '', null, '管理员');
INSERT INTO `sys_notice` VALUES ('2', '维护通知：2018-07-01 若依系统凌晨维护', '1', '维护内容', '0', 'admin', '2020-12-28 17:13:15', '', null, '管理员');

-- ----------------------------
-- Table structure for `sys_oper_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log` (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(50) DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) DEFAULT '' COMMENT '返回参数',
  `status` int(1) DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`)
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=utf8mb4 COMMENT='操作日志记录';

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES ('131', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateAvatar()', 'POST', '1', 'admin', '研发部门', '/system/user/profile/updateAvatar', '127.0.0.1', '内网IP', null, '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2020-12-31 14:31:45');
INSERT INTO `sys_oper_log` VALUES ('132', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.update()', 'POST', '1', 'admin', '研发部门', '/system/user/profile/update', '127.0.0.1', '内网IP', '{\"id\":[\"\"],\"userName\":[\"admin\"],\"phonenumber\":[\"13710637136\"],\"email\":[\"475660@qq.com\"],\"sex\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2020-12-31 14:32:18');
INSERT INTO `sys_oper_log` VALUES ('133', '站点', '1', 'com.ruoyi.web.controller.databand.DatabandSiteController.addSave()', 'POST', '1', 'admin', '研发部门', '/web/site/add', '127.0.0.1', '内网IP', '{\"deptid\":[\"103\"],\"code\":[\"111\"],\"title\":[\"33\"],\"deptName\":[\"研发部门\"],\"roleid\":[\"\"],\"templateid\":[\"\"],\"url\":[\"\"],\"homepage\":[\"\"],\"descri\":[\"\"],\"action\":[\"\"],\"icon\":[\"fa fa-american-sign-language-interpreting\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2020-12-31 15:14:40');
INSERT INTO `sys_oper_log` VALUES ('134', '站点', '3', 'com.ruoyi.web.controller.databand.DatabandSiteController.remove()', 'POST', '1', 'admin', '研发部门', '/web/site/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2020-12-31 15:14:51');
INSERT INTO `sys_oper_log` VALUES ('135', '部门管理', '2', 'com.ruoyi.web.controller.system.SysDeptController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/dept/edit', '127.0.0.1', '内网IP', '{\"deptId\":[\"100\"],\"parentId\":[\"0\"],\"parentName\":[\"无\"],\"deptName\":[\"总集团\"],\"orderNum\":[\"0\"],\"leader\":[\"\"],\"phone\":[\"\"],\"email\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2020-12-31 15:16:47');
INSERT INTO `sys_oper_log` VALUES ('136', '站点', '1', 'com.ruoyi.web.controller.databand.DatabandSiteController.addSave()', 'POST', '1', 'admin', '研发部门', '/web/site/add', '127.0.0.1', '内网IP', '{\"deptid\":[\"104\"],\"code\":[\"a\"],\"title\":[\"a\"],\"deptName\":[\"市场部门\"],\"url\":[\"\"],\"icon\":[\"fa fa-asl-interpreting\"],\"descri\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2020-12-31 16:00:00');
INSERT INTO `sys_oper_log` VALUES ('137', '站点', '2', 'com.ruoyi.web.controller.databand.DatabandSiteController.editSave()', 'POST', '1', 'admin', '研发部门', '/web/site/edit', '127.0.0.1', '内网IP', '{\"id\":[\"2\"],\"deptid\":[\"103\"],\"code\":[\"a1\"],\"title\":[\"aa\"],\"deptName\":[\"研发部门\"],\"url\":[\"/wangxin\"],\"icon\":[\"fa fa-asl-interpreting\"],\"descri\":[\"asdf\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2020-12-31 16:15:20');
INSERT INTO `sys_oper_log` VALUES ('138', '站点', '3', 'com.ruoyi.web.controller.databand.DatabandSiteController.remove()', 'POST', '1', 'admin', '研发部门', '/web/site/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2020-12-31 16:17:17');
INSERT INTO `sys_oper_log` VALUES ('139', '站点历史', '1', 'com.ruoyi.web.controller.databand.DatabandSitehistroyController.addSave()', 'POST', '1', 'admin', '研发部门', '/web/sitehistroy/add', '127.0.0.1', '内网IP', '{\"siteid\":[\"1\"],\"obj\":[\"诗圣杜甫\"],\"updatime\":[\"2020-12-01\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2020-12-31 16:20:02');
INSERT INTO `sys_oper_log` VALUES ('140', '站点历史', '1', 'com.ruoyi.web.controller.databand.DatabandSitehistroyController.addSave()', 'POST', '1', 'admin', '研发部门', '/web/sitehistroy/add', '127.0.0.1', '内网IP', '{\"siteid\":[\"44\"],\"obj\":[\"对方\"],\"updatime\":[\"2020-12-01\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2020-12-31 16:20:20');
INSERT INTO `sys_oper_log` VALUES ('141', '站点历史', '1', 'com.ruoyi.web.controller.databand.DatabandSitehistroyController.addSave()', 'POST', '1', 'admin', '研发部门', '/web/sitehistroy/add', '127.0.0.1', '内网IP', '{\"siteid\":[\"2\"],\"obj\":[\"二天3\"],\"updatime\":[\"\"],\"updateby\":[\"q\"]}', 'null', '1', '\r\n### Error updating database.  Cause: java.sql.SQLException: Incorrect integer value: \'q\' for column \'updateby\' at row 1\r\n### The error may exist in file [D:\\DataBandSrc\\databand\\databand-admin-thymeleaf\\target\\classes\\mapper\\web\\DatabandSitehistroyMapper.xml]\r\n### The error may involve com.ruoyi.web.mapper.DatabandSitehistroyMapper.insertDatabandSitehistroy-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into databand_sitehistroy          ( siteid,             obj,                          updateby )           values ( ?,             ?,                          ? )\r\n### Cause: java.sql.SQLException: Incorrect integer value: \'q\' for column \'updateby\' at row 1\n; uncategorized SQLException; SQL state [HY000]; error code [1366]; Incorrect integer value: \'q\' for column \'updateby\' at row 1; nested exception is java.sql.SQLException: Incorrect integer value: \'q\' for column \'updateby\' at row 1', '2020-12-31 16:25:35');
INSERT INTO `sys_oper_log` VALUES ('142', '站点历史', '1', 'com.ruoyi.web.controller.databand.DatabandSitehistroyController.addSave()', 'POST', '1', 'admin', '研发部门', '/web/sitehistroy/add', '127.0.0.1', '内网IP', '{\"siteid\":[\"2\"],\"obj\":[\"2\"],\"updatime\":[\"\"],\"updateby\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2020-12-31 16:26:12');
INSERT INTO `sys_oper_log` VALUES ('143', '站点历史', '1', 'com.ruoyi.web.controller.databand.DatabandSitehistroyController.addSave()', 'POST', '1', 'admin', '研发部门', '/web/sitehistroy/add', '127.0.0.1', '内网IP', '{\"updateby\":[\"1\"],\"siteid\":[\"9\"],\"obj\":[\"\"],\"updatime\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2020-12-31 16:38:09');
INSERT INTO `sys_oper_log` VALUES ('144', '站点历史', '3', 'com.ruoyi.web.controller.databand.DatabandSitehistroyController.remove()', 'POST', '1', 'admin', '研发部门', '/web/sitehistroy/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"1,2,3,4\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2020-12-31 16:45:52');
INSERT INTO `sys_oper_log` VALUES ('145', '报', '1', 'com.ruoyi.web.controller.databand.DatabandReportController.addSave()', 'POST', '1', 'admin', '研发部门', '/web/report/add', '127.0.0.1', '内网IP', '{\"title\":[\"asdf\"],\"subtitle\":[\"asf\"],\"descri\":[\"\"],\"reporturl\":[\"\"],\"daterange\":[\"\"],\"options\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-04 17:36:21');
INSERT INTO `sys_oper_log` VALUES ('146', '报', '1', 'com.ruoyi.web.controller.databand.DatabandReportController.addSave()', 'POST', '1', 'admin', '研发部门', '/web/report/add', '127.0.0.1', '内网IP', '{\"title\":[\"啊啊\"],\"subtitle\":[\"方式\"],\"descri\":[\"去\"],\"reporturl\":[\"\"],\"daterange\":[\"2\"],\"options\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-04 17:37:17');
INSERT INTO `sys_oper_log` VALUES ('147', '报', '3', 'com.ruoyi.web.controller.databand.DatabandReportController.remove()', 'POST', '1', 'admin', '研发部门', '/web/report/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-04 17:40:20');
INSERT INTO `sys_oper_log` VALUES ('148', '数据源', '2', 'com.ruoyi.web.controller.databand.DatabandSourceController.editSave()', 'POST', '1', 'admin', '研发部门', '/web/source/edit', '127.0.0.1', '内网IP', '{\"id\":[\"2\"],\"title\":[\"产品分析源-mysql\"],\"drive\":[\"com.mysql.jdbc.Driver\"],\"conn\":[\"jdbc:mysql://localhost:3306/databand?useUnicode=true&characterEncoding=utf8&useSSL=true\"],\"username\":[\"root\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-05 10:38:30');
INSERT INTO `sys_oper_log` VALUES ('149', '报', '2', 'com.ruoyi.web.controller.databand.DatabandReportController.editSave()', 'POST', '1', 'admin', '研发部门', '/web/report/edit', '127.0.0.1', '内网IP', '{\"id\":[\"2\"],\"title\":[\"啊啊报表\"],\"subtitle\":[\"方式\"],\"descri\":[\"去\"],\"reporturl\":[\"\"],\"daterange\":[\"2\"],\"options\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-05 10:39:10');
INSERT INTO `sys_oper_log` VALUES ('150', '站点', '1', 'com.ruoyi.web.controller.databand.DatabandSiteController.addSave()', 'POST', '1', 'admin', '研发部门', '/web/site/add', '127.0.0.1', '内网IP', '{\"deptid\":[\"\"],\"code\":[\"demo\"],\"title\":[\"demo站点\"],\"deptName\":[\"\"],\"url\":[\"/demo\"],\"icon\":[\"fa fa-address-book\"],\"descri\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-05 10:54:07');
INSERT INTO `sys_oper_log` VALUES ('151', '报', '1', 'com.ruoyi.web.controller.databand.DatabandReportController.addSave()', 'POST', '1', 'admin', '研发部门', '/web/report/add', '127.0.0.1', '内网IP', '{\"title\":[\"\"],\"subtitle\":[\"\"],\"descri\":[\"\"],\"reporturl\":[\"\"],\"daterange\":[\"1\"],\"options\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-05 11:37:45');
INSERT INTO `sys_oper_log` VALUES ('152', '报', '3', 'com.ruoyi.web.controller.databand.DatabandReportController.remove()', 'POST', '1', 'admin', '研发部门', '/web/report/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"3\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-05 11:37:48');
INSERT INTO `sys_oper_log` VALUES ('153', '报页', '1', 'com.ruoyi.web.controller.databand.DatabandReporttabController.addSave()', 'POST', '1', 'admin', '研发部门', '/web/reporttab/add', '127.0.0.1', '内网IP', '{\"reportid\":[\"2\"],\"title\":[\"asdfasf\"],\"sql\":[\"\"],\"listsql\":[\"\"],\"sortnum\":[\"1\"],\"sourceid\":[\"2\"]}', 'null', '1', '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql,\n            listsql,\n            sortnum,\n            sourceid ) \n         \' at line 4\r\n### The error may exist in file [D:\\DataBandSrc\\databand\\databand-admin-thymeleaf\\target\\classes\\mapper\\web\\DatabandReporttabMapper.xml]\r\n### The error may involve com.ruoyi.web.mapper.DatabandReporttabMapper.insertDatabandReporttab-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into databand_reporttab          ( reportid,             title,             sql,             listsql,             sortnum,             sourceid )           values ( ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql,\n            listsql,\n            sortnum,\n            sourceid ) \n         \' at line 4\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql,\n            listsql,\n            sortnum,\n            sourceid ) \n         \' at line 4', '2021-01-05 11:38:05');
INSERT INTO `sys_oper_log` VALUES ('154', '报页', '1', 'com.ruoyi.web.controller.databand.DatabandReporttabController.addSave()', 'POST', '1', 'admin', '研发部门', '/web/reporttab/add', '127.0.0.1', '内网IP', '{\"reportid\":[\"2\"],\"title\":[\"示例执行器\"],\"sql\":[\"\"],\"listsql\":[\"\"],\"sortnum\":[\"1\"],\"sourceid\":[\"2\"]}', 'null', '1', '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql,\n            listsql,\n            sortnum,\n            sourceid ) \n         \' at line 4\r\n### The error may exist in file [D:\\DataBandSrc\\databand\\databand-admin-thymeleaf\\target\\classes\\mapper\\web\\DatabandReporttabMapper.xml]\r\n### The error may involve com.ruoyi.web.mapper.DatabandReporttabMapper.insertDatabandReporttab-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into databand_reporttab          ( reportid,             title,             sql,             listsql,             sortnum,             sourceid )           values ( ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql,\n            listsql,\n            sortnum,\n            sourceid ) \n         \' at line 4\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql,\n            listsql,\n            sortnum,\n            sourceid ) \n         \' at line 4', '2021-01-05 14:37:40');
INSERT INTO `sys_oper_log` VALUES ('155', '报页', '1', 'com.ruoyi.web.controller.databand.DatabandReporttabController.addSave()', 'POST', '1', 'admin', '研发部门', '/web/reporttab/add', '127.0.0.1', '内网IP', '{\"reportid\":[\"2\"],\"title\":[\"示例执行器\"],\"sql\":[\"qq\"],\"listsql\":[\"qqq\"],\"sortnum\":[\"1\"],\"sourceid\":[\"2\"]}', 'null', '1', '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql,\n            listsql,\n            sortnum,\n            sourceid ) \n         \' at line 4\r\n### The error may exist in file [D:\\DataBandSrc\\databand\\databand-admin-thymeleaf\\target\\classes\\mapper\\web\\DatabandReporttabMapper.xml]\r\n### The error may involve com.ruoyi.web.mapper.DatabandReporttabMapper.insertDatabandReporttab-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into databand_reporttab          ( reportid,             title,             sql,             listsql,             sortnum,             sourceid )           values ( ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql,\n            listsql,\n            sortnum,\n            sourceid ) \n         \' at line 4\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql,\n            listsql,\n            sortnum,\n            sourceid ) \n         \' at line 4', '2021-01-05 14:37:58');
INSERT INTO `sys_oper_log` VALUES ('156', '报页', '1', 'com.ruoyi.web.controller.databand.DatabandReporttabController.addSave()', 'POST', '1', 'admin', '研发部门', '/web/reporttab/add', '127.0.0.1', '内网IP', '{\"reportid\":[\"2\"],\"title\":[\"示例执行器\"],\"sql\":[\"qq\"],\"listsql\":[\"qqq\"],\"sortnum\":[\"1\"],\"sourceid\":[\"2\"]}', 'null', '1', '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql,\n            listsql,\n            sortnum,\n            sourceid ) \n         \' at line 4\r\n### The error may exist in file [D:\\DataBandSrc\\databand\\databand-admin-thymeleaf\\target\\classes\\mapper\\web\\DatabandReporttabMapper.xml]\r\n### The error may involve com.ruoyi.web.mapper.DatabandReporttabMapper.insertDatabandReporttab-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into databand_reporttab          ( reportid,             title,             sql,             listsql,             sortnum,             sourceid )           values ( ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql,\n            listsql,\n            sortnum,\n            sourceid ) \n         \' at line 4\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql,\n            listsql,\n            sortnum,\n            sourceid ) \n         \' at line 4', '2021-01-05 14:38:39');
INSERT INTO `sys_oper_log` VALUES ('157', '报页', '1', 'com.ruoyi.web.controller.databand.DatabandReporttabController.addSave()', 'POST', '1', 'admin', '研发部门', '/web/reporttab/add', '127.0.0.1', '内网IP', '{\"reportid\":[\"2\"],\"title\":[\"大数据执行器\"],\"sql\":[\"啊啊\"],\"listsql\":[\"啊啊的\"],\"sortnum\":[\"2\"],\"sourceid\":[\"2\"]}', 'null', '1', '\r\n### Error updating database.  Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql,\n            listsql,\n            sortnum,\n            sourceid ) \n         \' at line 4\r\n### The error may exist in file [D:\\DataBandSrc\\databand\\databand-admin-thymeleaf\\target\\classes\\mapper\\web\\DatabandReporttabMapper.xml]\r\n### The error may involve com.ruoyi.web.mapper.DatabandReporttabMapper.insertDatabandReporttab-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into databand_reporttab          ( reportid,             title,             sql,             listsql,             sortnum,             sourceid )           values ( ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql,\n            listsql,\n            sortnum,\n            sourceid ) \n         \' at line 4\n; bad SQL grammar []; nested exception is java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'sql,\n            listsql,\n            sortnum,\n            sourceid ) \n         \' at line 4', '2021-01-05 14:39:57');
INSERT INTO `sys_oper_log` VALUES ('158', '报页', '1', 'com.ruoyi.web.controller.databand.DatabandReporttabController.addSave()', 'POST', '1', 'admin', '研发部门', '/web/reporttab/add', '127.0.0.1', '内网IP', '{\"reportid\":[\"2\"],\"title\":[\"示例执行器\"],\"sql\":[\"gghjg\"],\"listsql\":[\"\"],\"sortnum\":[\"5\"],\"sourceid\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-06 09:25:07');
INSERT INTO `sys_oper_log` VALUES ('159', '字典类型', '1', 'com.ruoyi.web.controller.system.SysDictTypeController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/add', '127.0.0.1', '内网IP', '{\"dictName\":[\"产品类型\"],\"dictType\":[\"productcate\"],\"status\":[\"0\"],\"remark\":[\"报表页类型\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-06 09:32:23');
INSERT INTO `sys_oper_log` VALUES ('160', '字典类型', '1', 'com.ruoyi.web.controller.system.SysDictTypeController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/add', '127.0.0.1', '内网IP', '{\"dictName\":[\"销售额\"],\"dictType\":[\"salerange\"],\"status\":[\"0\"],\"remark\":[\"报表页类型\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-06 09:33:14');
INSERT INTO `sys_oper_log` VALUES ('161', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\"dictLabel\":[\"PC\"],\"dictValue\":[\"PC\"],\"dictType\":[\"productcate\"],\"cssClass\":[\"\"],\"dictSort\":[\"1\"],\"listClass\":[\"success\"],\"isDefault\":[\"Y\"],\"status\":[\"0\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-06 09:33:38');
INSERT INTO `sys_oper_log` VALUES ('162', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\"dictLabel\":[\"手机\"],\"dictValue\":[\"phone\"],\"dictType\":[\"productcate\"],\"cssClass\":[\"\"],\"dictSort\":[\"2\"],\"listClass\":[\"info\"],\"isDefault\":[\"Y\"],\"status\":[\"0\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-06 09:34:00');
INSERT INTO `sys_oper_log` VALUES ('163', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\"dictLabel\":[\"1-500\"],\"dictValue\":[\"1\"],\"dictType\":[\"salerange\"],\"cssClass\":[\"\"],\"dictSort\":[\"1\"],\"listClass\":[\"\"],\"isDefault\":[\"Y\"],\"status\":[\"0\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-06 09:34:33');
INSERT INTO `sys_oper_log` VALUES ('164', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\"dictLabel\":[\"500-2000\"],\"dictValue\":[\"2\"],\"dictType\":[\"salerange\"],\"cssClass\":[\"\"],\"dictSort\":[\"2\"],\"listClass\":[\"\"],\"isDefault\":[\"Y\"],\"status\":[\"0\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-06 09:34:44');
INSERT INTO `sys_oper_log` VALUES ('165', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\"dictLabel\":[\"2000-5000\"],\"dictValue\":[\"3\"],\"dictType\":[\"salerange\"],\"cssClass\":[\"\"],\"dictSort\":[\"3\"],\"listClass\":[\"\"],\"isDefault\":[\"Y\"],\"status\":[\"0\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-06 09:34:57');
INSERT INTO `sys_oper_log` VALUES ('166', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', '1', 'admin', '研发部门', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\"dictLabel\":[\"5000以上\"],\"dictValue\":[\"4\"],\"dictType\":[\"salerange\"],\"cssClass\":[\"\"],\"dictSort\":[\"4\"],\"listClass\":[\"\"],\"isDefault\":[\"Y\"],\"status\":[\"0\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-06 09:35:12');
INSERT INTO `sys_oper_log` VALUES ('167', '报', '2', 'com.ruoyi.web.controller.databand.DatabandReportController.editSave()', 'POST', '1', 'admin', '研发部门', '/web/report/edit', '127.0.0.1', '内网IP', '{\"id\":[\"2\"],\"title\":[\"测试报表\"],\"subtitle\":[\"测试报表子标题\"],\"descri\":[\"\"],\"reporturl\":[\"/demoreport\"],\"daterange\":[\"2\"],\"options\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-06 09:36:48');
INSERT INTO `sys_oper_log` VALUES ('168', '报页', '2', 'com.ruoyi.web.controller.databand.DatabandReporttabController.editSave()', 'POST', '1', 'admin', '研发部门', '/web/reporttab/edit', '127.0.0.1', '内网IP', '{\"id\":[\"1\"],\"reportid\":[\"2\"],\"title\":[\"报表页一\"],\"sql\":[\"\"],\"listsql\":[\"\"],\"sortnum\":[\"1\"],\"sourceid\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-06 09:45:36');
INSERT INTO `sys_oper_log` VALUES ('169', '报页', '2', 'com.ruoyi.web.controller.databand.DatabandReporttabController.editSave()', 'POST', '1', 'admin', '研发部门', '/web/reporttab/edit', '127.0.0.1', '内网IP', '{\"id\":[\"2\"],\"reportid\":[\"2\"],\"title\":[\"报表页二\"],\"sql\":[\"\"],\"listsql\":[\"\"],\"sortnum\":[\"2\"],\"sourceid\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-06 09:45:52');
INSERT INTO `sys_oper_log` VALUES ('170', '报页', '1', 'com.ruoyi.web.controller.databand.DatabandReporttabController.addSave()', 'POST', '1', 'admin', '研发部门', '/web/reporttab/add', '127.0.0.1', '内网IP', '{\"reportid\":[\"2\"],\"title\":[\"页三\"],\"sql\":[\"\"],\"listsql\":[\"\"],\"apiurl\":[\"/getdata\"],\"apiparam\":[\"productcate={productcate}&city={city}\"],\"apitag\":[\"\"],\"sortnum\":[\"3\"],\"sourceid\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-06 11:08:14');
INSERT INTO `sys_oper_log` VALUES ('171', '站点菜单', '1', 'com.ruoyi.web.controller.databand.DatabandSitemenuController.addSave()', 'POST', '1', 'admin', '研发部门', '/web/sitemenu/add', '127.0.0.1', '内网IP', '{\"siteid\":[\"\"],\"menuname\":[\"产品销售分析\"],\"sort\":[\"1\"],\"parentid\":[\"\"],\"icon\":[\"fa fa-area-chart\"],\"nodetype\":[\"1\"],\"reportid\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-06 11:13:02');
INSERT INTO `sys_oper_log` VALUES ('172', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'GET', '1', 'admin', '研发部门', '/system/menu/remove/2048', '127.0.0.1', '内网IP', null, '{\r\n  \"msg\" : \"存在子菜单,不允许删除\",\r\n  \"code\" : 301\r\n}', '0', null, '2021-01-07 09:31:24');
INSERT INTO `sys_oper_log` VALUES ('173', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', '1', 'admin', '研发部门', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2048\"],\"parentId\":[\"2060\"],\"menuType\":[\"C\"],\"menuName\":[\"站点菜单\"],\"url\":[\"/web/sitemenu\"],\"target\":[\"menuItem\"],\"perms\":[\"web:sitemenu:view\"],\"orderNum\":[\"10\"],\"icon\":[\"#\"],\"visible\":[\"1\"],\"isRefresh\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-07 09:31:43');
INSERT INTO `sys_oper_log` VALUES ('174', '站点菜单', '2', 'com.ruoyi.web.controller.databand.DatabandSitemenuController.editSave()', 'POST', '1', 'admin', '研发部门', '/web/sitemenu/edit', '127.0.0.1', '内网IP', '{\"id\":[\"1\"],\"siteid\":[\"1\"],\"menuname\":[\"产品销售分析\"],\"sort\":[\"1\"],\"parentid\":[\"\"],\"icon\":[\"fa fa-area-chart\"],\"nodetype\":[\"1\"],\"reportid\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', '0', null, '2021-01-07 10:42:20');

-- ----------------------------
-- Table structure for `sys_post`
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post` (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='岗位信息表';

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES ('1', 'ceo', '董事长', '1', '0', 'admin', '2020-12-28 17:12:57', '', null, '');
INSERT INTO `sys_post` VALUES ('2', 'se', '项目经理', '2', '0', 'admin', '2020-12-28 17:12:57', '', null, '');
INSERT INTO `sys_post` VALUES ('3', 'hr', '人力资源', '3', '0', 'admin', '2020-12-28 17:12:58', '', null, '');
INSERT INTO `sys_post` VALUES ('4', 'user', '普通员工', '4', '0', 'admin', '2020-12-28 17:12:58', '', null, '');

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `status` char(1) NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='角色信息表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级管理员', 'admin', '1', '1', '0', '0', 'admin', '2020-12-28 17:12:58', '', null, '超级管理员');
INSERT INTO `sys_role` VALUES ('2', '普通角色', 'common', '2', '2', '0', '0', 'admin', '2020-12-28 17:12:58', '', null, '普通角色');

-- ----------------------------
-- Table structure for `sys_role_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`,`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色和部门关联表';

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES ('2', '100');
INSERT INTO `sys_role_dept` VALUES ('2', '101');
INSERT INTO `sys_role_dept` VALUES ('2', '105');

-- ----------------------------
-- Table structure for `sys_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('2', '1');
INSERT INTO `sys_role_menu` VALUES ('2', '2');
INSERT INTO `sys_role_menu` VALUES ('2', '3');
INSERT INTO `sys_role_menu` VALUES ('2', '4');
INSERT INTO `sys_role_menu` VALUES ('2', '100');
INSERT INTO `sys_role_menu` VALUES ('2', '101');
INSERT INTO `sys_role_menu` VALUES ('2', '102');
INSERT INTO `sys_role_menu` VALUES ('2', '103');
INSERT INTO `sys_role_menu` VALUES ('2', '104');
INSERT INTO `sys_role_menu` VALUES ('2', '105');
INSERT INTO `sys_role_menu` VALUES ('2', '106');
INSERT INTO `sys_role_menu` VALUES ('2', '107');
INSERT INTO `sys_role_menu` VALUES ('2', '108');
INSERT INTO `sys_role_menu` VALUES ('2', '109');
INSERT INTO `sys_role_menu` VALUES ('2', '110');
INSERT INTO `sys_role_menu` VALUES ('2', '111');
INSERT INTO `sys_role_menu` VALUES ('2', '112');
INSERT INTO `sys_role_menu` VALUES ('2', '113');
INSERT INTO `sys_role_menu` VALUES ('2', '114');
INSERT INTO `sys_role_menu` VALUES ('2', '115');
INSERT INTO `sys_role_menu` VALUES ('2', '116');
INSERT INTO `sys_role_menu` VALUES ('2', '500');
INSERT INTO `sys_role_menu` VALUES ('2', '501');
INSERT INTO `sys_role_menu` VALUES ('2', '1000');
INSERT INTO `sys_role_menu` VALUES ('2', '1001');
INSERT INTO `sys_role_menu` VALUES ('2', '1002');
INSERT INTO `sys_role_menu` VALUES ('2', '1003');
INSERT INTO `sys_role_menu` VALUES ('2', '1004');
INSERT INTO `sys_role_menu` VALUES ('2', '1005');
INSERT INTO `sys_role_menu` VALUES ('2', '1006');
INSERT INTO `sys_role_menu` VALUES ('2', '1007');
INSERT INTO `sys_role_menu` VALUES ('2', '1008');
INSERT INTO `sys_role_menu` VALUES ('2', '1009');
INSERT INTO `sys_role_menu` VALUES ('2', '1010');
INSERT INTO `sys_role_menu` VALUES ('2', '1011');
INSERT INTO `sys_role_menu` VALUES ('2', '1012');
INSERT INTO `sys_role_menu` VALUES ('2', '1013');
INSERT INTO `sys_role_menu` VALUES ('2', '1014');
INSERT INTO `sys_role_menu` VALUES ('2', '1015');
INSERT INTO `sys_role_menu` VALUES ('2', '1016');
INSERT INTO `sys_role_menu` VALUES ('2', '1017');
INSERT INTO `sys_role_menu` VALUES ('2', '1018');
INSERT INTO `sys_role_menu` VALUES ('2', '1019');
INSERT INTO `sys_role_menu` VALUES ('2', '1020');
INSERT INTO `sys_role_menu` VALUES ('2', '1021');
INSERT INTO `sys_role_menu` VALUES ('2', '1022');
INSERT INTO `sys_role_menu` VALUES ('2', '1023');
INSERT INTO `sys_role_menu` VALUES ('2', '1024');
INSERT INTO `sys_role_menu` VALUES ('2', '1025');
INSERT INTO `sys_role_menu` VALUES ('2', '1026');
INSERT INTO `sys_role_menu` VALUES ('2', '1027');
INSERT INTO `sys_role_menu` VALUES ('2', '1028');
INSERT INTO `sys_role_menu` VALUES ('2', '1029');
INSERT INTO `sys_role_menu` VALUES ('2', '1030');
INSERT INTO `sys_role_menu` VALUES ('2', '1031');
INSERT INTO `sys_role_menu` VALUES ('2', '1032');
INSERT INTO `sys_role_menu` VALUES ('2', '1033');
INSERT INTO `sys_role_menu` VALUES ('2', '1034');
INSERT INTO `sys_role_menu` VALUES ('2', '1035');
INSERT INTO `sys_role_menu` VALUES ('2', '1036');
INSERT INTO `sys_role_menu` VALUES ('2', '1037');
INSERT INTO `sys_role_menu` VALUES ('2', '1038');
INSERT INTO `sys_role_menu` VALUES ('2', '1039');
INSERT INTO `sys_role_menu` VALUES ('2', '1040');
INSERT INTO `sys_role_menu` VALUES ('2', '1041');
INSERT INTO `sys_role_menu` VALUES ('2', '1042');
INSERT INTO `sys_role_menu` VALUES ('2', '1043');
INSERT INTO `sys_role_menu` VALUES ('2', '1044');
INSERT INTO `sys_role_menu` VALUES ('2', '1045');
INSERT INTO `sys_role_menu` VALUES ('2', '1046');
INSERT INTO `sys_role_menu` VALUES ('2', '1047');
INSERT INTO `sys_role_menu` VALUES ('2', '1048');
INSERT INTO `sys_role_menu` VALUES ('2', '1049');
INSERT INTO `sys_role_menu` VALUES ('2', '1050');
INSERT INTO `sys_role_menu` VALUES ('2', '1051');
INSERT INTO `sys_role_menu` VALUES ('2', '1052');
INSERT INTO `sys_role_menu` VALUES ('2', '1053');
INSERT INTO `sys_role_menu` VALUES ('2', '1054');
INSERT INTO `sys_role_menu` VALUES ('2', '1055');
INSERT INTO `sys_role_menu` VALUES ('2', '1056');
INSERT INTO `sys_role_menu` VALUES ('2', '1057');
INSERT INTO `sys_role_menu` VALUES ('2', '1058');
INSERT INTO `sys_role_menu` VALUES ('2', '1059');
INSERT INTO `sys_role_menu` VALUES ('2', '1060');
INSERT INTO `sys_role_menu` VALUES ('2', '1061');

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `login_name` varchar(30) NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) DEFAULT '' COMMENT '用户昵称',
  `user_type` varchar(2) DEFAULT '00' COMMENT '用户类型（00系统用户 01注册用户）',
  `email` varchar(50) DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) DEFAULT '' COMMENT '手机号码',
  `sex` char(1) DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) DEFAULT '' COMMENT '头像路径',
  `password` varchar(50) DEFAULT '' COMMENT '密码',
  `salt` varchar(20) DEFAULT '' COMMENT '盐加密',
  `status` char(1) DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(50) DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登录时间',
  `pwd_update_date` datetime DEFAULT NULL COMMENT '密码最后更新时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='用户信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', '103', 'admin', 'admin', '00', '475660@qq.com', '13710637136', '0', '/profile/avatar/2020/12/31/311a9d22-8115-4ca6-b02a-076ee07a0c81.png', '29c67a30398638269fe600f73a054934', '111111', '0', '0', '127.0.0.1', '2021-01-07 10:35:44', '2020-12-28 17:12:57', 'admin', '2020-12-28 17:12:57', '', '2021-01-07 10:35:43', '管理员');
INSERT INTO `sys_user` VALUES ('2', '105', 'ry', '若依', '00', 'ry@qq.com', '15666666666', '1', '', '8e6d98b90472783cc73c17047ddccf36', '222222', '0', '0', '127.0.0.1', '2020-12-28 17:12:57', '2020-12-28 17:12:57', 'admin', '2020-12-28 17:12:57', '', null, '测试员');

-- ----------------------------
-- Table structure for `sys_user_online`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online` (
  `sessionId` varchar(50) NOT NULL DEFAULT '' COMMENT '用户会话id',
  `login_name` varchar(50) DEFAULT '' COMMENT '登录账号',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `ipaddr` varchar(50) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int(5) DEFAULT '0' COMMENT '超时时间，单位为分钟',
  PRIMARY KEY (`sessionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='在线用户记录';

-- ----------------------------
-- Records of sys_user_online
-- ----------------------------
INSERT INTO `sys_user_online` VALUES ('a7a47a56-0989-4f39-9d40-018a7bf79988', 'admin', '研发部门', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', 'on_line', '2021-01-07 10:32:38', '2021-01-07 10:48:25', '1800000');

-- ----------------------------
-- Table structure for `sys_user_post`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`,`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户与岗位关联表';

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES ('1', '1');
INSERT INTO `sys_user_post` VALUES ('2', '2');

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户和角色关联表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');
INSERT INTO `sys_user_role` VALUES ('2', '2');
