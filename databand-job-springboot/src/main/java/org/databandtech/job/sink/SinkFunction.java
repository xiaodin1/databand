package org.databandtech.job.sink;

import java.io.Serializable;

public interface SinkFunction<IN> extends Serializable {
	
	void open() throws Exception;
	void close() throws Exception;
	default void invoke(IN value) throws Exception {}

}
