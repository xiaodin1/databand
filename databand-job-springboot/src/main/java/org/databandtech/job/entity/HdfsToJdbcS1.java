package org.databandtech.job.entity;

/**
 * Sqoop1版的实体类
 *       虽然有点旧但还是推荐用这个版本，问题会比较少
 */
public class HdfsToJdbcS1 {
	
	String jobname;
	String dburl;
	String driver;
	String dbuser;
	String dbpassword;
	String totable;
	String m;
	String exportdir;
	String fieldsterminated;
	String linesterminated;
	String defaultfs;
	String cron;
	
	public String getCron() {
		return cron;
	}
	public void setCron(String cron) {
		this.cron = cron;
	}
	public String getDefaultfs() {
		return defaultfs;
	}
	public void setDefaultfs(String defaultfs) {
		this.defaultfs = defaultfs;
	}
	public String getJobname() {
		return jobname;
	}
	public void setJobname(String jobname) {
		this.jobname = jobname;
	}
	public String getDburl() {
		return dburl;
	}
	public void setDburl(String dburl) {
		this.dburl = dburl;
	}
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}
	public String getDbuser() {
		return dbuser;
	}
	public void setDbuser(String dbuser) {
		this.dbuser = dbuser;
	}
	public String getDbpassword() {
		return dbpassword;
	}
	public void setDbpassword(String dbpassword) {
		this.dbpassword = dbpassword;
	}
	public String getTotable() {
		return totable;
	}
	public void setTotable(String totable) {
		this.totable = totable;
	}
	public String getM() {
		return m;
	}
	public void setM(String m) {
		this.m = m;
	}
	public String getExportdir() {
		return exportdir;
	}
	public void setExportdir(String exportdir) {
		this.exportdir = exportdir;
	}
	public String getFieldsterminated() {
		return fieldsterminated;
	}
	public void setFieldsterminated(String fieldsterminated) {
		this.fieldsterminated = fieldsterminated;
	}
	public String getLinesterminated() {
		return linesterminated;
	}
	public void setLinesterminated(String linesterminated) {
		this.linesterminated = linesterminated;
	}

}
