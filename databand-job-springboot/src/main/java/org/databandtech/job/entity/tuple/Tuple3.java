package org.databandtech.job.entity.tuple;

public class Tuple3<E, T, F> {
	private E e;
	private T t;
	private F f;

	public Tuple3(E e, T t, F f) {
		this.e = e;
		this.t = t;
		this.f = f;
	}

	public E _1() {
		return e;
	}

	public T _2() {
		return t;
	}

	public F _3() {
		return f;
	}

}
