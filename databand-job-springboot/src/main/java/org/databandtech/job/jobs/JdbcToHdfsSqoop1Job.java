package org.databandtech.job.jobs;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.sqoop.Sqoop;
import org.apache.sqoop.hive.HiveConfig;
import org.apache.sqoop.util.OptionsFileUtil;

import com.cloudera.sqoop.tool.SqoopTool;

/**
 * hdfs-site.xml
<property>
<name>dfs.permissions</name>
<value>false</value>
<description>
If "true", enable permission checking in HDFS.
If "false", permission checking is turned off,
but all other behavior is unchanged.
Switching from one parameter value to the other does not change the mode,
owner or group of files or directories.
</description>
</property>
 *
 */
public class JdbcToHdfsSqoop1Job {

	public static void main(String[] args) {

		try {
			MysqlToHdfs();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static int MysqlToHdfs() throws Exception {
        String[] args = new String[] {
                "--connect","jdbc:mysql://localhost:3307/databand?useSSL=false",
                "--driver","com.mysql.jdbc.Driver",
                "-username","root",
                "-password","mysql",
                "--table","databand_video",
                "-m","2",
                "--target-dir","/user/java_import_mockinstances1",
                "--fields-terminated-by",",",
//                "--incremental","append", //增量方式
//                "--check-column","id",
//                "--last-value","123456",
                "--split-by","id"
        };

        String[] expandArguments = OptionsFileUtil.expandArguments(args);

        SqoopTool tool = SqoopTool.getTool("import");

        Configuration conf = new Configuration();
        //单机
        //conf.set("fs.default.name", "hdfs://hadoop001:8020");
        //集群
        conf.set("fs.defaultFS", "hdfs://hadoop001:8020");
        
        Configuration loadPlugins = SqoopTool.loadPlugins(conf);

        Sqoop sqoop = new Sqoop(tool, loadPlugins);
        return Sqoop.runSqoop(sqoop, expandArguments);
    }
}
