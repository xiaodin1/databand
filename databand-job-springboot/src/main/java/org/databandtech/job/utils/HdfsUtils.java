package org.databandtech.job.utils;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.fs.permission.FsPermission;
import org.apache.hadoop.io.IOUtils;

import java.io.*;
import java.net.URI;

public class HdfsUtils {
	
	public static Configuration configuration = new Configuration();

    public static void copyFromLocalFile(boolean delSrc, boolean overwrite,
            String src, String srcHDFS) {

        FileSystem fileSystem = null;
        try {

            fileSystem = FileSystem.get(URI.create(srcHDFS), configuration);
            fileSystem.copyFromLocalFile(delSrc,overwrite,new Path(src), new Path(srcHDFS));

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fileSystem.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
            }
        }

    }
    
    public static void copyToLocalFile(boolean delSrc,String srcHDFS, String dst) {

        FileSystem fileSystem = null;
        try {

            fileSystem = FileSystem.get(URI.create(srcHDFS), configuration);
            fileSystem.copyToLocalFile(delSrc, new Path(srcHDFS), new Path(dst));

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fileSystem.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
            }
        }

    }
    
    
}

